﻿using SQLite.Net;
using System;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.Storage;
using System.Collections.Generic;

namespace WindowsBeacons
{
    public enum SignInResult
    {
        SignInOK,
        SignInFail,
        SignInCancel,
        Nothing
    }


    public sealed partial class LoginPopup : ContentDialog
    {

        public SignInResult Result { get; private set; }

        public LoginPopup()
        {
            
            this.InitializeComponent();
            this.Opened += SignInContentDialog_Opened;
            this.Closing += SignInContentDialog_Closing;
            
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            // Ensure the user name and password fields aren't empty. If a required field
            // is empty, set args.Cancel = true to keep the dialog open.
            if (string.IsNullOrEmpty(passwordTextBox.Password.Trim()))
            {
                args.Cancel = true;
                errorTextBlock.Text = "Password is required.";
            }

            if (string.IsNullOrEmpty(masterIdTextBox.Text.Trim()))
            {
                args.Cancel = true;
                errorTextBlock.Text = "Master Id is required.";
            }
            
            // If you're performing async operations in the button click handler,
            // get a deferral before you await the operation. Then, complete the
            // deferral when the async operation is complete.

            ContentDialogButtonClickDeferral deferral = args.GetDeferral();

            if (passwordTextBox.Password.Trim().ToString().Equals("119"))
            {
                this.Result = SignInResult.SignInOK;
            }
            else {
                this.Result = SignInResult.SignInFail;
                args.Cancel = true;
                errorTextBlock.Text = "Please check your Password!";
            }

            
            /* if (await SomeAsyncSignInOperation())
             {
                 this.Result = SignInResult.SignInOK;
             }
             else
             {
                 this.Result = SignInResult.SignInFail;
             }*/
            deferral.Complete();
        }

        private Task<bool> SomeAsyncSignInOperation()
        {
            throw new NotImplementedException();
        }

        void SignInContentDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            this.Result = SignInResult.Nothing;

            // If the user name is saved, get it and populate the user name field.
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            if (roamingSettings.Values.ContainsKey("masterId"))
            {
                masterIdTextBox.Text = roamingSettings.Values["masterId"].ToString();
                saveMasterIdCheckBox.IsChecked = true;
            }
        }

        void SignInContentDialog_Closing(ContentDialog sender, ContentDialogClosingEventArgs args)
        {
            // If sign in was successful, save or clear the user name based on the user choice.
            if (this.Result == SignInResult.SignInOK)
            {
                if (saveMasterIdCheckBox.IsChecked == true)
                {
                    SaveMasterId();
                }
                else
                {
                    ClearMasterId();
                }
            }
            else {
                args.Cancel = true;
            }

            // If the user entered a name and checked or cleared the 'save user name' checkbox, then clicked the back arrow,
            // confirm if it was their intention to save or clear the user name without signing in.
            if (this.Result == SignInResult.Nothing && !string.IsNullOrEmpty(masterIdTextBox.Text))
            {
                if (saveMasterIdCheckBox.IsChecked == false)
                {
                    args.Cancel = true;
                    FlyoutBase.SetAttachedFlyout(this, (FlyoutBase)this.Resources["DiscardIdFlyout"]);
                    FlyoutBase.ShowAttachedFlyout(this);
                }
                else if (saveMasterIdCheckBox.IsChecked == true && !string.IsNullOrEmpty(masterIdTextBox.Text))
                {
                    args.Cancel = true;
                    FlyoutBase.SetAttachedFlyout(this, (FlyoutBase)this.Resources["SaveIdFlyout"]);
                    FlyoutBase.ShowAttachedFlyout(this);
                }

            }
            
        }

        private void SaveMasterId()
        {
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["masterId"] = masterIdTextBox.Text;
        }

        private void ClearMasterId()
        {
            Windows.Storage.ApplicationDataContainer roamingSettings = Windows.Storage.ApplicationData.Current.RoamingSettings;
            roamingSettings.Values["masterId"] = null;
            masterIdTextBox.Text = string.Empty;
        }

        // Handle the button clicks from the flyouts.
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveMasterId();
            FlyoutBase.GetAttachedFlyout(this).Hide();
        }

        private void DiscardButton_Click(object sender, RoutedEventArgs e)
        {
            ClearMasterId();
            FlyoutBase.GetAttachedFlyout(this).Hide();
        }


        // When the flyout closes, hide the sign in dialog, too.
        private void Flyout_Closed(object sender, object e)
        {
            this.Hide();
        }

    }
    } 
