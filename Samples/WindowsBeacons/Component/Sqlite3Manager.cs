﻿using SQLite.Net;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using SQLite.Net.Platform.WinRT;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;

namespace WindowsBeacons.Component
{
    public sealed partial class Sqlite3Manager
    {
        SQLitePlatformWinRT sqlitePlatform;
        String path;
        public Boolean isCreate
        {
            get; set;
        }

        public Sqlite3Manager()
        {
            //StorageFile sf = await DownloadsFolder.CreateFileAsync("testMarker");
            //StorageFolder dlFolder = await sf.GetParentAsync();
        }

        public async Task InitSqlite3(String initLocationData, String initMotionData)
        {
            //StorageFile sf = await DownloadsFolder.CreateFileAsync("test.db");
            //await sf.DeleteAsync();
            //CreateTable(sf.Path, initLocationData, initMotionData);
            CreateTable(ApplicationData.Current.LocalFolder.Path, initLocationData, initMotionData);
        }

        public void CreateTable(String folderPath, String initLocationData, String initMotionData)
        {
            folderPath = folderPath.Replace("test.db", "");
            path = Path.Combine(folderPath, "DB_BS");
            sqlitePlatform = new SQLitePlatformWinRT();
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(sqlitePlatform, path))
                {
                    conn.CreateTable<SensorPayload_DB>();
                    if (GetSensorCount() <= 0)
                    {
                        InsertSensorPayload(initLocationData, initMotionData);
                        isCreate = true;
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
        }

        public void InsertSensorPayload(String lData, String mData)
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(sqlitePlatform, path))
                {
                    SensorPayload_DB sensorPayload = new SensorPayload_DB();
                    sensorPayload.LocationData = lData;
                    sensorPayload.MotionData = mData;
                    conn.RunInTransaction(() =>
                    {
                        conn.Insert(sensorPayload);
                    });
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void UpdateSensorPayload(String lData, String mData)
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(sqlitePlatform, path))
                {
                    SensorPayload_DB sensorPayload = new SensorPayload_DB();
                    sensorPayload.Num = 1;
                    sensorPayload.LocationData = lData;
                    sensorPayload.MotionData = mData;
                    conn.RunInTransaction(() =>
                    {
                        conn.Update(sensorPayload);
                        //conn.Query<SensorPayload_DB>("update SensorPayload_DB set Sensor_Data='"+data+"' where Num=1");
                    });
                    //conn.Update(sensorPayload);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public int GetSensorCount()
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(sqlitePlatform, path)) {
                    int result = conn.Query<SensorPayload_DB>("select * from SensorPayload_DB").Count;
                    return result;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [Table("SensorPayload_DB")]
        class SensorPayload_DB
        {
            [Column("Num")]
            [PrimaryKey, AutoIncrement]
            public int Num { get; set; }
            [Column("LocationData")]
            public String LocationData { get; set; }
            [Column("MotionData")]
            public String MotionData { get; set; }
        }
    }
}
