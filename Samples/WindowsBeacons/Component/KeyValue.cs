﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Data
{
    class KeyValue<T>
    {
        public String key { get; set; }
        public T value { get; set; }

        public KeyValue(String key, T value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
