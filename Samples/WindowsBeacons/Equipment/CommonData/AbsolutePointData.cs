﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class AbsolutePointData
    {
        public String Receiver { get; set; }
        public String EndPoint { get; set; }
        public String Position { get; set; }

        public AbsolutePointData()
        {

        }

        public AbsolutePointData(String Receiver, String EndPoint, String Position)
        {
            this.Receiver = Receiver;
            this.EndPoint = EndPoint;
            this.Position = Position;
        }
    }
}
