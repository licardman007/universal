﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class ReceiverData
    {
        public Distance P1 { get; set; }
        public Distance P2 { get; set; }

        public ReceiverData()
        {
            P1 = new Distance();
            P2 = new Distance();
        }

        public ReceiverData(Distance P1, Distance P2)
        {
            this.P1 = P1;
            this.P2 = P2;
        }
    }
}
