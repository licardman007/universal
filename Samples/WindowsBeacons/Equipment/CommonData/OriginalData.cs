﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class OriginalData
    {
        public String Distance { get; set; }
        public String IMUSensor { get; set; }

        public OriginalData()
        {

        }

        public OriginalData(String Distance, String IMUSensor)
        {
            this.Distance = Distance;
            this.IMUSensor = IMUSensor;
        }
    }
}
