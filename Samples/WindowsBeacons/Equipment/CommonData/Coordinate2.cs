﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class Coordinate2
    {
        public String X { get; set; }
        public String Y { get; set; }
        public String Z { get; set; }
        public String Z2 { get; set; }

        public Coordinate2()
        {

        }

        public Coordinate2(String X, String Y, String Z, String Z2)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
            this.Z2 = Z2;
        }
    }
}
