﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class Distance
    {
        public String DistanceB1 { get; set; }
        public String DistanceB2 { get; set; }
        public String DistanceB3 { get; set; }
        public String DistanceB4 { get; set; }//비컨이 4개로 인한 추가된 부분(0517)

        public Distance()
        {

        }

        //public Distance(String DistanceB1, String DistanceB2, String DistanceB3)
        public Distance(String DistanceB1, String DistanceB2, String DistanceB3, String DistanceB4)//비컨이 4개로 인한 추가된 부분(0517)
        {
            this.DistanceB1 = DistanceB1;
            this.DistanceB2 = DistanceB2;
            this.DistanceB3 = DistanceB3;
            this.DistanceB4 = DistanceB4;//비컨이 4개로 인한 추가된 부분(0517)
        }
    }
}
