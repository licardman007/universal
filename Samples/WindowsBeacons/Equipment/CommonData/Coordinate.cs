﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.CommonData
{
    class Coordinate
    {
        public String X { get; set; }
        public String Y { get; set; }
        public String Z { get; set; }

        public Coordinate()
        {

        }

        public Coordinate(String X, String Y, String Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
    }
}
