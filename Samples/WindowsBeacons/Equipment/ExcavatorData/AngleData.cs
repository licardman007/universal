﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsBeacons.Equipment.ExcavatorData
{
    class AngleData
    {
        public String BodyBoom { get; set; }
        public String BoomArm { get; set; }
        public String ArmBucket { get; set; }
        public String UnityOrientation { get; set; }

        public AngleData()
        {

        }

        public AngleData(String BodyBoom, String BoomArm, String ArmBucket, String UnityOrientation)
        {
            this.BodyBoom = BodyBoom;
            this.BoomArm = BoomArm;
            this.ArmBucket = ArmBucket;
            this.UnityOrientation = UnityOrientation;
        }
    }
}
