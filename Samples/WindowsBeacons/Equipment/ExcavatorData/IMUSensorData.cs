﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.ExcavatorData
{
    class IMUSensorData
    {
        public Coordinate Body { get; set; }
        public Coordinate Boom { get; set; }
        public Coordinate Arm { get; set; }
        public Coordinate Bucket { get; set; }

        public IMUSensorData()
        {
            Body = new Coordinate();
            Boom = new Coordinate();
            Arm = new Coordinate();
            Bucket = new Coordinate();
        }

        public IMUSensorData(Coordinate Body, Coordinate Boom, Coordinate Arm, Coordinate Bucket)
        {
            this.Body = Body;
            this.Boom = Boom;
            this.Arm = Arm;
            this.Bucket = Bucket;
        }
    }
}
