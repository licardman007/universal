﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.ExcavatorData
{
    class ExcavatorData
    {
        public String Location;
        public String Motion;

        public RelativePointData RelativePoint;
        public AbsolutePointData AbsolutePoint;

        public ReceiverData Receiver;
        public IMUSensorData IMUSensor;
        public RelativeCoordinateData RelativeCoordinate;
        public AbsoluteCoordinateData AbsoluteCoordinate;

        public AngleData Angle;
        public OriginalData Original;

        public ExcavatorData()
        {
            RelativePoint = new RelativePointData();
            AbsolutePoint = new AbsolutePointData();
            Receiver = new ReceiverData();
            IMUSensor = new IMUSensorData();
            RelativeCoordinate = new RelativeCoordinateData();
            AbsoluteCoordinate = new AbsoluteCoordinateData();
            Angle = new AngleData();
            Original = new OriginalData();
        }

        public ExcavatorData(String locationData, String motionData, RelativePointData relativePointData, AbsolutePointData absolutePointData,
            ReceiverData receiverData, IMUSensorData iMUSensorData, RelativeCoordinateData relativeCoordinateData, AbsoluteCoordinateData absoluteCoordinateData,
            AngleData angleData, OriginalData originalData)
        {
            this.Location = locationData;
            this.Motion = motionData;
            this.RelativePoint = relativePointData;
            this.AbsolutePoint = absolutePointData;
            this.AbsoluteCoordinate = absoluteCoordinateData;
            this.Receiver = receiverData;
            this.IMUSensor = iMUSensorData;
            this.RelativeCoordinate = relativeCoordinateData;
            this.AbsoluteCoordinate = absoluteCoordinateData;
            this.Angle = angleData;
            this.Original = originalData;
        }
    }
}
