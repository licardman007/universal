﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.ExcavatorData
{
    class AbsoluteCoordinateData
    {
        public Coordinate Center { get; set; }
        public Coordinate Hinge { get; set; }
        public Coordinate Boom { get; set; }
        public Coordinate Arm { get; set; }

        public AbsoluteCoordinateData()
        {
            Center = new Coordinate();
            Hinge = new Coordinate();
            Boom = new Coordinate();
            Arm = new Coordinate();
        }

        public AbsoluteCoordinateData(Coordinate Center, Coordinate Hinge, Coordinate Boom, Coordinate Arm)
        {
            this.Center = Center;
            this.Hinge = Hinge;
            this.Boom = Boom;
            this.Arm = Arm;
        }
    }
}
