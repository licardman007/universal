﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using WindowsBeacons.Equipment.DozerData;
using System.Diagnostics;

namespace WindowsBeacons.Component
{
    class DozerCalculator
    {
        
        ////상수입력
        //수신기 좌표이동거리
        //사잇거리
        //커팅거리

        ////const_name
        //@_1
        //@_2
        //@_3


        //calibration
        double[] p1_calibration = new double[4];
        double[] p2_calibration = new double[4];
        
        //좌표
        double[] becon_1 = new double[3];
        double[] becon_2 = new double[3];
        double[] becon_3 = new double[3];
        double[] becon_4 = new double[3];

        double[] r_becon_1 = new double[3];
        double[] r_becon_2 = new double[3];
        double[] r_becon_3 = new double[3];
        double[] r_becon_4 = new double[3];

        double[] instrument_station_1 = new double[3];
        double[] instrument_station_2 = new double[3];
        double[] instrument_station_3 = new double[3];

        //측점의 상대좌표
        double[] r_instrument_station_2 = new double[3];
        double[] r_instrument_station_3 = new double[3];

        //수신기
        double[] p_1_u = new double[3]; double[] p_2_u = new double[3];
        double[] p_1_d = new double[3]; double[] p_2_d = new double[3];
        
        double[] p_1_u_o = new double[3]; double[] p_2_u_o = new double[3];
        double[] p_1_d_o = new double[3]; double[] p_2_d_o = new double[3];


        //수신기 계산 판별
        Int16 p_calc_1 = 1;
        Int16 p_calc_2 = 1;

        //다운된 거리값 보정
        double[] d_1_t = new double[3];
        double[] d_2_t = new double[3];

        
        double[] d_1_t_o = new double[3];
        double[] d_2_t_o = new double[3];

        UInt16 stop_signal = 0;//멈춰있는지의 유무

        //수신기와 장비 바닥까지의 높이 - 추가된 부분(0714)
        double d_reciver_down = 0;


        //--------------------------------평균화 시킨값과 비교------------------------------
        double[] value_1 = new double[8];
        double[] value_2 = new double[8];

        //const_name
        //칼만값 초기화 커팅, 초기화 근접 상수 /50, 
        Int16 K_time2_boundary_constant_up = 50;
        Int16 K_time2_boundary_constant_down = -50;

        //평균화값과 칼만필터값과의 비교를 위한 변수
        UInt16 p1b1_K = 0;
        UInt16 p1b2_K = 0;
        UInt16 p1b3_K = 0;
        UInt16 p1b4_K = 0;

        UInt16 p2b1_K = 0;
        UInt16 p2b2_K = 0;
        UInt16 p2b3_K = 0;
        UInt16 p2b4_K = 0;

        UInt16 P1P2_distance_signal = 0;//초기 상태로 돌아갈수있는지 확인을 위한 추가된 부분

        double[] save_value_2 = new double[8];//초기화단계에서 거리값 저장



        //-------------------------------------거리 중간값 계산----------------------------------
        Int16 number_standard_P1 = 0;
        Int16 number_standard_P2 = 0;

        Int16 number_divide_P1 = 0;
        Int16 number_divide_P2 = 0;

        //-----정렬할 갯수 입력 - 비컨 4개로 인한 수정
        //배열의 총수 6/99
        double[] real_data_d_p1_b1 = new double[99];
        double[] real_data_d_p1_b2 = new double[99];
        double[] real_data_d_p1_b3 = new double[99];
        double[] real_data_d_p1_b4 = new double[99];

        double[] real_data_d_p2_b1 = new double[99];
        double[] real_data_d_p2_b2 = new double[99];
        double[] real_data_d_p2_b3 = new double[99];
        double[] real_data_d_p2_b4 = new double[99];

        double[] temp_array_sort_d_P1_b1 = new double[99];
        double[] temp_array_sort_d_P1_b2 = new double[99];
        double[] temp_array_sort_d_P1_b3 = new double[99];
        double[] temp_array_sort_d_P1_b4 = new double[99];

        double[] temp_array_sort_d_P2_b1 = new double[99];
        double[] temp_array_sort_d_P2_b2 = new double[99];
        double[] temp_array_sort_d_P2_b3 = new double[99];
        double[] temp_array_sort_d_P2_b4 = new double[99];
       
        Int16 constant_number_d_P1 = 0;
        Int16 constant_number_d_P2 = 0;

        Int32 i = 0, k = 0;

        Int16 const_array = 0;

        double data_value_align = 0;

        double sum_P1b1_1 = 0;
        double sum_P1b2_1 = 0;
        double sum_P1b3_1 = 0;
        double sum_P1b4_1 = 0;

        double sum_P1b1_2 = 0;
        double sum_P1b2_2 = 0;
        double sum_P1b3_2 = 0;
        double sum_P1b4_2 = 0;

        double sum_P2b1_1 = 0;
        double sum_P2b2_1 = 0;
        double sum_P2b3_1 = 0;
        double sum_P2b4_1 = 0;

        double sum_P2b1_2 = 0;
        double sum_P2b2_2 = 0;
        double sum_P2b3_2 = 0;
        double sum_P2b4_2 = 0;

        double[] a_becon_p1_dozer = new double[4];
        double[] a_becon_p2_dozer = new double[4];

        //통합된 변수선언
        double[] receiver_p1_data = new double[4];
        double[] receiver_p2_data = new double[4];

        Int16 average_count = 0;
        Int16 average_stop = 0;




        //-------------------------------------------------------안정화 구간----------------------------------------------------
        UInt16 stop_selection = 0;//멈춘상태로 가는중인지 파악
        UInt16 move_point = 0;//이동안된 상태 선언

        //const_name
        double time2_boundary_constant_up = 2000;
        double time2_boundary_constant_down = -2000;

        //const_name
        double move_up = 2000;
        double move_down = -2000;

        double debug_count = 0;



        //-----------------------------------------------@_1---------------------------------------------------
        UInt16 no_cut_save = 0;//이전 칼만 필터로 보낼수 있는지 확인부분

        //컷팅한값 저장
        double[] save_cut_becon_p1_dozer = new double[4];
        double[] save_cut_becon_p2_dozer = new double[4];

        //로우값으로 계산된 수신기좌표
        double[] c_becon_p1_dozer = new double[4];
        double[] c_becon_p2_dozer = new double[4];

        UInt16 const_becon_p2_dozer = 0;

        double[] dozer_data_1 = new double[89];//평균값 추가로 인한 - 수정된 부분(0717)

        double P1P2_absolute = 0;

        //const_name
        UInt16 P1P2_absolute_constat = 50;//수신기 좌표이동 범위 /500, 100, 50, 

        UInt16 no_p12_1 = 0;//수신기 좌표이동 튀는값

        UInt32 p_move_count = 0;//수신기 좌표이동 true 카운트

        //위치거리값 저장을 위한 선언
        double[] becon_p1_dozer_d_a = new double[4];
        double[] becon_p2_dozer_d_a = new double[4];

        UInt16 stabilization_position_cut_save = 0;//이전값으로 부내는 부분

        //--------------------두 수신기의 이동된 거리를 가지고 오차를 판단
        double[] r_receiving_set_k_s1 = new double[6];
        double[] r_receiving_set_k_s2 = new double[6];
        
        double P1bP1a_distance = 0;
        double P2bP2a_distance = 0;



        //------------------------------------------------@_2-------------------------------------------------
        double[] dozer_data_2 = new double[89];//평균값 추가로 인한 - 수정된 부분(0717)

        //double P1P2_distance = 0;//두 수신기 사이의 거리값 계산 - 초기화 단계에서 사이거리값 적용
        double P1P2_distance_o = 0;

        //const_name
        double const_distance = 986;//비율로 거리값 범위 결정을 위한 추가된 부분 /1250(스티로폼), 438(도저모형), 986(수정된 도저모형)
        //double distance_value = 3;//rate 5, 3
        double distance_value_const = 50;//사잇거리 허용 범위 /50, 30, 100, 200, 150, 

        //사이거리값 저장을 위한 선언
        double[] becon_p1_dozer_a = new double[4];
        double[] becon_p2_dozer_a = new double[4];

        UInt16 const_becon_p_dozer_a = 0;//사이거리 이전 이후를 구분하기 위해

        UInt32 p12_distance_count = 0;//사잇거리 true 카운트

        //커팅 부분이 적용 가능한지 구분
        UInt16 no_distance_1 = 0;
        //UInt16 no_distance_2 = 0;



        //------------------------------------------------ @_3 --------------------------------------------------
        Int16 constant_number = 0;

        //로우값 끼리 비교할때 필요한 변수
        double[] cut_value_1 = new double[8];
        double[] cut_value_2 = new double[8];

        Int16 constant_number_2 = 0;

        UInt16 cut_output = 0;//커팅부분 이전 수신기값 구분해서 디버그

        UInt16 cut_signal = 0;//커팅통과시 구분을 위한 변수

        UInt32 cut_count = 0;//커팅 true 카운트




        //---------------------------------------------K.F.-----------------------------------------------------
        //---------------------------------------verage

        //double kf_constant_number_1 = 0;//로우값과 칼만필터와의 비교할때 사용

        //double[] kf_value_1 = new double[6];
        //double[] kf_value_2 = new double[6];

        //비컨이 4개로 인한 수정
        double[] kf_value_1 = new double[8];
        double[] kf_value_2 = new double[8];

        //double[] kf_value_a = new double[6];//칼만필터 평균화 시킬때 사용




        //----------------------------------------variable
        
        double x_est_p1b1 = 0;
        double x_est_p1b2 = 0;
        double x_est_p1b3 = 0;
        double x_est_p1b4 = 0;//비컨이 4개로 인한 수정


        double x_est_p2b1 = 0;
        double x_est_p2b2 = 0;
        double x_est_p2b3 = 0;
        double x_est_p2b4 = 0;//비컨이 4개로 인한 수정

        double x_est_last_p1b1 = 0;
        double x_est_last_p1b2 = 0;
        double x_est_last_p1b3 = 0;
        double x_est_last_p1b4 = 0;//비컨이 4개로 인한 수정된

        double x_est_last_p2b1 = 0;
        double x_est_last_p2b2 = 0;
        double x_est_last_p2b3 = 0;
        double x_est_last_p2b4 = 0;//비컨이 4개로 인한 수정

        //#1 사용시
        //double v_value_p1b1 = 1;//1
        //double v_value_p1b2 = 1;//1
        //double v_value_p1b3 = 1;//1
        //double v_value_p2b1 = 1;//1
        //double v_value_p2b2 = 1;//1
        //double v_value_p2b3 = 1;//1

        double P_last = 0;//0, //계속적 감소, 멈춤(update_covariance)


        //반응속도 (R = 0.0001, Q = 0.0075 확장칼만보다 더빠름)x_수신기 4개

        //반응속도 (R = 0.0001, Q = 0.005 확장칼만보다 더빠름)x_수신기 4개

        //반응속도 (R = 0.001, Q = 0.0001 확장칼만보다 더느림)_수신기 2개

        //반응속도 (R = 0.001, Q = 0.0005 확장칼만보다 더느림)_수신기 4개

        //반응속도 (R = 0.001, Q = 0.001 확장칼만보다 더느림)_수신기 4개

        //반응속도 (R = 0.0001, Q = 0.001 확장칼만보다 더빠름)_수신기 4개



        //const_name
        double Q = 0.0001;//fixing 0.000001,  0.001, 0.0001, 
        double R = 0.001;// fixing 0.001,     0.001, 0.001, 

        double stop_signal_Q = 0.0001;//안정화 단계에서 Q값 

        //double[] std_dev = { 0, 0, 0, 0, 0, 0 };//칼만필터에서 표준편차를 구할때 사용

        //1이면 적용이 안된것임
        double A_value = 1;
        double H_value = 1;

        //---------------------------------------------------------------declaration
        //double update_befor_x_p1b1 = 0;
        //double update_befor_x_p1b2 = 0;
        //double update_befor_x_p1b3 = 0;
        //double update_befor_x_p2b1 = 0;
        //double update_befor_x_p2b2 = 0;
        //double update_befor_x_p2b3 = 0;

        double x_temp_est_p1b1 = 0;
        double x_temp_est_p1b2 = 0;
        double x_temp_est_p1b3 = 0;
        double x_temp_est_p1b4 = 0;//비컨4개로 인한 수정
        //----------------------------------------------------------------------------

        double x_temp_est_p2b1 = 0;
        double x_temp_est_p2b2 = 0;
        double x_temp_est_p2b3 = 0;
        double x_temp_est_p2b4 = 0;//비컨4개로 인한 수정

        double P_temp = 0;
        double K = 0;
        double P = 0;

        UInt16 k_a_signal = 0;//안정화 후 칼만 정상화 확인 부분

        //--------------------------------------------도저-------------------------------------------------------

        //요값 조정
        double d_calc_body_y_c = 0;

        double d_frontL_y_c = 0;
        double d_frontR_y_c = 0;

        double d_blade_y_c = 0;

        double[] dozer_blade_length = { 0, 0, 0 };// 도저 {블레이드 Front, Down, Left}

 
        double[] becon_p1_dozer = new double[4];
        double[] becon_p2_dozer = new double[4];

        double[] f_becon_p1_dozer = new double[4];
        double[] f_becon_p2_dozer = new double[4];

        double[] dozer_body_imu = new double[3];
        double[] dozer_blade_imu = new double[3];
        double[] dozer_front_imu_L = new double[3];
        double[] dozer_front_imu_R = new double[3];

        double dozer_ultrasonic_left = 0;
        double dozer_ultrasonic_right = 0;

        //평균값 계산을 위한 추가된 부분(0629)
        double dozer_front_imu_roll_unity = 0;
        double dozer_front_imu_pitch_unity = 0;


        double[] dozer_body_length = new double[8];
        double dozer_front_length = 0;
        double dozer_front2_length = 0;
        double dozer_between = 0;

        double[] dozer_data = new double[89];//평균값 추가로 인한 - 수정된 부분(0717)


        double dozer_span_left = 0;
        double dozer_zero_left = 0;

        double dozer_span_right = 0;
        double dozer_zero_right = 0;

        //imu_calibration
        double[] body_calibration  = new double[2];
        double[] frontL_calibration = new double[2];
        double[] frontR_calibration = new double[2];
        double[] blade_calibration = new double[2];



        

        //수신기 좌표계산 1
        double[] r_dozer_tri_point = new double[12];
        double[] r_receiving_set_k = new double[6];

        //수신기 좌표계산 2
        double[] r_dozer_tri_point_o = new double[12];
        double[] r_receiving_set_o = new double[6];

        //-------칼만필터 통과후 위치좌표 이동된 거리 표시를 위한
        UInt16 pb_signal = 0;
        UInt16 pb_count = 0;
        double pb_distance = 0;

        double[] pb_a = new double[3];
        double[] pb_b = new double[3];

        //yaw 값 판별
        double calc_body_y = 0;//저장을 위한 전역 변수 설정 - 수정된 부분(0709)
        double calc_body_y_o = 0;
        double calc_body_y_a = 0;//요값 저장을 위한 추가된 부분(0722)
        //double calc_body_y_n = 0;//사용하지 않으므로 주석처리 - 수정된 부분(0709)
        UInt16 NaN_yaw_count = 0;//계산 실패한 yaw 카운트

        double[] r_receiving_set = new double[6];//수신기값 저장을 위한 전역변수 선언 - 수정된 부분(0709)

        //------------------ z 중간값 계산(시작) - 추가된 부분(0702)--------------------

        Int16 number_standard_P1_z = 0;
        Int16 number_standard_P2_z = 0;

        //각 구간의 배열 갯수(총 9개일때)
        Int16 number_divide_P1_z = 3;
        Int16 number_divide_P2_z = 3;

        //------음수값 비교를 위한 수정된 부분(0729)-------
        //double[] real_data_d_p1_z = new double[9];
        //double[] real_data_d_p2_z = new double[9];

        double[] real_data_d_p1_z = { -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999 };
        double[] real_data_d_p2_z = { -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999 };

        double[] temp_array_sort_d_P1_z = new double[9];
        double[] temp_array_sort_d_P2_z = new double[9];
        //---------------------------------------------------
        
        Int16 constant_number_d_P1_z = 0;
        Int16 constant_number_d_P2_z = 0;

        Int32 iz = 0, kz = 0;

        Int32 jz = 0;//초기화를 위한 변수


        double data_value_align_z = 0;

        Int16 center_position_p1 = 0;
        Int16 center_position_p2 = 0;

        double calc_center_p1 = 0;
        double calc_center_p2 = 0;
        //------------------z 중간값 계산(끝)--------------------



        //--------------------비컨 각도를 위한 추가된 부분(0711) - 시작-----------------
        //비컨 2개 계산 결정 준비
        Int16 beacon_2_calc = 0;

        //비컨 선택시 사용될 변수 선언
        Int16 s_beacon_0 = 0;
        Int16 s_beacon_1 = 0;
        Int16 s_beacon_2 = 0;
        Int16 s_beacon_3 = 0;

        //나머지 각도인 비컨의 좌표에 사용될 부분
        double angle_s_b1_x = 0;
        double angle_s_b1_y = 0;
        double angle_s_b1_z = 0;

        double angle_s_b2_x = 0;
        double angle_s_b2_y = 0;
        double angle_s_b2_z = 0;

        double angle_s_b3_x = 0;
        double angle_s_b3_y = 0;
        double angle_s_b3_z = 0;

        //각도가 가장 큰 비컨의 좌표에 사용될 부분
        double angle_s_b0_x = 0;
        double angle_s_b0_y = 0;
        double angle_s_b0_z = 0;
        //--------------------비컨 각도를 위한 추가된 부분 - 끝-----------------



        Int16 first_const = 0;//처음값 확인용 - 추가된 부분(0706)
        Int16 kalman_after = 0;//칼만통과 재확인용



        //---------수신기 계산에서의 값 저장 부분 시작 - 추가된 부분(0706)---------

        //지역변수에서 전역변수로 수정된 부분(0707)
        double[] r_becon_1_c = new double[3];
        double[] r_becon_2_c = new double[3];
        double[] r_becon_3_c = new double[3];



        //----계산_1
        //거리값 저장
        double save_p1b1 = 0;
        double save_p1b2 = 0;
        double save_p1b3 = 0;

        double save_p2b1 = 0;
        double save_p2b2 = 0;
        double save_p2b3 = 0;


        //비컨값 저장
        double save_b1_x = 0;
        double save_b1_y = 0;
        double save_b1_z = 0;

        double save_b2_x = 0;
        double save_b2_y = 0;
        double save_b2_z = 0;

        double save_b3_x = 0;
        double save_b3_y = 0;
        double save_b3_z = 0;

        Int16 save_id_b1 = 0;
        Int16 save_id_b2 = 0;
        Int16 save_id_b3 = 0;

        //-----계산_2
        //거리값 저장
        double save_p1b1_o = 0;
        double save_p1b2_o = 0;
        double save_p1b3_o = 0;

        double save_p2b1_o = 0;
        double save_p2b2_o = 0;
        double save_p2b3_o = 0;

        //비컨값 저장   
        double save_b1_x_o = 0;
        double save_b1_y_o = 0;
        double save_b1_z_o = 0;

        double save_b2_x_o = 0;
        double save_b2_y_o = 0;
        double save_b2_z_o = 0;

        double save_b3_x_o = 0;
        double save_b3_y_o = 0;
        double save_b3_z_o = 0;

        Int16 save_id_b1_o = 0;
        Int16 save_id_b2_o = 0;
        Int16 save_id_b3_o = 0;

        //이전거리값을 사용 해야할경우
        Int16 check_calc = 0;
        Int16 before_d_value = 0;

        //거리값이 999999 일경우 비컨표시를 위한 변수 선언 - 추가된 부분(0716)
        Int16 b1_sign = 0;
        Int16 b2_sign = 0;
        Int16 b3_sign = 0;
        Int16 b4_sign = 0;


        public DozerData DozerData
        {
            get;set;
        }
        



        //------------------------------------------------------시리얼 포트 수신 부분-------------------------------------------------------------
        public DozerCalculator()
        {
            DozerData = new DozerData();
        }

        //UI 와 다른 부분
        public void InitDozerCalcData(TextBox ConstCalibration, TextBox ConstIMUCalibration, TextBox ConstAbsolutePoint, TextBox ConstBeaconPoint, TextBox ConstDozer)
        {
            //------calibration - 비컨이 4개로 인한 수정
            p1_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[0]);
            p1_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[1]);
            p1_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[2]);
            p1_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[3]);

            p2_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[4]);
            p2_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[5]);
            p2_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[6]);
            p2_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[7]);

            //-----imu calibration - 순서 변경에 따른 수정
            //body_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[0]);//body_r
            //body_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[1]);//body_p

            //frontL_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[2]);//frontL_r
            //frontL_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[3]);//frontL_p

            //blade_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[4]);//blade_r
            //blade_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[5]);//blade_p

            //d_calc_body_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[6]);//body_y
            //d_frontL_y_c    = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[7]);//frontL_y
            //d_blade_y_c     = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[8]);//blade_y

            ////imu 추가로 인한
            //d_frontR_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[9]);//frontR_r
            //frontR_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[10]);//frontR_p
            //frontR_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[11]);//frontR_y

            body_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[0]);//body_r
            body_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[1]);//body_p
            d_calc_body_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[2]);//body_y

            frontL_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[3]);//frontL_r
            frontL_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[4]);//frontL_p
            d_frontL_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[5]);//frontL_y
            
            frontR_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[6]);//frontR_r
            frontR_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[7]);//frontR_p
            d_frontR_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[8]);//frontR_y

            blade_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[9]);//blade_r
            blade_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[10]);//blade_p
            d_blade_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[11]);//blade_y
            
            

            //-----측점
            instrument_station_1[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[0]);
            instrument_station_1[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[1]);
            instrument_station_1[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[2]);
            instrument_station_2[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[3]);
            instrument_station_2[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[4]);
            instrument_station_2[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[5]);
           
            //-----비콘
            becon_1[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[0]);
            becon_1[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[1]);
            becon_1[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[2]);
            becon_2[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[3]);
            becon_2[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[4]);
            becon_2[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[5]);
            becon_3[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[6]);
            becon_3[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[7]);
            becon_3[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[8]);
            becon_4[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[9]);
            becon_4[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[10]);
            becon_4[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[11]);



            ////----------------------------------dozer-------------------------------
            dozer_body_length[0] = Convert.ToDouble(ConstDozer.Text.Split('\r')[0]);//두 수신기 사이의 거리 입력 - 추가된 부분(0717)
            dozer_body_length[1] = 0;//Convert.ToDouble(ConstDozer.Text.Split('\r')[1]);
            dozer_body_length[2] = 0;//Convert.ToDouble(ConstDozer.Text.Split('\r')[2]);
            dozer_body_length[3] = Convert.ToDouble(ConstDozer.Text.Split('\r')[3]);
            dozer_body_length[4] = Convert.ToDouble(ConstDozer.Text.Split('\r')[4]);
            dozer_body_length[5] = 0;//Convert.ToDouble(ConstDozer.Text.Split('\r')[5]);//f


            dozer_front_length = Convert.ToDouble(ConstDozer.Text.Split('\r')[6]);
            dozer_front2_length = Convert.ToDouble(ConstDozer.Text.Split('\r')[7]);

            dozer_blade_length[0] = Convert.ToDouble(ConstDozer.Text.Split('\r')[8]);
            dozer_blade_length[1] = Convert.ToDouble(ConstDozer.Text.Split('\r')[9]);

            dozer_between = Convert.ToDouble(ConstDozer.Text.Split('\r')[10]);

            dozer_span_left = Convert.ToDouble(ConstDozer.Text.Split('\r')[11]);
            dozer_zero_left = Convert.ToDouble(ConstDozer.Text.Split('\r')[12]);

            dozer_span_right = Convert.ToDouble(ConstDozer.Text.Split('\r')[13]);
            dozer_zero_right = Convert.ToDouble(ConstDozer.Text.Split('\r')[14]);

            
            dozer_body_length[6] = Convert.ToDouble(ConstDozer.Text.Split('\r')[15]);//도저 g 값
            dozer_body_length[7] = Convert.ToDouble(ConstDozer.Text.Split('\r')[16]);//도저 P4_length 값

            d_reciver_down = Convert.ToDouble(ConstDozer.Text.Split('\r')[17]);//수신기와 장비 바닥까지의 높이 - 추가된 부분(0714)

        }


        
        public void DozerDataReceived(String lData, String mData)
        {
            DozerData.Location = lData;
            DozerData.Motion = mData;

            string real_time = System.DateTime.Now.ToString("HH:mm:ss");

            //imu 및 켈리브레이션 변경에 따른 수정된 부분(0628)
            //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> body_calibration[0] " + body_calibration[0] + " body_calibration[1] " + body_calibration[1] + " d_calc_body_y_c " + d_calc_body_y_c + " frontL_calibration[0] " + frontL_calibration[0] + " frontL_calibration[1] " + frontL_calibration[1] + " d_frontL_y_c" + d_frontL_y_c + " frontR_calibration[0] " + frontR_calibration[0] + " frontR_calibration[1] " + frontR_calibration[1] + " d_frontR_y_c " + d_frontR_y_c + " blade_calibration[0] " + blade_calibration[0] + " blade_calibration[1] " + blade_calibration[1] + " d_blade_y_c " + d_blade_y_c);

            const_distance = dozer_body_length[0];//두 수신기 사이의 거리 - 추가된 부분(0717)
            //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> const_distance : " + const_distance);

            //비컨이 4개로 인한
            becon_p1_dozer[0] = Convert.ToDouble(lData.Substring(2, 6));
            becon_p1_dozer[1] = Convert.ToDouble(lData.Substring(8, 6));
            becon_p1_dozer[2] = Convert.ToDouble(lData.Substring(14, 6));
            becon_p1_dozer[3] = Convert.ToDouble(lData.Substring(20, 6));

            becon_p2_dozer[0] = Convert.ToDouble(lData.Substring(26, 6));
            becon_p2_dozer[1] = Convert.ToDouble(lData.Substring(32, 6));
            becon_p2_dozer[2] = Convert.ToDouble(lData.Substring(38, 6));
            becon_p2_dozer[3] = Convert.ToDouble(lData.Substring(44, 6));


            //출력
            DozerData.Original.Distance = Convert.ToString(becon_p1_dozer[0]) + " " + Convert.ToString(becon_p1_dozer[1]) + " " + Convert.ToString(becon_p1_dozer[2]) + " " + Convert.ToString(becon_p1_dozer[3]) + " "
                                + Convert.ToString(becon_p2_dozer[0]) + " " + Convert.ToString(becon_p2_dozer[1]) + " " + Convert.ToString(becon_p2_dozer[2] + " " + Convert.ToString(becon_p2_dozer[3]));


            //거리값이 999999일 경우 해당되는 비컨 표시 - 추가된 부분(0716)
            if (becon_p1_dozer[0] == 999999 || becon_p2_dozer[0] == 999999) b1_sign = 1;
            else if (becon_p1_dozer[0] < 999999 && becon_p2_dozer[0] < 999999) b1_sign = 0;

            if (becon_p1_dozer[1] == 999999 || becon_p2_dozer[1] == 999999) b2_sign = 1;
            else if (becon_p1_dozer[1] < 999999 && becon_p2_dozer[1] < 999999) b2_sign = 0;

            if (becon_p1_dozer[2] == 999999 || becon_p2_dozer[2] == 999999) b3_sign = 1;
            else if (becon_p1_dozer[2] < 999999 && becon_p2_dozer[2] < 999999) b3_sign = 0;

            if (becon_p1_dozer[3] == 999999 || becon_p2_dozer[3] == 999999) b4_sign = 1;
            else if (becon_p1_dozer[3] < 999999 && becon_p2_dozer[3] < 999999) b4_sign = 0;


            ////calibration
            //becon_p1_dozer[0] += p1_calibration[0];
            //becon_p1_dozer[1] += p1_calibration[1];
            //becon_p1_dozer[2] += p1_calibration[2];
            //becon_p1_dozer[3] += p1_calibration[3];//비컨이 4개로 인한

            //becon_p2_dozer[0] += p2_calibration[0];
            //becon_p2_dozer[1] += p2_calibration[1];
            //becon_p2_dozer[2] += p2_calibration[2];
            //becon_p2_dozer[3] += p2_calibration[3];//비컨이 4개로 인한

            //수신기 케이블 연장에 따른 캘리브레이션 상수 추가 - 캘리브레이션 open으로 수정
            becon_p1_dozer[0] = Math.Round(Math.Abs(becon_p1_dozer[0] + p1_calibration[0]));
            becon_p1_dozer[1] = Math.Round(Math.Abs(becon_p1_dozer[1] + p1_calibration[1]));
            becon_p1_dozer[2] = Math.Round(Math.Abs(becon_p1_dozer[2] + p1_calibration[2]));
            becon_p1_dozer[3] = Math.Round(Math.Abs(becon_p1_dozer[3] + p1_calibration[3]));
                                                                            
            becon_p2_dozer[0] = Math.Round(Math.Abs(becon_p2_dozer[0] + p2_calibration[0]));
            becon_p2_dozer[1] = Math.Round(Math.Abs(becon_p2_dozer[1] + p2_calibration[1]));
            becon_p2_dozer[2] = Math.Round(Math.Abs(becon_p2_dozer[2] + p2_calibration[2]));
            becon_p2_dozer[3] = Math.Round(Math.Abs(becon_p2_dozer[3] + p2_calibration[3]));




            //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " P1s: " + becon_p1_dozer[0] + "  " + becon_p1_dozer[1] + "  " + becon_p1_dozer[2]
            //+ " P2s: " + becon_p2_dozer[0] + "  " + becon_p2_dozer[1] + "  " + becon_p2_dozer[2]);



            //4개의 거리 로우값 확인 -비컨 4개로 인한
            Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1s: " + becon_p1_dozer[0] + "  " + becon_p1_dozer[1] + "  " + becon_p1_dozer[2] + "  " + becon_p1_dozer[3]
                 + " dP2s: " + becon_p2_dozer[0] + "  " + becon_p2_dozer[1] + "  " + becon_p2_dozer[2] + "  " + becon_p2_dozer[3]);

            //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>> d_reciver_down " + d_reciver_down);


            //--------------중간값 계산식 시작----------
            if (average_stop == 0)//평균화 작동이 멈출경우 1
            {

                average_count++;//카운트를 셈



                //P1
                real_data_d_p1_b1[constant_number_d_P1] = becon_p1_dozer[0];
                //real_data_d_p1_b1[0] = 1;                      
                real_data_d_p1_b2[constant_number_d_P1] = becon_p1_dozer[1];
                real_data_d_p1_b3[constant_number_d_P1] = becon_p1_dozer[2];
                real_data_d_p1_b4[constant_number_d_P1] = becon_p1_dozer[3];

                //P2                                             
                real_data_d_p2_b1[constant_number_d_P2] = becon_p2_dozer[0];
                real_data_d_p2_b2[constant_number_d_P2] = becon_p2_dozer[1];
                real_data_d_p2_b3[constant_number_d_P2] = becon_p2_dozer[2];
                real_data_d_p2_b4[constant_number_d_P2] = becon_p2_dozer[3];

                //각 구간의 배열 갯수 /2/33
                number_divide_P1 = 33;
                number_divide_P2 = 33;

                ////배열의 무한 증가 배제를 위한 수정
                //if (constant_number_d_P1 == number_divide_P1 * 3) constant_number_d_P1 = 0;//P1
                //if (constant_number_d_P2 == number_divide_P2 * 3) constant_number_d_P2 = 0;//P2

                //중앙구간에서의 첫번째수(배열 맨처음에 0부터 시작이므로)
                number_standard_P1 = number_divide_P1;
                number_standard_P2 = number_divide_P2;

                //real_data_d_p1_b1[0] = 1;

                //P1
                //P1b1
                temp_array_sort_d_P1_b1 = real_data_d_p1_b1;//가상의 공간을 생성 
                //Array.Sort(temp_array_sort_d_P1_b1);

                //temp_array_sort_d_P1_b1[0] = 1;

                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)//5
                {
                    for (k = i + 1; k < number_divide_P1 * 3; k++)//6
                    {
                        if (temp_array_sort_d_P1_b1[i] > temp_array_sort_d_P1_b1[k])//i 가 0 ~ 4 까지일때 각각 + 1 부터 5까지 비교(처음에는 0 배열을 제외하고 전부 0임)
                        {
                            //크기별로 순서화
                            data_value_align = temp_array_sort_d_P1_b1[i];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P1_b1[i] = temp_array_sort_d_P1_b1[k];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P1_b1[k] = data_value_align;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }

                ////Debug.WriteLine(" data_value_align " + data_value_align);

                data_value_align = 0;
                ////-------직접계산 끝

                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4]) / number_divide_P1;//구간의 범위가 5개일때
                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3]) / number_divide_P1;//구간의 범위가 4개일때
                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8]) / number_divide_P1;//구간의 범위가 9개일때
                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8] + temp_array_sort_d_P1[number_standard_P1 + 9] + temp_array_sort_d_P1[number_standard_P1 + 10] + temp_array_sort_d_P1[number_standard_P1 + 11]) / number_divide_P1;//구간의 범위가 12개일때
                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1]) / number_divide_P1;//구간의 범위가 1개일때
                //a_becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1]) / number_divide_P1;//구간의 범위가 2개일때
                //becon_p1_dozer[0] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2]) / number_divide_P1;//구간의 범위가 3개일때

                //Debug.WriteLine(" a_becon_p1_dozer[0] " + a_becon_p1_dozer[0]);

                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b1_1 = temp_array_sort_d_P1_b1[number_standard_P1];
                        //Debug.WriteLine(" temp_array_sort_d_P1_b1[number_standard_P1] " + temp_array_sort_d_P1_b1[number_standard_P1] + " number_standard_P1 " + number_standard_P1);

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b1_2 = temp_array_sort_d_P1_b1[number_standard_P1 + const_array];

                        sum_P1b1_1 = sum_P1b1_1 + sum_P1b1_2;
                    }
                }

                a_becon_p1_dozer[0] = sum_P1b1_1 / number_divide_P1;//평균화



                //P1b2
                temp_array_sort_d_P1_b2 = real_data_d_p1_b2;
                //Array.Sort(temp_array_sort_d_P1_b2);

                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b2[i] > temp_array_sort_d_P1_b2[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b2[i];
                            temp_array_sort_d_P1_b2[i] = temp_array_sort_d_P1_b2[k];
                            temp_array_sort_d_P1_b2[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4]) / number_divide_P1;//구간의 범위가 5개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3]) / number_divide_P1;//구간의 범위가 4개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8]) / number_divide_P1;//구간의 범위가 9개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8] + temp_array_sort_d_P1[number_standard_P1 + 9] + temp_array_sort_d_P1[number_standard_P1 + 10] + temp_array_sort_d_P1[number_standard_P1 + 11]) / number_divide_P1;//구간의 범위가 12개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1]) / number_divide_P1;//구간의 범위가 1개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1]) / number_divide_P1;//구간의 범위가 2개일때
                //becon_p1_dozer[1] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2]) / number_divide_P1;//구간의 범위가 3개일때

                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b2_1 = temp_array_sort_d_P1_b2[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b2_2 = temp_array_sort_d_P1_b2[number_standard_P1 + const_array];

                        sum_P1b2_1 = sum_P1b2_1 + sum_P1b2_2;
                    }
                }

                a_becon_p1_dozer[1] = sum_P1b2_1 / number_divide_P1;//평균화

                //P1b3
                temp_array_sort_d_P1_b3 = real_data_d_p1_b3;
                //Array.Sort(temp_array_sort_d_P1_b3);

                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b3[i] > temp_array_sort_d_P1_b3[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b3[i];
                            temp_array_sort_d_P1_b3[i] = temp_array_sort_d_P1_b3[k];
                            temp_array_sort_d_P1_b3[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4]) / number_divide_P1;//구간의 범위가 5개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3]) / number_divide_P1;//구간의 범위가 4개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8]) / number_divide_P1;//구간의 범위가 9개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8] + temp_array_sort_d_P1[number_standard_P1 + 9] + temp_array_sort_d_P1[number_standard_P1 + 10] + temp_array_sort_d_P1[number_standard_P1 + 11]) / number_divide_P1;//구간의 범위가 12개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1]) / number_divide_P1;//구간의 범위가 1개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1]) / number_divide_P1;//구간의 범위가 2개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2]) / number_divide_P1;//구간의 범위가 3개일때

                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b3_1 = temp_array_sort_d_P1_b3[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b3_2 = temp_array_sort_d_P1_b3[number_standard_P1 + const_array];

                        sum_P1b3_1 = sum_P1b3_1 + sum_P1b3_2;
                    }
                }

                a_becon_p1_dozer[2] = sum_P1b3_1 / number_divide_P1;//평균화

                //P1b4
                temp_array_sort_d_P1_b4 = real_data_d_p1_b4;
                //Array.Sort(temp_array_sort_d_P1_b4);

                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b4[i] > temp_array_sort_d_P1_b4[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b4[i];
                            temp_array_sort_d_P1_b4[i] = temp_array_sort_d_P1_b4[k];
                            temp_array_sort_d_P1_b4[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4]) / number_divide_P1;//구간의 범위가 5개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3]) / number_divide_P1;//구간의 범위가 4개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8]) / number_divide_P1;//구간의 범위가 9개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2] + temp_array_sort_d_P1[number_standard_P1 + 3] + temp_array_sort_d_P1[number_standard_P1 + 4] + temp_array_sort_d_P1[number_standard_P1 + 5] + temp_array_sort_d_P1[number_standard_P1 + 6] + temp_array_sort_d_P1[number_standard_P1 + 7] + temp_array_sort_d_P1[number_standard_P1 + 8] + temp_array_sort_d_P1[number_standard_P1 + 9] + temp_array_sort_d_P1[number_standard_P1 + 10] + temp_array_sort_d_P1[number_standard_P1 + 11]) / number_divide_P1;//구간의 범위가 12개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1]) / number_divide_P1;//구간의 범위가 1개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1]) / number_divide_P1;//구간의 범위가 2개일때
                //becon_p1_dozer[2] = (temp_array_sort_d_P1[number_standard_P1] + temp_array_sort_d_P1[number_standard_P1 + 1] + temp_array_sort_d_P1[number_standard_P1 + 2]) / number_divide_P1;//구간의 범위가 3개일때

                //for문 수정으로 인한 추가된 부분
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b4_1 = temp_array_sort_d_P1_b4[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b4_2 = temp_array_sort_d_P1_b4[number_standard_P1 + const_array];

                        sum_P1b4_1 = sum_P1b4_1 + sum_P1b4_2;
                    }
                }

                a_becon_p1_dozer[3] = sum_P1b4_1 / number_divide_P1;//평균화

                //P2
                //P2b1
                temp_array_sort_d_P2_b1 = real_data_d_p2_b1;//가상의 공간을 생성
                //Array.Sort(temp_array_sort_d_P2_b1);

                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b1[i] > temp_array_sort_d_P2_b1[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b1[i];
                            temp_array_sort_d_P2_b1[i] = temp_array_sort_d_P2_b1[k];
                            temp_array_sort_d_P2_b1[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p2_dozer[0] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4]) / number_divide_P2;//구간의 범위가 5개일때
                //becon_p2_dozer[0] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1]) / number_divide_P2;//구간의 범위가 2개일때
                //becon_p2_dozer[0] = (temp_array_sort_d_P2[number_standard_P2]) / number_divide_P2;//구간의 범위가 1개일때
                //becon_p2_dozer[0] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2]) / number_divide_P2;//구간의 범위가 3개일때
                //becon_p2_dozer[0] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4] + temp_array_sort_d_P2[number_standard_P2 + 5] + temp_array_sort_d_P2[number_standard_P2 + 6] + temp_array_sort_d_P2[number_standard_P2 + 7] + temp_array_sort_d_P2[number_standard_P2 + 8] + temp_array_sort_d_P2[number_standard_P2 + 9] + temp_array_sort_d_P2[number_standard_P2 + 10] + temp_array_sort_d_P2[number_standard_P2 + 11]) / number_divide_P2;//구간의 범위가 12개일때

                //for문 수정으로 인한 추가된 부분
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b1_1 = temp_array_sort_d_P2_b1[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b1_2 = temp_array_sort_d_P2_b1[number_standard_P1 + const_array];

                        sum_P2b1_1 = sum_P2b1_1 + sum_P2b1_2;
                    }
                }

                a_becon_p2_dozer[0] = sum_P2b1_1 / number_divide_P2;//평균화

                //P2b2
                temp_array_sort_d_P2_b2 = real_data_d_p2_b2;
                //Array.Sort(temp_array_sort_d_P2_b2);

                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b2[i] > temp_array_sort_d_P2_b2[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b2[i];
                            temp_array_sort_d_P2_b2[i] = temp_array_sort_d_P2_b2[k];
                            temp_array_sort_d_P2_b2[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p2_dozer[1] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4]) / number_divide_P2;//구간의 범위가 5개일때
                //becon_p2_dozer[1] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1]) / number_divide_P2;//구간의 범위가 2개일때
                //becon_p2_dozer[1] = (temp_array_sort_d_P2[number_standard_P2]) / number_divide_P2;//구간의 범위가 1개일때
                //becon_p2_dozer[1] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2]) / number_divide_P2;//구간의 범위가 3개일때
                //becon_p2_dozer[1] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4] + temp_array_sort_d_P2[number_standard_P2 + 5] + temp_array_sort_d_P2[number_standard_P2 + 6] + temp_array_sort_d_P2[number_standard_P2 + 7] + temp_array_sort_d_P2[number_standard_P2 + 8] + temp_array_sort_d_P2[number_standard_P2 + 9] + temp_array_sort_d_P2[number_standard_P2 + 10] + temp_array_sort_d_P2[number_standard_P2 + 11]) / number_divide_P2;//구간의 범위가 12개일때

                //for문 수정으로 인한 추가된 부분
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b2_1 = temp_array_sort_d_P2_b2[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b2_2 = temp_array_sort_d_P2_b2[number_standard_P1 + const_array];

                        sum_P2b2_1 = sum_P2b2_1 + sum_P2b2_2;
                    }
                }

                a_becon_p2_dozer[1] = sum_P2b2_1 / number_divide_P2;//평균화

                //P2b3
                temp_array_sort_d_P2_b3 = real_data_d_p2_b3;
                //Array.Sort(temp_array_sort_d_P2_b3);

                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b3[i] > temp_array_sort_d_P2_b3[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b3[i];
                            temp_array_sort_d_P2_b3[i] = temp_array_sort_d_P2_b3[k];
                            temp_array_sort_d_P2_b3[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4]) / number_divide_P2;//구간의 범위가 5개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1]) / number_divide_P2;//구간의 범위가 2개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2]) / number_divide_P2;//구간의 범위가 1개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2]) / number_divide_P2;//구간의 범위가 3개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4] + temp_array_sort_d_P2[number_standard_P2 + 5] + temp_array_sort_d_P2[number_standard_P2 + 6] + temp_array_sort_d_P2[number_standard_P2 + 7] + temp_array_sort_d_P2[number_standard_P2 + 8] + temp_array_sort_d_P2[number_standard_P2 + 9] + temp_array_sort_d_P2[number_standard_P2 + 10] + temp_array_sort_d_P2[number_standard_P2 + 11]) / number_divide_P2;//구간의 범위가 12개일때

                //for문 수정으로 인한 추가된 부분
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b3_1 = temp_array_sort_d_P2_b3[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b3_2 = temp_array_sort_d_P2_b3[number_standard_P1 + const_array];

                        sum_P2b3_1 = sum_P2b3_1 + sum_P2b3_2;
                    }
                }

                a_becon_p2_dozer[2] = sum_P2b3_1 / number_divide_P2;//평균화

                //P2b4
                temp_array_sort_d_P2_b4 = real_data_d_p2_b4;
                //Array.Sort(temp_array_sort_d_P2_b4);

                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b4[i] > temp_array_sort_d_P2_b4[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b4[i];
                            temp_array_sort_d_P2_b4[i] = temp_array_sort_d_P2_b4[k];
                            temp_array_sort_d_P2_b4[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝

                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4]) / number_divide_P2;//구간의 범위가 5개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1]) / number_divide_P2;//구간의 범위가 2개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2]) / number_divide_P2;//구간의 범위가 1개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2]) / number_divide_P2;//구간의 범위가 3개일때
                //becon_p2_dozer[2] = (temp_array_sort_d_P2[number_standard_P2] + temp_array_sort_d_P2[number_standard_P2 + 1] + temp_array_sort_d_P2[number_standard_P2 + 2] + temp_array_sort_d_P2[number_standard_P2 + 3] + temp_array_sort_d_P2[number_standard_P2 + 4] + temp_array_sort_d_P2[number_standard_P2 + 5] + temp_array_sort_d_P2[number_standard_P2 + 6] + temp_array_sort_d_P2[number_standard_P2 + 7] + temp_array_sort_d_P2[number_standard_P2 + 8] + temp_array_sort_d_P2[number_standard_P2 + 9] + temp_array_sort_d_P2[number_standard_P2 + 10] + temp_array_sort_d_P2[number_standard_P2 + 11]) / number_divide_P2;//구간의 범위가 12개일때

                //for문 수정으로 인한 추가된 부분
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b4_1 = temp_array_sort_d_P2_b4[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b4_2 = temp_array_sort_d_P2_b4[number_standard_P1 + const_array];

                        sum_P2b4_1 = sum_P2b4_1 + sum_P2b4_2;
                    }
                }

                a_becon_p2_dozer[3] = sum_P2b4_1 / number_divide_P2;//평균화

                //증가문 수정
                //Debug.WriteLine(" constant_number_d_P1 " + constant_number_d_P1 + " constant_number_d_P2 " + constant_number_d_P2);

                //constant_number_d_P1++;
                //constant_number_d_P2++;

                //에러로 인한 추가된 부분
                DozerData.Original.IMUSensor = DozerData.Motion.Substring(10, 4) + " " + DozerData.Motion.Substring(6, 4) + " " + DozerData.Motion.Substring(22, 4) + " "
                                + DozerData.Motion.Substring(18, 4) + " " + DozerData.Motion.Substring(34, 4) + " " + DozerData.Motion.Substring(30, 4) + " " + DozerData.Motion.Substring(46, 4) + " " + DozerData.Motion.Substring(42, 4);


                Debug.WriteLine(" average_count " + average_count);//배열카운트 위치 이동에 따른 수정


            }





            //배열이 차면 작동 중지(배열의 갯수 입력) - 평균화로 인한 추가된 부분
            if (average_count == 99 && stop_signal == 0)//초기화 단계에서만 사용으로 수정
            {
                //temp_array_sort_d_P1_b1[0] = 1;

                average_stop = 1;//작동중지 신호(0 일때 평균화 작동)



                //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " average_P1B1 " + temp_array_sort_d_P1_b1[0] + "  " + temp_array_sort_d_P1_b1[1] + "  " + temp_array_sort_d_P1_b1[2] + "  " + temp_array_sort_d_P1_b1[3] + "  " + temp_array_sort_d_P1_b1[4] + "  " + temp_array_sort_d_P1_b1[5]);

                ////통합된 변수 저장
                //receiver_p1_data[0] = a_becon_p1_dozer[0];
                //receiver_p1_data[1] = a_becon_p1_dozer[1];
                //receiver_p1_data[2] = a_becon_p1_dozer[2];
                //receiver_p1_data[3] = a_becon_p1_dozer[3];

                //receiver_p2_data[0] = a_becon_p2_dozer[0];
                //receiver_p2_data[1] = a_becon_p2_dozer[1];
                //receiver_p2_data[2] = a_becon_p2_dozer[2];
                //receiver_p2_data[3] = a_becon_p2_dozer[3];

                Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1a: " + Math.Round(a_becon_p1_dozer[0]) + "  " + Math.Round(a_becon_p1_dozer[1]) + "  " + Math.Round(a_becon_p1_dozer[2]) + "  " + Math.Round(a_becon_p1_dozer[3]) + " dP2a: " + Math.Round(a_becon_p2_dozer[0]) + "  " + Math.Round(a_becon_p2_dozer[1]) + "  " + Math.Round(a_becon_p2_dozer[2]) + "  " + Math.Round(a_becon_p2_dozer[3]) + " 중간값을 평균시킨값");
            }



            if (average_stop == 1)//배열이 작동 중일때는 다른 필터 계산을 하지 않는다(평균화 작동 0) - 평균화로 인한 추가된 부분
            {
                if (stop_signal == 0)//초기화 단계일때(0)
                {
                    //평균화 시킨값과 비교하는 부분
                    value_1[0] = becon_p1_dozer[0];
                    value_1[1] = becon_p1_dozer[1];
                    value_1[2] = becon_p1_dozer[2];
                    value_1[3] = becon_p1_dozer[3];

                    value_1[4] = becon_p2_dozer[0];
                    value_1[5] = becon_p2_dozer[1];
                    value_1[6] = becon_p2_dozer[2];
                    value_1[7] = becon_p2_dozer[3];

                    if ((value_1[0] > a_becon_p1_dozer[0] + K_time2_boundary_constant_up) || (value_1[0] < a_becon_p1_dozer[0] + K_time2_boundary_constant_down))
                    {
                        value_1[0] = a_becon_p1_dozer[0];
                        becon_p1_dozer[0] = value_1[0];
                    }


                    if ((value_1[1] > a_becon_p1_dozer[1] + K_time2_boundary_constant_up) || (value_1[1] < a_becon_p1_dozer[1] + K_time2_boundary_constant_down))
                    {
                        value_1[1] = a_becon_p1_dozer[1];
                        becon_p1_dozer[1] = value_1[1];
                    }

                    if ((value_1[2] > a_becon_p1_dozer[2] + K_time2_boundary_constant_up) || (value_1[2] < a_becon_p1_dozer[2] + K_time2_boundary_constant_down))
                    {
                        value_1[2] = a_becon_p1_dozer[2];
                        becon_p1_dozer[2] = value_1[2];
                    }

                    if ((value_1[3] > a_becon_p1_dozer[3] + K_time2_boundary_constant_up) || (value_1[3] < a_becon_p1_dozer[3] + K_time2_boundary_constant_down))
                    {
                        value_1[3] = a_becon_p1_dozer[3];
                        becon_p1_dozer[3] = value_1[3];
                    }

                    //---------------------------------------------------------------------------------------
                    if ((value_1[4] > a_becon_p2_dozer[0] + K_time2_boundary_constant_up) || (value_1[4] < a_becon_p2_dozer[0] + K_time2_boundary_constant_down))
                    {
                        value_1[4] = a_becon_p2_dozer[0];
                        becon_p2_dozer[0] = value_1[4];
                    }
                    if ((value_1[5] > a_becon_p2_dozer[1] + K_time2_boundary_constant_up) || (value_1[5] < a_becon_p2_dozer[1] + K_time2_boundary_constant_down))
                    {
                        value_1[5] = a_becon_p2_dozer[1];
                        becon_p2_dozer[1] = value_1[5];
                    }

                    if ((value_1[6] > a_becon_p2_dozer[2] + K_time2_boundary_constant_up) || (value_1[6] < a_becon_p2_dozer[2] + K_time2_boundary_constant_down))
                    {
                        value_1[6] = a_becon_p2_dozer[2];
                        becon_p2_dozer[2] = value_1[6];
                    }

                    if ((value_1[7] > a_becon_p2_dozer[3] + K_time2_boundary_constant_up) || (value_1[7] < a_becon_p2_dozer[3] + K_time2_boundary_constant_down))
                    {
                        value_1[7] = a_becon_p2_dozer[3];
                        becon_p2_dozer[3] = value_1[7];
                    }

                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1c: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        + " dP2c: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 평균값 비교후 컷트 시키고 난후 거리값 ");



                    //평균화시킨값과 칼만필터값과의 비교 부분

                    //평균화가 완료되었을때의 칼만필터값과 비교
                    value_2[0] = becon_p1_dozer[0];
                    value_2[1] = becon_p1_dozer[1];
                    value_2[2] = becon_p1_dozer[2];
                    value_2[3] = becon_p1_dozer[3];

                    value_2[4] = becon_p2_dozer[0];
                    value_2[5] = becon_p2_dozer[1];
                    value_2[6] = becon_p2_dozer[2];
                    value_2[7] = becon_p2_dozer[3];

                    if ((value_1[0] < x_est_p1b1 + K_time2_boundary_constant_up) && (value_1[0] > x_est_p1b1 + K_time2_boundary_constant_down))
                    {
                        //P1B1에 해당 되는 값들을 비교후 오차 범위 이내에 값이 들어온다면 표시
                        p1b1_K = 1;
                    }


                    if ((value_1[1] < x_est_p1b2 + K_time2_boundary_constant_up) && (value_1[1] > x_est_p1b2 + K_time2_boundary_constant_down))
                    {
                        p1b2_K = 1;
                    }

                    if ((value_1[2] < x_est_p1b3 + K_time2_boundary_constant_up) && (value_1[2] > x_est_p1b3 + K_time2_boundary_constant_down))
                    {
                        p1b3_K = 1;
                    }

                    if ((value_1[3] < x_est_p1b4 + K_time2_boundary_constant_up) && (value_1[3] > x_est_p1b4 + K_time2_boundary_constant_down))
                    {
                        p1b4_K = 1;
                    }

                    //---------------------------------------------------------------------------------------
                    if ((value_1[4] < x_est_p2b1 + K_time2_boundary_constant_up) && (value_1[4] > x_est_p2b1 + K_time2_boundary_constant_down))
                    {
                        p2b1_K = 1;
                    }
                    if ((value_1[5] < x_est_p2b2 + K_time2_boundary_constant_up) && (value_1[5] > x_est_p2b2 + K_time2_boundary_constant_down))
                    {
                        p2b2_K = 1;
                    }

                    if ((value_1[6] < x_est_p2b3 + K_time2_boundary_constant_up) && (value_1[6] > x_est_p2b3 + K_time2_boundary_constant_down))
                    {
                        p2b3_K = 1;
                    }

                    if ((value_1[7] < x_est_p2b4 + K_time2_boundary_constant_up) && (value_1[7] > x_est_p2b4 + K_time2_boundary_constant_down))
                    {
                        p2b4_K = 1;
                    }

                    if (p1b1_K == 1 && p1b2_K == 1 && p1b3_K == 1 && p1b4_K == 1 && p2b1_K == 1 && p2b2_K == 1 && p2b3_K == 1 && p2b4_K == 1)
                    {
                        P1P2_distance_signal = 1;//만족하면 안정화 단계 진입

                        //안정화 되기전의 칼만필터값 저장 - 초기값 적용할때 이 거리값을 사용 한다
                        save_value_2[0] = value_2[0];
                        save_value_2[1] = value_2[1];
                        save_value_2[2] = value_2[2];
                        save_value_2[3] = value_2[3];

                        save_value_2[4] = value_2[4];
                        save_value_2[5] = value_2[5];
                        save_value_2[6] = value_2[6];
                        save_value_2[7] = value_2[7];

                    }

                    //처음 일치하지않을경우 다시 비교하기위한 초기화 부분
                    p1b1_K = 0;
                    p1b2_K = 0;
                    p1b3_K = 0;
                    p1b4_K = 0;

                    p2b1_K = 0;
                    p2b2_K = 0;
                    p2b3_K = 0;
                    p2b4_K = 0;




                }



                //--------------------------------------------------------------------------------안정화 구간 추가된 부분----------------------------------------------------------------------------------------
                //else if (time2_constant_number_1 == 201)
                //if (stop_selection >= 2 && time2_constant_number_1 >= 200)// 200, 400 최소 6
                //if (time2_constant_number_1 >= 200)
                if (P1P2_distance_signal >= 1)//안정화 상태
                {
                    P1P2_distance_signal = 1;//안정화 신호를 유지하기위해 추가된 부분

                    stop_selection = 2;//한번 값이 들어오면 유지 하기위해

                    if (move_point == 0)//이동안된 상태 - 한번들어가면 적용
                    {


                        time2_boundary_constant_up = move_up;
                        time2_boundary_constant_down = move_down;

                        Q = stop_signal_Q;


                        stop_signal = 1;
                        //Debug.Write("stop" + "\r\n");
                        //Debug.WriteLine(" P_distance_o_1 " + P_distance_o_1 + " P_distance_o_2 " + P_distance_o_2 + " P_distance_o_3 " + P_distance_o_3 + " P_distance_o_4 " + P_distance_o_4);

                        debug_count++;
                    }

                    ////이동한상태로 파악될경우 조건문
                    //if (P_distance_o_12 + 400 < P_distance_o_34)//움직이기 시작 할때 값을 따라가면 됨 정지 상태로 돌아가는건 아직 고려 안함, 최소 3개의 거리값과 비교
                    //{

                    //    time2_boundary_constant_up = move_up;
                    //    time2_boundary_constant_down = move_down;
                    //    Q = 0.001;
                    //    stop_signal = 0;//정지된 상태가 아니기때문

                    //    //Debug.Write("move" + "\r\n");
                    //    //Debug.WriteLine(" P_distance_o_1 " + P_distance_o_1 + " P_distance_o_2 " + P_distance_o_2 + " P_distance_o_3 " + P_distance_o_3 + " P_distance_o_4 " + P_distance_o_4);

                    //    move_point = 1;
                    //}

                }




                if (stop_signal == 1)//안정화 단계
                {

                    //@_1
                    // -------------------  이 부분은 두 수신기의 좌표이동된 거리를 확인하는 구간임(입출력 : becon_p12_dozer)-----------------------------
                    //커팅된 거리값을 가지고 수신기의 좌표를 구한다.(입력 부분)

                    becon_p1_dozer[0] = Math.Round(becon_p1_dozer[0]);
                    becon_p1_dozer[1] = Math.Round(becon_p1_dozer[1]);
                    becon_p1_dozer[2] = Math.Round(becon_p1_dozer[2]);
                    becon_p1_dozer[3] = Math.Round(becon_p1_dozer[3]);

                    becon_p2_dozer[0] = Math.Round(becon_p2_dozer[0]);
                    becon_p2_dozer[1] = Math.Round(becon_p2_dozer[1]);
                    becon_p2_dozer[2] = Math.Round(becon_p2_dozer[2]);
                    becon_p2_dozer[3] = Math.Round(becon_p2_dozer[3]);

                    //이 부분은 수신기의 비교할 두개의 좌표를 계산하기 위한 구간이다
                    //조건문을 걸어서 초기단계와 안정화단계를 구분한다
                    if (no_cut_save > 0)//안정화 수신 데이터가 존재할때
                    {
                        //이전 수신데이터가 커팅 통과한값을 적용 시킬때)
                        receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                        receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                        receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                        receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                        receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                        receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                        receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                        receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                        c_becon_p1_dozer[0] = receiver_p1_data[0];
                        c_becon_p1_dozer[1] = receiver_p1_data[1];
                        c_becon_p1_dozer[2] = receiver_p1_data[2];
                        c_becon_p1_dozer[3] = receiver_p1_data[3];

                        c_becon_p2_dozer[0] = receiver_p2_data[0];
                        c_becon_p2_dozer[1] = receiver_p2_data[1];
                        c_becon_p2_dozer[2] = receiver_p2_data[2];
                        c_becon_p2_dozer[3] = receiver_p2_data[3];

                        const_becon_p2_dozer = 2;//안정화 된후 거리값 가지고 사이값이 만족 했을때 수신기 좌표값 구분

                        first_const = 1;//처음으로 값이 들어옴

                        dozer_data_1 = Calc_dozer_point(becon_p1_dozer, becon_p2_dozer, c_becon_p1_dozer, c_becon_p2_dozer);//2로 들어가서 1로 나온다 becon_p12_dozer 입력으로 들어감

                        first_const = 0;//처음으로 값이 들어온것을 리셋

                    }

                    else if (no_cut_save == 0)//초가화 수신 데이터가 존재할때
                    {

                        //이전 초기화단계 수신 데이터 거리값(이전 수신데이터가 중간값 적용 할때) 
                        receiver_p1_data[0] = a_becon_p1_dozer[0];
                        receiver_p1_data[1] = a_becon_p1_dozer[1];
                        receiver_p1_data[2] = a_becon_p1_dozer[2];
                        receiver_p1_data[3] = a_becon_p1_dozer[3];

                        receiver_p2_data[0] = a_becon_p2_dozer[0];
                        receiver_p2_data[1] = a_becon_p2_dozer[1];
                        receiver_p2_data[2] = a_becon_p2_dozer[2];
                        receiver_p2_data[3] = a_becon_p2_dozer[3];


                        c_becon_p1_dozer[0] = receiver_p1_data[0];
                        c_becon_p1_dozer[1] = receiver_p1_data[1];
                        c_becon_p1_dozer[2] = receiver_p1_data[2];
                        c_becon_p1_dozer[3] = receiver_p1_data[3];

                        c_becon_p2_dozer[0] = receiver_p2_data[0];
                        c_becon_p2_dozer[1] = receiver_p2_data[1];
                        c_becon_p2_dozer[2] = receiver_p2_data[2];
                        c_becon_p2_dozer[3] = receiver_p2_data[3];

                        const_becon_p2_dozer = 2;//안정화 된후 거리값 가지고 사이값이 만족 했을때 수신기 좌표값 구분

                        first_const = 1;//처음으로 값이 들어옴

                        dozer_data_1 = Calc_dozer_point(becon_p1_dozer, becon_p2_dozer, c_becon_p1_dozer, c_becon_p2_dozer);//2로 들어가서 1로 나온다 becon_p12_dozer 입력으로 들어감

                        first_const = 0;//처음으로 값이 들어온것을 리셋

                    }





                    if (P1P2_absolute > P1P2_absolute_constat)//튄값일때
                    {
                        //------------------------------------------이 부분은 이전값을 보내는 부분--------------------------------
                        //if (stabilization_position_cut_save == 1)//조건을 걸어서 저장된 값이 있다면 이전 통과한 값을 보낸다
                        //{
                        //    becon_p1_dozer[0] = becon_p1_dozer_d_a[0];
                        //    becon_p1_dozer[1] = becon_p1_dozer_d_a[1];
                        //    becon_p1_dozer[2] = becon_p1_dozer_d_a[2];
                        //    becon_p1_dozer[3] = becon_p1_dozer_d_a[3];

                        //    becon_p2_dozer[0] = becon_p2_dozer_d_a[0];
                        //    becon_p2_dozer[1] = becon_p2_dozer_d_a[1];
                        //    becon_p2_dozer[2] = becon_p2_dozer_d_a[2];
                        //    becon_p2_dozer[3] = becon_p2_dozer_d_a[3];

                        //    Debug.WriteLine(" P1P2_absolute " + P1P2_absolute + " false ");

                        //    //stabilization_position_cut = 0;//칼만핕터 open
                        //}

                        //if (stabilization_position_cut_save == 0)
                        //{
                        //    Debug.WriteLine(" P1P2_absolute " + P1P2_absolute + " false(x) ");
                        //    //stabilization_position_cut = 1;//이전 통과한 값이 없을 경우 칼만핕터를 닫는다

                        //    //이전 통과한 값이 없을 경우 이전 칼만필터로 보낸 수신데이터를 적용 시키기 때문에 칼만필터를 닫을 필요가 없다
                        //}


                        //--------------------------------------튄값일 경우 이전 칼만필터로 보낸 데이터 전달----------------------------

                        Debug.WriteLine(" P1P2_absolute " + P1P2_absolute + " false ");

                        //if (no_cut_save == 1)//이전 칼만 필터라면 커팅후 거리값이다(조건문 적용)
                        //{
                        //    becon_p1_dozer[0] = save_cut_becon_p1_dozer[0];
                        //    becon_p1_dozer[1] = save_cut_becon_p1_dozer[1];
                        //    becon_p1_dozer[2] = save_cut_becon_p1_dozer[2];
                        //    becon_p1_dozer[3] = save_cut_becon_p1_dozer[3];

                        //    becon_p2_dozer[0] = save_cut_becon_p2_dozer[0];
                        //    becon_p2_dozer[1] = save_cut_becon_p2_dozer[1];
                        //    becon_p2_dozer[2] = save_cut_becon_p2_dozer[2];
                        //    becon_p2_dozer[3] = save_cut_becon_p2_dozer[3];

                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    no_p12_1 = 1;

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //    + " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터) ");

                        //}

                        //else if (no_cut_save == 0)//커팅후 거리값이 없을경우 초기화 단계에서의 칼만필터 거리값을 가져온다(조건문 적용)
                        //{
                        //    becon_p1_dozer[0] = save_value_2[0];
                        //    becon_p1_dozer[1] = save_value_2[1];
                        //    becon_p1_dozer[2] = save_value_2[2];
                        //    becon_p1_dozer[3] = save_value_2[3];

                        //    becon_p2_dozer[0] = save_value_2[4];
                        //    becon_p2_dozer[1] = save_value_2[5];
                        //    becon_p2_dozer[2] = save_value_2[6];
                        //    becon_p2_dozer[3] = save_value_2[7];

                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    no_p12_2 = 1;

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //    + " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터_x) ");

                        //}






                        //false 일경우 이전거리값을 쓰는것이 아닌 그대로 사잇거리필터로 넘긴다 따라서 주석 처리
                        //if (no_cut_save > 0)
                        //{

                        //    becon_p1_dozer[0] = save_cut_becon_p1_dozer[0];
                        //    becon_p1_dozer[1] = save_cut_becon_p1_dozer[1];
                        //    becon_p1_dozer[2] = save_cut_becon_p1_dozer[2];
                        //    becon_p1_dozer[3] = save_cut_becon_p1_dozer[3];

                        //    becon_p2_dozer[0] = save_cut_becon_p2_dozer[0];
                        //    becon_p2_dozer[1] = save_cut_becon_p2_dozer[1];
                        //    becon_p2_dozer[2] = save_cut_becon_p2_dozer[2];
                        //    becon_p2_dozer[3] = save_cut_becon_p2_dozer[3];

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //+ " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터_커팅값) ");


                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    //no_p12_1 = 2;


                        //}

                        //else if (no_cut_save == 0)//저장되지 않을때
                        //{

                        //    //이전 초기화단계 수신 데이터 거리값
                        //    becon_p1_dozer[0] = save_value_2[0];
                        //    becon_p1_dozer[1] = save_value_2[1];
                        //    becon_p1_dozer[2] = save_value_2[2];
                        //    becon_p1_dozer[3] = save_value_2[3];

                        //    becon_p2_dozer[0] = save_value_2[4];
                        //    becon_p2_dozer[1] = save_value_2[5];
                        //    becon_p2_dozer[2] = save_value_2[6];
                        //    becon_p2_dozer[3] = save_value_2[7];

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //    + " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터_사잇거리값_x) ");

                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    //no_p12_2 = 2;
                        //}

                        Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(false) ");


                        //false 일때
                        no_p12_1 = 2;
                    }

                    else if (P1P2_absolute <= P1P2_absolute_constat)//맞는 값일때
                    {
                        //맞을경우 카운팅
                        p_move_count++;

                        //현재값을 저장한다
                        becon_p1_dozer_d_a[0] = becon_p1_dozer[0];
                        becon_p1_dozer_d_a[1] = becon_p1_dozer[1];
                        becon_p1_dozer_d_a[2] = becon_p1_dozer[2];
                        becon_p1_dozer_d_a[3] = becon_p1_dozer[3];

                        becon_p2_dozer_d_a[0] = becon_p2_dozer[0];
                        becon_p2_dozer_d_a[1] = becon_p2_dozer[1];
                        becon_p2_dozer_d_a[2] = becon_p2_dozer[2];
                        becon_p2_dozer_d_a[3] = becon_p2_dozer[3];

                        ////사잇거리, 커팅 부분에 적용됨 - 2분화로 인하여 수정
                        //no_p12_1 = 0;
                        //no_p12_2 = 0;

                        //true 일 경우 커팅으로 전달
                        no_p12_1 = 1;

                        Debug.WriteLine(" P1P2_absolute " + P1P2_absolute + " true ");

                        Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                           + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(true) ");

                        Debug.WriteLine("                                                                                                                                                                                                                        move_count " + p_move_count);

                        //값이 저장되었다는 것을 표시(계속적 1)
                        stabilization_position_cut_save = 1;//(save = 1)

                        //stabilization_position_cut = 0;
                    }

                    else//NaN 이 나올경우
                    {
                        Debug.WriteLine(" P1P2_absolute " + P1P2_absolute + " false(NaN) ");

                        ////false 일경우 이전거리값을 쓰는것이 아닌 그대로 사잇거리필터로 넘긴다 따라서 주석 처리
                        ////--------------------------------------튄값일 경우 이전 칼만필터로 보낸 데이터 전달----------------------------
                        //if (no_cut_save > 0)
                        //{

                        //    becon_p1_dozer[0] = save_cut_becon_p1_dozer[0];
                        //    becon_p1_dozer[1] = save_cut_becon_p1_dozer[1];
                        //    becon_p1_dozer[2] = save_cut_becon_p1_dozer[2];
                        //    becon_p1_dozer[3] = save_cut_becon_p1_dozer[3];

                        //    becon_p2_dozer[0] = save_cut_becon_p2_dozer[0];
                        //    becon_p2_dozer[1] = save_cut_becon_p2_dozer[1];
                        //    becon_p2_dozer[2] = save_cut_becon_p2_dozer[2];
                        //    becon_p2_dozer[3] = save_cut_becon_p2_dozer[3];

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //+ " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터_커팅값) ");


                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    //no_p12_1 = 2;


                        //}

                        //else if (no_cut_save == 0)//저장되지 않을때
                        //{

                        //    //이전 초기화단계 수신 데이터 거리값
                        //    becon_p1_dozer[0] = save_value_2[0];
                        //    becon_p1_dozer[1] = save_value_2[1];
                        //    becon_p1_dozer[2] = save_value_2[2];
                        //    becon_p1_dozer[3] = save_value_2[3];

                        //    becon_p2_dozer[0] = save_value_2[4];
                        //    becon_p2_dozer[1] = save_value_2[5];
                        //    becon_p2_dozer[2] = save_value_2[6];
                        //    becon_p2_dozer[3] = save_value_2[7];

                        //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        //    + " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(이전 수신 데이터_사잇거리값_x) ");

                        //    //이 거리값은 사잇거리, 커팅 부분에 적용되면 안됨
                        //    //no_p12_2 = 2;
                        //}

                        Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 좌표이동거리(false_NaN) ");



                        //false 일때
                        no_p12_1 = 2;

                    }






                    //@_2
                    //------------------------------안정화된후 사잇거리값 적용(becon_p12_dozer 입력)----------------------------
                    if (no_p12_1 == 2)//수신기 좌표이동 거리가 false 되는경우
                    {

                        //수신기 좌표이동 통과 확인 초기화(true 2, false 1, 초기값 0)
                        no_p12_1 = 0;

                        //사잇거리 계산을 위한 입력 becon_p12_dozer
                        c_becon_p1_dozer[0] = Math.Round(becon_p1_dozer[0]);
                        c_becon_p1_dozer[1] = Math.Round(becon_p1_dozer[1]);
                        c_becon_p1_dozer[2] = Math.Round(becon_p1_dozer[2]);
                        c_becon_p1_dozer[3] = Math.Round(becon_p1_dozer[3]);

                        c_becon_p2_dozer[0] = Math.Round(becon_p2_dozer[0]);
                        c_becon_p2_dozer[1] = Math.Round(becon_p2_dozer[1]);
                        c_becon_p2_dozer[2] = Math.Round(becon_p2_dozer[2]);
                        c_becon_p2_dozer[3] = Math.Round(becon_p2_dozer[3]);

                        const_becon_p2_dozer = 0;

                        ////Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1_kk: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        ////    + " eP2_kk: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]));

                        dozer_data_2 = Calc_dozer_point(becon_p1_dozer, becon_p2_dozer, c_becon_p1_dozer, c_becon_p2_dozer);//0으로 들어가서 1로 나온다 그리고 나오는 값은 커트된 거리값(_o) 이다 - 추가된 부분(0605)

                        ////Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1_test: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                        ////     + " eP2_test: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]));

                        ////Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1_test_c: " + Math.Round(c_becon_p1_dozer[0]) + "  " + Math.Round(c_becon_p1_dozer[1]) + "  " + Math.Round(c_becon_p1_dozer[2]) + "  " + Math.Round(c_becon_p1_dozer[3])
                        ////     + " eP2_test_c: " + Math.Round(c_becon_p2_dozer[0]) + "  " + Math.Round(c_becon_p2_dozer[1]) + "  " + Math.Round(c_becon_p2_dozer[2]) + "  " + Math.Round(c_becon_p2_dozer[3]));



                        

                        //------------------------------------------------이 부분은 안정화 된후 사잇거리값 비교후 커트 시키는 구간이다(입력 : becon_p12_dozer)---------------------------------------------

                        //if ((P1P2_distance_o <= (const_distance * (1 + distance_value / 100))) && (P1P2_distance_o >= (const_distance * (1 - distance_value / 100))))
                        if ((P1P2_distance_o <= (const_distance + distance_value_const)) && (P1P2_distance_o >= (const_distance - distance_value_const)))
                        {
                            Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " true ");//P1P2_distance_o 는 안정화 된후 커트 시키고난후의 사이거리값

                            becon_p1_dozer_a[0] = becon_p1_dozer[0];
                            becon_p1_dozer_a[1] = becon_p1_dozer[1];
                            becon_p1_dozer_a[2] = becon_p1_dozer[2];
                            becon_p1_dozer_a[3] = becon_p1_dozer[3];

                            becon_p2_dozer_a[0] = becon_p2_dozer[0];
                            becon_p2_dozer_a[1] = becon_p2_dozer[1];
                            becon_p2_dozer_a[2] = becon_p2_dozer[2];
                            becon_p2_dozer_a[3] = becon_p2_dozer[3];

                            const_becon_p_dozer_a = 1;//초기 값이 저장되었는지 확인 하기위해

                            //저장된 이전 거리값이 없을때는 칼만필터로 거리값을 넘기지 말아야 된다(신호 보냄 : 칼만 통과 0, 못 통가 1) - 저장된 이전 거리값이 없을 경우 이전 칼만필터 값을 사용하므로 사이거리값 비교 구문에서 칼만필터를 제어 하지 않는다 따라서 이 구문에서는 무조건 0
                            //stabilization_distance_cut = 0;


                            //stabilization_distance_cut_save = 1;

                            //cutting_filter_open = 0;//0 일경우 cutting filter close

                            p12_distance_count++;//사잇거리 true 카운트

                            //if (cutting_filter_open == 0) //0 일경우 거리값 출력되도록 적용
                            // {
                            Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                         + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(true) ");
                            Debug.WriteLine("                                                                                                                                                                                                                       p12_distance_count " + p12_distance_count);
                            // }

                            no_distance_1 = 1;//커팅부분으로 적용된다(커팅적용 1, 미적용 2, 초기값 0)
                                              //no_distance_2 = 1;

                        }

                        //else if ((stop_signal == 1 && P1P2_distance_o < 420) || (stop_signal == 1 && P1P2_distance_o > 620))//520 기준으로 54차이/100차이

                        else//false 인 경우
                        {

                            ////---------------------------------- 이 영역은 false 인 경우 이전 값을 사용한다 --------------------------------------
                            //if (const_becon_p_dozer_a == 1)
                            //{
                            //    becon_p1_dozer[0] = becon_p1_dozer_a[0];
                            //    becon_p1_dozer[1] = becon_p1_dozer_a[1];
                            //    becon_p1_dozer[2] = becon_p1_dozer_a[2];
                            //    becon_p1_dozer[3] = becon_p1_dozer_a[3];

                            //    becon_p2_dozer[0] = becon_p2_dozer_a[0];
                            //    becon_p2_dozer[1] = becon_p2_dozer_a[1];
                            //    becon_p2_dozer[2] = becon_p2_dozer_a[2];
                            //    becon_p2_dozer[3] = becon_p2_dozer_a[3];

                            //    //const_becon_p_dozer_a = 0;

                            //    stabilization_distance_cut = 0;//칼만통과

                            //    Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");
                            //}

                            //else if (const_becon_p_dozer_a == 0)//초기 상태에서 사이거리 범위를 넘어간 경우
                            //{

                            //    stabilization_distance_cut = 1;//칼만 못 통과

                            //    Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false(x) ");
                            //}//-----------------------------------------------------------------------------------------------------------------------




                            ////------------------------- 이 영역은 false 인 경우 cutting filter 로 넘어갈때 사용한다(이때는 초기값의 유무는 의미없음) ----------------------------------
                            //if (const_becon_p_dozer_a == 1)
                            //{
                            //    //cutting filter open 하기위한 부분
                            //    cutting_filter_open = 1;//1 일경우 cutting filter open

                            //    stabilization_distance_cut = 0;//칼만통과

                            //    Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");
                            //}

                            //else if (const_becon_p_dozer_a == 0)//초기 상태에서 사이거리 범위를 넘어간 경우(false 인 경우)
                            //{

                            //    //cutting filter open 하기위한 부분
                            //    cutting_filter_open = 1;//1 일경우 cutting filter open

                            //    //stabilization_distance_cut = 1;//칼만 못 통과(무조건 통과 시킴으로 수정)

                            //    Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false(x) ");
                            //}
                            //---------------------------------------------------------------------------------------------------------------------------------------------------------------





                            ////------------------------- 이 영역은 false 인 경우 이전에 칼만필터로 넘어간 거리값을 사용한다----------------------------------
                            if (const_becon_p_dozer_a == 1)//초기값 저장이 된 경우라도 이전 칼만필터값을 사용 하므로 의미없음
                            {
                                Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");

                                if (no_cut_save == 1)//커팅후 거리값이 저장됨(조건문 적용)
                                {
                                    receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                    receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                    receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                    receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                    receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                    receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                    receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                    receiver_p2_data[3] = save_cut_becon_p2_dozer[3];


                                    becon_p1_dozer[0] = receiver_p1_data[0];
                                    becon_p1_dozer[1] = receiver_p1_data[1];
                                    becon_p1_dozer[2] = receiver_p1_data[2];
                                    becon_p1_dozer[3] = receiver_p1_data[3];

                                    becon_p2_dozer[0] = receiver_p2_data[0];
                                    becon_p2_dozer[1] = receiver_p2_data[1];
                                    becon_p2_dozer[2] = receiver_p2_data[2];
                                    becon_p2_dozer[3] = receiver_p2_data[3];



                                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                             + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(이전 수신 데이터) ");//사잇거리값 비교후 거리값(false)

                                    check_calc = 1;//이전 수신 데이터를 사용할 경우 1 - 추가된 부분(0711)

                                }

                                else if (no_cut_save == 0)//커팅후 거리값이 없을경우 초기화 단계에서의 중간 거리값을 가져온다(조건문 적용)
                                {

                                    receiver_p1_data[0] = a_becon_p1_dozer[0];
                                    receiver_p1_data[1] = a_becon_p1_dozer[1];
                                    receiver_p1_data[2] = a_becon_p1_dozer[2];
                                    receiver_p1_data[3] = a_becon_p1_dozer[3];

                                    receiver_p2_data[0] = a_becon_p2_dozer[0];
                                    receiver_p2_data[1] = a_becon_p2_dozer[1];
                                    receiver_p2_data[2] = a_becon_p2_dozer[2];
                                    receiver_p2_data[3] = a_becon_p2_dozer[3];



                                    becon_p1_dozer[0] = receiver_p1_data[0];
                                    becon_p1_dozer[1] = receiver_p1_data[1];
                                    becon_p1_dozer[2] = receiver_p1_data[2];
                                    becon_p1_dozer[3] = receiver_p1_data[3];

                                    becon_p2_dozer[0] = receiver_p2_data[0];
                                    becon_p2_dozer[1] = receiver_p2_data[1];
                                    becon_p2_dozer[2] = receiver_p2_data[2];
                                    becon_p2_dozer[3] = receiver_p2_data[3];



                                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                             + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(이전 수신 데이터_x) ");//사잇거리값 비교후 거리값(false)

                                }

                                //이 거리값은 커팅 부분에 적용되면 안됨(칼만필터로 보냄) - 이후 다시 0 으로 초기화가 됨
                                no_distance_1 = 2;
                                // no_distance_2 = 2;
                            }





                            else if (const_becon_p_dozer_a == 0)//이전 값이 저장 되지 않더라도 이전 칼만필터값을 사용 하므로 의미없음
                            {
                                Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");

                                if (no_cut_save == 1)//커팅후 거리값이 저장됨(조건문 적용)
                                {
                                    receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                    receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                    receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                    receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                    receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                    receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                    receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                    receiver_p2_data[3] = save_cut_becon_p2_dozer[3];


                                    becon_p1_dozer[0] = receiver_p1_data[0];
                                    becon_p1_dozer[1] = receiver_p1_data[1];
                                    becon_p1_dozer[2] = receiver_p1_data[2];
                                    becon_p1_dozer[3] = receiver_p1_data[3];

                                    becon_p2_dozer[0] = receiver_p2_data[0];
                                    becon_p2_dozer[1] = receiver_p2_data[1];
                                    becon_p2_dozer[2] = receiver_p2_data[2];
                                    becon_p2_dozer[3] = receiver_p2_data[3];


                                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                             + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(이전 수신 데이터) ");//사잇거리값 비교후 거리값(false)

                                    check_calc = 1;//이전 수신 데이터를 사용할 경우 1 - 추가된 부분(0711)

                                }

                                else if (no_cut_save == 0)//커팅후 거리값이 없을경우 초기화 단계에서의 중간 거리값을 가져온다(조건문 적용)
                                {

                                    receiver_p1_data[0] = a_becon_p1_dozer[0];
                                    receiver_p1_data[1] = a_becon_p1_dozer[1];
                                    receiver_p1_data[2] = a_becon_p1_dozer[2];
                                    receiver_p1_data[3] = a_becon_p1_dozer[3];

                                    receiver_p2_data[0] = a_becon_p2_dozer[0];
                                    receiver_p2_data[1] = a_becon_p2_dozer[1];
                                    receiver_p2_data[2] = a_becon_p2_dozer[2];
                                    receiver_p2_data[3] = a_becon_p2_dozer[3];



                                    becon_p1_dozer[0] = receiver_p1_data[0];
                                    becon_p1_dozer[1] = receiver_p1_data[1];
                                    becon_p1_dozer[2] = receiver_p1_data[2];
                                    becon_p1_dozer[3] = receiver_p1_data[3];

                                    becon_p2_dozer[0] = receiver_p2_data[0];
                                    becon_p2_dozer[1] = receiver_p2_data[1];
                                    becon_p2_dozer[2] = receiver_p2_data[2];
                                    becon_p2_dozer[3] = receiver_p2_data[3];



                                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                             + " dP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(이전 수신 데이터_x) ");//사잇거리값 비교후 거리값(false)

                                }

                                //이 거리값은 커팅 부분에 적용되면 안됨(칼만필터로 보냄) - 이후 다시 0 으로 초기화가 됨
                                no_distance_1 = 2;
                                //   no_distance_2 = 2;


                                ////사잇거리값이 저장 안되었을경우 - 사잇거리값 저장 유무와는 상관없이 이전값이 저장되지 않으면 중간값으로 보내버리고 이는 이미 적용된 상태이므로 주석처리)
                                ////이전 초기화 단계에서 칼만필터로 보내는 값 적용
                                //becon_p1_dozer[0] = save_value_2[0];
                                //becon_p1_dozer[1] = save_value_2[1];
                                //becon_p1_dozer[2] = save_value_2[2];
                                //becon_p1_dozer[3] = save_value_2[3];

                                //becon_p2_dozer[0] = save_value_2[4];
                                //becon_p2_dozer[1] = save_value_2[5];
                                //becon_p2_dozer[2] = save_value_2[6];
                                //becon_p2_dozer[3] = save_value_2[7];

                                //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                                //+ " eP2d: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 사잇거리(이전 수신 데이터_x) ");

                            }
                            //---------------------------------------------------------------------------------------------------------------------------------------------------------------

                        }

                    }



                    //@_3
                    //----------------------------------------------------안정화단계에서 커팅하는 부분(입출력 부분 becon_p12_dozer)-----------------------------------
                    //if (no_distance_1 == 0 && no_distance_2 == 0 && no_p12_1 == 0 && no_p12_2 == 0)//이전 버젼 : 사잇거리값과 좌표이동거리값이 모두 true 일때만 통과
                    if (no_distance_1 == 1 || no_p12_1 == 1)// "사잇거리값" 과 "좌표이동거리값" 중 하나라도 true 이면 통과
                    {

                        //초기화 부분
                        no_distance_1 = 0;
                        no_p12_1 = 0;

                        //////-----------------------------------------------------------------distance fiter 1 - 로우값끼리 비교-----------------------------------------------
                        constant_number += 1;

                        //현재 수신 데이터
                        if (constant_number == 1)//처음값은 다음값을 필터할때의 기준이 된다 따라서 그대로 넘어감
                        {
                            cut_value_1[0] = becon_p1_dozer[0];
                            cut_value_1[1] = becon_p1_dozer[1];
                            cut_value_1[2] = becon_p1_dozer[2];
                            cut_value_1[3] = becon_p1_dozer[3];
                                                      
                            cut_value_1[4] = becon_p2_dozer[0];
                            cut_value_1[5] = becon_p2_dozer[1];
                            cut_value_1[6] = becon_p2_dozer[2];
                            cut_value_1[7] = becon_p2_dozer[3];


                            if (constant_number_2 == 0)
                            {
                                //필터링된 4개 거리
                                Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1c: " + Math.Round(cut_value_1[0]) + "  " + Math.Round(cut_value_1[1]) + "  " + Math.Round(cut_value_1[2]) + "  " + Math.Round(cut_value_1[3])
                                 + " dP2c: " + Math.Round(cut_value_1[4]) + "  " + Math.Round(cut_value_1[5]) + "  " + Math.Round(cut_value_1[6]) + "  " + Math.Round(cut_value_1[7]) + " 비교입력_(초기값) ");
                            }

                            else if (constant_number_2 == 1)
                            {
                                Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1c: " + Math.Round(cut_value_1[0]) + "  " + Math.Round(cut_value_1[1]) + "  " + Math.Round(cut_value_1[2]) + "  " + Math.Round(cut_value_1[3])
                                      + " dP2c: " + Math.Round(cut_value_1[4]) + "  " + Math.Round(cut_value_1[5]) + "  " + Math.Round(cut_value_1[6]) + "  " + Math.Round(cut_value_1[7]) + " 비교입력_2 ");
                            }


                            //이 부분은 카운터가 1번돌 경우임 이전 수신 데이터인 중간값 평균을 적용 시켜야된다.(초기값 0, 이후 1, 2)
                            if (constant_number_2 == 0 && (cut_value_1[0] > a_becon_p1_dozer[0] + time2_boundary_constant_up || cut_value_1[0] < a_becon_p1_dozer[0] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[0] = cut_value_2[0];
                                //becon_p1_dozer[0] = cut_value_1[0];

                                ////개별적 수정
                                //receiver_p1_data[0] = a_becon_p1_dozer[0];
                                //becon_p1_dozer[0] = receiver_p1_data[0];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[1] > a_becon_p1_dozer[1] + time2_boundary_constant_up || cut_value_1[1] < a_becon_p1_dozer[1] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[1] = cut_value_2[1];
                                //becon_p1_dozer[1] = cut_value_1[1];

                                ////개별적 수정
                                //receiver_p1_data[1] = a_becon_p1_dozer[1];
                                //becon_p1_dozer[1] = receiver_p1_data[1];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[2] > a_becon_p1_dozer[2] + time2_boundary_constant_up || cut_value_1[2] < a_becon_p1_dozer[2] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[2] = cut_value_2[2];
                                //becon_p1_dozer[2] = cut_value_1[2];

                                ////개별적 수정
                                //receiver_p1_data[2] = a_becon_p1_dozer[2];
                                //becon_p1_dozer[2] = receiver_p1_data[2];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[3] > a_becon_p1_dozer[3] + time2_boundary_constant_up || cut_value_1[3] < a_becon_p1_dozer[3] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[3] = cut_value_2[3];
                                //becon_p1_dozer[3] = cut_value_1[3];

                                ////개별적 수정
                                //receiver_p1_data[3] = a_becon_p1_dozer[3];
                                //becon_p1_dozer[3] = receiver_p1_data[3];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //---------------------------------------------------------------------------------------
                            if (constant_number_2 == 0 && (cut_value_1[4] > a_becon_p2_dozer[0] + time2_boundary_constant_up || cut_value_1[4] < a_becon_p2_dozer[0] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[4] = cut_value_2[4];
                                //becon_p2_dozer[0] = cut_value_1[4];

                                ////개별적 수정
                                //receiver_p2_data[0] = a_becon_p2_dozer[0];
                                //becon_p2_dozer[0] = receiver_p2_data[0];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            if (constant_number_2 == 0 && (cut_value_1[5] > a_becon_p2_dozer[1] + time2_boundary_constant_up || cut_value_1[5] < a_becon_p2_dozer[1] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[5] = cut_value_2[5];
                                //becon_p2_dozer[1] = cut_value_1[5];

                                ////개별적 수정
                                //receiver_p2_data[1] = a_becon_p2_dozer[1];
                                //becon_p2_dozer[1] = receiver_p2_data[1];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[6] > a_becon_p2_dozer[2] + time2_boundary_constant_up || cut_value_1[6] < a_becon_p2_dozer[2] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[6] = cut_value_2[6];
                                //becon_p2_dozer[2] = cut_value_1[6];

                                ////개별적 수정
                                //receiver_p2_data[2] = a_becon_p2_dozer[2];
                                //becon_p2_dozer[2] = receiver_p2_data[2];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[7] > a_becon_p2_dozer[3] + time2_boundary_constant_up || cut_value_1[7] < a_becon_p2_dozer[3] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[7] = cut_value_2[7];
                                //becon_p2_dozer[3] = cut_value_1[7];

                                ////개별적 수정
                                //receiver_p2_data[3] = a_becon_p2_dozer[3];
                                //becon_p2_dozer[3] = receiver_p2_data[3];

                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_dozer[0];
                                receiver_p1_data[1] = a_becon_p1_dozer[1];
                                receiver_p1_data[2] = a_becon_p1_dozer[2];
                                receiver_p1_data[3] = a_becon_p1_dozer[3];
                                                                 
                                receiver_p2_data[0] = a_becon_p2_dozer[0];
                                receiver_p2_data[1] = a_becon_p2_dozer[1];
                                receiver_p2_data[2] = a_becon_p2_dozer[2];
                                receiver_p2_data[3] = a_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }






                            //이 부분은 카운터가 2번 이상 돌았을경우(1번 돌경우 이전버젼일 경우그냥 넘어감) 적용되기 때문에 카운터가 1번돌 경우 이전 수신 데이터인 중간값 평균을 적용 시켜야된다.
                            //이 부분은 카운터가 2번 이상 돌았을경우 적용된다 따라서 커팅된 값은 저장되어져 있다
                            if (constant_number_2 == 1 && (cut_value_1[0] > cut_value_2[0] + time2_boundary_constant_up || cut_value_1[0] < cut_value_2[0] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[0] = cut_value_2[0];
                                //becon_p1_dozer[0] = cut_value_1[0];

                                ////개별적 수정
                                //receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                //becon_p1_dozer[0] = receiver_p1_data[0];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];
                                                                        
                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];
                                         
                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[1] > cut_value_2[1] + time2_boundary_constant_up || cut_value_1[1] < cut_value_2[1] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[1] = cut_value_2[1];
                                //becon_p1_dozer[1] = cut_value_1[1];

                                ////개별적 수정
                                //receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                //becon_p1_dozer[1] = receiver_p1_data[1];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[2] > cut_value_2[2] + time2_boundary_constant_up || cut_value_1[2] < cut_value_2[2] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[2] = cut_value_2[2];
                                //becon_p1_dozer[2] = cut_value_1[2];

                                ////개별적 수정
                                //receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                //becon_p1_dozer[2] = receiver_p1_data[2];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[3] > cut_value_2[3] + time2_boundary_constant_up || cut_value_1[3] < cut_value_2[3] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[3] = cut_value_2[3];
                                //becon_p1_dozer[3] = cut_value_1[3];

                                ////개별적 수정
                                //receiver_p1_data[3] = save_cut_becon_p1_dozer[3];
                                //becon_p1_dozer[3] = receiver_p1_data[3];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //---------------------------------------------------------------------------------------
                            if (constant_number_2 == 1 && (cut_value_1[4] > cut_value_2[4] + time2_boundary_constant_up || cut_value_1[4] < cut_value_2[4] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[4] = cut_value_2[4];
                                //becon_p2_dozer[0] = cut_value_1[4];

                                ////개별적 수정
                                //receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                //becon_p2_dozer[0] = receiver_p2_data[0];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            if (constant_number_2 == 1 && (cut_value_1[5] > cut_value_2[5] + time2_boundary_constant_up || cut_value_1[5] < cut_value_2[5] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[5] = cut_value_2[5];
                                //becon_p2_dozer[1] = cut_value_1[5];

                                ////개별적 수정
                                //receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                //becon_p2_dozer[1] = receiver_p2_data[1];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[6] > cut_value_2[6] + time2_boundary_constant_up || cut_value_1[6] < cut_value_2[6] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[6] = cut_value_2[6];
                                //becon_p2_dozer[2] = cut_value_1[6];

                                ////개별적 수정
                                //receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                //becon_p2_dozer[2] = receiver_p2_data[2];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[7] > cut_value_2[7] + time2_boundary_constant_up || cut_value_1[7] < cut_value_2[7] + time2_boundary_constant_down))
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_1[7] = cut_value_2[7];
                                //becon_p2_dozer[3] = cut_value_1[7];

                                ////개별적 수정
                                //receiver_p2_data[3] = save_cut_becon_p2_dozer[3];
                                //becon_p2_dozer[3] = receiver_p2_data[3];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            ////그대로 넘어갈 경우 이전 칼만필터로 보내진 데이터로 칼만필터 적용 - 그대로 넘어가는 경우 구분
                            //if (constant_number_2 == 0)
                            //{
                            //    becon_p1_dozer[0] = save_value_2[0];
                            //    becon_p1_dozer[1] = save_value_2[1];
                            //    becon_p1_dozer[2] = save_value_2[2];
                            //    becon_p1_dozer[3] = save_value_2[3];

                            //    becon_p2_dozer[0] = save_value_2[4];
                            //    becon_p2_dozer[1] = save_value_2[5];
                            //    becon_p2_dozer[2] = save_value_2[6];
                            //    becon_p2_dozer[3] = save_value_2[7];

                            //    no_cut_save = 0;//커팅부분에서 초기화가 되지 않을 경우

                            //    //필터링된 4개 거리
                            //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1c: " + Math.Round(cut_value_1[0]) + "  " + Math.Round(cut_value_1[1]) + "  " + Math.Round(cut_value_1[2]) + "  " + Math.Round(cut_value_1[3])
                            //         + " eP2c: " + Math.Round(cut_value_1[4]) + "  " + Math.Round(cut_value_1[5]) + "  " + Math.Round(cut_value_1[6]) + "  " + Math.Round(cut_value_1[7]) + " 비교_이전 수신 데이터 ");

                            //}


                            no_cut_save = 1;//커팅 안정화(한번 들어오면 계속 1인 상태)

                        }

                        //현재 수신 데이터
                        if (constant_number == 2)
                        {
                            cut_value_2[0] = becon_p1_dozer[0];
                            cut_value_2[1] = becon_p1_dozer[1];
                            cut_value_2[2] = becon_p1_dozer[2];
                            cut_value_2[3] = becon_p1_dozer[3];

                            cut_value_2[4] = becon_p2_dozer[0];
                            cut_value_2[5] = becon_p2_dozer[1];
                            cut_value_2[6] = becon_p2_dozer[2];
                            cut_value_2[7] = becon_p2_dozer[3];

                            Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1c: " + Math.Round(cut_value_2[0]) + "  " + Math.Round(cut_value_2[1]) + "  " + Math.Round(cut_value_2[2]) + "  " + Math.Round(cut_value_2[3])
                                      + " dP2c: " + Math.Round(cut_value_2[4]) + "  " + Math.Round(cut_value_2[5]) + "  " + Math.Round(cut_value_2[6]) + "  " + Math.Round(cut_value_2[7]) + " 비교입력_1 ");


                            if (cut_value_2[0] > cut_value_1[0] + time2_boundary_constant_up || cut_value_2[0] < cut_value_1[0] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[0] = cut_value_1[0];
                                //becon_p1_dozer[0] = cut_value_2[0];

                                ////개별적 수정
                                //receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                //becon_p1_dozer[0] = receiver_p1_data[0];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[1] > cut_value_1[1] + time2_boundary_constant_up || cut_value_2[1] < cut_value_1[1] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[1] = cut_value_1[1];
                                //becon_p1_dozer[1] = cut_value_2[1];

                                ////개별적 수정
                                //receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                //becon_p1_dozer[1] = receiver_p1_data[1];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[2] > cut_value_1[2] + time2_boundary_constant_up || cut_value_2[2] < cut_value_1[2] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[2] = cut_value_1[2];
                                //becon_p1_dozer[2] = cut_value_2[2];

                                ////개별적 수정
                                //receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                //becon_p1_dozer[2] = receiver_p1_data[2];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[3] > cut_value_1[3] + time2_boundary_constant_up || cut_value_2[3] < cut_value_1[3] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[3] = cut_value_1[3];
                                //becon_p1_dozer[3] = cut_value_2[3];

                                ////개별적 수정
                                //receiver_p1_data[3] = save_cut_becon_p1_dozer[3];
                                //becon_p1_dozer[3] = receiver_p1_data[3];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            //----------------------------------------------------------------------------
                            if (cut_value_2[4] > cut_value_1[4] + time2_boundary_constant_up || cut_value_2[4] < cut_value_1[4] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[4] = cut_value_1[4];
                                //becon_p2_dozer[0] = cut_value_2[4];

                                ////개별적 수정
                                //receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                //becon_p2_dozer[0] = receiver_p2_data[0];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[5] > cut_value_1[5] + time2_boundary_constant_up || cut_value_2[5] < cut_value_1[5] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[5] = cut_value_1[5];
                                //becon_p2_dozer[1] = cut_value_2[5];

                                ////개별적 수정
                                //receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                //becon_p2_dozer[1] = receiver_p2_data[1];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[6] > cut_value_1[6] + time2_boundary_constant_up || cut_value_2[6] < cut_value_1[6] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[6] = cut_value_1[6];
                                //becon_p2_dozer[2] = cut_value_2[6];

                                ////개별적 수정
                                //receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                //becon_p2_dozer[2] = receiver_p2_data[2];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[7] > cut_value_1[7] + time2_boundary_constant_up || cut_value_2[7] < cut_value_1[7] + time2_boundary_constant_down)
                            {
                                //범위를 벗어나면 이전 수신 데이터 적용 하기 때문에 주석처리
                                //cut_value_2[7] = cut_value_1[7];
                                //becon_p2_dozer[3] = cut_value_2[7];

                                ////개별적 수정
                                //receiver_p2_data[3] = save_cut_becon_p2_dozer[3];
                                //becon_p2_dozer[3] = receiver_p2_data[3];

                                receiver_p1_data[0] = save_cut_becon_p1_dozer[0];
                                receiver_p1_data[1] = save_cut_becon_p1_dozer[1];
                                receiver_p1_data[2] = save_cut_becon_p1_dozer[2];
                                receiver_p1_data[3] = save_cut_becon_p1_dozer[3];

                                receiver_p2_data[0] = save_cut_becon_p2_dozer[0];
                                receiver_p2_data[1] = save_cut_becon_p2_dozer[1];
                                receiver_p2_data[2] = save_cut_becon_p2_dozer[2];
                                receiver_p2_data[3] = save_cut_becon_p2_dozer[3];

                                becon_p1_dozer[0] = receiver_p1_data[0];
                                becon_p1_dozer[1] = receiver_p1_data[1];
                                becon_p1_dozer[2] = receiver_p1_data[2];
                                becon_p1_dozer[3] = receiver_p1_data[3];

                                becon_p2_dozer[0] = receiver_p2_data[0];
                                becon_p2_dozer[1] = receiver_p2_data[1];
                                becon_p2_dozer[2] = receiver_p2_data[2];
                                becon_p2_dozer[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //비교를 하기위한 카운트 초기화
                            constant_number = 0;
                            constant_number_2 = 1;






                        }


                        //커팅된값 저장
                        save_cut_becon_p1_dozer[0] = becon_p1_dozer[0];
                        save_cut_becon_p1_dozer[1] = becon_p1_dozer[1];
                        save_cut_becon_p1_dozer[2] = becon_p1_dozer[2];
                        save_cut_becon_p1_dozer[3] = becon_p1_dozer[3];

                        save_cut_becon_p2_dozer[0] = becon_p2_dozer[0];
                        save_cut_becon_p2_dozer[1] = becon_p2_dozer[1];
                        save_cut_becon_p2_dozer[2] = becon_p2_dozer[2];
                        save_cut_becon_p2_dozer[3] = becon_p2_dozer[3];

                        //커팅된값 저장되었을때의 비컨 id를 저장시키기 위한 구문 - 추가된 부분(07011)
                        before_d_value = 1;

                        if (cut_output == 0) cut_count++;

                        //필터링된 4개 거리
                        Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1c: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
                          + " dP2c: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]) + " 컷팅 비교후 거리값(1 = 이전값, 0 = 통과) " + cut_output);

                        Debug.WriteLine("                                                                                                                                                                                                                        cut_count " + cut_count);

                        //출력값 최기화
                        cut_output = 0;

                        //커팅이 됨으로 해서 칼만으로 신호를 보내기 위한 변수(칼만 통과시 다시 0 이 된다)
                        cut_signal = 1;


                    }



                }



                //---------------------------------------------------------------------------------------- 이부분은 칼만필터 구간 -----------------------------------------------------------------------------
                //if (stabilization_distance_cut == 0 && stabilization_position_cut == 0) - "사잇거리값의 초기값" 그리고 "수신기의 위치 초기값" 이 저장될 경우 적용 된다
                //if (stabilization_distance_cut == 0)//"사잇거리값의 초기값(의미없음)" 일 경우 적용 된다
                if (stop_signal == 0 || cut_signal == 1 || no_distance_1 == 2)//칼만필터는 "초기화", "커팅", "사잇거리  false" 일경우만 통과 시킨다.
                {
                    //안정화후 칼만 정상화 확인 조건문(계속 1)
                    if (cut_signal == 1 || no_distance_1 == 2)
                    {
                        k_a_signal = 1;
                        //Debug.WriteLine("test " + no_distance_1);
                    }

                    //커팅 통과 변수 초기화
                    cut_signal = 0;

                    //사잇거리 false 신호 초기화
                    no_distance_1 = 0;
                    //no_distance_2 = 0;

                    //------------------------------------------------------------------------------K.F.

                    //-----------------------------------------------------averaging
                    //kf_constant_number_1 += 1;

                    //if (kf_constant_number_1 == 1)
                    //{
                    //    kf_value_1[0] = becon_p1_dozer[0];
                    //    kf_value_1[1] = becon_p1_dozer[1];
                    //    kf_value_1[2] = becon_p1_dozer[2];

                    //    kf_value_1[3] = becon_p2_dozer[0];
                    //    kf_value_1[4] = becon_p2_dozer[1];
                    //    kf_value_1[5] = becon_p2_dozer[2];

                    //    //kf_value_a[0] = kf_value_1[0];
                    //    //kf_value_a[1] = kf_value_1[1];
                    //    //kf_value_a[2] = kf_value_1[2];

                    //    //kf_value_a[3] = kf_value_1[3];
                    //    //kf_value_a[4] = kf_value_1[4];
                    //    //kf_value_a[5] = kf_value_1[5];

                    //}


                    //else if (kf_constant_number_1 == 2)
                    //{
                    //    kf_value_2[0] = becon_p1_dozer[0];
                    //    kf_value_2[1] = becon_p1_dozer[1];
                    //    kf_value_2[2] = becon_p1_dozer[2];

                    //    kf_value_2[3] = becon_p2_dozer[0];
                    //    kf_value_2[4] = becon_p2_dozer[1];
                    //    kf_value_2[5] = becon_p2_dozer[2];

                    //    kf_value_1[0] = (kf_value_1[0] + kf_value_2[0]) / 2;
                    //    kf_value_1[1] = (kf_value_1[1] + kf_value_2[1]) / 2;
                    //    kf_value_1[2] = (kf_value_1[2] + kf_value_2[2]) / 2;

                    //    kf_value_1[3] = (kf_value_1[3] + kf_value_2[3]) / 2;
                    //    kf_value_1[4] = (kf_value_1[4] + kf_value_2[4]) / 2;
                    //    kf_value_1[5] = (kf_value_1[5] + kf_value_2[5]) / 2;

                    //    //std_dev[0] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
                    //    //std_dev[1] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
                    //    //std_dev[2] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);

                    //    //std_dev[3] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
                    //    //std_dev[4] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
                    //    //std_dev[5] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);

                    //    kf_constant_number_1 = 1;
                    //}

                    //-----------------------------------------------------------------------------prediction
                    //#1
                    //update_befor_x_p1b1 = update_x_p1b1;
                    //predicton_x_p1b1 = update_x_p1b1 + v_value_p1b1;

                    //update_befor_x_p1b2 = update_x_p1b2;
                    //predicton_x_p1b2 = update_x_p1b2 + v_value_p1b2;

                    //update_befor_x_p1b3 = update_x_p1b3;
                    //predicton_x_p1b3 = update_x_p1b3 + v_value_p1b3;

                    //update_befor_x_p2b1 = update_x_p2b1;
                    //predicton_x_p2b1 = update_x_p2b1 + v_value_p2b1;

                    //update_befor_x_p2b2 = update_x_p2b2;
                    //predicton_x_p2b2 = update_x_p2b2 + v_value_p2b2;

                    //update_befor_x_p2b3 = update_x_p2b3;
                    //predicton_x_p2b3 = update_x_p2b3 + v_value_p2b3;

                    //#2
                    //update_x_p1b1 = becon_p1_dozer[0];
                    //update_x_p1b2 = becon_p1_dozer[1];
                    //update_x_p1b3 = becon_p1_dozer[2];
                    //update_x_p2b1 = becon_p2_dozer[0];
                    //update_x_p2b2 = becon_p2_dozer[1];
                    //update_x_p2b3 = becon_p2_dozer[2];

                    x_temp_est_p1b1 = A_value * x_est_last_p1b1;
                    x_temp_est_p1b2 = A_value * x_est_last_p1b2;
                    x_temp_est_p1b3 = A_value * x_est_last_p1b3;
                    x_temp_est_p1b4 = A_value * x_est_last_p1b4;//비컨이 4개로 인한

                    x_temp_est_p2b1 = A_value * x_est_last_p2b1;
                    x_temp_est_p2b2 = A_value * x_est_last_p2b2;
                    x_temp_est_p2b3 = A_value * x_est_last_p2b3;
                    x_temp_est_p2b4 = A_value * x_est_last_p2b4;//비컨이 4개로 인한

                    P_temp = A_value * P_last * (1 / A_value) + Q;

                    //#3(IMU)
                    //predicton_x_p1b1 = predicton_x_p1b1 + dt * (becon_p1_dozer[0] - x_bias_p1b1);
                    //predicton_x_p1b2 = predicton_x_p1b2 + dt * (becon_p1_dozer[1] - x_bias_p1b2);
                    //predicton_x_p1b3 = predicton_x_p1b3 + dt * (becon_p1_dozer[2] - x_bias_p1b3);
                    //predicton_x_p2b1 = predicton_x_p2b1 + dt * (becon_p2_dozer[0] - x_bias_p2b1);
                    //predicton_x_p2b2 = predicton_x_p2b2 + dt * (becon_p2_dozer[1] - x_bias_p2b2);
                    //predicton_x_p2b3 = predicton_x_p2b3 + dt * (becon_p2_dozer[2] - x_bias_p2b3);

                    ////x_bias_p1b1 += 0.014; 
                    ////x_bias_p1b2 += 0.014;
                    ////x_bias_p1b3 += 0.014;
                    ////x_bias_p2b1 += 0.014;
                    ////x_bias_p2b2 += 0.014;
                    ////x_bias_p2b3 += 0.014;

                    //p_00 = p_00 - dt * (p_10 + p_01) + Math.Pow(dt, 2) * p_11 + predicton_noise_covariance;
                    //p_01 = p_01 - dt * p_11;
                    //p_10 = p_10 - dt * p_11;
                    //p_11 = p_11 + predicton_noise_covariance;

                    //---------------------------------------------------------------------------------------------------------------------------------------------update
                    ////평균값 계산 적용시
                    //if (time2_constant_number_1 < 32)
                    //{
                    //    becon_p1_dozer[0] = kf_value_1[0];
                    //    becon_p1_dozer[1] = kf_value_1[1];
                    //    becon_p1_dozer[2] = kf_value_1[2];
                    //    becon_p1_dozer[3] = kf_value_1[3];

                    //    becon_p2_dozer[0] = kf_value_1[4];
                    //    becon_p2_dozer[1] = kf_value_1[5];
                    //    becon_p2_dozer[2] = kf_value_1[6];
                    //    becon_p2_dozer[3] = kf_value_1[7];
                    //}

                    //#1, 2
                    K = P_temp * (1 / H_value) * (1 / (H_value * P_temp * (1 / H_value) + R));

                    //update_x_p1b1 = predicton_x_p1b1 + kalman_gain * (kf_value_1[0] + std_dev[0] - predicton_x_p1b1);
                    x_est_p1b1 = x_temp_est_p1b1 + K * (becon_p1_dozer[0] - H_value * x_temp_est_p1b1);
                    //v_value_p1b1 = update_x_p1b1 - update_befor_x_p1b1;

                    //update_x_p1b2 = predicton_x_p1b2 + kalman_gain * (kf_value_1[1] + std_dev[1] - predicton_x_p1b2);
                    x_est_p1b2 = x_temp_est_p1b2 + K * (becon_p1_dozer[1] - H_value * x_temp_est_p1b2);
                    //v_value_p1b2 = update_x_p1b2 - update_befor_x_p1b2;

                    //update_x_p1b3 = predicton_x_p1b3 + kalman_gain * (kf_value_1[2] + std_dev[2] - predicton_x_p1b3);
                    x_est_p1b3 = x_temp_est_p1b3 + K * (becon_p1_dozer[2] - H_value * x_temp_est_p1b3);
                    //v_value_p1b3 = update_x_p1b3 - update_befor_x_p1b3;

                    x_est_p1b4 = x_temp_est_p1b4 + K * (becon_p1_dozer[3] - H_value * x_temp_est_p1b4);//비컨이 4개로 인한

                    //-----------------------------------------------------------------


                    //update_x_p2b1 = predicton_x_p2b1 + kalman_gain * (kf_value_1[3] + std_dev[3] - predicton_x_p2b1);
                    x_est_p2b1 = x_temp_est_p2b1 + K * (becon_p2_dozer[0] - H_value * x_temp_est_p2b1);
                    // v_value_p2b1 = update_x_p2b1 - update_befor_x_p2b1;

                    //update_x_p2b2 = predicton_x_p2b2 + kalman_gain * (kf_value_1[4] + std_dev[4] - predicton_x_p2b2);
                    x_est_p2b2 = x_temp_est_p2b2 + K * (becon_p2_dozer[1] - H_value * x_temp_est_p2b2);
                    // v_value_p2b2 = update_x_p2b2 - update_befor_x_p2b2;

                    //update_x_p2b3 = predicton_x_p2b3 + kalman_gain * (kf_value_1[5] + std_dev[5] - predicton_x_p2b3);
                    x_est_p2b3 = x_temp_est_p2b3 + K * (becon_p2_dozer[2] - H_value * x_temp_est_p2b3);
                    // v_value_p2b3 = update_x_p2b3 - update_befor_x_p2b3;

                    x_est_p2b4 = x_temp_est_p2b4 + K * (becon_p2_dozer[3] - H_value * x_temp_est_p2b4);//비컨이 4개로 인한


                    P = P_temp * (1 - K * H_value);

                    P_last = P;
                    x_est_last_p1b1 = x_est_p1b1;
                    x_est_last_p1b2 = x_est_p1b2;
                    x_est_last_p1b3 = x_est_p1b3;
                    x_est_last_p1b4 = x_est_p1b4;//비컨이 4개로 인한
                                                 //----------------------


                    x_est_last_p2b1 = x_est_p2b1;
                    x_est_last_p2b2 = x_est_p2b2;
                    x_est_last_p2b3 = x_est_p2b3;
                    x_est_last_p2b4 = x_est_p2b4;//비컨이 4개로 인한


                    //#3(I.M.U.)
                    //y_value_p1b1 = becon_p1_dozer[0] - predicton_x_p1b1;
                    //y_value_p1b2 = becon_p1_dozer[1] - predicton_x_p1b2;
                    //y_value_p1b3 = becon_p1_dozer[2] - predicton_x_p1b3;
                    //y_value_p2b1 = becon_p2_dozer[0] - predicton_x_p2b1;
                    //y_value_p2b2 = becon_p2_dozer[1] - predicton_x_p2b2;
                    //y_value_p2b3 = becon_p2_dozer[2] - predicton_x_p2b3;

                    //s_value = p_00 + measurement_noise_covariance;

                    //predicton_x_p1b1 = predicton_x_p1b1 + p_00 * (y_value_p1b1 / s_value);
                    //predicton_x_p1b2 = predicton_x_p1b2 + p_00 * (y_value_p1b2 / s_value);
                    //predicton_x_p1b3 = predicton_x_p1b3 + p_00 * (y_value_p1b3 / s_value);
                    //predicton_x_p2b1 = predicton_x_p2b1 + p_00 * (y_value_p2b1 / s_value);
                    //predicton_x_p2b2 = predicton_x_p2b2 + p_00 * (y_value_p2b2 / s_value);
                    //predicton_x_p2b3 = predicton_x_p2b3 + p_00 * (y_value_p2b3 / s_value);

                    //x_bias_p1b1 = x_bias_p1b1 + p_10 * (y_value_p1b1 / s_value);
                    //x_bias_p1b2 = x_bias_p1b2 + p_10 * (y_value_p1b2 / s_value);
                    //x_bias_p1b3 = x_bias_p1b3 + p_10 * (y_value_p1b3 / s_value);
                    //x_bias_p2b1 = x_bias_p2b1 + p_10 * (y_value_p2b1 / s_value);
                    //x_bias_p2b2 = x_bias_p2b2 + p_10 * (y_value_p2b2 / s_value);
                    //x_bias_p2b3 = x_bias_p2b3 + p_10 * (y_value_p2b3 / s_value);

                    //p_00 = p_00 - Math.Pow(p_00, 2) * (1 / s_value);
                    //p_01 = p_01 - p_00 * p_01 * (1 / s_value);
                    //p_10 = p_10 - p_10 * p_00 * (1 / s_value);
                    //p_11 = p_11 - p_10 * p_01 * (1 / s_value);


                    //계산편의상 정수로 표현
                    becon_p1_dozer[0] = Math.Round(x_est_p1b1);
                    becon_p1_dozer[1] = Math.Round(x_est_p1b2);
                    becon_p1_dozer[2] = Math.Round(x_est_p1b3);
                    becon_p1_dozer[3] = Math.Round(x_est_p1b4);//비컨이 4개로 인한


                    becon_p2_dozer[0] = Math.Round(x_est_p2b1);
                    becon_p2_dozer[1] = Math.Round(x_est_p2b2);
                    becon_p2_dozer[2] = Math.Round(x_est_p2b3);
                    becon_p2_dozer[3] = Math.Round(x_est_p2b4);//비컨이 4개로 인한


                    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " dP1k: " + becon_p1_dozer[0] + "  " + becon_p1_dozer[1] + "  " + becon_p1_dozer[2] + "  " + becon_p1_dozer[3]
                             + " dP2k: " + becon_p2_dozer[0] + "  " + becon_p2_dozer[1] + "  " + becon_p2_dozer[2] + "  " + becon_p2_dozer[3] + " 칼만필터 거리값 ");//칼만필터 통과후 거리값

                    kalman_after = 1;//칼만통과 재확인용

                    dozer_data = Calc_dozer_point(becon_p1_dozer, becon_p2_dozer, c_becon_p1_dozer, c_becon_p2_dozer);//칼만필터 통과한 값만 계산적용을 위한 수정

                    kalman_after = 0;//칼만통과 재확인용

                    //최종거리값 표현을 위한 부분
                    f_becon_p1_dozer[0] = becon_p1_dozer[0];
                    f_becon_p1_dozer[1] = becon_p1_dozer[1];
                    f_becon_p1_dozer[2] = becon_p1_dozer[2];
                    f_becon_p1_dozer[3] = becon_p1_dozer[3];

                    f_becon_p2_dozer[0] = becon_p2_dozer[0];
                    f_becon_p2_dozer[1] = becon_p2_dozer[1];
                    f_becon_p2_dozer[2] = becon_p2_dozer[2];
                    f_becon_p2_dozer[3] = becon_p2_dozer[3];

                }

                Debug.WriteLine(" c: " + debug_count + " Q: " + Q + " 칼만필터와 비교후 근접: " + stop_signal + " 수신기 좌표이동 정상화: " + stabilization_position_cut_save + " 사잇거리 정상화: " + const_becon_p_dozer_a + " 커팅 정상화: " + no_cut_save + " 안정화된 후 칼만 정상화: " + k_a_signal);


            }


            //------------------------------------------------------거리값 검사-----------------------------------------------------------
            DozerData.Receiver.P1.DistanceB1 = Convert.ToString(Math.Round(f_becon_p1_dozer[0]));
            DozerData.Receiver.P1.DistanceB2 = Convert.ToString(Math.Round(f_becon_p1_dozer[1]));
            DozerData.Receiver.P1.DistanceB3 = Convert.ToString(Math.Round(f_becon_p1_dozer[2]));
            DozerData.Receiver.P1.DistanceB4 = Convert.ToString(Math.Round(f_becon_p1_dozer[3]));//비컨이 4개로 인한
                                                                          
            DozerData.Receiver.P2.DistanceB1 = Convert.ToString(Math.Round(f_becon_p2_dozer[0]));
            DozerData.Receiver.P2.DistanceB2 = Convert.ToString(Math.Round(f_becon_p2_dozer[1]));
            DozerData.Receiver.P2.DistanceB3 = Convert.ToString(Math.Round(f_becon_p2_dozer[2]));
            DozerData.Receiver.P2.DistanceB4 = Convert.ToString(Math.Round(f_becon_p2_dozer[3]));//비컨이 4개로 인한
            //------------------------------------------------------------------------------------------------------------------------------
            




            /////////////////////////상대 좌표계/////////////////////////

            //비콘1 좌표
            DozerData.RelativeCoordinate.Beacon1.X = Convert.ToString(dozer_data[24]);
            DozerData.RelativeCoordinate.Beacon1.Y = Convert.ToString(dozer_data[25]);
            DozerData.RelativeCoordinate.Beacon1.Z = Convert.ToString(dozer_data[26]) + " (" + Convert.ToString(dozer_data[84]) + ")";//비컨과 수신기 사이각으로 인한 수정된 부분(0709)

            //비콘2 좌표
            DozerData.RelativeCoordinate.Beacon2.X = Convert.ToString(dozer_data[27]);
            DozerData.RelativeCoordinate.Beacon2.Y = Convert.ToString(dozer_data[28]);
            DozerData.RelativeCoordinate.Beacon2.Z = Convert.ToString(dozer_data[29]) + " (" + Convert.ToString(dozer_data[85]) + ")";//비컨과 수신기 사이각으로 인한 수정된 부분(0709)

            //비콘3 좌표
            DozerData.RelativeCoordinate.Beacon3.X = Convert.ToString(dozer_data[30]);
            DozerData.RelativeCoordinate.Beacon3.Y = Convert.ToString(dozer_data[31]);
            DozerData.RelativeCoordinate.Beacon3.Z = Convert.ToString(dozer_data[32]) + " (" + Convert.ToString(dozer_data[86]) + ")";//비컨과 수신기 사이각으로 인한 수정된 부분(0709)

            //비콘4 좌표 - 비컨이 4개로 인한 추가된 부분(0517)
            DozerData.RelativeCoordinate.Beacon4.X = Convert.ToString(dozer_data[76]);
            DozerData.RelativeCoordinate.Beacon4.Y = Convert.ToString(dozer_data[77]);
            DozerData.RelativeCoordinate.Beacon4.Z = Convert.ToString(dozer_data[78]) + " (" + Convert.ToString(dozer_data[87]) + ")";//비컨과 수신기 사이각으로 인한 수정된 부분(0709)


            //측점1 좌표
            DozerData.RelativeCoordinate.MeasuringPoint1.X = "0";
            DozerData.RelativeCoordinate.MeasuringPoint1.Y = "0";
            DozerData.RelativeCoordinate.MeasuringPoint1.Z = "0";

            //측점2 좌표
            DozerData.RelativeCoordinate.MeasuringPoint2.X = Convert.ToString(dozer_data[45]);
            DozerData.RelativeCoordinate.MeasuringPoint2.Y = Convert.ToString(dozer_data[46]);
            DozerData.RelativeCoordinate.MeasuringPoint2.Z = Convert.ToString(dozer_data[47]);

            /*
            //측점3 좌표
            DozerData.RelativeCoordinate.MeasuringPoint3.X = Convert.ToString(dozer_data[48]);
            DozerData.RelativeCoordinate.MeasuringPoint3.Y = Convert.ToString(dozer_data[49]);
            DozerData.RelativeCoordinate.MeasuringPoint3.Z = Convert.ToString(dozer_data[50]);*/

            //중심 좌표(e)
            DozerData.RelativeCoordinate.Center.X = Convert.ToString(dozer_data[51]);
            DozerData.RelativeCoordinate.Center.Y = Convert.ToString(dozer_data[52]);
            DozerData.RelativeCoordinate.Center.Z = Convert.ToString(dozer_data[53]);

            //orientation(e)
            DozerData.UnityOrientationBody = Convert.ToString(dozer_data[54]);
            DozerData.UnityOrientationFront = Convert.ToString(dozer_data[88]);//수정된 부분(0717)
            DozerData.UnityOrientationBlade = Convert.ToString(dozer_data[75]);

            //힌지 좌표(e)
            DozerData.RelativeCoordinate.Hinge.X = Convert.ToString(dozer_data[55]);
            DozerData.RelativeCoordinate.Hinge.Y = Convert.ToString(dozer_data[56]);
            DozerData.RelativeCoordinate.Hinge.Z = Convert.ToString(dozer_data[57]);

            //front_p
            DozerData.RelativeCoordinate.BladeCenter.X = Convert.ToString(dozer_data[67]);
            DozerData.RelativeCoordinate.BladeCenter.Y = Convert.ToString(dozer_data[68]);
            DozerData.RelativeCoordinate.BladeCenter.Z = Convert.ToString(dozer_data[69]);

            /////////////////////////절대 좌표계(e)/////////////////////////
            //중심 좌표
            DozerData.AbsoluteCoordinate.Center.X = Convert.ToString(dozer_data[39]);
            DozerData.AbsoluteCoordinate.Center.Y = Convert.ToString(dozer_data[40]);
            DozerData.AbsoluteCoordinate.Center.Z = Convert.ToString(dozer_data[41]);

            //힌지 좌표
            DozerData.AbsoluteCoordinate.Hinge.X = Convert.ToString(dozer_data[42]);
            DozerData.AbsoluteCoordinate.Hinge.Y = Convert.ToString(dozer_data[43]);
            DozerData.AbsoluteCoordinate.Hinge.Z = Convert.ToString(dozer_data[44]);

            //front_p
            DozerData.AbsoluteCoordinate.BladeCenter.X = Convert.ToString(dozer_data[64]);
            DozerData.AbsoluteCoordinate.BladeCenter.Y = Convert.ToString(dozer_data[65]);
            DozerData.AbsoluteCoordinate.BladeCenter.Z = Convert.ToString(dozer_data[66]);

            //////////////////////////장비의 회전방향/////////////////////////
            //몸체
            DozerData.IMUSensor.Body.X = Convert.ToString(-dozer_data[59]) + "(" + Convert.ToString(dozer_data[58]) + ")";
            DozerData.IMUSensor.Body.Y = Convert.ToString(dozer_data[58])  + "(" + Convert.ToString(dozer_data[59]) + ")";
            DozerData.IMUSensor.Body.Z = Convert.ToString(dozer_data[60]);

            //블레이드
            DozerData.IMUSensor.Blade.X = Convert.ToString(dozer_data[61]);
            DozerData.IMUSensor.Blade.Y = Convert.ToString(dozer_data[62]);
            DozerData.IMUSensor.Blade.Z = Convert.ToString(dozer_data[63]);
            DozerData.IMUSensor.Blade.Z2 = Convert.ToString(dozer_data[73]);

            //프론트 - 유니버셜 적용시 수정된 부분(0625)
            DozerData.IMUSensor.Front.X = "(" + Convert.ToString(dozer_data[70]) + "/" + Convert.ToString(dozer_data[79]) + ")";
            DozerData.IMUSensor.Front.Y = "(" + Convert.ToString(dozer_data[71]) + "/" + Convert.ToString(dozer_data[80]) + ")";
            DozerData.IMUSensor.Front.Z = "(" + Convert.ToString(dozer_data[72]) + "/" + Convert.ToString(dozer_data[81]) + ")";

            ////프론트 - UI 적용시 수정된 부분(0717)
            //DozerData.IMUSensor.Front.X = Convert.ToString(dozer_data[82]);//평균값 적용된 롤 값
            //DozerData.IMUSensor.Front.Y = Convert.ToString(dozer_data[83]);//평균값 적용된 피치 값
            //DozerData.IMUSensor.Front.Z = Convert.ToString(dozer_data[88]);//평균값 적용된 오리엔테이션 값

            //DozerData.IMUSensor.FrontL.X = Convert.ToString(dozer_data[70]);//imu 추가로 인한
            //DozerData.IMUSensor.FrontL.Y = Convert.ToString(dozer_data[71]);
            //DozerData.IMUSensor.FrontL.Z = Convert.ToString(dozer_data[72]);

            //DozerData.IMUSensor.FrontR.X = Convert.ToString(dozer_data[79]);//imu 추가로 인한
            //DozerData.IMUSensor.FrontR.Y = Convert.ToString(dozer_data[80]);
            //DozerData.IMUSensor.FrontR.Z = Convert.ToString(dozer_data[81]);


            //상대 좌표계(끝단 좌표 값)
            DozerData.RelativePoint.EndPoint = "d_endpoint_1, " + dozer_data[18] + ", " + dozer_data[19] + ", " + dozer_data[20] + ", " + "d_endpoint_2, " + dozer_data[21] + ", " + dozer_data[22] + ", " + dozer_data[23];

            //상대 좌표계(위치 좌표 값)
            DozerData.RelativePoint.Position = "d_position, " + dozer_data[15] + ", " + dozer_data[16] + ", " + dozer_data[17];

            //상대 좌표계(수신기 좌표 값)
            if (p_calc_1 == 1 && p_calc_2 == 1)
            {
                DozerData.RelativePoint.Receiver = "d_receiver_1(c), " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "d_receiver_2(c), " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38];
                //Debug.WriteLine("e_receiver_1(c), " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "e_receiver_2(c), " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38]);
            }

            else if (p_calc_1 == 1 && p_calc_2 == 0)
            {
                DozerData.RelativePoint.Receiver = "d_receiver_1(c), " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "d_receiver_2, " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38];
                //Debug.WriteLine("e_receiver_1(c), " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "e_receiver_2, " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38]);

            }

            else if (p_calc_1 == 0 && p_calc_2 == 1)
            {
                DozerData.RelativePoint.Receiver = "d_receiver_1, " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "d_receiver_2(c), " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38];
                //Debug.WriteLine("e_receiver_1, " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "e_receiver_2(c), " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38]);

            }

            else
            {
                DozerData.RelativePoint.Receiver = "d_receiver_1, " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "d_receiver_2, " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38];
                //Debug.WriteLine("e_receiver_1, " + dozer_data[33] + ", " + dozer_data[34] + ", " + dozer_data[35] + ", " + "e_receiver_2, " + dozer_data[36] + ", " + dozer_data[37] + ", " + dozer_data[38]);

            }

            

            //절대 좌표계 (끝단 좌표 값)
            DozerData.AbsolutePoint.EndPoint = "d_endpoint_1, " + dozer_data[9] + ", " + dozer_data[10] + ", " + dozer_data[11] + ", " + "d_endpoint_2, " + dozer_data[12] + ", " + dozer_data[13] + ", " + dozer_data[14];

            //절대 좌표계 (위치 좌표 값)
            DozerData.AbsolutePoint.Position = "d_position, " + dozer_data[6] + ", " + dozer_data[7] + ", " + dozer_data[8];

            //절대 좌표계 (수신기 좌표 값)
            DozerData.AbsolutePoint.Receiver = "d_receiver_1, " + dozer_data[0] + ", " + dozer_data[1] + ", " + dozer_data[2] + ", " + "d_receiver_2, " + dozer_data[3] + ", " + dozer_data[4] + ", " + dozer_data[5];

            Debug.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        }

        //---------------------------------------------------------------- 수학 공식 부분-----------------------------------------------------------------

        //==========================상대 좌표계의 삼각 측량 계산부=================================

        double[] r_Calc_tri(double[] d_1, double[] d_2)
        {
            //-------비컨이 4개 사용시---------
            double[] d_1_c = new double[3];
            double[] d_2_c = new double[3];

            //전역변수로 변경에 따른 수정된 부분(0711)
            //double[] r_becon_1_c = new double[3];
            //double[] r_becon_2_c = new double[3];
            //double[] r_becon_3_c = new double[3];

            //큰수
            double p1_dN1 = 0;
            double p1_dN2 = 0;
            double p1_dN3 = 0;

            //수신기 평균 거리값
            double[] d_12 = new double[4];

            //-----------원본 계산 변수-------------
            double[] result = new double[20];

            double[] becon_1_1 = new double[3];
            double[] becon_2_1 = new double[3];
            double[] becon_3_1 = new double[3];

            double a_1 = 0, b_1 = 0, c_1 = 0, p_1_w_1 = 0, p_2_w_1 = 0;
            double a_2 = 0, b_2 = 0, c_2 = 0, p_1_w_2 = 0, p_2_w_2 = 0;

            double s_A_1 = 0, p_1_s_B_1 = 0, p_2_s_B_1 = 0;
            double s_A_2 = 0, p_1_s_B_2 = 0, p_2_s_B_2 = 0;

            double Aa = 0, p_1_Bb = 0, p_1_Cc = 0, p_2_Bb = 0, p_2_Cc = 0;

            double[] p_1_t_u = new double[3]; double[] p_2_t_u = new double[3];
            double[] p_1_t_d = new double[3]; double[] p_2_t_d = new double[3];

            //사용하지 않으므로 수정된 부분(0711)
            ////-----------오차 계산 변수(시작)-------------
            //double B1B2 = 0, B1B3 = 0, B2B3 = 0;//비콘 사이의 거리

            ////------수신기 1---------
            //double D1T_u = 0, D2T_u = 0;
            //double D1T_d = 0, D3T_u = 0;
            //double D2T_d = 0, D3T_d = 0;

            //double D1T_1 = 0, D2T_1, D3T_1 = 0;
            //double D1T_2 = 0, D2T_2, D3T_2 = 0;

            ////직선의 방정식
            //double A1 = 0, B1 = 0, C1 = 0;
            //double A2 = 0, B2 = 0, C2 = 0;
            //double A3 = 0, B3 = 0, C3 = 0;

            ////직선의 교점
            //double x1 = 0, y1 = 0;
            //double x2 = 0, y2 = 0;
            //double x3 = 0, y3 = 0;

            //double cx = 0, cy = 0;//무게중심
            ////--------------------------

            ////------수신기 2---------
            //double D1T_u_2 = 0, D2T_u_2 = 0;
            //double D1T_d_2 = 0, D3T_u_2 = 0;
            //double D2T_d_2 = 0, D3T_d_2 = 0;

            //double D1T_1_2 = 0, D2T_1_2, D3T_1_2 = 0;
            //double D1T_2_2 = 0, D2T_2_2, D3T_2_2 = 0;

            ////직선의 방정식
            //double A1_2 = 0, B1_2 = 0, C1_2 = 0;
            //double A2_2 = 0, B2_2 = 0, C2_2 = 0;
            //double A3_2 = 0, B3_2 = 0, C3_2 = 0;

            ////직선의 교점
            //double x1_2 = 0, y1_2 = 0;
            //double x2_2 = 0, y2_2 = 0;
            //double x3_2 = 0, y3_2 = 0;

            //double cx_2 = 0, cy_2 = 0;//무게중심
            // //-----------오차 계산 변수(끝)-------------



            //로우값 계산을 위힌
            double[] r_becon_1_m = new double[3];
            double[] r_becon_2_m = new double[3];
            double[] r_becon_3_m = new double[3];
            
            double d_1_c_0 = 0;
            double d_1_c_1 = 0;
            double d_1_c_2 = 0;

            double d_2_c_0 = 0;
            double d_2_c_1 = 0;
            double d_2_c_2 = 0;


            //----------비컨 각도 적용에 따른 추가된 부분(0711) - 시작-------------
            //각도가 가장 높은것과 나머지 비컨의 좌표를 위한 부분
            double beacon_2_calc_p1b0 = 0;
            double beacon_2_calc_p1b1 = 0;
            double beacon_2_calc_p1b2 = 0;
            double beacon_2_calc_p1b3 = 0;

            double beacon_2_calc_p2b0 = 0;
            double beacon_2_calc_p2b1 = 0;
            double beacon_2_calc_p2b2 = 0;
            double beacon_2_calc_p2b3 = 0;

            //평균 거리값 계산을 위한 부분
            double beacon_2_calc_a_b1 = 0;
            double beacon_2_calc_a_b2 = 0;
            double beacon_2_calc_a_b3 = 0;

            //가장 큰 평균 거리값 계산을 위한 부분
            double angle_dN1 = 0;
            double angle_dN2 = 0;

            //비컨 id 확인을 위한 부분
            Int16 b1_id = 0;
            Int16 b2_id = 0;
            Int16 b3_id = 0;
            //-------------------------------------------------------끝-------------


            //비컨 2개 결정 준비 확인부분 - 추가된 부분(0711)
            if (beacon_2_calc == 0)
            {

                //---------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘1 = (r_becon_1[0], r_becon_1[1], r_becon_1[2])
                //비콘2 = (r_becon_2[0], r_becon_2[1], r_becon_2[2])
                //비콘3 = (r_becon_3[0], r_becon_3[1], r_becon_3[2])
                //비콘4 = (r_becon_4[0], r_becon_4[1], r_becon_4[2])//비컨이 4개일 경우

                //------------계산에 사용되는 비컨값이 결정된후 비컨값 비컨이 4개일 경우
                //비콘1 = (r_becon_1_c[0], r_becon_1_c[1], r_becon_1_c[2])
                //비콘2 = (r_becon_2_c[0], r_becon_2_c[1], r_becon_2_c[2])
                //비콘3 = (r_becon_3_c[0], r_becon_3_c[1], r_becon_3_c[2])

                //-------------계산에 사용되는 거리값이 결정되기전 거리값
                //----수신기 1------
                //거리1 = d_1[0]
                //거리2 = d_1[1]
                //거리3 = d_1[2]
                //거리4 = d_1[3]//비컨이 4개일 경우

                //----수신기 2------
                //거리1 = d_2[0]
                //거리2 = d_2[1]
                //거리3 = d_2[2]
                //거리4 = d_2[3]//비컨이 4개일 경우

                //-----수신기 평균 거리값
                //거리1 = d_12[0]
                //거리2 = d_12[1]
                //거리3 = d_12[2]
                //거리4 = d_12[3]

                //--------------계산에 사용되는 거리값이 결정된후 거리값 비컨이 4개일 경우
                //----수신기 1------
                //거리1 = d_1_c[0]
                //거리2 = d_1_c[1]
                //거리3 = d_1_c[2]

                //----수신기 2------
                //거리1 = d_2_c[0]
                //거리2 = d_2_c[1]
                //거리3 = d_2_c[2]

                ////******************************************************************결정 코드 부분 비컨이 4개일경우*************************************************************



                //수신기의 평균 거리값 계산
                d_12[0] = (d_1[0] + d_2[0]) / 2;
                d_12[1] = (d_1[1] + d_2[1]) / 2;
                d_12[2] = (d_1[2] + d_2[2]) / 2;
                d_12[3] = (d_1[3] + d_2[3]) / 2;

                //거리값이 같아지는경우는 고려 안함
                //가장긴거리값 결정(dN3이 가장김)
                if (d_12[0] < d_12[1]) p1_dN1 = d_12[1];
                else p1_dN1 = d_12[0];

                if (p1_dN1 < d_12[2]) p1_dN2 = d_12[2];
                else p1_dN2 = p1_dN1;

                if (p1_dN2 < d_12[3]) p1_dN3 = d_12[3];
                else p1_dN3 = p1_dN2;

                //Debug.WriteLine(" P_d : " + p1_dN3);

                //계산되는 비컨과 거리값 결정
                if (p1_dN3 == d_12[0])
                {
                    d_1_c[0] = d_1[1];
                    d_1_c[1] = d_1[2];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[1];
                    d_2_c[1] = d_2[2];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_2[0];
                    r_becon_1_c[1] = r_becon_2[1];
                    r_becon_1_c[2] = r_becon_2[2];

                    r_becon_2_c[0] = r_becon_3[0];
                    r_becon_2_c[1] = r_becon_3[1];
                    r_becon_2_c[2] = r_becon_3[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2) Debug.WriteLine(" B2  B3  B4 ");

                }

                else if (p1_dN3 == d_12[1])
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[2];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[2];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_3[0];
                    r_becon_2_c[1] = r_becon_3[1];
                    r_becon_2_c[2] = r_becon_3[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2) Debug.WriteLine(" B1  B3  B4 ");
                }

                else if (p1_dN3 == d_12[2])
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[1];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[1];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_2[0];
                    r_becon_2_c[1] = r_becon_2[1];
                    r_becon_2_c[2] = r_becon_2[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2) Debug.WriteLine(" B1  B2  B4 ");
                }

                else
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[1];
                    d_1_c[2] = d_1[2];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[1];
                    d_2_c[2] = d_2[2];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_2[0];
                    r_becon_2_c[1] = r_becon_2[1];
                    r_becon_2_c[2] = r_becon_2[2];

                    r_becon_3_c[0] = r_becon_3[0];
                    r_becon_3_c[1] = r_becon_3[1];
                    r_becon_3_c[2] = r_becon_3[2];

                    if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2) Debug.WriteLine(" B1  B2  B3 ");
                }
            }


            //-------------비컨 각도 적용에 따른 계산부----------------
            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 1)
            {
                //Debug.WriteLine(" 111111111111111111111111111111111111 ");

                //------------------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘0 = (angle_s_b0_x, angle_s_b0_y, angle_s_b0_z)
                //비콘1 = (angle_s_b1_x, angle_s_b1_y, angle_s_b1_z)
                //비콘2 = (angle_s_b2_x, angle_s_b2_y, angle_s_b2_z)
                //비콘3 = (angle_s_b3_x, angle_s_b3_y, angle_s_b3_z)

                //------------계산에 사용되는 비컨값이 결정된후 비컨값
                //비콘1 = (r_becon_1_c[0], r_becon_1_c[1], r_becon_1_c[2])
                //비콘2 = (r_becon_2_c[0], r_becon_2_c[1], r_becon_2_c[2])
                //비콘3 = (r_becon_3_c[0], r_becon_3_c[1], r_becon_3_c[2]) <----- 무조건 비컨0 부분

                ////-------------계산에 사용되는 거리값이 결정되기전 거리값
                //비컨번호를 확인해서 해당되는 거리값 선택(각각 비컨 1, 2, 3, 4 사이의 거리값임)
                //----수신기 1------
                //거리1 = d_1[0]
                //거리2 = d_1[1]
                //거리3 = d_1[2]
                //거리4 = d_1[3]

                //----수신기 2------
                //거리1 = d_2[0]
                //거리2 = d_2[1]
                //거리3 = d_2[2]
                //거리4 = d_2[3]

                //비컨번호(예를 들어서, s_beacon_0 = 1 일 경우 거리1 은 각각 d_1[0], d_2[0] 가 된다.)
                //s_beacon_0
                //s_beacon_1
                //s_beacon_2
                //s_beacon_3

                //--------------계산에 사용되는 거리값이 결정된후 거리값
                //----수신기 1------
                //거리1 = d_1_c[0]
                //거리2 = d_1_c[1]
                //거리3 = d_1_c[2]    <-------- 무조건 비컨0과 수신기1번 사이의 거리

                //----수신기 2------
                //거리1 = d_2_c[0]
                //거리2 = d_2_c[1]
                //거리3 = d_2_c[2]    <-------- 무조건 비컨0과 수신기2번 사이의 거리



                //-------------------------------------거리값 계산 부분--------------------------------------
                //------비컨0에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b0)
                if (s_beacon_0 == 1) beacon_2_calc_p1b0 = d_1[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p1b0 = d_1[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p1b0 = d_1[2];
                else beacon_2_calc_p1b0 = d_1[3];

                //p2(beacon_2_calc_p2b0)
                if (s_beacon_0 == 1) beacon_2_calc_p2b0 = d_2[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p2b0 = d_2[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p2b0 = d_2[2];
                else beacon_2_calc_p2b0 = d_2[3];

                //--------비컨1에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b1)
                if (s_beacon_1 == 1) beacon_2_calc_p1b1 = d_1[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p1b1 = d_1[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p1b1 = d_1[2];
                else beacon_2_calc_p1b1 = d_1[3];

                //p2(beacon_2_calc_p2b1)
                if (s_beacon_1 == 1) beacon_2_calc_p2b1 = d_2[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p2b1 = d_2[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p2b1 = d_2[2];
                else beacon_2_calc_p2b1 = d_2[3];

                //---------비컨2에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b2)
                if (s_beacon_2 == 1) beacon_2_calc_p1b2 = d_1[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p1b2 = d_1[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p1b2 = d_1[2];
                else beacon_2_calc_p1b2 = d_1[3];

                //p2(beacon_2_calc_p2b2)
                if (s_beacon_2 == 1) beacon_2_calc_p2b2 = d_2[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p2b2 = d_2[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p2b2 = d_2[2];
                else beacon_2_calc_p2b2 = d_2[3];

                //---------비컨3에 관련된 거리값 계산 - ID 오류로 인한 수정
                //p1(beacon_2_calc_p1b2)
                if (s_beacon_3 == 1) beacon_2_calc_p1b3 = d_1[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p1b3 = d_1[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p1b3 = d_1[2];
                else beacon_2_calc_p1b3 = d_1[3];

                //p2(beacon_2_calc_p2b2)
                if (s_beacon_3 == 1) beacon_2_calc_p2b3 = d_2[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p2b3 = d_2[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p2b3 = d_2[2];
                else beacon_2_calc_p2b3 = d_2[3];


                //비컨1, 2, 3을 가지고 계산을 해서 결정된 비컨1, 2가되고 비컨0은 결정된 비컨 3이 됨 
                //수신기의 평균 거리값 계산
                beacon_2_calc_a_b1 = (beacon_2_calc_p1b1 + beacon_2_calc_p2b1) / 2;
                beacon_2_calc_a_b2 = (beacon_2_calc_p1b2 + beacon_2_calc_p2b2) / 2;
                beacon_2_calc_a_b3 = (beacon_2_calc_p1b3 + beacon_2_calc_p2b3) / 2;

                //가장 큰 평균 거리값 결정 부분
                if (beacon_2_calc_a_b1 < beacon_2_calc_a_b2) angle_dN1 = beacon_2_calc_a_b2;
                else angle_dN1 = beacon_2_calc_a_b1;

                if (angle_dN1 < beacon_2_calc_a_b3) angle_dN2 = beacon_2_calc_a_b3;
                else angle_dN2 = angle_dN1;


                //비컨 2개의 거리값과 좌표값 결정

                //------거리값 결정
                //p1(d_1_c[0])_b1
                if (angle_dN2 == beacon_2_calc_a_b1) d_1_c[0] = beacon_2_calc_p1b2;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_1_c[0] = beacon_2_calc_p1b1;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_1_c[0] = beacon_2_calc_p1b1;
                else d_1_c[0] = beacon_2_calc_p1b1;

                //p1(d_1_c[1])_b2
                if (angle_dN2 == beacon_2_calc_a_b1) d_1_c[1] = beacon_2_calc_p1b3;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_1_c[1] = beacon_2_calc_p1b3;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_1_c[1] = beacon_2_calc_p1b2;
                else d_1_c[1] = beacon_2_calc_p1b2;

                //p2(d_1_c[2])_b3
                d_1_c[2] = beacon_2_calc_p1b0;

                //p2(d_2_c[0])_b1
                if (angle_dN2 == beacon_2_calc_a_b1) d_2_c[0] = beacon_2_calc_p2b2;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_2_c[0] = beacon_2_calc_p2b1;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_2_c[0] = beacon_2_calc_p2b1;
                else d_2_c[0] = beacon_2_calc_p2b1;

                //p2(d_2_c[1])_b2
                if (angle_dN2 == beacon_2_calc_a_b1) d_2_c[1] = beacon_2_calc_p2b3;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_2_c[1] = beacon_2_calc_p2b3;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_2_c[1] = beacon_2_calc_p2b2;
                else d_2_c[1] = beacon_2_calc_p2b2;

                //p2(d_2_c[2])_b3
                d_2_c[2] = beacon_2_calc_p2b0;



                //-------좌표값 결정
                //----------b1
                //x
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[0] = angle_s_b2_x;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[0] = angle_s_b1_x;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[0] = angle_s_b1_x;
                else r_becon_1_c[0] = angle_s_b1_x;

                //y
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[1] = angle_s_b2_y;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[1] = angle_s_b1_y;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[1] = angle_s_b1_y;
                else r_becon_1_c[1] = angle_s_b1_y;

                //z
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[2] = angle_s_b2_z;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[2] = angle_s_b1_z;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[2] = angle_s_b1_z;
                else r_becon_1_c[2] = angle_s_b1_z;

                //------------b2
                //x
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[0] = angle_s_b3_x;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[0] = angle_s_b3_x;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[0] = angle_s_b2_x;
                else r_becon_2_c[0] = angle_s_b2_x;

                // Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> angle_s_b3_x " + angle_s_b3_x + " angle_s_b2_x " + angle_s_b2_x);
                //  Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> r_becon_2_c[0] " + r_becon_2_c[0]);

                //y
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[1] = angle_s_b3_y;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[1] = angle_s_b3_y;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[1] = angle_s_b2_y;
                else r_becon_2_c[1] = angle_s_b2_y;

                //z
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[2] = angle_s_b3_z;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[2] = angle_s_b3_z;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[2] = angle_s_b2_z;
                else r_becon_2_c[2] = angle_s_b2_z;

                //-----------------b3
                r_becon_3_c[0] = angle_s_b0_x;
                r_becon_3_c[1] = angle_s_b0_y;
                r_becon_3_c[2] = angle_s_b0_z;

                //좌표값 결정에 대한 비컨 id 표현 - 수식 오류로 인한 수정
                //b1에 대한 id
                if (angle_s_b1_x == r_becon_1_c[0] && angle_s_b1_y == r_becon_1_c[1] && angle_s_b1_z == r_becon_1_c[2]) b1_id = s_beacon_1;
                else b1_id = s_beacon_2;

                //b2에 대한 id
                if (angle_s_b2_x == r_becon_2_c[0] && angle_s_b2_y == r_becon_2_c[1] && angle_s_b2_z == r_becon_2_c[2]) b2_id = s_beacon_2;
                else b2_id = s_beacon_3;

                //b3에 대한 id
                b3_id = s_beacon_0;

                // beacon_2_calc = 0;

                //비컨 id 출력
                //if (kalman_after == 1) Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> s_beacon_0 " + s_beacon_0 + " s_beacon_1 " + s_beacon_1 + " s_beacon_2 " + s_beacon_2 + " s_beacon_3 " + s_beacon_3);




                //처음 값이 들어온경우 값 저장 부분(이 부분은 계산도중 비컨의 좌표가 바뀌는 것을 방지하기 위함이다) - 추가된 부분(0711) 
                if (first_const == 1)
                {


                    //좌표값 저장
                    save_b1_x = r_becon_1_c[0];
                    save_b1_y = r_becon_1_c[1];
                    save_b1_z = r_becon_1_c[2];

                    save_b2_x = r_becon_2_c[0];
                    save_b2_y = r_becon_2_c[1];
                    save_b2_z = r_becon_2_c[2];

                    save_b3_x = r_becon_3_c[0];
                    save_b3_y = r_becon_3_c[1];
                    save_b3_z = r_becon_3_c[2];

                    //비컨 아이디 저장
                    save_id_b1 = b1_id;
                    save_id_b2 = b2_id;
                    save_id_b3 = b3_id;

                    ////해당되는 거리값 저장 - 저장하면 필터가 무의미 하므로 주석
                    ////pb1
                    //if (b1_id == 1)
                    //{
                    //    save_p1b1 = d_1[0];
                    //    save_p2b1 = d_2[0];
                    //}
                    //else if (b1_id == 2)
                    //{
                    //    save_p1b1 = d_1[1];
                    //    save_p2b1 = d_2[1];
                    //}
                    //else if (b1_id == 3)
                    //{
                    //    save_p1b1 = d_1[2];
                    //    save_p2b1 = d_2[2];
                    //}

                    ////pb2
                    //if (b2_id == 2)
                    //{
                    //    save_p1b2 = d_1[1];
                    //    save_p2b2 = d_2[1];
                    //}
                    //else if (b2_id == 3)
                    //{
                    //    save_p1b2 = d_1[2];
                    //    save_p2b2 = d_2[2];
                    //}
                    //else if (b2_id == 4)
                    //{
                    //    save_p1b2 = d_1[3];
                    //    save_p2b2 = d_2[3];
                    //}

                    ////pb3
                    //if (b3_id == 1)
                    //{
                    //    save_p1b3 = d_1[0];
                    //    save_p2b3 = d_2[0];
                    //}
                    //else if (b3_id == 2)
                    //{
                    //    save_p1b3 = d_1[1];
                    //    save_p2b3 = d_2[1];
                    //}
                    //else if (b3_id == 3)
                    //{
                    //    save_p1b3 = d_1[2];
                    //    save_p2b3 = d_2[2];
                    //}
                    //else if (b3_id == 4)
                    //{
                    //    save_p1b3 = d_1[3];
                    //    save_p2b3 = d_2[3];
                    //}





                    ////거리값 출력
                    //d_1_c[0] = save_p1b1;
                    //d_1_c[1] = save_p1b2;
                    //d_1_c[2] = save_p1b3;

                    //d_2_c[0] = save_p2b1;
                    //d_2_c[1] = save_p2b2;
                    //d_2_c[2] = save_p2b3;

                    ////이전 거리값을 사용할경우 - 알고리즘 꼬이므로 주석
                    //if (check_calc == 1)
                    //{
                    //    ////거리 적용 - 오류거리값 방지를 위한 주석 처리
                    //    //d_1_c[0] = before_p1d1_save;
                    //    //d_1_c[1] = before_p1d2_save;
                    //    //d_1_c[2] = before_p1d3_save;

                    //    //d_2_c[0] = before_p2d1_save;
                    //    //d_2_c[1] = before_p2d2_save;
                    //    //d_2_c[2] = before_p2d3_save;

                    //    //해당되는 거리값 저장
                    //    //pb1
                    //    if (before_id_b1_save == 1)
                    //    {
                    //        save_p1b1_before = d_1[0];
                    //        save_p2b1_before = d_2[0];
                    //    }
                    //    else if (before_id_b1_save == 2)
                    //    {
                    //        save_p1b1_before = d_1[1];
                    //        save_p2b1_before = d_2[1];
                    //    }
                    //    else if (before_id_b1_save == 3)
                    //    {
                    //        save_p1b1_before = d_1[2];
                    //        save_p2b1_before = d_2[2];
                    //    }

                    //    //pb2
                    //    if (before_id_b2_save == 2)
                    //    {
                    //        save_p1b2_before = d_1[1];
                    //        save_p2b2_before = d_2[1];
                    //    }
                    //    else if (before_id_b2_save == 3)
                    //    {
                    //        save_p1b2_before = d_1[2];
                    //        save_p2b2_before = d_2[2];
                    //    }
                    //    else if (before_id_b2_save == 4)
                    //    {
                    //        save_p1b2_before = d_1[3];
                    //        save_p2b2_before = d_2[3];
                    //    }

                    //    //pb3
                    //    if (before_id_b3_save == 1)
                    //    {
                    //        save_p1b3_before = d_1[0];
                    //        save_p2b3_before = d_2[0];
                    //    }
                    //    else if (before_id_b3_save == 2)
                    //    {
                    //        save_p1b3_before = d_1[1];
                    //        save_p2b3_before = d_2[1];
                    //    }
                    //    else if (before_id_b3_save == 3)
                    //    {
                    //        save_p1b3_before = d_1[2];
                    //        save_p2b3_before = d_2[2];
                    //    }
                    //    else if (before_id_b3_save == 4)
                    //    {
                    //        save_p1b3_before = d_1[3];
                    //        save_p2b3_before = d_2[3];
                    //    }


                    //    //거리적용
                    //    d_1_c[0] = save_p1b1_before;
                    //    d_1_c[1] = save_p1b2_before;
                    //    d_1_c[2] = save_p1b3_before;

                    //    d_2_c[0] = save_p2b1_before;
                    //    d_2_c[1] = save_p2b2_before;
                    //    d_2_c[2] = save_p2b3_before;


                    //    //비컨 적용
                    //    r_becon_1_c[0] = before_b1x_save;
                    //    r_becon_1_c[1] = before_b1y_save;
                    //    r_becon_1_c[2] = before_b1z_save;

                    //    r_becon_2_c[0] = before_b2x_save;
                    //    r_becon_2_c[1] = before_b2y_save;
                    //    r_becon_2_c[2] = before_b2z_save;

                    //    r_becon_3_c[0] = before_b3x_save;
                    //    r_becon_3_c[1] = before_b3y_save;
                    //    r_becon_3_c[2] = before_b3z_save;

                    //    check_calc = 0;
                    //}

                    if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2)//보기
                    {
                        //사용된 비컨 출력부분 - 좌표값 표시로 인한 추가
                        Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_1 " + save_id_b1 + " x " + r_becon_1_c[0] + " y " + r_becon_1_c[1] + " z " + r_becon_1_c[2]);
                        Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_1 " + save_id_b2 + " x " + r_becon_2_c[0] + " y " + r_becon_2_c[1] + " z " + r_becon_2_c[2]);
                        Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_1 " + save_id_b3 + " x " + r_becon_3_c[0] + " y " + r_becon_3_c[1] + " z " + r_becon_3_c[2]);

                        //사용된 거리값 확인
                        Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_1 " + d_1_c[0] + " " + d_1_c[1] + " " + d_1_c[2] + " " + d_2_c[0] + " " + d_2_c[1] + " " + d_2_c[2]);
                    }

                }
                else if (first_const == 0)//처음이 아닐경우
                {
                    ////거리 저장된값 출력 - 저장시킨 거리값 사용시 필터가 무의미함 주석 처리
                    //d_1_c[0] = save_p1b1;
                    //d_1_c[1] = save_p1b2;
                    //d_1_c[2] = save_p1b3;

                    //d_2_c[0] = save_p2b1;
                    //d_2_c[1] = save_p2b2;
                    //d_2_c[2] = save_p2b3;

                    //거리값 결정
                    //pb1
                    if (save_id_b1 == 1)
                    {
                        save_p1b1 = d_1[0];
                        save_p2b1 = d_2[0];
                    }
                    else if (save_id_b1 == 2)
                    {
                        save_p1b1 = d_1[1];
                        save_p2b1 = d_2[1];
                    }
                    else if (save_id_b1 == 3)
                    {
                        save_p1b1 = d_1[2];
                        save_p2b1 = d_2[2];
                    }

                    //pb2
                    if (save_id_b2 == 2)
                    {
                        save_p1b2 = d_1[1];
                        save_p2b2 = d_2[1];
                    }
                    else if (save_id_b2 == 3)
                    {
                        save_p1b2 = d_1[2];
                        save_p2b2 = d_2[2];
                    }
                    else if (save_id_b2 == 4)
                    {
                        save_p1b2 = d_1[3];
                        save_p2b2 = d_2[3];
                    }

                    //pb3
                    if (save_id_b3 == 1)
                    {
                        save_p1b3 = d_1[0];
                        save_p2b3 = d_2[0];
                    }
                    else if (save_id_b3 == 2)
                    {
                        save_p1b3 = d_1[1];
                        save_p2b3 = d_2[1];
                    }
                    else if (save_id_b3 == 3)
                    {
                        save_p1b3 = d_1[2];
                        save_p2b3 = d_2[2];
                    }
                    else if (save_id_b3 == 4)
                    {
                        save_p1b3 = d_1[3];
                        save_p2b3 = d_2[3];
                    }

                    //해당 거리값 출력
                    d_1_c[0] = save_p1b1;
                    d_1_c[1] = save_p1b2;
                    d_1_c[2] = save_p1b3;

                    d_2_c[0] = save_p2b1;
                    d_2_c[1] = save_p2b2;
                    d_2_c[2] = save_p2b3;


                    //비컨 저장된값 출력
                    r_becon_1_c[0] = save_b1_x;
                    r_becon_1_c[1] = save_b1_y;
                    r_becon_1_c[2] = save_b1_z;

                    r_becon_2_c[0] = save_b2_x;
                    r_becon_2_c[1] = save_b2_y;
                    r_becon_2_c[2] = save_b2_z;

                    r_becon_3_c[0] = save_b3_x;
                    r_becon_3_c[1] = save_b3_y;
                    r_becon_3_c[2] = save_b3_z;



                    //if (before_d_value == 1)////이전거리 사용을 위한 저장 - 알고리즘 꼬이므로 주석
                    //{
                    //    //수식수정에 따른 주석처리 - 이전 거리를 이용하면 무의미 하므로 수정
                    //    //before_p1d1_save = d_1_c[0];
                    //    //before_p1d2_save = d_1_c[1];
                    //    //before_p1d3_save = d_1_c[2];

                    //    //before_p2d1_save = d_2_c[0];
                    //    //before_p2d2_save = d_2_c[1];
                    //    //before_p2d3_save = d_2_c[2];

                    //    //비컨 아이디 저장
                    //    before_id_b1_save = save_id_b1;
                    //    before_id_b2_save = save_id_b2;
                    //    before_id_b3_save = save_id_b3;

                    //    //이전비컨 사용을 위한 저장
                    //    before_b1x_save = r_becon_1_c[0];
                    //    before_b1y_save = r_becon_1_c[1];
                    //    before_b1z_save = r_becon_1_c[2];

                    //    before_b2x_save = r_becon_2_c[0];
                    //    before_b2y_save = r_becon_2_c[1];
                    //    before_b2z_save = r_becon_2_c[2];

                    //    before_b3x_save = r_becon_3_c[0];
                    //    before_b3y_save = r_becon_3_c[1];
                    //    before_b3z_save = r_becon_3_c[2];

                    //    before_d_value = 0;
                    //}

                    //if (const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2)//보기
                    //{
                    //    //사용된 비컨 출력부분 - 좌표값 표시로 인한 추가
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_2 " + save_id_b1 + " x " + r_becon_1_c[0] + " y " + r_becon_1_c[1] + " z " + r_becon_1_c[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_2 " + save_id_b2 + " x " + r_becon_2_c[0] + " y " + r_becon_2_c[1] + " z " + r_becon_2_c[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_2 " + save_id_b3 + " x " + r_becon_3_c[0] + " y " + r_becon_3_c[1] + " z " + r_becon_3_c[2]);

                    //    //사용된 거리값 확인                    
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_2 " + d_1_c[0] + " " + d_1_c[1] + " " + d_1_c[2] + " " + d_2_c[0] + " " + d_2_c[1] + " " + d_2_c[2]);
                    //}

                }

            }



            ////******************************************************************다운된 거리값 보정*************************************************************
            //----변환 부분 시작 비컨이 4개일 경우------
            //거리
            d_1_c_0 = d_1_c[0];
            d_1_c_1 = d_1_c[1];
            d_1_c_2 = d_1_c[2];

            d_2_c_0 = d_2_c[0];
            d_2_c_1 = d_2_c[1];
            d_2_c_2 = d_2_c[2];

            if (kalman_after == 1) Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> kalman_after_d " + d_1_c_0 + " " + d_1_c_1 + " " + d_1_c_2 + " " + d_2_c_0 + " " + d_2_c_1 + " " + d_2_c_2);


            //if (const_becon_p2_dozer == 0)
            //{
            //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1_kkk: " + Math.Round(becon_p1_dozer[0]) + "  " + Math.Round(becon_p1_dozer[1]) + "  " + Math.Round(becon_p1_dozer[2]) + "  " + Math.Round(becon_p1_dozer[3])
            //    + " eP2_kkk: " + Math.Round(becon_p2_dozer[0]) + "  " + Math.Round(becon_p2_dozer[1]) + "  " + Math.Round(becon_p2_dozer[2]) + "  " + Math.Round(becon_p2_dozer[3]));
            //}

            //비컨 - 로우값 계산의로 인한
            r_becon_1_m[0] = r_becon_1_c[0];
            r_becon_1_m[1] = r_becon_1_c[1];
            r_becon_1_m[2] = r_becon_1_c[2];

            r_becon_2_m[0] = r_becon_2_c[0];
            r_becon_2_m[1] = r_becon_2_c[1];
            r_becon_2_m[2] = r_becon_2_c[2];

            r_becon_3_m[0] = r_becon_3_c[0];
            r_becon_3_m[1] = r_becon_3_c[1];
            r_becon_3_m[2] = r_becon_3_c[2];

            //Debug.WriteLine(" cB1P1 : " + d_1_c_0 + " cB2P1 : " + d_1_c_1 + " cB3P1 : " + d_1_c_2);



            //----변환 부분 끝------

            //P1일 경우
            if (d_1_c_0 == 111111) d_1_c_0 = d_1_t[0];
            d_1_t[0] = d_1_c_0;

            if (d_1_c_1 == 222222) d_1_c_1 = d_1_t[1];
            d_1_t[1] = d_1_c_1;

            if (d_1_c_2 == 333333) d_1_c_2 = d_1_t[2];
            d_1_t[2] = d_1_c_2;

            //P2일 경우
            if (d_2_c_0 == 111111) d_2_c_0 = d_2_t[0];
            d_2_t[0] = d_2_c_0;

            if (d_2_c_1 == 222222) d_2_c_1 = d_2_t[1];
            d_2_t[1] = d_2_c_1;

            if (d_2_c_2 == 333333) d_2_c_2 = d_2_t[2];
            d_2_t[2] = d_2_c_2;



            //******************************************************************becon의 변형된 위치*************************************************************

            becon_1_1[0] = r_becon_1_m[0] - r_becon_1_m[0]; becon_1_1[1] = r_becon_1_m[1] - r_becon_1_m[1]; becon_1_1[2] = r_becon_1_m[2] - r_becon_1_m[2];//비컨1
            becon_2_1[0] = r_becon_2_m[0] - r_becon_1_m[0]; becon_2_1[1] = r_becon_2_m[1] - r_becon_1_m[1]; becon_2_1[2] = r_becon_2_m[2] - r_becon_1_m[2];//비컨2
            becon_3_1[0] = r_becon_3_m[0] - r_becon_1_m[0]; becon_3_1[1] = r_becon_3_m[1] - r_becon_1_m[1]; becon_3_1[2] = r_becon_3_m[2] - r_becon_1_m[2];//비컨3

            //*********************************************************************평면의 방정식*****************************************************************
            a_1 = 2 * becon_2_1[0]; b_1 = 2 * becon_2_1[1]; c_1 = 2 * becon_2_1[2];
            a_2 = 2 * becon_3_1[0]; b_2 = 2 * becon_3_1[1]; c_2 = 2 * becon_3_1[2];

            //////////////////P1일 경우///////////////////
            p_1_w_1 = Math.Pow(d_1_t[0], 2) - Math.Pow(d_1_t[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_1_w_2 = Math.Pow(d_1_t[0], 2) - Math.Pow(d_1_t[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);

            //////////////////P2일 경우///////////////////
            p_2_w_1 = Math.Pow(d_2_t[0], 2) - Math.Pow(d_2_t[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_2_w_2 = Math.Pow(d_2_t[0], 2) - Math.Pow(d_2_t[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);

            //*******************************************************************2차 연립방정식******************************************************************
            s_A_1 = (c_1 * a_2 - c_2 * a_1) / (a_1 * b_2 - a_2 * b_1);
            s_A_2 = (c_2 * b_1 - c_1 * b_2) / (a_1 * b_2 - a_2 * b_1);

            //////////////////P1일 경우////////////////////
            p_1_s_B_1 = (a_1 * p_1_w_2 - a_2 * p_1_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_1_s_B_2 = (b_2 * p_1_w_1 - b_1 * p_1_w_2) / (a_1 * b_2 - a_2 * b_1);

            //////////////////P2일 경우////////////////////
            p_2_s_B_1 = (a_1 * p_2_w_2 - a_2 * p_2_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_2_s_B_2 = (b_2 * p_2_w_1 - b_1 * p_2_w_2) / (a_1 * b_2 - a_2 * b_1);

            //******************************************************************z에 관한 2차 방정식****************************************************************
            Aa = Math.Pow(s_A_2, 2) + Math.Pow(s_A_1, 2) + 1;

            //////////////////P1일 경우////////////////////
            p_1_Bb = s_A_2 * p_1_s_B_2 + s_A_1 * p_1_s_B_1;
            p_1_Cc = Math.Pow(p_1_s_B_2, 2) + Math.Pow(p_1_s_B_1, 2) - Math.Pow(d_1_t[0], 2);

            //////////////////P2일 경우////////////////////
            p_2_Bb = s_A_2 * p_2_s_B_2 + s_A_1 * p_2_s_B_1;
            p_2_Cc = Math.Pow(p_2_s_B_2, 2) + Math.Pow(p_2_s_B_1, 2) - Math.Pow(d_2_t[0], 2);

            //******************************************************************becon의 오차 계산*************************************************************
            ////수신기 1번 계산 판별
            //if (Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc < 0)
            //{


            //    //---------------비콘 사이의 거리 계산-----------------
            //    B1B2 = Math.Sqrt(Math.Pow(r_becon_2_m[0] - r_becon_1_m[0], 2) + Math.Pow(r_becon_2_m[1] - r_becon_1_m[1], 2) + Math.Pow(r_becon_2_m[2] - r_becon_1_m[2], 2));
            //    B1B3 = Math.Sqrt(Math.Pow(r_becon_3_m[0] - r_becon_1_m[0], 2) + Math.Pow(r_becon_3_m[1] - r_becon_1_m[1], 2) + Math.Pow(r_becon_3_m[2] - r_becon_1_m[2], 2));
            //    B2B3 = Math.Sqrt(Math.Pow(r_becon_3_m[0] - r_becon_2_m[0], 2) + Math.Pow(r_becon_3_m[1] - r_becon_2_m[1], 2) + Math.Pow(r_becon_3_m[2] - r_becon_2_m[2], 2));

            //    //-------------------------------------------------------------------수신기 1 계산------------------------------------------------------
            //    //---------------구의 반지름 계산(수신기 1)-----------------
            //    D1T_u = d_1_t[0] + (B1B2 + 2 - d_1_t[0] - d_1_t[1]) / 2;
            //    D2T_u = d_1_t[1] + (B1B2 + 2 - d_1_t[0] - d_1_t[1]) / 2;

            //    D1T_d = d_1_t[0] + (B1B3 + 2 - d_1_t[0] - d_1_t[2]) / 2;
            //    D3T_u = d_1_t[2] + (B1B3 + 2 - d_1_t[0] - d_1_t[2]) / 2;

            //    D2T_d = d_1_t[1] + (B2B3 + 2 - d_1_t[1] - d_1_t[2]) / 2;
            //    D3T_d = d_1_t[2] + (B2B3 + 2 - d_1_t[1] - d_1_t[2]) / 2;

            //    D1T_1 = (D1T_u - D1T_d > 0) ? D1T_u : D1T_d;
            //    D2T_1 = (D2T_u - D2T_d > 0) ? D2T_u : D2T_d;
            //    D3T_1 = (D3T_u - D3T_d > 0) ? D3T_u : D3T_d;

            //    D1T_2 = (d_1_t[0] - D1T_1 > 0) ? d_1_t[0] : D1T_1;
            //    D2T_2 = (d_1_t[1] - D2T_1 > 0) ? d_1_t[1] : D2T_1;
            //    D3T_2 = (d_1_t[2] - D3T_1 > 0) ? d_1_t[2] : D3T_1;

            //    //---------------직선의 방정식(수신기 1)-----------------
            //    A1 = 2 * (r_becon_2_m[0] - r_becon_1_m[0]);
            //    B1 = 2 * (r_becon_2_m[1] - r_becon_1_m[1]);
            //    C1 = Math.Pow(r_becon_1_m[0], 2) + Math.Pow(r_becon_1_m[1], 2) - Math.Pow(r_becon_2_m[0], 2) - Math.Pow(r_becon_2_m[1], 2) - Math.Pow(D1T_2, 2) + Math.Pow(D2T_2, 2);

            //    A2 = 2 * (r_becon_3_m[0] - r_becon_1_m[0]);
            //    B2 = 2 * (r_becon_3_m[1] - r_becon_1_m[1]);
            //    C2 = Math.Pow(r_becon_1_m[0], 2) + Math.Pow(r_becon_1_m[1], 2) - Math.Pow(r_becon_3_m[0], 2) - Math.Pow(r_becon_3_m[1], 2) - Math.Pow(D1T_2, 2) + Math.Pow(D3T_2, 2);

            //    A3 = 2 * (r_becon_3_m[0] - r_becon_2_m[0]);
            //    B3 = 2 * (r_becon_3_m[1] - r_becon_2_m[1]);
            //    C3 = Math.Pow(r_becon_2_m[0], 2) + Math.Pow(r_becon_2_m[1], 2) - Math.Pow(r_becon_3_m[0], 2) - Math.Pow(r_becon_3_m[1], 2) - Math.Pow(D2T_2, 2) + Math.Pow(D3T_2, 2);

            //    //---------------교점 계산(수신기 1)-----------------
            //    y1 = (-C1 * A2 + A1 * C2) / (B1 * A2 - A1 * B2);
            //    x1 = -(C1 + B1 * y1) / A1;

            //    y2 = (-C1 * A3 + A1 * C3) / (B1 * A3 - A1 * B3);
            //    x2 = -(C1 + B1 * y2) / A1;

            //    y3 = (-C2 * A3 + A2 * C3) / (B2 * A3 - A2 * B3);
            //    x3 = -(C2 + B2 * y3) / A2;

            //    //--------------삼각형의 무게중심(수신기 1)-------------
            //    cx = (x1 + x2 + x3) / 3;
            //    cy = (y1 + y2 + y3) / 3;

            //    ////임시
            //    //cx = 1111111111111;
            //    //cy = 1111111111111;

            //    p_calc_1 = 1;

            //    //--------------------------------------------------------------------------------------------------------------------------------------
            //    //************************************************************************값 출력***********************************************************************
            //    //////////////////P1일 경우////////////////////
            //    result[0] = cx; result[1] = cy; result[2] = p_1_u[2];
            //    result[3] = cx; result[4] = cy; result[5] = p_1_d[2];

            //}

            //else if (Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc >= 0)
            //{
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P1일 경우////////////////////
            p_1_t_u[2] = (-p_1_Bb + Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa; p_1_t_d[2] = (-p_1_Bb - Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa;
            p_1_t_u[0] = s_A_2 * p_1_t_u[2] + p_1_s_B_2; p_1_t_d[0] = s_A_2 * p_1_t_d[2] + p_1_s_B_2;
            p_1_t_u[1] = s_A_1 * p_1_t_u[2] + p_1_s_B_1; p_1_t_d[1] = s_A_1 * p_1_t_d[2] + p_1_s_B_1;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P1일 경우////////////////////
            p_1_u[0] = p_1_t_u[0] + r_becon_1_m[0]; p_1_u[1] = p_1_t_u[1] + r_becon_1_m[1]; p_1_u[2] = p_1_t_u[2] + r_becon_1_m[2];
            p_1_d[0] = p_1_t_d[0] + r_becon_1_m[0]; p_1_d[1] = p_1_t_d[1] + r_becon_1_m[1]; p_1_d[2] = p_1_t_d[2] + r_becon_1_m[2];

            //임시
            //p_1_u[0] = 22222; p_1_u[1] = 22222; p_1_u[2] = p_1_t_u[2] + r_becon_1_m[2];
            //p_1_d[0] = 22222; p_1_d[1] = 22222; p_1_d[2] = p_1_t_d[2] + r_becon_1_m[2];

            p_calc_1 = 0;

            //************************************************************************값 출력***********************************************************************
            //////////////////P1일 경우////////////////////
            result[0] = p_1_u[0]; result[1] = p_1_u[1]; result[2] = p_1_u[2];
            result[3] = p_1_d[0]; result[4] = p_1_d[1]; result[5] = p_1_d[2];
            //}

            ////수신기 2번 계산 판별
            //if (Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc < 0)
            //{
            //    //---------------비콘 사이의 거리 계산-----------------
            //    B1B2 = Math.Sqrt(Math.Pow(r_becon_2_m[0] - r_becon_1_m[0], 2) + Math.Pow(r_becon_2_m[1] - r_becon_1_m[1], 2) + Math.Pow(r_becon_2_m[2] - r_becon_1_m[2], 2));
            //    B1B3 = Math.Sqrt(Math.Pow(r_becon_3_m[0] - r_becon_1_m[0], 2) + Math.Pow(r_becon_3_m[1] - r_becon_1_m[1], 2) + Math.Pow(r_becon_3_m[2] - r_becon_1_m[2], 2));
            //    B2B3 = Math.Sqrt(Math.Pow(r_becon_3_m[0] - r_becon_2_m[0], 2) + Math.Pow(r_becon_3_m[1] - r_becon_2_m[1], 2) + Math.Pow(r_becon_3_m[2] - r_becon_2_m[2], 2));

            //    //-------------------------------------------------------------------수신기 2 계산------------------------------------------------------
            //    //---------------구의 반지름 계산(수신기 2)-----------------
            //    D1T_u_2 = d_2_t[0] + (B1B2 + 2 - d_2_t[0] - d_2_t[1]) / 2;
            //    D2T_u_2 = d_2_t[1] + (B1B2 + 2 - d_2_t[0] - d_2_t[1]) / 2;

            //    D1T_d_2 = d_2_t[0] + (B1B3 + 2 - d_2_t[0] - d_2_t[2]) / 2;
            //    D3T_u_2 = d_2_t[2] + (B1B3 + 2 - d_2_t[0] - d_2_t[2]) / 2;

            //    D2T_d_2 = d_2_t[1] + (B2B3 + 2 - d_2_t[1] - d_2_t[2]) / 2;
            //    D3T_d_2 = d_2_t[2] + (B2B3 + 2 - d_2_t[1] - d_2_t[2]) / 2;

            //    D1T_1_2 = (D1T_u_2 - D1T_d_2 > 0) ? D1T_u_2 : D1T_d_2;
            //    D2T_1_2 = (D2T_u_2 - D2T_d_2 > 0) ? D2T_u_2 : D2T_d_2;
            //    D3T_1_2 = (D3T_u_2 - D3T_d_2 > 0) ? D3T_u_2 : D3T_d_2;

            //    D1T_2_2 = (d_2_t[0] - D1T_1_2 > 0) ? d_2_t[0] : D1T_1_2;
            //    D2T_2_2 = (d_2_t[1] - D2T_1_2 > 0) ? d_2_t[1] : D2T_1_2;
            //    D3T_2_2 = (d_2_t[2] - D3T_1_2 > 0) ? d_2_t[2] : D3T_1_2;

            //    //---------------직선의 방정식(수신기 2)-----------------
            //    A1_2 = 2 * (r_becon_2_m[0] - r_becon_1_m[0]);
            //    B1_2 = 2 * (r_becon_2_m[1] - r_becon_1_m[1]);
            //    C1_2 = Math.Pow(r_becon_1_m[0], 2) + Math.Pow(r_becon_1_m[1], 2) - Math.Pow(r_becon_2_m[0], 2) - Math.Pow(r_becon_2_m[1], 2) - Math.Pow(D1T_2_2, 2) + Math.Pow(D2T_2_2, 2);

            //    A2_2 = 2 * (r_becon_3_m[0] - r_becon_1_m[0]);
            //    B2_2 = 2 * (r_becon_3_m[1] - r_becon_1_m[1]);
            //    C2_2 = Math.Pow(r_becon_1_m[0], 2) + Math.Pow(r_becon_1_m[1], 2) - Math.Pow(r_becon_3_m[0], 2) - Math.Pow(r_becon_3_m[1], 2) - Math.Pow(D1T_2_2, 2) + Math.Pow(D3T_2_2, 2);

            //    A3_2 = 2 * (r_becon_3_m[0] - r_becon_2_m[0]);
            //    B3_2 = 2 * (r_becon_3_m[1] - r_becon_2_m[1]);
            //    C3_2 = Math.Pow(r_becon_2_m[0], 2) + Math.Pow(r_becon_2_m[1], 2) - Math.Pow(r_becon_3_m[0], 2) - Math.Pow(r_becon_3_m[1], 2) - Math.Pow(D2T_2_2, 2) + Math.Pow(D3T_2_2, 2);

            //    //---------------교점 계산(수신기 1)-----------------
            //    y1_2 = (-C1_2 * A2_2 + A1_2 * C2_2) / (B1_2 * A2_2 - A1_2 * B2_2);
            //    x1_2 = -(C1_2 + B1_2 * y1_2) / A1_2;

            //    y2_2 = (-C1_2 * A3_2 + A1_2 * C3_2) / (B1_2 * A3_2 - A1_2 * B3_2);
            //    x2_2 = -(C1_2 + B1_2 * y2_2) / A1_2;

            //    y3_2 = (-C2_2 * A3_2 + A2_2 * C3_2) / (B2_2 * A3_2 - A2_2 * B3_2);
            //    x3_2 = -(C2_2 + B2_2 * y3_2) / A2_2;

            //    //--------------삼각형의 무게중심(수신기 1)-------------
            //    cx_2 = (x1_2 + x2_2 + x3_2) / 3;
            //    cy_2 = (y1_2 + y2_2 + y3_2) / 3;


            //    ////임시
            //    //cx_2 = 11111111;
            //    //cy_2 = 11111111;

            //    p_calc_2 = 1;

            //    //---------------------------------------------------------------------------------------------------------------------------------------

            //    //************************************************************************값 출력***********************************************************************
            //    //////////////////P2일 경우////////////////////
            //    result[6] = cx_2; result[7] = cy_2; result[8] = p_2_u[2];
            //    result[9] = cx_2; result[10] = cy_2; result[11] = p_2_d[2];

            //}

            //else if (Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc >= 0)
            //{

            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P2일 경우////////////////////
            p_2_t_u[2] = (-p_2_Bb + Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa; p_2_t_d[2] = (-p_2_Bb - Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa;
            p_2_t_u[0] = s_A_2 * p_2_t_u[2] + p_2_s_B_2; p_2_t_d[0] = s_A_2 * p_2_t_d[2] + p_2_s_B_2;
            p_2_t_u[1] = s_A_1 * p_2_t_u[2] + p_2_s_B_1; p_2_t_d[1] = s_A_1 * p_2_t_d[2] + p_2_s_B_1;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P2일 경우////////////////////
            p_2_u[0] = p_2_t_u[0] + r_becon_1_m[0]; p_2_u[1] = p_2_t_u[1] + r_becon_1_m[1]; p_2_u[2] = p_2_t_u[2] + r_becon_1_m[2];
            p_2_d[0] = p_2_t_d[0] + r_becon_1_m[0]; p_2_d[1] = p_2_t_d[1] + r_becon_1_m[1]; p_2_d[2] = p_2_t_d[2] + r_becon_1_m[2];

            //임시
            //p_2_u[0] = 22222222; p_2_u[1] = 2222222222; p_2_u[2] = p_2_t_u[2] + r_becon_1_m[2];
            //p_2_d[0] = 22222222; p_2_d[1] = 2222222222; p_2_d[2] = p_2_t_d[2] + r_becon_1_m[2];

            p_calc_2 = 0;

            //************************************************************************값 출력***********************************************************************
            //////////////////P2일 경우////////////////////
            result[6] = p_2_u[0]; result[7] = p_2_u[1]; result[8] = p_2_u[2];
            result[9] = p_2_d[0]; result[10] = p_2_d[1]; result[11] = p_2_d[2];

            //}

            return result;
        }




        double[] r_Calc_tri_o(double[] d_1_o, double[] d_2_o)
        {
            //-------비컨이 4개 사용시---------
            double[] d_1_c_o = new double[3];
            double[] d_2_c_o = new double[3];

            double[] r_becon_1_c_o = new double[3];
            double[] r_becon_2_c_o = new double[3];
            double[] r_becon_3_c_o = new double[3];

            //큰수
            double p1_dN1_o = 0;
            double p1_dN2_o = 0;
            double p1_dN3_o = 0;

            //수신기 평균 거리값
            double[] d_12_o = new double[4];

            //-----------원본 계산 변수-------------
            double[] result_1 = new double[20];

            double[] becon_1_1_o = new double[3];
            double[] becon_2_1_o = new double[3];
            double[] becon_3_1_o = new double[3];

            double a_1_o = 0, b_1_o = 0, c_1_o = 0, p_1_w_1_o = 0, p_2_w_1_o = 0;
            double a_2_o = 0, b_2_o = 0, c_2_o = 0, p_1_w_2_o = 0, p_2_w_2_o = 0;

            double s_a_1_o = 0, p_1_s_b_1_o = 0, p_2_s_b_1_o = 0;
            double s_a_2_o = 0, p_1_s_b_2_o = 0, p_2_s_b_2_o = 0;

            double Aa_o = 0, p_1_Bb_o = 0, p_1_Cc_o = 0, p_2_Bb_o = 0, p_2_Cc_o = 0;

            double[] p_1_t_u_o = new double[3]; double[] p_2_t_u_o = new double[3];
            double[] p_1_t_d_o = new double[3]; double[] p_2_t_d_o = new double[3];

            //사용하지 않으므로 수정된 부분(0711)
            ////-----------오차 계산 변수(시작)-------------
            //double B1_oB2_o_o = 0, B1_oB3_o_o = 0, B2_oB3_o_o = 0;//비콘 사이의 거리

            ////------수신기 1---------
            //double D1T_u_o = 0, D2T_u_o = 0;
            //double D1T_d_o = 0, D3T_u_o = 0;
            //double D2T_d_o = 0, D3T_d_o = 0;

            //double D1T_1_o = 0, D2T_1_o, D3T_1_o = 0;
            //double D1T_2_o = 0, D2T_2_o, D3T_2_o = 0;

            ////직선의 방정식
            //double A1_o = 0, B1_o = 0, C1_o = 0;
            //double A2_o = 0, B2_o = 0, C2_o = 0;
            //double A3_o = 0, B3_o = 0, C3_o = 0;

            ////직선의 교점
            //double x1_o = 0, y1_o = 0;
            //double x2_o = 0, y2_o = 0;
            //double x3_o = 0, y3_o = 0;

            //double cx_o = 0, cy_o = 0;//무게중심
            ////--------------------------

            ////------수신기 2---------
            //double D1T_u_2_o = 0, D2T_u_2_o = 0;
            //double D1T_d_2_o = 0, D3T_u_2_o = 0;
            //double D2T_d_2_o = 0, D3T_d_2_o = 0;

            //double D1T_1_2_o = 0, D2T_1_2_o, D3T_1_2_o = 0;
            //double D1T_2_2_o = 0, D2T_2_2_o, D3T_2_2_o = 0;

            ////직선의 방정식
            //double A1_2_o = 0, B1_2_o = 0, C1_2_o = 0;
            //double A2_2_o = 0, B2_2_o = 0, C2_2_o = 0;
            //double A3_2_o = 0, B3_2_o = 0, C3_2_o = 0;

            ////직선의 교점
            //double x1_2_o = 0, y1_2_o = 0;
            //double x2_2_o = 0, y2_2_o = 0;
            //double x3_2_o = 0, y3_2_o = 0;

            //double cx_2_o = 0, cy_2_o = 0;//무게중심
            ////-----------오차 계산 변수(끝)-------------


            //로우값 계산을 위한
            double[] r_becon_1_m_o = new double[3];
            double[] r_becon_2_m_o = new double[3];
            double[] r_becon_3_m_o = new double[3];

            // 배열에서 변수로 변환을 위한
            double d_1_c_o_0 = 0;
            double d_1_c_o_1 = 0;
            double d_1_c_o_2 = 0;

            double d_2_c_o_0 = 0;
            double d_2_c_o_1 = 0;
            double d_2_c_o_2 = 0;


            //----------비컨 각도 적용에 따른 추가된 부분(0711) - 시작-------------
            //각도가 가장 높은것과 나머지 비컨의 좌표를 위한 부분
            double beacon_2_calc_p1b0_o = 0;
            double beacon_2_calc_p1b1_o = 0;
            double beacon_2_calc_p1b2_o = 0;
            double beacon_2_calc_p1b3_o = 0;

            double beacon_2_calc_p2b0_o = 0;
            double beacon_2_calc_p2b1_o = 0;
            double beacon_2_calc_p2b2_o = 0;
            double beacon_2_calc_p2b3_o = 0;

            //평균 거리값 계산을 위한 부분
            double beacon_2_calc_a_b1_o = 0;
            double beacon_2_calc_a_b2_o = 0;
            double beacon_2_calc_a_b3_o = 0;

            //가장 큰 평균 거리값 계산을 위한 부분
            double angle_dN1_o = 0;
            double angle_dN2_o = 0;

            //비컨 id 확인을 위한 부분
            Int16 b1_id_o = 0;
            Int16 b2_id_o = 0;
            Int16 b3_id_o = 0;
            //-------------------------------------------------------끝-------------




            //비컨 2개 결정 준비 확인부분 - 추가된 부분(0711)
            if (beacon_2_calc == 0)
            {

                //---------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘1 = (r_becon_1[0], r_becon_1[1], r_becon_1[2])
                //비콘2 = (r_becon_2[0], r_becon_2[1], r_becon_2[2])
                //비콘3 = (r_becon_3[0], r_becon_3[1], r_becon_3[2])
                //비콘4 = (r_becon_4[0], r_becon_4[1], r_becon_4[2])//비컨이 4개일 경우

                //------------계산에 사용되는 비컨값이 결정된후 비컨값 비컨이 4개일 경우
                //비콘1 = (r_becon_1_c_o[0], r_becon_1_c_o[1], r_becon_1_c_o[2])
                //비콘2 = (r_becon_2_c_o[0], r_becon_2_c_o[1], r_becon_2_c_o[2])
                //비콘3 = (r_becon_3_c_o[0], r_becon_3_c_o[1], r_becon_3_c_o[2])

                //-------------계산에 사용되는 거리값이 결정되기전 거리값
                //----수신기 1------
                //거리1 = d_1_o[0]
                //거리2 = d_1_o[1]
                //거리3 = d_1_o[2]
                //거리4 = d_1_o[3]//비컨이 4개일 경우

                //----수신기 2------
                //거리1 = d_2_o[0]
                //거리2 = d_2_o[1]
                //거리3 = d_2_o[2]
                //거리4 = d_2_o[3]//비컨이 4개일 경우

                //-----수신기 평균 거리값
                //거리1 = d_12_o[0]
                //거리2 = d_12_o[1]
                //거리3 = d_12_o[2]
                //거리4 = d_12_o[3]

                //--------------계산에 사용되는 거리값이 결정된후 거리값 비컨이 4개일 경우
                //----수신기 1------
                //거리1 = d_1_c_o[0]
                //거리2 = d_1_c_o[1]
                //거리3 = d_1_c_o[2]

                //----수신기 2------
                //거리1 = d_2_c_o[0]
                //거리2 = d_2_c_o[1]
                //거리3 = d_2_c_o[2]

                ////******************************************************************결정 코드 부분 비컨이 4개일경우*************************************************************

                //수신기의 평균 거리값 계산
                d_12_o[0] = (d_1_o[0] + d_2_o[0]) / 2;
                d_12_o[1] = (d_1_o[1] + d_2_o[1]) / 2;
                d_12_o[2] = (d_1_o[2] + d_2_o[2]) / 2;
                d_12_o[3] = (d_1_o[3] + d_2_o[3]) / 2;

                //거리값이 같아지는경우는 고려 안함
                //가장긴거리값 결정(dN3이 가장김)
                if (d_12_o[0] < d_12_o[1]) p1_dN1_o = d_12_o[1];
                else p1_dN1_o = d_12_o[0];

                if (p1_dN1_o < d_12_o[2]) p1_dN2_o = d_12_o[2];
                else p1_dN2_o = p1_dN1_o;

                if (p1_dN2_o < d_12_o[3]) p1_dN3_o = d_12_o[3];
                else p1_dN3_o = p1_dN2_o;

                // Debug.WriteLine(" P_d_o : " + p1_dN3_o);


                //계산되는 비컨과 거리값 결정
                if (p1_dN3_o == d_12_o[0])
                {
                    d_1_c_o[0] = d_1_o[1];
                    d_1_c_o[1] = d_1_o[2];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[1];
                    d_2_c_o[1] = d_2_o[2];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_2[0];
                    r_becon_1_c_o[1] = r_becon_2[1];
                    r_becon_1_c_o[2] = r_becon_2[2];

                    r_becon_2_c_o[0] = r_becon_3[0];
                    r_becon_2_c_o[1] = r_becon_3[1];
                    r_becon_2_c_o[2] = r_becon_3[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 0) Debug.WriteLine(" B2_o B3_o B4_o ");
                }

                else if (p1_dN3_o == d_12_o[1])
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[2];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[2];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_3[0];
                    r_becon_2_c_o[1] = r_becon_3[1];
                    r_becon_2_c_o[2] = r_becon_3[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 0) Debug.WriteLine(" B1_o B3_o B4_o ");
                }

                else if (p1_dN3_o == d_12_o[2])
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[1];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[1];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_2[0];
                    r_becon_2_c_o[1] = r_becon_2[1];
                    r_becon_2_c_o[2] = r_becon_2[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

                    if (const_becon_p2_dozer == 0) Debug.WriteLine(" B1_o B2_o B4_o ");
                }

                else
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[1];
                    d_1_c_o[2] = d_1_o[2];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[1];
                    d_2_c_o[2] = d_2_o[2];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_2[0];
                    r_becon_2_c_o[1] = r_becon_2[1];
                    r_becon_2_c_o[2] = r_becon_2[2];

                    r_becon_3_c_o[0] = r_becon_3[0];
                    r_becon_3_c_o[1] = r_becon_3[1];
                    r_becon_3_c_o[2] = r_becon_3[2];

                    if (const_becon_p2_dozer == 0) Debug.WriteLine(" B1_o B2_o B3_o ");
                }
            }


            //-------------비컨 각도 적용에 따른 추가된 부분(0704)----------------
            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 1)
            {
                //------------------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘0 = (angle_s_b0_x, angle_s_b0_y, angle_s_b0_z)
                //비콘1 = (angle_s_b1_x, angle_s_b1_y, angle_s_b1_z)
                //비콘2 = (angle_s_b2_x, angle_s_b2_y, angle_s_b2_z)
                //비콘3 = (angle_s_b3_x, angle_s_b3_y, angle_s_b3_z)

                //------------계산에 사용되는 비컨값이 결정된후 비컨값
                //비콘1 = (r_becon_1_c_o[0], r_becon_1_c_o[1], r_becon_1_c_o[2])
                //비콘2 = (r_becon_2_c_o[0], r_becon_2_c_o[1], r_becon_2_c_o[2])
                //비콘3 = (r_becon_3_c_o[0], r_becon_3_c_o[1], r_becon_3_c_o[2]) <----- 무조건 비컨0 부분

                ////-------------계산에 사용되는 거리값이 결정되기전 거리값
                //비컨번호를 확인해서 해당되는 거리값 선택(각각 비컨 1, 2, 3, 4 사이의 거리값임)
                //----수신기 1------
                //거리1 = d_1_o[0]
                //거리2 = d_1_o[1]
                //거리3 = d_1_o[2]
                //거리4 = d_1_o[3]

                //----수신기 2------
                //거리1 = d_2_o[0]
                //거리2 = d_2_o[1]
                //거리3 = d_2_o[2]
                //거리4 = d_2_o[3]

                //비컨번호(예를 들어서, s_beacon_0 = 1 일 경우 거리1 은 각각 d_1[0], d_2[0] 가 된다.)
                //s_beacon_0
                //s_beacon_1
                //s_beacon_2
                //s_beacon_3

                //--------------계산에 사용되는 거리값이 결정된후 거리값
                //----수신기 1------
                //거리1 = d_1_c_o[0]
                //거리2 = d_1_c_o[1]
                //거리3 = d_1_c_o[2]    <-------- 무조건 비컨0과 수신기1번 사이의 거리

                //----수신기 2------
                //거리1 = d_2_c_o[0]
                //거리2 = d_2_c_o[1]
                //거리3 = d_2_c_o[2]    <-------- 무조건 비컨0과 수신기2번 사이의 거리



                //-------------------------------------거리값 계산 부분--------------------------------------
                //------비컨0에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b0_o)
                if (s_beacon_0 == 1) beacon_2_calc_p1b0_o = d_1_o[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p1b0_o = d_1_o[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p1b0_o = d_1_o[2];
                else beacon_2_calc_p1b0_o = d_1_o[3];

                //p2(beacon_2_calc_p2b0_o)
                if (s_beacon_0 == 1) beacon_2_calc_p2b0_o = d_2_o[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p2b0_o = d_2_o[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p2b0_o = d_2_o[2];
                else beacon_2_calc_p2b0_o = d_2_o[3];

                //--------비컨1에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b1_o)
                if (s_beacon_1 == 1) beacon_2_calc_p1b1_o = d_1_o[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p1b1_o = d_1_o[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p1b1_o = d_1_o[2];
                else beacon_2_calc_p1b1_o = d_1_o[3];

                //p2(beacon_2_calc_p2b1_o)
                if (s_beacon_1 == 1) beacon_2_calc_p2b1_o = d_2_o[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p2b1_o = d_2_o[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p2b1_o = d_2_o[2];
                else beacon_2_calc_p2b1_o = d_2_o[3];

                //---------비컨2에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b2_o)
                if (s_beacon_2 == 1) beacon_2_calc_p1b2_o = d_1_o[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p1b2_o = d_1_o[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p1b2_o = d_1_o[2];
                else beacon_2_calc_p1b2_o = d_1_o[3];

                //p2(beacon_2_calc_p2b2_o)
                if (s_beacon_2 == 1) beacon_2_calc_p2b2_o = d_2_o[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p2b2_o = d_2_o[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p2b2_o = d_2_o[2];
                else beacon_2_calc_p2b2_o = d_2_o[3];

                //---------비컨3에 관련된 거리값 계산 - ID 오류로 인한 수정
                //p1(beacon_2_calc_p1b2_o)
                if (s_beacon_3 == 1) beacon_2_calc_p1b3_o = d_1_o[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p1b3_o = d_1_o[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p1b3_o = d_1_o[2];
                else beacon_2_calc_p1b3_o = d_1_o[3];

                //p2(beacon_2_calc_p2b2_o)
                if (s_beacon_3 == 1) beacon_2_calc_p2b3_o = d_2_o[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p2b3_o = d_2_o[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p2b3_o = d_2_o[2];
                else beacon_2_calc_p2b3_o = d_2_o[3];


                //비컨1, 2, 3을 가지고 계산을 해서 결정된 비컨1, 2가되고 비컨0은 결정된 비컨 3이 됨 
                //수신기의 평균 거리값 계산
                beacon_2_calc_a_b1_o = (beacon_2_calc_p1b1_o + beacon_2_calc_p2b1_o) / 2;
                beacon_2_calc_a_b2_o = (beacon_2_calc_p1b2_o + beacon_2_calc_p2b2_o) / 2;
                beacon_2_calc_a_b3_o = (beacon_2_calc_p1b3_o + beacon_2_calc_p2b3_o) / 2;

                //가장 큰 평균 거리값 결정 부분
                if (beacon_2_calc_a_b1_o < beacon_2_calc_a_b2_o) angle_dN1_o = beacon_2_calc_a_b2_o;
                else angle_dN1_o = beacon_2_calc_a_b1_o;

                if (angle_dN1_o < beacon_2_calc_a_b3_o) angle_dN2_o = beacon_2_calc_a_b3_o;
                else angle_dN2_o = angle_dN1_o;


                //비컨 2개의 거리값과 좌표값 결정

                //------거리값 결정
                //p1(d_1_c_o[0])_b1
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_1_c_o[0] = beacon_2_calc_p1b2_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_1_c_o[0] = beacon_2_calc_p1b1_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_1_c_o[0] = beacon_2_calc_p1b1_o;
                else d_1_c_o[0] = beacon_2_calc_p1b1_o;

                //p1(d_1_c_o[1])_b2
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_1_c_o[1] = beacon_2_calc_p1b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_1_c_o[1] = beacon_2_calc_p1b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_1_c_o[1] = beacon_2_calc_p1b2_o;
                else d_1_c_o[1] = beacon_2_calc_p1b2_o;

                //p2(d_1_c_o[2])_b3
                d_1_c_o[2] = beacon_2_calc_p1b0_o;

                //p2(d_2_c_o[0])_b1
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_2_c_o[0] = beacon_2_calc_p2b2_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_2_c_o[0] = beacon_2_calc_p2b1_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_2_c_o[0] = beacon_2_calc_p2b1_o;
                else d_2_c_o[0] = beacon_2_calc_p2b1_o;

                //p2(d_2_c_o[1])_b2
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_2_c_o[1] = beacon_2_calc_p2b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_2_c_o[1] = beacon_2_calc_p2b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_2_c_o[1] = beacon_2_calc_p2b2_o;
                else d_2_c_o[1] = beacon_2_calc_p2b2_o;

                //p2(d_2_c_o[2])_b3
                d_2_c_o[2] = beacon_2_calc_p2b0_o;



                //-------좌표값 결정
                //----------b1
                //x
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[0] = angle_s_b2_x;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[0] = angle_s_b1_x;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[0] = angle_s_b1_x;
                else r_becon_1_c_o[0] = angle_s_b1_x;

                //y
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[1] = angle_s_b2_y;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[1] = angle_s_b1_y;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[1] = angle_s_b1_y;
                else r_becon_1_c_o[1] = angle_s_b1_y;

                //z
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[2] = angle_s_b2_z;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[2] = angle_s_b1_z;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[2] = angle_s_b1_z;
                else r_becon_1_c_o[2] = angle_s_b1_z;

                //------------b2
                //x
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[0] = angle_s_b3_x;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[0] = angle_s_b3_x;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[0] = angle_s_b2_x;
                else r_becon_2_c_o[0] = angle_s_b2_x;

                //y
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[1] = angle_s_b3_y;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[1] = angle_s_b3_y;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[1] = angle_s_b2_y;
                else r_becon_2_c_o[1] = angle_s_b2_y;

                //z
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[2] = angle_s_b3_z;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[2] = angle_s_b3_z;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[2] = angle_s_b2_z;
                else r_becon_2_c_o[2] = angle_s_b2_z;

                //-----------------b3
                r_becon_3_c_o[0] = angle_s_b0_x;
                r_becon_3_c_o[1] = angle_s_b0_y;
                r_becon_3_c_o[2] = angle_s_b0_z;

                //beacon_2_calc = 0;

                //좌표값 결정에 대한 비컨 id 표현 - 수식 오류로 인한 수정
                //b1에 대한 id
                if (angle_s_b1_x == r_becon_1_c_o[0] && angle_s_b1_y == r_becon_1_c_o[1] && angle_s_b1_z == r_becon_1_c_o[2]) b1_id_o = s_beacon_1;
                else b1_id_o = s_beacon_2;

                //b2에 대한 id
                if (angle_s_b2_x == r_becon_2_c_o[0] && angle_s_b2_y == r_becon_2_c_o[1] && angle_s_b2_z == r_becon_2_c_o[2]) b2_id_o = s_beacon_2;
                else b2_id_o = s_beacon_3;

                //b3에 대한 id
                b3_id_o = s_beacon_0;

                // beacon_2_calc = 0;

                //비컨 id 출력
                //if (kalman_after == 1) Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> s_beacon_0 " + s_beacon_0 + " s_beacon_1 " + s_beacon_1 + " s_beacon_2 " + s_beacon_2 + " s_beacon_3 " + s_beacon_3);





                //처음 값이 들어온경우 값 저장 부분 - 추가된 부분(0711)
                if (first_const == 1)
                {


                    //좌표값 저장
                    save_b1_x_o = r_becon_1_c[0];
                    save_b1_y_o = r_becon_1_c[1];
                    save_b1_z_o = r_becon_1_c[2];

                    //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> find " + save_b1_x_o + " " + save_b1_y_o + " " + save_b1_z_o);

                    save_b2_x_o = r_becon_2_c[0];
                    save_b2_y_o = r_becon_2_c[1];
                    save_b2_z_o = r_becon_2_c[2];

                    save_b3_x_o = r_becon_3_c[0];
                    save_b3_y_o = r_becon_3_c[1];
                    save_b3_z_o = r_becon_3_c[2];

                    //비컨 아이디 저장
                    save_id_b1_o = save_id_b1;
                    save_id_b2_o = save_id_b2;
                    save_id_b3_o = save_id_b3;

                    //해당되는 거리값
                    //pb1
                    if (save_id_b1_o == 1)
                    {
                        save_p1b1_o = d_1_o[0];
                        save_p2b1_o = d_2_o[0];
                    }
                    else if (save_id_b1_o == 2)
                    {
                        save_p1b1_o = d_1_o[1];
                        save_p2b1_o = d_2_o[1];
                    }
                    else if (save_id_b1_o == 3)
                    {
                        save_p1b1_o = d_1_o[2];
                        save_p2b1_o = d_2_o[2];
                    }

                    //pb2
                    if (save_id_b2_o == 2)
                    {
                        save_p1b2_o = d_1_o[1];
                        save_p2b2_o = d_2_o[1];
                    }
                    else if (save_id_b2_o == 3)
                    {
                        save_p1b2_o = d_1_o[2];
                        save_p2b2_o = d_2_o[2];
                    }
                    else if (save_id_b2_o == 4)
                    {
                        save_p1b2_o = d_1_o[3];
                        save_p2b2_o = d_2_o[3];
                    }

                    //pb3
                    if (save_id_b3_o == 1)
                    {
                        save_p1b3_o = d_1_o[0];
                        save_p2b3_o = d_2_o[0];
                    }
                    else if (save_id_b3_o == 2)
                    {
                        save_p1b3_o = d_1_o[1];
                        save_p2b3_o = d_2_o[1];
                    }
                    else if (save_id_b3_o == 3)
                    {
                        save_p1b3_o = d_1_o[2];
                        save_p2b3_o = d_2_o[2];
                    }
                    else if (save_id_b3_o == 4)
                    {
                        save_p1b3_o = d_1_o[3];
                        save_p2b3_o = d_2_o[3];
                    }





                    ////거리값 출력
                    //d_1_c_o[0] = save_p1b1_o;
                    //d_1_c_o[1] = save_p1b2_o;
                    //d_1_c_o[2] = save_p1b3_o;

                    //d_2_c_o[0] = save_p2b1_o;
                    //d_2_c_o[1] = save_p2b2_o;
                    //d_2_c_o[2] = save_p2b3_o;

                    ////이전 거리값을 사용할경우 - 알고리즘 꼬이므로 주석
                    //if (check_calc == 1)
                    //{
                    //    ////거리 적용 - 오류거리값 방지를 위한 주석 처리
                    //    //d_1_c[0] = before_p1d1_save;
                    //    //d_1_c[1] = before_p1d2_save;
                    //    //d_1_c[2] = before_p1d3_save;

                    //    //d_2_c[0] = before_p2d1_save;
                    //    //d_2_c[1] = before_p2d2_save;
                    //    //d_2_c[2] = before_p2d3_save;

                    //    //해당되는 거리값 저장
                    //    //pb1
                    //    if (before_id_b1_save == 1)
                    //    {
                    //        save_p1b1_before_o = d_1_o[0];
                    //        save_p2b1_before_o = d_2_o[0];
                    //    }
                    //    else if (before_id_b1_save == 2)
                    //    {
                    //        save_p1b1_before_o = d_1_o[1];
                    //        save_p2b1_before_o = d_2_o[1];
                    //    }
                    //    else if (before_id_b1_save == 3)
                    //    {
                    //        save_p1b1_before_o = d_1_o[2];
                    //        save_p2b1_before_o = d_2_o[2];
                    //    }

                    //    //pb2
                    //    if (before_id_b2_save == 2)
                    //    {
                    //        save_p1b2_before_o = d_1_o[1];
                    //        save_p2b2_before_o = d_2_o[1];
                    //    }
                    //    else if (before_id_b2_save == 3)
                    //    {
                    //        save_p1b2_before_o = d_1_o[2];
                    //        save_p2b2_before_o = d_2_o[2];
                    //    }
                    //    else if (before_id_b2_save == 4)
                    //    {
                    //        save_p1b2_before_o = d_1_o[3];
                    //        save_p2b2_before_o = d_2_o[3];
                    //    }

                    //    //pb3
                    //    if (before_id_b3_save == 1)
                    //    {
                    //        save_p1b3_before_o = d_1_o[0];
                    //        save_p2b3_before_o = d_2_o[0];
                    //    }
                    //    else if (before_id_b3_save == 2)
                    //    {
                    //        save_p1b3_before_o = d_1_o[1];
                    //        save_p2b3_before_o = d_2_o[1];
                    //    }
                    //    else if (before_id_b3_save == 3)
                    //    {
                    //        save_p1b3_before_o = d_1_o[2];
                    //        save_p2b3_before_o = d_2_o[2];
                    //    }
                    //    else if (before_id_b3_save == 4)
                    //    {
                    //        save_p1b3_before_o = d_1_o[3];
                    //        save_p2b3_before_o = d_2_o[3];
                    //    }


                    //    //거리적용
                    //    d_1_c_o[0] = save_p1b1_before_o;
                    //    d_1_c_o[1] = save_p1b2_before_o;
                    //    d_1_c_o[2] = save_p1b3_before_o;

                    //    d_2_c_o[0] = save_p2b1_before_o;
                    //    d_2_c_o[1] = save_p2b2_before_o;
                    //    d_2_c_o[2] = save_p2b3_before_o;


                    //    //비컨 적용
                    //    r_becon_1_c_o[0] = before_b1x_save_o;
                    //    r_becon_1_c_o[1] = before_b1y_save_o;
                    //    r_becon_1_c_o[2] = before_b1z_save_o;

                    //    r_becon_2_c_o[0] = before_b2x_save_o;
                    //    r_becon_2_c_o[1] = before_b2y_save_o;
                    //    r_becon_2_c_o[2] = before_b2z_save_o;

                    //    r_becon_3_c_o[0] = before_b3x_save_o;
                    //    r_becon_3_c_o[1] = before_b3y_save_o;
                    //    r_becon_3_c_o[2] = before_b3z_save_o;

                    //    check_calc = 0;
                    //}

                    //if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2)//보기
                    //{
                    //    //사용된 비컨 출력부분
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_1 " + save_id_b1_o + " x " + r_becon_1_c_o[0] + " y " + r_becon_1_c_o[1] + " z " + r_becon_1_c_o[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_1 " + save_id_b2_o + " x " + r_becon_2_c_o[0] + " y " + r_becon_2_c_o[1] + " z " + r_becon_2_c_o[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_1 " + save_id_b3_o + " x " + r_becon_3_c_o[0] + " y " + r_becon_3_c_o[1] + " z " + r_becon_3_c_o[2]);

                    //    //사용된 거리값 확인                    
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_1 " + save_p1b1_o + " " + save_p1b2_o + " " + save_p1b3_o + " " + save_p2b1_o + " " + save_p2b2_o + " " + save_p2b3_o);
                    //}

                }
                else if (first_const == 0)//처음이 아닐경우
                {
                    //거리 저장된값 출력 - 저장시킨 거리값 사용시 필터가 무의미함 주석 처리
                    //d_1_c_o[0] = save_p1b1_o;
                    //d_1_c_o[1] = save_p1b2_o;
                    //d_1_c_o[2] = save_p1b3_o;

                    //d_2_c_o[0] = save_p2b1_o;
                    //d_2_c_o[1] = save_p2b2_o;
                    //d_2_c_o[2] = save_p2b3_o;

                    //거리값 결정
                    //pb1
                    if (save_id_b1_o == 1)
                    {
                        save_p1b1_o = d_1_o[0];
                        save_p2b1_o = d_2_o[0];
                    }
                    else if (save_id_b1_o == 2)
                    {
                        save_p1b1_o = d_1_o[1];
                        save_p2b1_o = d_2_o[1];
                    }
                    else if (save_id_b1_o == 3)
                    {
                        save_p1b1_o = d_1_o[2];
                        save_p2b1_o = d_2_o[2];
                    }

                    //pb2
                    if (save_id_b2_o == 2)
                    {
                        save_p1b2_o = d_1_o[1];
                        save_p2b2_o = d_2_o[1];
                    }
                    else if (save_id_b2_o == 3)
                    {
                        save_p1b2_o = d_1_o[2];
                        save_p2b2_o = d_2_o[2];
                    }
                    else if (save_id_b2_o == 4)
                    {
                        save_p1b2_o = d_1_o[3];
                        save_p2b2_o = d_2_o[3];
                    }

                    //pb3
                    if (save_id_b3_o == 1)
                    {
                        save_p1b3_o = d_1_o[0];
                        save_p2b3_o = d_2_o[0];
                    }
                    else if (save_id_b3_o == 2)
                    {
                        save_p1b3_o = d_1_o[1];
                        save_p2b3_o = d_2_o[1];
                    }
                    else if (save_id_b3_o == 3)
                    {
                        save_p1b3_o = d_1_o[2];
                        save_p2b3_o = d_2_o[2];
                    }
                    else if (save_id_b3_o == 4)
                    {
                        save_p1b3_o = d_1_o[3];
                        save_p2b3_o = d_2_o[3];
                    }

                    //해당 거리값 출력
                    d_1_c_o_0 = save_p1b1_o;
                    d_1_c_o_1 = save_p1b2_o;
                    d_1_c_o_2 = save_p1b3_o;

                    d_2_c_o_0 = save_p2b1_o;
                    d_2_c_o_1 = save_p2b2_o;
                    d_2_c_o_2 = save_p2b3_o;

                    //비컨 저장된값 출력
                    r_becon_1_c_o[0] = save_b1_x_o;
                    r_becon_1_c_o[1] = save_b1_y_o;
                    r_becon_1_c_o[2] = save_b1_z_o;

                    r_becon_2_c_o[0] = save_b2_x_o;
                    r_becon_2_c_o[1] = save_b2_y_o;
                    r_becon_2_c_o[2] = save_b2_z_o;

                    r_becon_3_c_o[0] = save_b3_x_o;
                    r_becon_3_c_o[1] = save_b3_y_o;
                    r_becon_3_c_o[2] = save_b3_z_o;



                    //if (before_d_value == 1)//이전거리 사용을 위한 부분 - 알고리즘 꼬이므로 주석
                    //{
                    //    ////이전거리 사용을 위한 저장 - 수식수정에 따른 주석처리
                    //    //before_p1d1_save = d_1_c[0];
                    //    //before_p1d2_save = d_1_c[1];
                    //    //before_p1d3_save = d_1_c[2];

                    //    //before_p2d1_save = d_2_c[0];
                    //    //before_p2d2_save = d_2_c[1];
                    //    //before_p2d3_save = d_2_c[2];

                    //    //비컨 아이디 저장
                    //    before_id_b1_save_o = save_id_b1_o;
                    //    before_id_b2_save_o = save_id_b2_o;
                    //    before_id_b3_save_o = save_id_b3_o;

                    //    //이전비컨 사용을 위한 저장
                    //    before_b1x_save_o = r_becon_1_c_o[0];
                    //    before_b1y_save_o = r_becon_1_c_o[1];
                    //    before_b1z_save_o = r_becon_1_c_o[2];

                    //    before_b2x_save_o = r_becon_2_c_o[0];
                    //    before_b2y_save_o = r_becon_2_c_o[1];
                    //    before_b2z_save_o = r_becon_2_c_o[2];

                    //    before_b3x_save_o = r_becon_3_c_o[0];
                    //    before_b3y_save_o = r_becon_3_c_o[1];
                    //    before_b3z_save_o = r_becon_3_c_o[2];

                    //    before_d_value = 0;
                    //}

                    //if (const_becon_p2_dozer == 1 || const_becon_p2_dozer == 2)//보기
                    //{
                    //    //사용된 비컨 출력부분 - 좌표값 표시로 인한 추가
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_2 " + save_id_b1_o + " x " + r_becon_1_c_o[0] + " y " + r_becon_1_c_o[1] + " z " + r_becon_1_c_o[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_2 " + save_id_b2_o + " x " + r_becon_2_c_o[0] + " y " + r_becon_2_c_o[1] + " z " + r_becon_2_c_o[2]);
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_2 " + save_id_b3_o + " x " + r_becon_3_c_o[0] + " y " + r_becon_3_c_o[1] + " z " + r_becon_3_c_o[2]);

                    //    //사용된 거리값 확인                      
                    //    Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 2_2 " + d_1_c_o[0] + " " + d_1_c_o[1] + " " + d_1_c_o[2] + " " + d_2_c_o[0] + " " + d_2_c_o[1] + " " + d_2_c_o[2]);
                    //}

                }
            }


            ////******************************************************************다운된 거리값 보정*************************************************************
            //----변환 부분 시작 비컨이 4개일 경우------
            //거리
            d_1_c_o_0 = d_1_c_o[0];
            d_1_c_o_1 = d_1_c_o[1];
            d_1_c_o_2 = d_1_c_o[2];

            d_2_c_o_0 = d_2_c_o[0];
            d_2_c_o_1 = d_2_c_o[1];
            d_2_c_o_2 = d_2_c_o[2];

            //비컨 - 로우값 계산으로 인한 수정
            r_becon_1_m_o[0] = r_becon_1_c_o[0];
            r_becon_1_m_o[1] = r_becon_1_c_o[1];
            r_becon_1_m_o[2] = r_becon_1_c_o[2];

            r_becon_2_m_o[0] = r_becon_2_c_o[0];
            r_becon_2_m_o[1] = r_becon_2_c_o[1];
            r_becon_2_m_o[2] = r_becon_2_c_o[2];

            r_becon_3_m_o[0] = r_becon_3_c_o[0];
            r_becon_3_m_o[1] = r_becon_3_c_o[1];
            r_becon_3_m_o[2] = r_becon_3_c_o[2];

            //Debug.WriteLine(" cB1_oP1 : " + d_1_c_o_0 + " cB2_oP1 : " + d_1_c_o_1 + " cB3_oP1 : " + d_1_c_o_2);


            //----변환 부분 끝------

            //P1일 경우
            if (d_1_c_o_0 == 111111) d_1_c_o_0 = d_1_t_o[0];
            d_1_t_o[0] = d_1_c_o_0;

            if (d_1_c_o_1 == 222222) d_1_c_o_1 = d_1_t_o[1];
            d_1_t_o[1] = d_1_c_o_1;

            if (d_1_c_o_2 == 333333) d_1_c_o_2 = d_1_t_o[2];
            d_1_t_o[2] = d_1_c_o_2;

            //P2일 경우
            if (d_2_c_o_0 == 111111) d_2_c_o_0 = d_2_t_o[0];
            d_2_t_o[0] = d_2_c_o_0;

            if (d_2_c_o_1 == 222222) d_2_c_o_1 = d_2_t_o[1];
            d_2_t_o[1] = d_2_c_o_1;

            if (d_2_c_o_2 == 333333) d_2_c_o_2 = d_2_t_o[2];
            d_2_t_o[2] = d_2_c_o_2;

            //******************************************************************becon의 변형된 위치*************************************************************

            becon_1_1_o[0] = r_becon_1_m_o[0] - r_becon_1_m_o[0]; becon_1_1_o[1] = r_becon_1_m_o[1] - r_becon_1_m_o[1]; becon_1_1_o[2] = r_becon_1_m_o[2] - r_becon_1_m_o[2];//비컨1
            becon_2_1_o[0] = r_becon_2_m_o[0] - r_becon_1_m_o[0]; becon_2_1_o[1] = r_becon_2_m_o[1] - r_becon_1_m_o[1]; becon_2_1_o[2] = r_becon_2_m_o[2] - r_becon_1_m_o[2];//비컨2
            becon_3_1_o[0] = r_becon_3_m_o[0] - r_becon_1_m_o[0]; becon_3_1_o[1] = r_becon_3_m_o[1] - r_becon_1_m_o[1]; becon_3_1_o[2] = r_becon_3_m_o[2] - r_becon_1_m_o[2];//비컨3

            //*********************************************************************평면의 방정식*****************************************************************
            a_1_o = 2 * becon_2_1_o[0]; b_1_o = 2 * becon_2_1_o[1]; c_1_o = 2 * becon_2_1_o[2];
            a_2_o = 2 * becon_3_1_o[0]; b_2_o = 2 * becon_3_1_o[1]; c_2_o = 2 * becon_3_1_o[2];

            //////////////////P1일 경우///////////////////
            p_1_w_1_o = Math.Pow(d_1_t_o[0], 2) - Math.Pow(d_1_t_o[1], 2) + Math.Pow(becon_2_1_o[0], 2) + Math.Pow(becon_2_1_o[1], 2) + Math.Pow(becon_2_1_o[2], 2);
            p_1_w_2_o = Math.Pow(d_1_t_o[0], 2) - Math.Pow(d_1_t_o[2], 2) + Math.Pow(becon_3_1_o[0], 2) + Math.Pow(becon_3_1_o[1], 2) + Math.Pow(becon_3_1_o[2], 2);

            //////////////////P2일 경우///////////////////
            p_2_w_1_o = Math.Pow(d_2_t_o[0], 2) - Math.Pow(d_2_t_o[1], 2) + Math.Pow(becon_2_1_o[0], 2) + Math.Pow(becon_2_1_o[1], 2) + Math.Pow(becon_2_1_o[2], 2);
            p_2_w_2_o = Math.Pow(d_2_t_o[0], 2) - Math.Pow(d_2_t_o[2], 2) + Math.Pow(becon_3_1_o[0], 2) + Math.Pow(becon_3_1_o[1], 2) + Math.Pow(becon_3_1_o[2], 2);

            //*******************************************************************2차 연립방정식******************************************************************
            s_a_1_o = (c_1_o * a_2_o - c_2_o * a_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            s_a_2_o = (c_2_o * b_1_o - c_1_o * b_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //////////////////P1일 경우////////////////////
            p_1_s_b_1_o = (a_1_o * p_1_w_2_o - a_2_o * p_1_w_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            p_1_s_b_2_o = (b_2_o * p_1_w_1_o - b_1_o * p_1_w_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //////////////////P2일 경우////////////////////
            p_2_s_b_1_o = (a_1_o * p_2_w_2_o - a_2_o * p_2_w_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            p_2_s_b_2_o = (b_2_o * p_2_w_1_o - b_1_o * p_2_w_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //******************************************************************z에 관한 2차 방정식****************************************************************
            Aa_o = Math.Pow(s_a_2_o, 2) + Math.Pow(s_a_1_o, 2) + 1;

            //////////////////P1일 경우////////////////////
            p_1_Bb_o = s_a_2_o * p_1_s_b_2_o + s_a_1_o * p_1_s_b_1_o;
            p_1_Cc_o = Math.Pow(p_1_s_b_2_o, 2) + Math.Pow(p_1_s_b_1_o, 2) - Math.Pow(d_1_t_o[0], 2);

            //////////////////P2일 경우////////////////////
            p_2_Bb_o = s_a_2_o * p_2_s_b_2_o + s_a_1_o * p_2_s_b_1_o;
            p_2_Cc_o = Math.Pow(p_2_s_b_2_o, 2) + Math.Pow(p_2_s_b_1_o, 2) - Math.Pow(d_2_t_o[0], 2);


            //******************************************************************becon의 오차 계산*************************************************************
            ////수신기 1번 계산 판별
            //if (Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o < 0)
            //{

            //    //Debug.Write(" P1_N : ");
            //    //---------------비콘 사이의 거리 계산-----------------
            //    B1_oB2_o_o = Math.Sqrt(Math.Pow(r_becon_2_m_o[0] - r_becon_1_m_o[0], 2) + Math.Pow(r_becon_2_m_o[1] - r_becon_1_m_o[1], 2) + Math.Pow(r_becon_2_m_o[2] - r_becon_1_m_o[2], 2));
            //    B1_oB3_o_o = Math.Sqrt(Math.Pow(r_becon_3_m_o[0] - r_becon_1_m_o[0], 2) + Math.Pow(r_becon_3_m_o[1] - r_becon_1_m_o[1], 2) + Math.Pow(r_becon_3_m_o[2] - r_becon_1_m_o[2], 2));
            //    B2_oB3_o_o = Math.Sqrt(Math.Pow(r_becon_3_m_o[0] - r_becon_2_m_o[0], 2) + Math.Pow(r_becon_3_m_o[1] - r_becon_2_m_o[1], 2) + Math.Pow(r_becon_3_m_o[2] - r_becon_2_m_o[2], 2));

            //    //-------------------------------------------------------------------수신기 1 계산------------------------------------------------------
            //    //---------------구의 반지름 계산(수신기 1)-----------------
            //    D1T_u_o = d_1_t_o[0] + (B1_oB2_o_o + 2 - d_1_t_o[0] - d_1_t_o[1]) / 2;
            //    D2T_u_o = d_1_t_o[1] + (B1_oB2_o_o + 2 - d_1_t_o[0] - d_1_t_o[1]) / 2;

            //    D1T_d_o = d_1_t_o[0] + (B1_oB3_o_o + 2 - d_1_t_o[0] - d_1_t_o[2]) / 2;
            //    D3T_u_o = d_1_t_o[2] + (B1_oB3_o_o + 2 - d_1_t_o[0] - d_1_t_o[2]) / 2;

            //    D2T_d_o = d_1_t_o[1] + (B2_oB3_o_o + 2 - d_1_t_o[1] - d_1_t_o[2]) / 2;
            //    D3T_d_o = d_1_t_o[2] + (B2_oB3_o_o + 2 - d_1_t_o[1] - d_1_t_o[2]) / 2;

            //    D1T_1_o = (D1T_u_o - D1T_d_o > 0) ? D1T_u_o : D1T_d_o;
            //    D2T_1_o = (D2T_u_o - D2T_d_o > 0) ? D2T_u_o : D2T_d_o;
            //    D3T_1_o = (D3T_u_o - D3T_d_o > 0) ? D3T_u_o : D3T_d_o;

            //    D1T_2_o = (d_1_t_o[0] - D1T_1_o > 0) ? d_1_t_o[0] : D1T_1_o;
            //    D2T_2_o = (d_1_t_o[1] - D2T_1_o > 0) ? d_1_t_o[1] : D2T_1_o;
            //    D3T_2_o = (d_1_t_o[2] - D3T_1_o > 0) ? d_1_t_o[2] : D3T_1_o;

            //    //---------------직선의 방정식(수신기 1)-----------------
            //    A1_o = 2 * (r_becon_2_m_o[0] - r_becon_1_m_o[0]);
            //    B1_o = 2 * (r_becon_2_m_o[1] - r_becon_1_m_o[1]);
            //    C1_o = Math.Pow(r_becon_1_m_o[0], 2) + Math.Pow(r_becon_1_m_o[1], 2) - Math.Pow(r_becon_2_m_o[0], 2) - Math.Pow(r_becon_2_m_o[1], 2) - Math.Pow(D1T_2_o, 2) + Math.Pow(D2T_2_o, 2);

            //    A2_o = 2 * (r_becon_3_m_o[0] - r_becon_1_m_o[0]);
            //    B2_o = 2 * (r_becon_3_m_o[1] - r_becon_1_m_o[1]);
            //    C2_o = Math.Pow(r_becon_1_m_o[0], 2) + Math.Pow(r_becon_1_m_o[1], 2) - Math.Pow(r_becon_3_m_o[0], 2) - Math.Pow(r_becon_3_m_o[1], 2) - Math.Pow(D1T_2_o, 2) + Math.Pow(D3T_2_o, 2);

            //    A3_o = 2 * (r_becon_3_m_o[0] - r_becon_2_m_o[0]);
            //    B3_o = 2 * (r_becon_3_m_o[1] - r_becon_2_m_o[1]);
            //    C3_o = Math.Pow(r_becon_2_m_o[0], 2) + Math.Pow(r_becon_2_m_o[1], 2) - Math.Pow(r_becon_3_m_o[0], 2) - Math.Pow(r_becon_3_m_o[1], 2) - Math.Pow(D2T_2_o, 2) + Math.Pow(D3T_2_o, 2);

            //    //---------------교점 계산(수신기 1)-----------------
            //    y1_o = (-C1_o * A2_o + A1_o * C2_o) / (B1_o * A2_o - A1_o * B2_o);
            //    x1_o = -(C1_o + B1_o * y1_o) / A1_o;

            //    y2_o = (-C1_o * A3_o + A1_o * C3_o) / (B1_o * A3_o - A1_o * B3_o);
            //    x2_o = -(C1_o + B1_o * y2_o) / A1_o;

            //    y3_o = (-C2_o * A3_o + A2_o * C3_o) / (B2_o * A3_o - A2_o * B3_o);
            //    x3_o = -(C2_o + B2_o * y3_o) / A2_o;

            //    //--------------삼각형의 무게중심(수신기 1)-------------
            //    cx_o = (x1_o + x2_o + x3_o) / 3;
            //    cy_o = (y1_o + y2_o + y3_o) / 3;

            //    ////임시
            //    //cx_o = 1111111111111;
            //    //cy_o = 1111111111111;

            //    //p_calc_1_o = 1;

            //    //--------------------------------------------------------------------------------------------------------------------------------------
            //    //************************************************************************값 출력***********************************************************************
            //    //////////////////P1일 경우////////////////////
            //    result_1[0] = cx_o; result_1[1] = cy_o; result_1[2] = p_1_u_o[2];
            //    result_1[3] = cx_o; result_1[4] = cy_o; result_1[5] = p_1_d_o[2];

            //    //Debug.Write(A1_o + " " + A2_o);
            //}

            //else if (Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o >= 0)
            //{
            //Debug.Write(" P1_O");
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P1일 경우////////////////////
            p_1_t_u_o[2] = (-p_1_Bb_o + Math.Sqrt(Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o)) / Aa_o; p_1_t_d_o[2] = (-p_1_Bb_o - Math.Sqrt(Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o)) / Aa_o;
            p_1_t_u_o[0] = s_a_2_o * p_1_t_u_o[2] + p_1_s_b_2_o; p_1_t_d_o[0] = s_a_2_o * p_1_t_d_o[2] + p_1_s_b_2_o;
            p_1_t_u_o[1] = s_a_1_o * p_1_t_u_o[2] + p_1_s_b_1_o; p_1_t_d_o[1] = s_a_1_o * p_1_t_d_o[2] + p_1_s_b_1_o;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P1일 경우////////////////////
            p_1_u_o[0] = p_1_t_u_o[0] + r_becon_1_m_o[0]; p_1_u_o[1] = p_1_t_u_o[1] + r_becon_1_m_o[1]; p_1_u_o[2] = p_1_t_u_o[2] + r_becon_1_m_o[2];
            p_1_d_o[0] = p_1_t_d_o[0] + r_becon_1_m_o[0]; p_1_d_o[1] = p_1_t_d_o[1] + r_becon_1_m_o[1]; p_1_d_o[2] = p_1_t_d_o[2] + r_becon_1_m_o[2];

            //임시
            //p_1_u[0] = 22222; p_1_u[1] = 22222; p_1_u[2] = p_1_t_u_o[2] + r_becon_1_m_o[2];
            //p_1_d[0] = 22222; p_1_d[1] = 22222; p_1_d[2] = p_1_t_d_o[2] + r_becon_1_m_o[2];


            //p_calc_1_o = 0;

            //************************************************************************값 출력***********************************************************************
            //////////////////P1일 경우////////////////////
            result_1[0] = p_1_u_o[0]; result_1[1] = p_1_u_o[1]; result_1[2] = p_1_u_o[2];
            result_1[3] = p_1_d_o[0]; result_1[4] = p_1_d_o[1]; result_1[5] = p_1_d_o[2];
            // }

            ////수신기 2번 계산 판별
            //if (Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o < 0)
            //{
            //    //Debug.Write(" P2_N : ");
            //    //---------------비콘 사이의 거리 계산-----------------
            //    B1_oB2_o_o = Math.Sqrt(Math.Pow(r_becon_2_m_o[0] - r_becon_1_m_o[0], 2) + Math.Pow(r_becon_2_m_o[1] - r_becon_1_m_o[1], 2) + Math.Pow(r_becon_2_m_o[2] - r_becon_1_m_o[2], 2));
            //    B1_oB3_o_o = Math.Sqrt(Math.Pow(r_becon_3_m_o[0] - r_becon_1_m_o[0], 2) + Math.Pow(r_becon_3_m_o[1] - r_becon_1_m_o[1], 2) + Math.Pow(r_becon_3_m_o[2] - r_becon_1_m_o[2], 2));
            //    B2_oB3_o_o = Math.Sqrt(Math.Pow(r_becon_3_m_o[0] - r_becon_2_m_o[0], 2) + Math.Pow(r_becon_3_m_o[1] - r_becon_2_m_o[1], 2) + Math.Pow(r_becon_3_m_o[2] - r_becon_2_m_o[2], 2));

            //    //-------------------------------------------------------------------수신기 2 계산------------------------------------------------------
            //    //---------------구의 반지름 계산(수신기 2)-----------------
            //    D1T_u_2_o = d_2_t_o[0] + (B1_oB2_o_o + 2 - d_2_t_o[0] - d_2_t_o[1]) / 2;
            //    D2T_u_2_o = d_2_t_o[1] + (B1_oB2_o_o + 2 - d_2_t_o[0] - d_2_t_o[1]) / 2;

            //    D1T_d_2_o = d_2_t_o[0] + (B1_oB3_o_o + 2 - d_2_t_o[0] - d_2_t_o[2]) / 2;
            //    D3T_u_2_o = d_2_t_o[2] + (B1_oB3_o_o + 2 - d_2_t_o[0] - d_2_t_o[2]) / 2;

            //    D2T_d_2_o = d_2_t_o[1] + (B2_oB3_o_o + 2 - d_2_t_o[1] - d_2_t_o[2]) / 2;
            //    D3T_d_2_o = d_2_t_o[2] + (B2_oB3_o_o + 2 - d_2_t_o[1] - d_2_t_o[2]) / 2;

            //    D1T_1_2_o = (D1T_u_2_o - D1T_d_2_o > 0) ? D1T_u_2_o : D1T_d_2_o;
            //    D2T_1_2_o = (D2T_u_2_o - D2T_d_2_o > 0) ? D2T_u_2_o : D2T_d_2_o;
            //    D3T_1_2_o = (D3T_u_2_o - D3T_d_2_o > 0) ? D3T_u_2_o : D3T_d_2_o;

            //    D1T_2_2_o = (d_2_t_o[0] - D1T_1_2_o > 0) ? d_2_t_o[0] : D1T_1_2_o;
            //    D2T_2_2_o = (d_2_t_o[1] - D2T_1_2_o > 0) ? d_2_t_o[1] : D2T_1_2_o;
            //    D3T_2_2_o = (d_2_t_o[2] - D3T_1_2_o > 0) ? d_2_t_o[2] : D3T_1_2_o;

            //    //---------------직선의 방정식(수신기 2)-----------------
            //    A1_2_o = 2 * (r_becon_2_m_o[0] - r_becon_1_m_o[0]);
            //    B1_2_o = 2 * (r_becon_2_m_o[1] - r_becon_1_m_o[1]);
            //    C1_2_o = Math.Pow(r_becon_1_m_o[0], 2) + Math.Pow(r_becon_1_m_o[1], 2) - Math.Pow(r_becon_2_m_o[0], 2) - Math.Pow(r_becon_2_m_o[1], 2) - Math.Pow(D1T_2_2_o, 2) + Math.Pow(D2T_2_2_o, 2);

            //    A2_2_o = 2 * (r_becon_3_m_o[0] - r_becon_1_m_o[0]);
            //    B2_2_o = 2 * (r_becon_3_m_o[1] - r_becon_1_m_o[1]);
            //    C2_2_o = Math.Pow(r_becon_1_m_o[0], 2) + Math.Pow(r_becon_1_m_o[1], 2) - Math.Pow(r_becon_3_m_o[0], 2) - Math.Pow(r_becon_3_m_o[1], 2) - Math.Pow(D1T_2_2_o, 2) + Math.Pow(D3T_2_2_o, 2);

            //    A3_2_o = 2 * (r_becon_3_m_o[0] - r_becon_2_m_o[0]);
            //    B3_2_o = 2 * (r_becon_3_m_o[1] - r_becon_2_m_o[1]);
            //    C3_2_o = Math.Pow(r_becon_2_m_o[0], 2) + Math.Pow(r_becon_2_m_o[1], 2) - Math.Pow(r_becon_3_m_o[0], 2) - Math.Pow(r_becon_3_m_o[1], 2) - Math.Pow(D2T_2_2_o, 2) + Math.Pow(D3T_2_2_o, 2);

            //    //---------------교점 계산(수신기 1)-----------------
            //    y1_2_o = (-C1_2_o * A2_2_o + A1_2_o * C2_2_o) / (B1_2_o * A2_2_o - A1_2_o * B2_2_o);
            //    x1_2_o = -(C1_2_o + B1_2_o * y1_2_o) / A1_2_o;

            //    y2_2_o = (-C1_2_o * A3_2_o + A1_2_o * C3_2_o) / (B1_2_o * A3_2_o - A1_2_o * B3_2_o);
            //    x2_2_o = -(C1_2_o + B1_2_o * y2_2_o) / A1_2_o;

            //    y3_2_o = (-C2_2_o * A3_2_o + A2_2_o * C3_2_o) / (B2_2_o * A3_2_o - A2_2_o * B3_2_o);
            //    x3_2_o = -(C2_2_o + B2_2_o * y3_2_o) / A2_2_o;

            //    //--------------삼각형의 무게중심(수신기 1)-------------
            //    cx_2_o = (x1_2_o + x2_2_o + x3_2_o) / 3;
            //    cy_2_o = (y1_2_o + y2_2_o + y3_2_o) / 3;


            //    ////임시
            //    //cx_2_o = 11111111;
            //    //cy_2_o = 11111111;

            //    //p_calc_2_o = 1;

            //    //---------------------------------------------------------------------------------------------------------------------------------------

            //    //************************************************************************값 출력***********************************************************************
            //    //////////////////P2일 경우////////////////////
            //    result_1[6] = cx_2_o; result_1[7] = cy_2_o; result_1[8] = p_2_u_o[2];
            //    result_1[9] = cx_2_o; result_1[10] = cy_2_o; result_1[11] = p_2_d_o[2];

            //    //Debug.Write(A1_2_o + " " + A2_2_o);
            //}

            //else if (Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o >= 0)
            //{
            //Debug.Write(" P2_O");
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P2일 경우////////////////////
            p_2_t_u_o[2] = (-p_2_Bb_o + Math.Sqrt(Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o)) / Aa_o; p_2_t_d_o[2] = (-p_2_Bb_o - Math.Sqrt(Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o)) / Aa_o;
            p_2_t_u_o[0] = s_a_2_o * p_2_t_u_o[2] + p_2_s_b_2_o; p_2_t_d_o[0] = s_a_2_o * p_2_t_d_o[2] + p_2_s_b_2_o;
            p_2_t_u_o[1] = s_a_1_o * p_2_t_u_o[2] + p_2_s_b_1_o; p_2_t_d_o[1] = s_a_1_o * p_2_t_d_o[2] + p_2_s_b_1_o;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P2일 경우////////////////////
            p_2_u_o[0] = p_2_t_u_o[0] + r_becon_1_m_o[0]; p_2_u_o[1] = p_2_t_u_o[1] + r_becon_1_m_o[1]; p_2_u_o[2] = p_2_t_u_o[2] + r_becon_1_m_o[2];
            p_2_d_o[0] = p_2_t_d_o[0] + r_becon_1_m_o[0]; p_2_d_o[1] = p_2_t_d_o[1] + r_becon_1_m_o[1]; p_2_d_o[2] = p_2_t_d_o[2] + r_becon_1_m_o[2];

            //임시
            //p_2_u[0] = 22222222; p_2_u[1] = 2222222222; p_2_u[2] = p_2_t_u_o[2] + r_becon_1_m_o[2];
            //p_2_d[0] = 22222222; p_2_d[1] = 2222222222; p_2_d[2] = p_2_t_d_o[2] + r_becon_1_m_o[2];

            //p_calc_2_o = 0;


            //************************************************************************값 출력***********************************************************************
            //////////////////P2일 경우////////////////////
            result_1[6] = p_2_u_o[0]; result_1[7] = p_2_u_o[1]; result_1[8] = p_2_u_o[2];
            result_1[9] = p_2_d_o[0]; result_1[10] = p_2_d_o[1]; result_1[11] = p_2_d_o[2];

            //}

            return result_1;
        }




        double[] Calc_dozer_point(double[] beacon_p1, double[] beacon_p2, double[] c_beacon_p1, double[] c_beacon_p2)
        {
            double[] result = new double[89];//평균값 추가에 따른 - 수정된 부분(0717)

            //length
            double[] pbb_length = new double[3];
            double[] length = new double[3];
            double[] body_length = new double[3];

            //수정된 모델의 수정된 수식 적용 - 수정된 부분(0712)
            double[] p4_left_length = new double[3];
            double[] p4_right_length = new double[3];

            double[] front_length = new double[3];
            double[] blade_right_length = new double[3];
            double[] blade_left_length = new double[3];
            double[] pbpc_length = new double[3];

            //절대 좌표계
            double[] r_r2_abs_receiving_set = new double[6];//수신기(상대 -> 절대)
            double[] abs_e = new double[6];//끝단
            double[] abs_p = new double[3];//위치
            double[] abs_pc = new double[3];//중심
            double[] abs_p3 = new double[3];//힌지
            double[] abs_front_p = new double[3];//블레이드 중심

            //위치 좌표계
            double[] p3_p = new double[3];
            double[] pc_p = new double[3];
            double[] pbb_p = new double[3];

            //힌지 좌표계
            // 수식 변경에 따른 추가된 부분(0713)
            double[] p4_left_p_h = new double[3];
            double[] p4_right_p_h = new double[3];

            double[] front_p_h = new double[3];
            double[] front_left_p_h = new double[3];
            double[] front_right_p_h = new double[3];
            double[] blade_right_down_p_h = new double[3];
            double[] blade_left_down_p_h = new double[3];

            double[] front_p_h_t = new double[3];
            double[] blade_left_down_p_h_t = new double[3];
            double[] blade_right_down_p_h_t = new double[3];

            //임의 좌표계
            double[] r2_becon_1 = new double[3];//비콘1
            double[] r2_becon_2 = new double[3];//비콘2
            double[] r2_becon_3 = new double[3];//비콘3
            double[] r2_becon_4 = new double[3];//비콘4 - 비컨이 4개일경우


            double[] r2_instrument_station_2 = new double[3];//측점2
            double[] r2_instrument_station_3 = new double[3];//측점3

            double[] r_r2_receiving_set = new double[6];//수신기(상대 -> 임의)

            double b_angle_2 = 0;

            double[] r2_blade_right_down_p = new double[3];//끝단1
            double[] r2_blade_left_down_p = new double[3];//끝단2

            double[] r2_pb = new double[3];//위치
            double[] r2_pc = new double[3];//중심
            double[] r2_p3 = new double[3];//힌지
            double[] r2_front_p = new double[3];//블레이드 중심

            //----------------수정된 모델 적용
            double[] r2_front_left_p_h  = new double[3];
            double[] r2_front_right_p_h = new double[3];

            double[] r2_front_left_p_h_t = new double[3];

            double blade_atan = 0;

            //---------상대 좌표계  
            //추가된 부분(0718)
            double[] p4_left_p = new double[3];
            double[] p4_right_p = new double[3];

            double[] pb = new double[3];
            double[] pc = new double[3];
            double[] p3 = new double[3];
            double[] front_p = new double[3];
            double[] blade_right_down_p = new double[3];
            double[] blade_left_down_p = new double[3];

            double a_angle = 0, b_angle = 0;
            double[] move_p1 = new double[3];
            double p1_atan = 0;

            //비컨이 4개로 인한
            double[] r_becon_1_v = new double[3];
            double[] r_becon_2_v = new double[3];
            double[] r_becon_3_v = new double[3];
            double[] r_becon_4_v = new double[3];

            //------------ultrasonic_y
            double ultrasonic_y = 0;

            //------------수신기 출력
            double[] find_p1_2 = new double[3];
            double[] find_p2_2 = new double[3];

            //string real_time = System.DateTime.Now.ToString("HH:mm:ss");

            //--------------yaw calc
            double calc_body_y = 0;
            double p1_calibration = 0;
            double calc_blade_calibration = 0;//수정된 모델에 의한 - 추가된 부분(0623)

            //-------------orientation값 보정
            double orientation = 0;
            double orientation_o = 0;//orientation 계산 판별 변수 - 추가된 부분(0624)

            double frontL_ori = 0;
            double frontR_ori = 0;

            double front_ori_unity = 0;//평균값 계산을 위한 추가된 부분(0625)

            double calc_blade_orientation = 0;//수정된 모델에 의한 - 추가된 부분(0623)
            double blade_ori = 0;

            //--------------yaw값 보정
            double body_y_1 = 0;

            double frontL_y_1 = 0, frontL_y_2 = 0;
            double frontR_y_1 = 0, frontR_y_2 = 0;

            double r_frontL_a = 0;
            double r_frontR_a = 0;

            double multy_a_L = 0;
            double multy_a_R = 0;

            double y_frontL_a = 0;
            double y_frontR_a = 0;

            double frontL_yaw = 0;
            double frontR_yaw = 0;

            //기존의 모델 yaw 값 계산
            //double blade_y_1 = 0, blade_y_2 = 0;
            //double r_blade_a = 0;
            //double multy_b = 0;
            //double y_blade_a = 0;

            double blade_yaw = 0;

            //---------각도 추가에 따른 추가된 부분(0711)-----------
            //수평거리 부분
            double p1b1_flat_distance = 0;
            double p1b2_flat_distance = 0;
            double p1b3_flat_distance = 0;
            double p1b4_flat_distance = 0;

            double p2b1_flat_distance = 0;
            double p2b2_flat_distance = 0;
            double p2b3_flat_distance = 0;
            double p2b4_flat_distance = 0;

            //각도 부분
            double p1b1_angle = 0;
            double p1b2_angle = 0;
            double p1b3_angle = 0;
            double p1b4_angle = 0;

            double p2b1_angle = 0;
            double p2b2_angle = 0;
            double p2b3_angle = 0;
            double p2b4_angle = 0;

            //각도 평균화
            double b1_angle = 0;
            double b2_angle = 0;
            double b3_angle = 0;
            double b4_angle = 0;

            //가장 큰 각도 계산
            double a_angle_N1 = 0;
            double a_angle_N2 = 0;
            double a_angle_N3 = 0;

            ////각도 계산 사용될 부분
            //double b1_angle_c = 0;
            //double b2_angle_c = 0;
            //double b3_angle_c = 0;
            //double b4_angle_c = 0;

            //아크코사인의 정의역이 범위를 벗어나는경우
            double p1b1_angle_value = 0;
            double p1b2_angle_value = 0;
            double p1b3_angle_value = 0;
            double p1b4_angle_value = 0;

            double p2b1_angle_value = 0;
            double p2b2_angle_value = 0;
            double p2b3_angle_value = 0;
            double p2b4_angle_value = 0;

            //수정된 도저 모델의 암 각도 보정식 부분 상수 선언 - 추가된 부분(0713)
            double a_const = 0, b_const = 0, c_const = 0;//입력 부분
            double calc_alpha = 0;//출력 부분

            //*******************************************************************************각도 부분*********************************************************************

            dozer_body_imu[0] = Convert.ToDouble(DozerData.Motion.Substring(10, 4)) / 10 - 200;     dozer_blade_imu[0] = Convert.ToDouble(DozerData.Motion.Substring(22, 4)) / 10 - 200;      dozer_front_imu_L[0] = Convert.ToDouble(DozerData.Motion.Substring(34, 4)) / 10 - 200;            dozer_front_imu_R[0] = Convert.ToDouble(DozerData.Motion.Substring(46, 4)) / 10 - 200;
            dozer_body_imu[1] = Convert.ToDouble(DozerData.Motion.Substring(6, 4)) / 10 - 200;      dozer_blade_imu[1] = Convert.ToDouble(DozerData.Motion.Substring(18, 4)) / 10 - 200;      dozer_front_imu_L[1] = Convert.ToDouble(DozerData.Motion.Substring(30, 4)) / 10 - 200;            dozer_front_imu_R[1] = Convert.ToDouble(DozerData.Motion.Substring(42, 4)) / 10 - 200;

            //프론트 R의 위치 수정에 따른 - 추가된 부분(0709)
            dozer_front_imu_R[0] *= -1;
            dozer_front_imu_R[1] *= -1;

            //Debug.Write(" roll  : " + dozer_front_imu_R[0] + " pitch : " + dozer_front_imu_R[1]);

            

            //imu 추가에 따른 수정
            //dozer_ultrasonic_left  = Convert.ToDouble(DozerData.Motion.Substring(38, 4));
            //dozer_ultrasonic_right = Convert.ToDouble(DozerData.Motion.Substring(42, 4));
            dozer_ultrasonic_left = Convert.ToDouble(DozerData.Motion.Substring(50, 4));
            dozer_ultrasonic_right = Convert.ToDouble(DozerData.Motion.Substring(54, 4));



            //출력 - imu 추가에 따른 수정
            //DozerData.Original.IMUSensor = DozerData.Motion.Substring(10, 4) + " " + DozerData.Motion.Substring(6, 4) + " "
            //                             + DozerData.Motion.Substring(34, 4) + " " + DozerData.Motion.Substring(30, 4) + " "
            //                             + DozerData.Motion.Substring(22, 4) + " " + DozerData.Motion.Substring(18, 4) + " "
            //                             + DozerData.Motion.Substring(38, 4) + " " + DozerData.Motion.Substring(42, 4);

            DozerData.Original.IMUSensor = DozerData.Motion.Substring(10, 4) + " " + DozerData.Motion.Substring(6, 4) + " "
                                         + DozerData.Motion.Substring(34, 4) + " " + DozerData.Motion.Substring(30, 4) + " "
                                         + DozerData.Motion.Substring(46, 4) + " " + DozerData.Motion.Substring(42, 4) + " "
                                         + DozerData.Motion.Substring(22, 4) + " " + DozerData.Motion.Substring(18, 4) + " "
                                         + DozerData.Motion.Substring(50, 4) + " " + DozerData.Motion.Substring(54, 4);



            ////imu_calibration
            dozer_body_imu[0] += body_calibration[0];
            dozer_body_imu[1] += body_calibration[1];

            dozer_front_imu_L[0] += frontL_calibration[0];
            dozer_front_imu_L[1] += frontL_calibration[1];

            //imu 추가
            dozer_front_imu_R[0] += frontR_calibration[0];
            dozer_front_imu_R[1] += frontR_calibration[1];

            dozer_blade_imu[0] += blade_calibration[0];
            dozer_blade_imu[1] += blade_calibration[1];

            //유니티 표현을 위한 프론트 롤, 피치 값 계산(평균값) - 수정된 부분(0713)
            dozer_front_imu_roll_unity = (dozer_front_imu_L[0] + dozer_front_imu_R[0]) / 2;//프론트 롤값 계산
            dozer_front_imu_pitch_unity = (dozer_front_imu_L[1] + dozer_front_imu_R[1]) / 2;//프론트 피치값 계산


            //각도 변경(deg를 rad으로)
            dozer_body_imu[0] *= (Math.PI / 180.0);    dozer_blade_imu[0] *= (Math.PI / 180.0);      dozer_front_imu_L[0] *= (Math.PI / 180.0);          dozer_front_imu_R[0] *= (Math.PI / 180.0);
            dozer_body_imu[1] *= (Math.PI / 180.0);    dozer_blade_imu[1] *= (Math.PI / 180.0);      dozer_front_imu_L[1] *= (Math.PI / 180.0);          dozer_front_imu_R[1] *= (Math.PI / 180.0);

            //**********************************************************************좌표 변환(절대 -> 상대)*********************************************************************
            /////////////////////////////////////////////////////////////////////////절대 -> 임의 좌표계///////////////////////////////////////////////////////////////////
            //측점2
            r2_instrument_station_2[0] = instrument_station_2[0] - instrument_station_1[0];
            r2_instrument_station_2[1] = instrument_station_2[1] - instrument_station_1[1];
            r2_instrument_station_2[2] = instrument_station_2[2] - instrument_station_1[2];

            /*
            // 측점3
            r2_instrument_station_3[0] = instrument_station_3[0] - instrument_station_1[0];
            r2_instrument_station_3[1] = instrument_station_3[1] - instrument_station_1[1];
            r2_instrument_station_3[2] = instrument_station_3[2] - instrument_station_1[2];
            */

            //비콘1
            r2_becon_1[0] = becon_1[0] - instrument_station_1[0];
            r2_becon_1[1] = becon_1[1] - instrument_station_1[1];
            r2_becon_1[2] = becon_1[2] - instrument_station_1[2];

            //비콘2
            r2_becon_2[0] = becon_2[0] - instrument_station_1[0];
            r2_becon_2[1] = becon_2[1] - instrument_station_1[1];
            r2_becon_2[2] = becon_2[2] - instrument_station_1[2];

            //비콘3
            r2_becon_3[0] = becon_3[0] - instrument_station_1[0];
            r2_becon_3[1] = becon_3[1] - instrument_station_1[1];
            r2_becon_3[2] = becon_3[2] - instrument_station_1[2];

            //비콘4
            r2_becon_4[0] = becon_4[0] - instrument_station_1[0];
            r2_becon_4[1] = becon_4[1] - instrument_station_1[1];
            r2_becon_4[2] = becon_4[2] - instrument_station_1[2];


            /////////////////////////////////////////////////////////////////////////임의 -> 상대 좌표계///////////////////////////////////////////////////////////////////
            a_angle = Math.Atan(r2_instrument_station_2[1] / r2_instrument_station_2[0]) * 180.0 / Math.PI;
            b_angle = ((r2_instrument_station_2[0] < 0) ? 180 + a_angle : ((r2_instrument_station_2[0] > 0) ? ((r2_instrument_station_2[1] >= 0) ? a_angle : 360 + a_angle) : ((r2_instrument_station_2[1] > 0) ? 90 : 270))) * Math.PI / 180.0;

            //비콘1
            r_becon_1[0] = r2_becon_1[0] * Math.Cos(b_angle) + r2_becon_1[1] * Math.Sin(b_angle);
            r_becon_1[1] = -r2_becon_1[0] * Math.Sin(b_angle) + r2_becon_1[1] * Math.Cos(b_angle);
            r_becon_1[2] = r2_becon_1[2];

            //비콘2
            r_becon_2[0] = r2_becon_2[0] * Math.Cos(b_angle) + r2_becon_2[1] * Math.Sin(b_angle);
            r_becon_2[1] = -r2_becon_2[0] * Math.Sin(b_angle) + r2_becon_2[1] * Math.Cos(b_angle);
            r_becon_2[2] = r2_becon_2[2];

            //비콘3
            r_becon_3[0] = r2_becon_3[0] * Math.Cos(b_angle) + r2_becon_3[1] * Math.Sin(b_angle);
            r_becon_3[1] = -r2_becon_3[0] * Math.Sin(b_angle) + r2_becon_3[1] * Math.Cos(b_angle);
            r_becon_3[2] = r2_becon_3[2];

            //비콘4
            r_becon_4[0] = r2_becon_4[0] * Math.Cos(b_angle) + r2_becon_4[1] * Math.Sin(b_angle);
            r_becon_4[1] = -r2_becon_4[0] * Math.Sin(b_angle) + r2_becon_4[1] * Math.Cos(b_angle);
            r_becon_4[2] = r2_becon_4[2];

            //비컨의 좌표 UI상에 표시
            r_becon_1_v[0] = r_becon_1[0];
            r_becon_1_v[1] = r_becon_1[1];
            r_becon_1_v[2] = r_becon_1[2];

            r_becon_2_v[0] = r_becon_2[0];
            r_becon_2_v[1] = r_becon_2[1];
            r_becon_2_v[2] = r_becon_2[2];

            r_becon_3_v[0] = r_becon_3[0];
            r_becon_3_v[1] = r_becon_3[1];
            r_becon_3_v[2] = r_becon_3[2];

            r_becon_4_v[0] = r_becon_4[0];
            r_becon_4_v[1] = r_becon_4[1];
            r_becon_4_v[2] = r_becon_4[2];

            //Debug.WriteLine(" Bx : " + r_becon_4[0] + " By : " + r_becon_4[1] + " Bz : " + r_becon_4[2]);


            //측점2
            r_instrument_station_2[0] = r2_instrument_station_2[0] * Math.Cos(b_angle) + r2_instrument_station_2[1] * Math.Sin(b_angle);
            r_instrument_station_2[1] = -r2_instrument_station_2[0] * Math.Sin(b_angle) + r2_instrument_station_2[1] * Math.Cos(b_angle);
            r_instrument_station_2[2] = r2_instrument_station_2[2];

            /*
            //측점3
            r_instrument_station_3[0] = r2_instrument_station_3[0] * Math.Cos(b_angle) + r2_instrument_station_3[1] * Math.Sin(b_angle);
            r_instrument_station_3[1] = -r2_instrument_station_3[0] * Math.Sin(b_angle) + r2_instrument_station_3[1] * Math.Cos(b_angle);
            r_instrument_station_3[2] = r2_instrument_station_3[2];*/

            //**********************************************************************임의 -> 상대 좌표계 삼각 측량 출력부********************************************************************
            //비콘을 상대좌표로 변환해서 수신기 구함
            r_dozer_tri_point = r_Calc_tri(beacon_p1, beacon_p2);

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 위에 있을때)////////////////////
            ////수신기1
            //r_receiving_set_k[0] = r_dozer_tri_point[0];
            //r_receiving_set_k[1] = r_dozer_tri_point[1];
            //r_receiving_set_k[2] = r_dozer_tri_point[2];

            ////수신기2
            //r_receiving_set_k[3] = r_dozer_tri_point[6];
            //r_receiving_set_k[4] = r_dozer_tri_point[7];
            //r_receiving_set_k[5] = r_dozer_tri_point[8];

            ////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 밑에 있을때)////////////////////
            //수신기1
            r_receiving_set_k[0] = r_dozer_tri_point[3];
            r_receiving_set_k[1] = r_dozer_tri_point[4];
            r_receiving_set_k[2] = r_dozer_tri_point[5];
                           
            //수신기2      
            r_receiving_set_k[3] = r_dozer_tri_point[9];
            r_receiving_set_k[4] = r_dozer_tri_point[10];
            r_receiving_set_k[5] = r_dozer_tri_point[11];

            //-------------- 상대좌표계에서 수신기 좌표 강제입력 ----------------------
            ////수신기1
            //r_receiving_set_k[0] = 0;
            //r_receiving_set_k[1] = 0;
            //r_receiving_set_k[2] = 0;

            ////수신기2      
            //r_receiving_set_k[3] = 0;
            //r_receiving_set_k[4] = 0;
            //r_receiving_set_k[5] = 0;
            //-------------------------------------------------------------------------


            //이부분은 칼만필터 통과후 좌표값이다
            if (const_becon_p2_dozer == 1) Debug.Write(" K P1x " + r_receiving_set_k[0] + " P1y " + r_receiving_set_k[1] + " P1z " + r_receiving_set_k[2] + " P2x " + r_receiving_set_k[3] + " P2y " + r_receiving_set_k[4] + " P2z " + r_receiving_set_k[5] + " 칼만필터 좌표값" + "\r\n");//칼만필터 값으로 좌표값 계산



            //-----------------------------비콘을 상대좌표로 변환해서 수신기 구함(로우값으로 계산된 수신기 좌표)-----------------------------

            r_dozer_tri_point_o = r_Calc_tri_o(c_beacon_p1, c_beacon_p2);

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 위에 있을때)////////////////////
            ////수신기1
            //r_receiving_set_o[0] = r_dozer_tri_point_o[0];
            //r_receiving_set_o[1] = r_dozer_tri_point_o[1];
            //r_receiving_set_o[2] = r_dozer_tri_point_o[2];

            ////수신기2                                 
            //r_receiving_set_o[3] = r_dozer_tri_point_o[6];
            //r_receiving_set_o[4] = r_dozer_tri_point_o[7];
            //r_receiving_set_o[5] = r_dozer_tri_point_o[8];

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 밑에 있을때)///////////////////////
            //수신기1
            r_receiving_set_o[0] = r_dozer_tri_point_o[3];
            r_receiving_set_o[1] = r_dozer_tri_point_o[4];
            r_receiving_set_o[2] = r_dozer_tri_point_o[5];

            //수신기2                                 
            r_receiving_set_o[3] = r_dozer_tri_point_o[9];
            r_receiving_set_o[4] = r_dozer_tri_point_o[10];
            r_receiving_set_o[5] = r_dozer_tri_point_o[11];


            //Debug.WriteLine(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + r_receiving_set_o[0] + "  " + r_receiving_set_o[1] + "  " + r_receiving_set_o[2] + "  " + r_receiving_set_o[3] + "  " + r_receiving_set_o[4] + "  " + r_receiving_set_o[5]);


            ////ultrasonic_y
            //ultrasonic_plus_y = Math.Atan((dozer_ultrasonic_left * dozer_span_left + dozer_zero_left) / dozer_between);
            //ultrasonic_minus_y = -Math.Atan((dozer_ultrasonic_right * dozer_span_right + dozer_zero_right) / dozer_between);

            //if (dozer_ultrasonic_left > dozer_ultrasonic_right) ultrasonic_y = ultrasonic_plus_y;
            //else if (dozer_ultrasonic_left < dozer_ultrasonic_right) ultrasonic_y = ultrasonic_minus_y;
            //else ultrasonic_y = 0;

            //ultrasonic_y = Math.Atan(((dozer_ultrasonic_left * dozer_span_left + dozer_zero_left) - (dozer_ultrasonic_right * dozer_span_right + dozer_zero_right)) / dozer_between);
            ultrasonic_y = Math.Atan((((dozer_ultrasonic_left * dozer_span_left + dozer_zero_left) - (dozer_ultrasonic_right * dozer_span_right + dozer_zero_right)) * Math.Cos(dozer_blade_imu[1])) / (dozer_between * Math.Cos(dozer_blade_imu[0])));

            //rad to deg
            ultrasonic_y *= 180.0 / Math.PI;




            // -------------------  이 부분은 두 수신기의 이동된 거리를 가지고 오차를 판단하는 구간임(꼬리표 설정 해야됨)------------------------------
            if (const_becon_p2_dozer == 2)//안정화된 커팅후의 사이거리값이 적용된 상태
            {

                //이 부분은 현재값과 이전 수신된 데이터와 비교한다

                //현재 로값
                //수신기1
                r_receiving_set_k_s1[0] = r_receiving_set_k[0];
                r_receiving_set_k_s1[1] = r_receiving_set_k[1];
                r_receiving_set_k_s1[2] = r_receiving_set_k[2];

                //수신기2                                  
                r_receiving_set_k_s1[3] = r_receiving_set_k[3];
                r_receiving_set_k_s1[4] = r_receiving_set_k[4];
                r_receiving_set_k_s1[5] = r_receiving_set_k[5];

                //이전 수신 데이터값
                //수신기1
                r_receiving_set_k_s2[0] = r_receiving_set_o[0];
                r_receiving_set_k_s2[1] = r_receiving_set_o[1];
                r_receiving_set_k_s2[2] = r_receiving_set_o[2];

                //수신기2                                     
                r_receiving_set_k_s2[3] = r_receiving_set_o[3];
                r_receiving_set_k_s2[4] = r_receiving_set_o[4];
                r_receiving_set_k_s2[5] = r_receiving_set_o[5];

                Debug.Write(" K P1x " + r_receiving_set_k_s1[0] + " P1y " + r_receiving_set_k_s1[1] + " P1z " + r_receiving_set_k_s1[2] + " P2x " + r_receiving_set_k_s1[3] + " P2y " + r_receiving_set_k_s1[4] + " P2z " + r_receiving_set_k_s1[5] + " 현재 수신 데이터 좌표값(좌표이동거리) " + "\r\n");
                Debug.Write(" K P1x " + r_receiving_set_k_s2[0] + " P1y " + r_receiving_set_k_s2[1] + " P1z " + r_receiving_set_k_s2[2] + " P2x " + r_receiving_set_k_s2[3] + " P2y " + r_receiving_set_k_s2[4] + " P2z " + r_receiving_set_k_s2[5] + " 이전 수신 데이터 좌표값(좌표이동거리) " + "\r\n");

                //--------------------------------두 좌표사이의 거리를 가지고 튀는값 잡기(튀지않으면 칼만필터로 전달되고 튄값이면 이전값을 칼만필터로 던진다)------------------
                //P1 과 P1' 거리값 계산
                P1bP1a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[0] - r_receiving_set_k_s2[0], 2) + Math.Pow(r_receiving_set_k_s1[1] - r_receiving_set_k_s2[1], 2) + Math.Pow(r_receiving_set_k_s1[2] - r_receiving_set_k_s2[2], 2));

                //P2 과 P2' 거리값 계산
                P2bP2a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[3] - r_receiving_set_k_s2[3], 2) + Math.Pow(r_receiving_set_k_s1[4] - r_receiving_set_k_s2[4], 2) + Math.Pow(r_receiving_set_k_s1[5] - r_receiving_set_k_s2[5], 2));

                P1P2_absolute = Math.Abs(P1bP1a_distance - P2bP2a_distance);


                ////이 부분은 카운트를 이용해서 이전과 이후를 나누는 부분이다.
                //counter_position++;//이전값과 이후값을 구분(수신기 이동된 거리 파악용)


                //if (counter_position == 1)
                //{
                //    ////첫번쨰 중점의 좌표를 구한다
                //    //pb_1[0] = (r_receiving_set_k[0] + r_receiving_set_k[3]) / 2;
                //    //pb_1[1] = (r_receiving_set_k[1] + r_receiving_set_k[4]) / 2;
                //    //pb_1[2] = (r_receiving_set_k[2] + r_receiving_set_k[5]) / 2;

                //    //첫번째 P1, P2 좌표값 저장
                //    //수신기1
                //    r_receiving_set_k_s1[0] = r_receiving_set_k[0];
                //    r_receiving_set_k_s1[1] = r_receiving_set_k[1];
                //    r_receiving_set_k_s1[2] = r_receiving_set_k[2];

                //    //수신기2                                  
                //    r_receiving_set_k_s1[3] = r_receiving_set_k[3];
                //    r_receiving_set_k_s1[4] = r_receiving_set_k[4];
                //    r_receiving_set_k_s1[5] = r_receiving_set_k[5];



                //    Debug.Write(" K P1x " + r_receiving_set_k_s1[0] + " P1y " + r_receiving_set_k_s1[1] + " P1z " + r_receiving_set_k_s1[2] + " P2x " + r_receiving_set_k_s1[3] + " P2y " + r_receiving_set_k_s1[4] + " P2z " + r_receiving_set_k_s1[5] + " 수신 데이터 좌표값_1(좌표이동거리) " + "\r\n");




                //    //--------------------------------두 좌표사이의 거리를 가지고 튀는값 잡기(튀지않으면 칼만필터로 전달되고 튄값이면 이전값을 칼만필터로 던진다) - 비율로 수정-----------------
                //    //P1 과 P1' 거리값 계산
                //    P1bP1a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[0] - r_receiving_set_k_s2[0], 2) + Math.Pow(r_receiving_set_k_s1[1] - r_receiving_set_k_s2[1], 2) + Math.Pow(r_receiving_set_k_s1[2] - r_receiving_set_k_s2[2], 2));

                //    //P2 과 P2' 거리값 계산
                //    P2bP2a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[3] - r_receiving_set_k_s2[3], 2) + Math.Pow(r_receiving_set_k_s1[4] - r_receiving_set_k_s2[4], 2) + Math.Pow(r_receiving_set_k_s1[5] - r_receiving_set_k_s2[5], 2));

                //    //초기 저장 상태일때는 0값을 출력한다.
                //    if (P1P2_save_signal == 0) P1P2_absolute = 100;//100인 이유는 50을 초과시키기위해
                //    //else if (P1P2_save_signal == 1) P1P2_absolute = P1bP1a_distance / P2bP2a_distance;//P1의 거리와 P2의 거리 사이의 비율 계산(P1 / P2) - 비율 계산에서 차이 계산으로의 수정으로 인한 주석 처리
                //    else if (P1P2_save_signal == 1) P1P2_absolute = Math.Abs(P1bP1a_distance - P2bP2a_distance);

                //}

                //if (counter_position == 2)// 이동된 거리를 가지고 오차확인(이때는 두번째 거리값이 나온경우)
                //{

                //    counter_position = 0;//새로운 2개씩 비교

                //    ////두번째 중점 좌표를 구한다
                //    //pb_2[0] = (r_receiving_set_k[0] + r_receiving_set_k[3]) / 2;
                //    //pb_2[1] = (r_receiving_set_k[1] + r_receiving_set_k[4]) / 2;
                //    //pb_2[2] = (r_receiving_set_k[2] + r_receiving_set_k[5]) / 2;

                //    //두번째 P1', P2' 좌표값 저장
                //    //수신기1
                //    r_receiving_set_k_s2[0] = r_receiving_set_k[0];
                //    r_receiving_set_k_s2[1] = r_receiving_set_k[1];
                //    r_receiving_set_k_s2[2] = r_receiving_set_k[2];

                //    //수신기2                                 
                //    r_receiving_set_k_s2[3] = r_receiving_set_k[3];
                //    r_receiving_set_k_s2[4] = r_receiving_set_k[4];
                //    r_receiving_set_k_s2[5] = r_receiving_set_k[5];








                //    //저장상태 구분을 위한 선언
                //    P1P2_save_signal = 1;

                //    Debug.Write(" K P1x " + r_receiving_set_k_s2[0] + " P1y " + r_receiving_set_k_s2[1] + " P1z " + r_receiving_set_k_s2[2] + " P2x " + r_receiving_set_k_s2[3] + " P2y " + r_receiving_set_k_s2[4] + " P2z " + r_receiving_set_k_s2[5] + " 수신 데이터 좌표값_2(좌표이동거리) " + "\r\n");


                //    //--------------------------------두 좌표사이의 거리를 가지고 튀는값 잡기(튀지않으면 칼만필터로 전달되고 튄값이면 이전값을 칼만필터로 던진다) - 비율로 수정------------------
                //    //P1 과 P1' 거리값 계산
                //    P1bP1a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[0] - r_receiving_set_k_s2[0], 2) + Math.Pow(r_receiving_set_k_s1[1] - r_receiving_set_k_s2[1], 2) + Math.Pow(r_receiving_set_k_s1[2] - r_receiving_set_k_s2[2], 2));

                //    //P2 과 P2' 거리값 계산
                //    P2bP2a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[3] - r_receiving_set_k_s2[3], 2) + Math.Pow(r_receiving_set_k_s1[4] - r_receiving_set_k_s2[4], 2) + Math.Pow(r_receiving_set_k_s1[5] - r_receiving_set_k_s2[5], 2));

                //    //초기 저장 상태일때는 0값을 출력한다.
                //    if (P1P2_save_signal == 0) P1P2_absolute = 100;//100인 이유는 50을 초과시키기위해
                //    //else if (P1P2_save_signal == 1) P1P2_absolute = P1bP1a_distance / P2bP2a_distance;//P1의 거리와 P2의 거리 사이의 비율 계산(P1 / P2) - 비율 계산에서 차이 계산으로의 수정으로 인한 주석 처리
                //    else if (P1P2_save_signal == 1) P1P2_absolute = Math.Abs(P1bP1a_distance - P2bP2a_distance);



                //    ////고정된 거리를 벗어나는 경우 표시 - 사용하지 않으므로 주석처리
                //    //if (P1bP1a_distance <= P1P2_distance_const)//P1 과 P1' 거리값이 범위 안에 있을경우
                //    //{
                //    //    if (P2bP2a_distance <= P1P2_distance_const)//P2 과 P2' 거리값이 범위 안에 있을경우
                //    //    {
                //    //        //P1P2_distance_signal++;//P1과 P1', P2와 P2'의 거리값이 모두 범위 안에 있을경우 안정화 P1p2_distance_const = 100
                //    //        Debug.WriteLine("P1bP1a_distance " + P1bP1a_distance + " P2bP2a_distance " + P2bP2a_distance + " 안정화 된후 수신기의 위치 차이");

                //    //    }
                //    //    else if (P2bP2a_distance > P1P2_distance_const) Debug.WriteLine("P1bP1a_distance " + P1bP1a_distance + " P2bP2a_distance " + P2bP2a_distance + " P2x ");
                //    //}
                //    //else if (P1bP1a_distance > P1P2_distance_const) Debug.WriteLine("P1bP1a_distance " + P1bP1a_distance + " P2bP2a_distance " + P2bP2a_distance + " P1x ");

                //}



            }


            if (const_becon_p2_dozer == 0 && P1P2_distance_signal == 1)//0 부분은 안정화된 처음값을 의미
            {
                //막 처음 들어온값을 가지고 바로 좌표 계산 따라서 == 0 (이부분은 안정화된 거리값을 가지고 좌표를 계산한것임)
                Debug.Write(" o P1x " + r_receiving_set_o[0] + " P1y " + r_receiving_set_o[1] + " P1z " + r_receiving_set_o[2] + " P2x " + r_receiving_set_o[3] + " P2y " + r_receiving_set_o[4] + " P2z " + r_receiving_set_o[5] + " 수신 데이터 좌표값(사잇거리)" + "\r\n");//안정화 된 후 원본거리값으로 계산된 좌표값

                //두 수신기 사이의 거리값 계산(안정화된 커팅후의 거리값을 가지고 사이거리값 계산)
                P1P2_distance_o = Math.Sqrt(Math.Pow(r_receiving_set_o[0] - r_receiving_set_o[3], 2) + Math.Pow(r_receiving_set_o[1] - r_receiving_set_o[4], 2) + Math.Pow(r_receiving_set_o[2] - r_receiving_set_o[5], 2));
                //Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o));
            }



            //*****************************************************************위치 추적 로직 부분**********************************************************

            //----------------------------------수신기 z값 수식적용에 따른 추가된 부분(0709)-----------------------------
            //처음 9개의 z값을 받은 후 중간값을 찾는다
            //입력된 z값
            //p1 z값 : r_receiving_set_k[2];
            //p2 z값 : r_receiving_set_k[5];
            
            if (const_becon_p2_dozer == 1 && stop_signal == 1)//칼만근접후 계산
            {

                //-------테스트용 - 추가된 부분(0729)-------
                //p1
                //r_receiving_set_k[2] -= 2000;

                //p2
                //r_receiving_set_k[5] -= 2000;
                //-----------------------

                //P1
                real_data_d_p1_z[constant_number_d_P1_z] = r_receiving_set_k[2];

                //P2
                real_data_d_p2_z[constant_number_d_P2_z] = r_receiving_set_k[5];



                //중앙구간에서의 첫번째수(배열 맨처음에 0부터 시작이므로)
                number_standard_P1_z = number_divide_P1_z;
                number_standard_P2_z = number_divide_P2_z;

                //P1z
                temp_array_sort_d_P1_z = real_data_d_p1_z;//가상의 공간을 생성 

                //------직접계산 시작
                for (iz = 0; iz < number_divide_P1_z * 3 - 1; iz++)// 0부터 7
                {
                    for (kz = iz + 1; kz < number_divide_P1_z * 3; kz++)//1 부터 8(예 : iz가 0일때 kz는 1부터 8까지 포문, iz가 1일때 kz는 2부터 8까지 포문)
                    {
                        if (temp_array_sort_d_P1_z[iz] > temp_array_sort_d_P1_z[kz])//iz 가 0 ~ 7 까지일때 각각 + 1 부터 8까지 비교(처음에는 0 배열을 제외하고 전부 0)
                        {                                                           //iz를 가지고 크기비교를 한뒤 큰값이면 뒤로 쉬프트
                            //크기별로 순서화
                            data_value_align_z = temp_array_sort_d_P1_z[iz];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P1_z[iz] = temp_array_sort_d_P1_z[kz];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P1_z[kz] = data_value_align_z;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }

                data_value_align_z = 0;
                ////-------직접계산 끝



                //P2z
                temp_array_sort_d_P2_z = real_data_d_p2_z;//가상의 공간을 생성 

                //------직접계산 시작
                for (iz = 0; iz < number_divide_P2_z * 3 - 1; iz++)//5
                {
                    for (kz = iz + 1; kz < number_divide_P2_z * 3; kz++)//6
                    {
                        if (temp_array_sort_d_P2_z[iz] > temp_array_sort_d_P2_z[kz])//iz 가 0 ~ 4 까지일때 각각 + 1 부터 5까지 비교(처음에는 0 배열을 제외하고 전부 0임)
                        {
                            //크기별로 순서화
                            data_value_align_z = temp_array_sort_d_P2_z[iz];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P2_z[iz] = temp_array_sort_d_P2_z[kz];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P2_z[kz] = data_value_align_z;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }

                data_value_align_z = 0;
                ////-------직접계산 끝

                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1 center : " + temp_array_sort_d_P1_z[0] + "    " + temp_array_sort_d_P1_z[1] + "    " + temp_array_sort_d_P1_z[2] + "    " + temp_array_sort_d_P1_z[3] + "    " + temp_array_sort_d_P1_z[4] + "    " + temp_array_sort_d_P1_z[5] + "    " + temp_array_sort_d_P1_z[6] + "    " + temp_array_sort_d_P1_z[7] + "    " + temp_array_sort_d_P1_z[8]);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2 center : " + temp_array_sort_d_P2_z[0] + "    " + temp_array_sort_d_P2_z[1] + "    " + temp_array_sort_d_P2_z[2] + "    " + temp_array_sort_d_P2_z[3] + "    " + temp_array_sort_d_P2_z[4] + "    " + temp_array_sort_d_P2_z[5] + "    " + temp_array_sort_d_P2_z[6] + "    " + temp_array_sort_d_P2_z[7] + "    " + temp_array_sort_d_P2_z[8]);

                //중간값 위치 계산
                calc_center_p1 = number_divide_P1_z / 2;
                calc_center_p2 = number_divide_P2_z / 2;

                center_position_p1 = Convert.ToInt16(Math.Round(calc_center_p1));
                center_position_p2 = Convert.ToInt16(Math.Round(calc_center_p2));

                if (temp_array_sort_d_P1_z[0] > -99999999999999)//음수값 설정을 위한 수정된 부분(0729)
                {
                    r_receiving_set[2] = temp_array_sort_d_P1_z[number_standard_P1_z + center_position_p1];//구간의 범위가 3개일때 중간값(012 345 678)
                    for (jz = 0; jz < number_divide_P1_z * 3; jz++) temp_array_sort_d_P1_z[jz] = -99999999999999;

                }

                if (temp_array_sort_d_P2_z[0] > -99999999999999)//음수값 설정을 위한 수정된 부분(0729)
                {
                    r_receiving_set[5] = temp_array_sort_d_P2_z[number_standard_P2_z + center_position_p2];//구간의 범위가 3개일때 중간값(012 345 678)
                    for (jz = 0; jz < number_divide_P2_z * 3; jz++) temp_array_sort_d_P2_z[jz] = -99999999999999;

                }


                //최종 출력된 수신기 좌표
                r_receiving_set[0] = r_receiving_set_k[0];
                r_receiving_set[1] = r_receiving_set_k[1];


                r_receiving_set[3] = r_receiving_set_k[3];
                r_receiving_set[4] = r_receiving_set_k[4];



                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1 z : " + r_receiving_set[2]);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2 z : " + r_receiving_set[5]);

                //----------------------------------------------비컨과 수신기 사이의 각도 계산부------------------------------------------------


                //비컨 좌표
                //r_becon_1_v[0]
                //r_becon_1_v[1]
                //r_becon_1_v[2]
                //    
                //r_becon_2_v[0]
                //r_becon_2_v[1]
                //r_becon_2_v[2]
                //    
                //r_becon_3_v[0]
                //r_becon_3_v[1]
                //r_becon_3_v[2]
                //    
                //r_becon_4_v[0]
                //r_becon_4_v[1]
                //r_becon_4_v[2]

                //----------------------수평거리 계산(평면 상에서  비컨과 수신기 사이의 거리)--------------------
                //p1b1
                p1b1_flat_distance = Math.Sqrt(Math.Pow(r_becon_1_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_1_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b1_down : " + p1b1_flat_distance);

                //p1b2
                p1b2_flat_distance = Math.Sqrt(Math.Pow(r_becon_2_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_2_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b2_down : " + p1b2_flat_distance);

                //p1b3
                p1b3_flat_distance = Math.Sqrt(Math.Pow(r_becon_3_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_3_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b3_down : " + p1b3_flat_distance);

                //p1b4
                p1b4_flat_distance = Math.Sqrt(Math.Pow(r_becon_4_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_4_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b4_down : " + p1b4_flat_distance);

                //------------------------

                //p2b1    
                p2b1_flat_distance = Math.Sqrt(Math.Pow(r_becon_1_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_1_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b1_down : " + p2b1_flat_distance);

                //p2b2
                p2b2_flat_distance = Math.Sqrt(Math.Pow(r_becon_2_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_2_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b2_down : " + p2b2_flat_distance);

                //p2b3
                p2b3_flat_distance = Math.Sqrt(Math.Pow(r_becon_3_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_3_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b3_down : " + p2b3_flat_distance);

                //p2b4
                p2b4_flat_distance = Math.Sqrt(Math.Pow(r_becon_4_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_4_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b4_down : " + p2b4_flat_distance);


                //-------------센서거리---------------
                //p1b1
                //beacon_p1[0]

                //p1b2
                //beacon_p1[1]

                //p1b3
                //beacon_p1[2]

                //p1b4
                //beacon_p1[3]

                //------------------------

                //p2b1
                //beacon_p2[0]

                //p2b2
                //beacon_p2[1]

                //p2b3
                //beacon_p2[2]

                //p2b4
                //beacon_p3[3]

                //------------------------각도계산----------------------

                //아크코사인 정의역이 1보다 클경우 추가

                //p1b1
                if (p1b1_flat_distance / beacon_p1[0] > 1) p1b1_angle_value = 2 - (p1b1_flat_distance / beacon_p1[0]);
                else if (p1b1_flat_distance / beacon_p1[0] <= 1) p1b1_angle_value = p1b1_flat_distance / beacon_p1[0];
                p1b1_angle = Math.Acos(p1b1_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b1_angle : " + p1b1_angle);

                //p1b2
                if (p1b2_flat_distance / beacon_p1[1] > 1) p1b2_angle_value = 2 - (p1b2_flat_distance / beacon_p1[1]);
                else if (p1b2_flat_distance / beacon_p1[1] <= 1) p1b2_angle_value = p1b2_flat_distance / beacon_p1[1];
                p1b2_angle = Math.Acos(p1b2_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b2_angle : " + p1b2_angle);

                //p1b3
                if (p1b3_flat_distance / beacon_p1[2] > 1) p1b3_angle_value = 2 - (p1b3_flat_distance / beacon_p1[2]);
                else if (p1b3_flat_distance / beacon_p1[2] <= 1) p1b3_angle_value = p1b3_flat_distance / beacon_p1[2];  
                p1b3_angle = Math.Acos(p1b3_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b3_angle : " + p1b3_angle);


                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>  calc 1 : " + Math.Acos(p1b3_flat_distance / beacon_p1[2]));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>  calc 2 : " + p1b3_flat_distance / beacon_p1[2]);

                //p1b4
                if (p1b4_flat_distance / beacon_p1[3] > 1) p1b4_angle_value = 2 - (p1b4_flat_distance / beacon_p1[3]);
                else if (p1b4_flat_distance / beacon_p1[3] <= 1) p1b4_angle_value = p1b4_flat_distance / beacon_p1[3];
                p1b4_angle = Math.Acos(p1b4_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b4_angle : " + p1b4_angle);



                //--------------------------------------------------



                //p2b1
                if (p2b1_flat_distance / beacon_p2[0] > 1) p2b1_angle_value = 2 - (p2b1_flat_distance / beacon_p2[0]);
                else if (p2b1_flat_distance / beacon_p2[0] <= 1) p2b1_angle_value = p2b1_flat_distance / beacon_p2[0];
                p2b1_angle = Math.Acos(p2b1_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b1_angle : " + p2b1_angle);

                //p2b2
                if (p2b2_flat_distance / beacon_p2[1] > 1) p2b2_angle_value = 2 - (p2b2_flat_distance / beacon_p2[1]);
                else if (p2b2_flat_distance / beacon_p2[1] <= 1) p2b2_angle_value = p2b2_flat_distance / beacon_p2[1];
                p2b2_angle = Math.Acos(p2b2_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b2_angle : " + p2b2_angle);

                //p2b3
                if (p2b3_flat_distance / beacon_p2[2] > 1) p2b3_angle_value = 2 - (p2b3_flat_distance / beacon_p2[2]);
                else if (p2b3_flat_distance / beacon_p2[2] <= 1) p2b3_angle_value = p2b3_flat_distance / beacon_p2[2];
                p2b3_angle = Math.Acos(p2b3_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b3_angle : " + p2b3_angle);

                //p2b4
                if (p2b4_flat_distance / beacon_p2[3] > 1) p2b4_angle_value = 2 - (p2b4_flat_distance / beacon_p2[3]);
                else if (p2b4_flat_distance / beacon_p2[3] <= 1) p2b4_angle_value = p2b4_flat_distance / beacon_p2[3];
                p2b4_angle = Math.Acos(p2b4_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b4_angle : " + p2b4_angle);




                //------------------각도 평균화----------------
                //b1
                b1_angle = (p1b1_angle + p2b1_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b1_angle : " + b1_angle);
                

                //b2
                b2_angle = (p1b2_angle + p2b2_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b2_angle : " + b2_angle);

                //b3
                b3_angle = (p1b3_angle + p2b3_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b3_angle : " + b3_angle);

                //b4
                b4_angle = (p1b4_angle + p2b4_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b4_angle : " + b4_angle);


                //거리값이 999999일 경우 해당되는 비컨의 각도를 0으로 표시 - 추가된 부분(0716)
                if      (b1_sign == 1) b1_angle = 0;
                else if (b2_sign == 1) b2_angle = 0;
                else if (b3_sign == 1) b3_angle = 0;
                else if (b4_sign == 1) b4_angle = 0;


                //---------------가장 큰 각도 계산 --------------
                //가장 큰 각도 값 결정(a_angle_N3가 가장큼)
                if (b1_angle < b2_angle) a_angle_N1 = b2_angle;
                else a_angle_N1 = b1_angle;

                if (a_angle_N1 < b3_angle) a_angle_N2 = b3_angle;
                else a_angle_N2 = a_angle_N1;

                if (a_angle_N2 < b4_angle) a_angle_N3 = b4_angle;
                else a_angle_N3 = a_angle_N2;

                a_angle_N3 = Math.Round(a_angle_N3, 4);//수식 오류로 인한 수정

                //계산에 사용될 각도 계산
                //b1_angle_c = b1_angle - 1;
                //b2_angle_c = b1_angle - 1;
                //b3_angle_c = b1_angle - 1;
                //b4_angle_c = b1_angle - 1;

                //수식 오류로 인한 수정
                b1_angle = Math.Round(b1_angle, 4);
                b2_angle = Math.Round(b2_angle, 4);
                b3_angle = Math.Round(b3_angle, 4);
                b4_angle = Math.Round(b4_angle, 4);

                //--------가장 큰 각도인 비컨 좌표값- 수식 오류로 수정--------
                //-----비컨 좌표 계산 
                //x 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_x = r_becon_1_v[0];
                else if (a_angle_N3 == b2_angle) angle_s_b0_x = r_becon_2_v[0];
                else if (a_angle_N3 == b3_angle) angle_s_b0_x = r_becon_3_v[0];
                else angle_s_b0_x = r_becon_4_v[0];

                //y 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_y = r_becon_1_v[1];
                else if (a_angle_N3 == b2_angle) angle_s_b0_y = r_becon_2_v[1];
                else if (a_angle_N3 == b3_angle) angle_s_b0_y = r_becon_3_v[1];
                else angle_s_b0_y = r_becon_4_v[1];

                //z 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_z = r_becon_1_v[2];
                else if (a_angle_N3 == b2_angle) angle_s_b0_z = r_becon_2_v[2];
                else if (a_angle_N3 == b3_angle) angle_s_b0_z = r_becon_3_v[2];
                else angle_s_b0_z = r_becon_4_v[2];

                //----비컨 번호 선택(s_beacon_0)
                if (angle_s_b0_x == r_becon_1_v[0] && angle_s_b0_y == r_becon_1_v[1] && angle_s_b0_z == r_becon_1_v[2]) s_beacon_0 = 1;
                else if (angle_s_b0_x == r_becon_2_v[0] && angle_s_b0_y == r_becon_2_v[1] && angle_s_b0_z == r_becon_2_v[2]) s_beacon_0 = 2;
                else if (angle_s_b0_x == r_becon_3_v[0] && angle_s_b0_y == r_becon_3_v[1] && angle_s_b0_z == r_becon_3_v[2]) s_beacon_0 = 3;
                else s_beacon_0 = 4;

                //----------- 나머지 비컨 좌표값 계산 - 추가 --------------
                //------s_beacon_1
                //x 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_x = r_becon_2_v[0];
                else angle_s_b1_x = r_becon_1_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_y = r_becon_2_v[1];
                else angle_s_b1_y = r_becon_1_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_z = r_becon_2_v[2];
                else angle_s_b1_z = r_becon_1_v[2];

                //비컨 번호 선택(s_beacon_1)
                if (s_beacon_0 == 1) s_beacon_1 = 2;
                else s_beacon_1 = 1;

                //-------s_beacon_2
                //x 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_x = r_becon_3_v[0];
                else if (s_beacon_0 == 2) angle_s_b2_x = r_becon_3_v[0];
                else if (s_beacon_0 == 3) angle_s_b2_x = r_becon_2_v[0];
                else angle_s_b2_x = r_becon_2_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_y = r_becon_3_v[1];
                else if (s_beacon_0 == 2) angle_s_b2_y = r_becon_3_v[1];
                else if (s_beacon_0 == 3) angle_s_b2_y = r_becon_2_v[1];
                else angle_s_b2_y = r_becon_2_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_z = r_becon_3_v[2];
                else if (s_beacon_0 == 2) angle_s_b2_z = r_becon_3_v[2];
                else if (s_beacon_0 == 3) angle_s_b2_z = r_becon_2_v[2];
                else angle_s_b2_z = r_becon_2_v[2];

                //비컨 번호 선택(s_beacon_2)
                if (s_beacon_0 == 1) s_beacon_2 = 3;
                else if (s_beacon_0 == 2) s_beacon_2 = 3;
                else if (s_beacon_0 == 3) s_beacon_2 = 2;
                else s_beacon_2 = 2;

                //--------s_beacon_3
                //x 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_x = r_becon_3_v[0];
                else angle_s_b3_x = r_becon_4_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_y = r_becon_3_v[1];
                else angle_s_b3_y = r_becon_4_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_z = r_becon_3_v[2];
                else angle_s_b3_z = r_becon_4_v[2];

                //비컨 번호 선택(s_beacon_3)
                if (s_beacon_0 == 4) s_beacon_3 = 3;
                else s_beacon_3 = 4;

                //------ 비컨 2개 결정 준비
                beacon_2_calc = 1;

            }


            ////////////////////////////////////////////////////////////////////상대 좌표계 Pb(동일한 높이인 수신기일 경우 적용)//////////////////////////////////////////////////////////////////

            //위치좌표 이전 이후값 구분(카운트로 구분)
            if (const_becon_p2_dozer == 1)
            {
                pb_count++;

                if (pb_count == 1)
                {
                    //이전값

                    //calc_body_y 적용
                    pb_b[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
                    pb_b[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
                    pb_b[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

                    Debug.WriteLine(" 위치좌표_1: " + " x " + pb_b[0] + " y " + pb_b[1] + " z " + pb_b[2]);

                    if (pb_signal == 1)
                    {
                        //이전값과 이후값 거리 구하기
                        pb_distance = Math.Sqrt(Math.Pow(pb_b[0] - pb_a[0], 2) + Math.Pow(pb_b[1] - pb_a[1], 2) + Math.Pow(pb_b[2] - pb_a[2], 2));
                    }
                }

                else if (pb_count > 1)
                {
                    //이후값

                    //calc_body_y 적용
                    pb_a[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
                    pb_a[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
                    pb_a[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

                    Debug.WriteLine(" 위치좌표_2: " + " x " + pb_a[0] + " y " + pb_a[1] + " z " + pb_a[2]);

                    pb_count = 0;
                    pb_signal = 1;

                    //이전값과 이후값 거리 구하기
                    pb_distance = Math.Sqrt(Math.Pow(pb_b[0] - pb_a[0], 2) + Math.Pow(pb_b[1] - pb_a[1], 2) + Math.Pow(pb_b[2] - pb_a[2], 2));
                }

                Debug.WriteLine(" 위치 좌표 계산후 튀는 거리 " + pb_distance);

            }




            //calc_body_y 적용
            pb[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
            pb[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
            pb[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

            //yaw calc
            move_p1[0] = r_receiving_set[0] - pb[0];
            move_p1[1] = r_receiving_set[1] - pb[1];
            move_p1[2] = 0;

            p1_atan = Math.Atan(move_p1[1] / move_p1[0]) * (180 / Math.PI);//deg
            p1_calibration = Math.Atan((-Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1])) / Math.Cos(dozer_body_imu[0])) * (180 / Math.PI);


            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) calc_body_y = 90;
                else calc_body_y = -90;
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) calc_body_y = 180;
                    else calc_body_y = 0;
                }

                else
                {
                    if (move_p1[0] < 0) calc_body_y = -p1_atan;
                    else
                    {
                        if (move_p1[1] < 0) calc_body_y = -180 - p1_atan;
                        else calc_body_y = 180 - p1_atan;
                    }
                }
            }


            ////yaw 값 계산 판별 - 수식 변경에 따른 수정된 부분(0706)
            //if (calc_body_y > 0 || calc_body_y < 0 || calc_body_y == 0) calc_body_y_o = calc_body_y;
            //else
            //{
            //    //if (const_becon_p2_dozer == 1) calc_body_y_n = calc_body_y;

            //    if (const_becon_p2_dozer == 1 && stop_signal == 1) NaN_yaw_count++;//정상화 이후 카운팅 시작

            //}


            //if (const_becon_p2_dozer == 1)
            //{
            //    //yaw_count++;

            //    Debug.WriteLine(" yaw_c " + calc_body_y_o + " yaw_o " + calc_body_y);//카운팅 추가로 인한 수정
            //    Debug.WriteLine("                                                                                                                                                                                                                        NaN_yaw_count " + NaN_yaw_count);
            //}

            //yaw 값 계산 판별
            if ((calc_body_y > 0 || calc_body_y < 0 || calc_body_y == 0) && const_becon_p2_dozer == 1 && stop_signal == 1)//계산이 잘될때
            {
                calc_body_y_o = calc_body_y;//값이 잘 나올때 저장
                calc_body_y_a = calc_body_y;//요값 저장을 위한 추가된 부분(0722)
                Debug.WriteLine(" yaw_o " + calc_body_y);//값이 잘 나올때 출력
            }
            else//계산이 안될때
            {
                if (const_becon_p2_dozer == 1 && stop_signal == 1)
                {
                    NaN_yaw_count++;//정상화 이후 카운팅 시작
                    calc_body_y_o = calc_body_y_a;//저장된 요값 사용을 위한 추가된 부분(0722)
                    Debug.WriteLine("                                                                                                                                                                                                                        NaN_yaw_count " + NaN_yaw_count);
                    Debug.WriteLine(" yaw_c " + calc_body_y_o);//계산이 안될경우 저장된값 출력
                    Debug.WriteLine(" yaw_o " + calc_body_y);//값이 안 나올때 원본값 출력 
                }

            }


            calc_body_y_o += d_calc_body_y_c;//요값 조정 3 
            //calc_body_y_o = 0;//body의 yaw값 강제입력

            body_y_1 = Math.Pow(Math.Cos(dozer_body_imu[0]), 2);


            //수정된 도저모델의 암 각도 보정식 - 추가된 부분(0713)
            //입력 상수 : a_const, b_const, c_const
            //암 피치 평균 값 : dozer_front_imu_pitch_unity
            
            dozer_blade_imu[0] *= (180 / Math.PI);// rad to deg
            calc_alpha = c_const + a_const * Math.Abs(dozer_front_imu_pitch_unity) - b_const * Math.Abs(dozer_blade_imu[0]);
            dozer_blade_imu[0] *= (Math.PI / 180);// deg to rad

            //frontL yaw calc
            frontL_y_1 = Math.Pow(Math.Tan(dozer_front_imu_L[1]), 2);
            frontL_y_2 = Math.Sqrt(frontL_y_1 / (body_y_1 + frontL_y_1 * body_y_1));

            if (frontL_y_2 > 1) r_frontL_a = Math.Asin(2 - frontL_y_2);
            else r_frontL_a = Math.Asin(frontL_y_2);

            multy_a_L = (dozer_body_imu[1] * dozer_front_imu_L[1] < 0) ? 1 : -1;
            y_frontL_a = Math.Abs(Math.Atan(Math.Sin(r_frontL_a) * Math.Sin(dozer_body_imu[1]) / Math.Cos(r_frontL_a))) * multy_a_L * 180.0 / Math.PI;//rad -> deg
            //frontL_yaw = (calc_body_y_o + y_frontL_a + d_frontL_y_c) * Math.PI / 180.0; // 3 //d_frontL_y_c : 도저 프론트 yaw값 보정 상수 // 수정된 도저 모델의 보정식 추가 - 수정된 부분(0713)
            frontL_yaw = (calc_body_y_o + y_frontL_a + d_frontL_y_c - calc_alpha) * Math.PI / 180.0;//deg to rad // 3 //d_frontL_y_c : 도저 프론트 yaw값 보정 상수 // 수정된 도저 모델의 보정식 추가 - 수정된 부분(0713)


            //frontR yaw calc - imu 추가
            frontR_y_1 = Math.Pow(Math.Tan(dozer_front_imu_R[1]), 2);
            frontR_y_2 = Math.Sqrt(frontR_y_1 / (body_y_1 + frontR_y_1 * body_y_1));

            if (frontR_y_2 > 1) r_frontR_a = Math.Asin(2 - frontR_y_2);
            else r_frontR_a = Math.Asin(frontR_y_2);

            multy_a_R = (dozer_body_imu[1] * dozer_front_imu_R[1] < 0) ? 1 : -1;
            y_frontR_a = Math.Abs(Math.Atan(Math.Sin(r_frontR_a) * Math.Sin(dozer_body_imu[1]) / Math.Cos(r_frontR_a))) * multy_a_R * 180.0 / Math.PI;
            //frontR_yaw = (calc_body_y_o + y_frontR_a + d_frontR_y_c) * Math.PI / 180.0; // 3 - 수정된 부분(0713)
            frontR_yaw = (calc_body_y_o + y_frontR_a + d_frontR_y_c + calc_alpha) * Math.PI / 180.0; // 3 - 수정된 부분(0713)



            ////blade yaw calc - 기존의 모델 적용시

            //blade_y_1 = Math.Pow(Math.Tan(dozer_blade_imu[1]), 2);
            //blade_y_2 = Math.Sqrt(blade_y_1 / (body_y_1 + blade_y_1 * body_y_1));

            //if (blade_y_2 > 1) r_blade_a = Math.Asin(2 - blade_y_2);
            //else r_blade_a = Math.Asin(blade_y_2);

            //multy_b = (dozer_body_imu[1] * dozer_blade_imu[1] < 0) ? 1 : -1;
            //y_blade_a = Math.Abs(Math.Atan(Math.Sin(r_blade_a) * Math.Sin(dozer_body_imu[1]) / Math.Cos(r_blade_a))) * multy_b * 180.0 / Math.PI;
            //blade_yaw = (calc_body_y_o + ultrasonic_y + y_blade_a + d_blade_y_c) * Math.PI / 180.0;




            //deg to rad
            calc_body_y_o *= (Math.PI / 180.0);

            //orientation
            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) orientation = 0;
                else orientation = 180;
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) orientation = 270;
                    else orientation = 90;
                }

                else
                {
                    if (move_p1[0] < 0) orientation = 90 + p1_atan;
                    else orientation = 270 + p1_atan;
                }
            }

            //orientation 계산 판별 - 추가된 부분(0624)
            if (orientation > 0 || orientation == 0 || orientation < 0) orientation_o = orientation;

            orientation_o = 360 - orientation_o - p1_calibration + d_calc_body_y_c;//orientation 미세조정에 따른 수정
            
            frontL_ori = orientation_o + y_frontL_a + d_frontL_y_c;
            frontR_ori = orientation_o + y_frontR_a + d_frontR_y_c;

            //유니티 표현을 위한 프론트 오리엔테이션 값 계산 - 추가된 부분(0625)
            front_ori_unity = (frontL_ori + frontR_ori) / 2;

            //수정된 모델 적용시 - 추가된 부분(0623)
            calc_blade_calibration = Math.Atan((-Math.Sin(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1])) / Math.Cos(dozer_blade_imu[0])) * (180 / Math.PI);//rad -> deg


            
            //blade_ori = orientation + ultrasonic_y + y_blade_a + d_blade_y_c;////blade orientation calc

            //////////////////////////////////////////////////////pb가 (0, 0, 0)일때, pbb_p구하기(_p : 위치 좌표계)////////////////////////////
            pbb_length[0] = dozer_body_length[6];//g값
            pbb_length[1] = 0;
            pbb_length[2] = 0;

            pbb_p[0] =  pbb_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Cos(calc_body_y_o);
            pbb_p[1] = -pbb_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Sin(calc_body_y_o);
            pbb_p[2] =  pbb_length[0] * Math.Sin(dozer_body_imu[1]);

            //////////////////////////////////////////////////////pbb_p일때, pc_p구하기(_p : 위치 좌표계)//////////////////////////////////
            pbpc_length[0] = 0; pbpc_length[1] = 0; pbpc_length[2] = -dozer_body_length[3];

            pc_p[0] = pbb_p[0] - pbpc_length[2] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Cos(calc_body_y_o) + Math.Sin(dozer_body_imu[0]) * Math.Sin(calc_body_y_o));
            pc_p[1] = pbb_p[1] + pbpc_length[2] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Sin(calc_body_y_o) - Math.Sin(dozer_body_imu[0]) * Math.Cos(calc_body_y_o));
            pc_p[2] = pbb_p[2] + pbpc_length[2] *  Math.Cos(dozer_body_imu[0]) * Math.Cos(dozer_body_imu[1]);


            /////////////////////////////////////////pb가 (0, 0, 0)일때, pc_p구하기(_p : 위치 좌표계)//////////////////////////////////
            //length[2] = -dozer_body_length[3];

            //pc_p[0] = -length[2] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Cos(calc_body_y_o) + Math.Sin(dozer_body_imu[0]) * Math.Sin(calc_body_y_o));
            //pc_p[1] = length[2] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Sin(calc_body_y_o) - Math.Sin(dozer_body_imu[0]) * Math.Cos(calc_body_y_o));
            //pc_p[2] = length[2] * Math.Cos(dozer_body_imu[0]) * Math.Cos(dozer_body_imu[1]);

            //////////////////////////////////////////////////////pc_p일때, p3_p구하기(_p : 위치 좌표계) - e(여기서 p3는 수식에서 p4를 의미)////////////////////////////////////
            length[0] = dozer_body_length[5]; length[1] = dozer_body_length[4];

            p3_p[0] = pc_p[0] + length[0] * Math.Cos(dozer_body_imu[1]) * Math.Cos(calc_body_y_o) + length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(calc_body_y_o) - Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Cos(calc_body_y_o));
            p3_p[1] = pc_p[1] - length[0] * Math.Cos(dozer_body_imu[1]) * Math.Sin(calc_body_y_o) + length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Cos(calc_body_y_o) + Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Sin(calc_body_y_o));
            p3_p[2] = pc_p[2] + length[0] * Math.Sin(dozer_body_imu[1]) + length[1] * Math.Sin(dozer_body_imu[0]) * Math.Cos(dozer_body_imu[1]);

            //기존의 모델
            ////////////////////////////////p3_p가 (0, 0, 0)일때, front_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////////
            //front_length[0] = dozer_front_length; front_length[1] = 0; front_length[2] = 0;

            //front_p_h[0] = front_length[0]  * Math.Cos(dozer_front_imu_L[1]) * Math.Cos(frontL_yaw);
            //front_p_h[1] = -front_length[0] * Math.Cos(dozer_front_imu_L[1]) * Math.Sin(frontL_yaw);
            //front_p_h[2] = front_length[0]  * Math.Sin(dozer_front_imu_L[1]);

            

            //수정된 모델의 수정된 수식 - 수정된 부분(0712)
            //////////////////////////////p3_p가 (0, 0, 0)일때, p4_left_p_h구하기(_h : 힌지 좌표계)////////////////////////////
            p4_left_length[0] = -dozer_body_length[7];                                   p4_left_length[1] = 0;                                         p4_left_length[2] = 0;
            
            //p4_p가 (0, 0, 0)일때 p4_left_p_h 값
            p4_left_p_h[0] =  p4_left_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Cos(calc_body_y_o) + p4_left_length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(calc_body_y_o) - Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Cos(calc_body_y_o));
            p4_left_p_h[1] = -p4_left_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Sin(calc_body_y_o) + p4_left_length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Cos(calc_body_y_o) + Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Sin(calc_body_y_o));
            p4_left_p_h[2] =  p4_left_length[0] * Math.Sin(dozer_body_imu[1]) + p4_left_length[1] * Math.Sin(dozer_body_imu[0]) * Math.Cos(dozer_body_imu[1]);

             

            //수정된 모델의 수정된 수식 - 수정된 부분(0712)
            //////////////////////////////p3_p가 (0, 0, 0)일때, p4_right_p_h구하기(_h : 힌지 좌표계)////////////////////////////
            p4_right_length[0] = dozer_body_length[7];            p4_right_length[1] = 0;              p4_right_length[2] = 0;

            //p4_p가 (0, 0, 0)일때 p4_left_p_h 값
            p4_right_p_h[0] =  p4_right_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Cos(calc_body_y_o) + p4_right_length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Sin(calc_body_y_o) - Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Cos(calc_body_y_o));
            p4_right_p_h[1] = -p4_right_length[0] * Math.Cos(dozer_body_imu[1]) * Math.Sin(calc_body_y_o) + p4_right_length[1] * (Math.Cos(dozer_body_imu[0]) * Math.Cos(calc_body_y_o) + Math.Sin(dozer_body_imu[0]) * Math.Sin(dozer_body_imu[1]) * Math.Sin(calc_body_y_o));
            p4_right_p_h[2] =  p4_right_length[0] * Math.Sin(dozer_body_imu[1]) + p4_right_length[1] * Math.Sin(dozer_body_imu[0]) * Math.Cos(dozer_body_imu[1]);

           

            //수정된 모델의 수정된 수식 - 수정된 부분(0712)
            //////////////////////////////p4_left_p_h일때, front_left_p_h구하기(_h : 힌지 좌표계) ////////////////////////////
            front_length[0] = dozer_front_length;      front_length[1] = 0;            front_length[2] = 0;

            //암길이 변동에 따른 수정된 부분(0716)
            dozer_blade_imu[0] *= (180.0 / Math.PI);//rad -> deg
            front_length[0] = front_length[0] - (0.1463 * Math.Pow(dozer_blade_imu[0], 2) + 0.0055 * dozer_blade_imu[0] + 0.5216);
            //if(kalman_after == 1) Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> front_length[0] : " + front_length[0]);
            dozer_blade_imu[0] *= (Math.PI / 180);//deg -> rad

            //암쪽 imu 제거에 따른 수정된 부분(0714)
            dozer_front_imu_L[0] = dozer_blade_imu[0];
            dozer_front_imu_L[1] = dozer_blade_imu[1];

            //p4_left_p_h가 (0, 0, 0)일때 front_left_p_h 값
            front_left_p_h[0] =  front_length[0] * Math.Cos(dozer_front_imu_L[1]) * Math.Cos(frontL_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_L[0]) * Math.Sin(frontL_yaw) - Math.Sin(dozer_front_imu_L[0]) * Math.Sin(dozer_front_imu_L[1]) * Math.Cos(frontL_yaw));
            front_left_p_h[1] = -front_length[0] * Math.Cos(dozer_front_imu_L[1]) * Math.Sin(frontL_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_L[0]) * Math.Cos(frontL_yaw) + Math.Sin(dozer_front_imu_L[0]) * Math.Sin(dozer_front_imu_L[1]) * Math.Sin(frontL_yaw));
            front_left_p_h[2] =  front_length[0] * Math.Sin(dozer_front_imu_L[1]) + front_length[1] * Math.Sin(dozer_front_imu_L[0]) * Math.Cos(dozer_front_imu_L[1]);

            //p4_p가 (0, 0, 0)일때 좌표변환된 front_left_p_h 값
            r2_front_left_p_h[0] = -front_left_p_h[1] + p4_left_p_h[0];
            r2_front_left_p_h[1] =  front_left_p_h[0] + p4_left_p_h[1];
            r2_front_left_p_h[2] =  front_left_p_h[2] + p4_left_p_h[2];

            //수정된 모델의 수정된 수식 - 수정된 부분(0712)
            //////////////////////////////p4_right_p_h일때, front_right_p_h구하기(_h : 힌지 좌표계)////////////////////////////

            //암쪽 imu 제거에 따른 수정된 부분(0714)
            dozer_front_imu_R[0] = dozer_blade_imu[0];
            dozer_front_imu_R[1] = dozer_blade_imu[1];

            //p4_right_p_h가 (0, 0, 0)일때 front_right_p_h 값
            front_right_p_h[0] =  front_length[0] * Math.Cos(dozer_front_imu_R[1]) * Math.Cos(frontR_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_R[0]) * Math.Sin(frontR_yaw) - Math.Sin(dozer_front_imu_R[0]) * Math.Sin(dozer_front_imu_R[1]) * Math.Cos(frontR_yaw));
            front_right_p_h[1] = -front_length[0] * Math.Cos(dozer_front_imu_R[1]) * Math.Sin(frontR_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_R[0]) * Math.Cos(frontR_yaw) + Math.Sin(dozer_front_imu_R[0]) * Math.Sin(dozer_front_imu_R[1]) * Math.Sin(frontR_yaw));
            front_right_p_h[2] =  front_length[0] * Math.Sin(dozer_front_imu_R[1]) + front_length[1] * Math.Sin(dozer_front_imu_R[0]) * Math.Cos(dozer_front_imu_R[1]);

            //p4_p가 (0, 0, 0)일때 좌표변환된 front_right_p_h 값
            r2_front_right_p_h[0] = -front_right_p_h[1] + p4_right_p_h[0];
            r2_front_right_p_h[1] =  front_right_p_h[0] + p4_right_p_h[1];
            r2_front_right_p_h[2] =  front_right_p_h[2] + p4_right_p_h[2];



            //수정된 모델의 기존 수식 주석처리 - 수정된 부분(0712)
            ////////////////////////////////p3_p가 (0, 0, 0)일때, front_left_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////////
            ////dozer_body_length[7] : p4_length
            //front_length[0] = dozer_front_length; front_length[1] = dozer_body_length[7]; front_length[2] = 0;

            //front_left_p_h[0] = front_length[0] * Math.Cos(dozer_front_imu_L[1]) * Math.Cos(frontL_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_L[0]) * Math.Sin(frontL_yaw) - Math.Sin(dozer_front_imu_L[0]) * Math.Sin(dozer_front_imu_L[1]) * Math.Cos(frontL_yaw));
            //front_left_p_h[1] = -front_length[0] * Math.Cos(dozer_front_imu_L[1]) * Math.Sin(frontL_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_L[0]) * Math.Cos(frontL_yaw) + Math.Sin(dozer_front_imu_L[0]) * Math.Sin(dozer_front_imu_L[1]) * Math.Sin(frontL_yaw));
            //front_left_p_h[2] = front_length[0] * Math.Sin(dozer_front_imu_L[1]) + front_length[1] * Math.Sin(dozer_front_imu_L[0]) * Math.Cos(dozer_front_imu_L[1]);



            //수정된 모델의 기존 수식 주석처리 - 수정된 부분(0712)
            ////////////////////////////////p3_p가 (0, 0, 0)일때, front_right_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////////
            //front_length[0] = dozer_front_length;                 front_length[1] = -dozer_body_length[7];            front_length[2] = 0;

            //front_right_p_h[0] =  front_length[0] * Math.Cos(dozer_front_imu_R[1]) * Math.Cos(frontR_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_R[0]) * Math.Sin(frontR_yaw) - Math.Sin(dozer_front_imu_R[0]) * Math.Sin(dozer_front_imu_R[1]) * Math.Cos(frontR_yaw));
            //front_right_p_h[1] = -front_length[0] * Math.Cos(dozer_front_imu_R[1]) * Math.Sin(frontR_yaw) + front_length[1] * (Math.Cos(dozer_front_imu_R[0]) * Math.Cos(frontR_yaw) + Math.Sin(dozer_front_imu_R[0]) * Math.Sin(dozer_front_imu_R[1]) * Math.Sin(frontR_yaw));
            //front_right_p_h[2] =  front_length[0] * Math.Sin(dozer_front_imu_R[1]) + front_length[1] * Math.Sin(dozer_front_imu_R[0]) * Math.Cos(dozer_front_imu_R[1]);




            //////////////////////////////r2_front_left_p_h 와, r2_front_right_p_h 가지고 front_p_h구하기(_h : 힌지 좌표계)////////////////////////////
            //p3_p 가 (0, 0, 0) 일때 좌표변환된 front_p_h 값 - 수정된 부분(0713)

            front_p_h[0] = (r2_front_left_p_h[0] + r2_front_right_p_h[0]) / 2;
            front_p_h[1] = (r2_front_left_p_h[1] + r2_front_right_p_h[1]) / 2; 
            front_p_h[2] = (r2_front_left_p_h[2] + r2_front_right_p_h[2]) / 2;




            ////-----------------blade yaw calc - 수정된 모델 적용시
            
            //이동된 front_p_left_p_h
            r2_front_left_p_h_t[0] = r2_front_left_p_h[0] - r2_front_right_p_h[0];
            r2_front_left_p_h_t[1] = r2_front_left_p_h[1] - r2_front_right_p_h[1];

            //atan 계산(rad -> deg)
            blade_atan = Math.Atan(r2_front_left_p_h_t[1] / r2_front_left_p_h_t[0]) * (180.0 / Math.PI);

            //blade yaw 계산 부분
            if (r2_front_left_p_h_t[0] == 0)
            {
                if (r2_front_left_p_h_t[1] > 0) blade_yaw = 90;
                else blade_yaw = -90;
            }

            else
            {
                if (r2_front_left_p_h_t[1] == 0)
                {
                    if (r2_front_left_p_h_t[0] > 0) blade_yaw = 180;
                    else blade_yaw = 0;
                }

                else
                {
                    if (r2_front_left_p_h_t[0] < 0) blade_yaw = -blade_atan;
                    else
                    {
                        if (r2_front_left_p_h_t[1] < 0) blade_yaw = -180 - blade_atan;
                        else blade_yaw = 180 - blade_atan;
                    }
                }
            }

            blade_yaw += ultrasonic_y + d_blade_y_c;//보정식 적용에 따른 추가된 부분(0717)

            //deg -> rad
            blade_yaw *= (Math.PI / 180);

            //blade orientation_o 계산 부분 수정된 모델에 의한 - 추가된 부분(0623)-----------------------------------------------------------------------시작
            if (r2_front_left_p_h_t[0] == 0)
            {
                if (r2_front_left_p_h_t[1] > 0) calc_blade_orientation = 0;
                else calc_blade_orientation = 180;
            }

            else
            {
                if (r2_front_left_p_h_t[1] == 0)
                {
                    if (r2_front_left_p_h_t[0] > 0) calc_blade_orientation = 270;
                    else calc_blade_orientation = 90;
                }

                else
                {
                    if (r2_front_left_p_h_t[0] < 0) calc_blade_orientation = 90 + blade_atan;
                    else calc_blade_orientation = 270 + blade_atan;
                }
            }

            blade_ori = 360 - calc_blade_orientation - calc_blade_calibration + ultrasonic_y + d_blade_y_c;//블레이드의 오리엔테이션값 조절
            //-------------------------------------------------------------------------------------------------------------------------------------------끝




            //////////////////////////////front_p_h일때, blade_left_down_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////////
            blade_left_length[0] = dozer_front2_length; blade_left_length[1] = dozer_blade_length[0]; blade_left_length[2] = -dozer_blade_length[1];

            //front_p_h가 (0, 0, 0) 일때 blade_left_down_p_h - 수정된 부분(0713)
            blade_left_down_p_h[0] =   blade_left_length[0] * Math.Cos(dozer_blade_imu[1]) * Math.Cos(blade_yaw) + blade_left_length[1] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(blade_yaw) - Math.Sin(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Cos(blade_yaw)) - blade_left_length[2] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Cos(blade_yaw) + Math.Sin(dozer_blade_imu[0]) * Math.Sin(blade_yaw));
            blade_left_down_p_h[1] = - blade_left_length[0] * Math.Cos(dozer_blade_imu[1]) * Math.Sin(blade_yaw) + blade_left_length[1] * (Math.Cos(dozer_blade_imu[0]) * Math.Cos(blade_yaw) + Math.Sin(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Sin(blade_yaw)) + blade_left_length[2] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Sin(blade_yaw) - Math.Sin(dozer_blade_imu[0]) * Math.Cos(blade_yaw));
            blade_left_down_p_h[2] =   blade_left_length[0] * Math.Sin(dozer_blade_imu[1]) + blade_left_length[1] * Math.Sin(dozer_blade_imu[0]) * Math.Cos(dozer_blade_imu[1]) + blade_left_length[2] * Math.Cos(dozer_blade_imu[0]) * Math.Cos(dozer_blade_imu[1]);

            ////////////////////////////front_p_h일때, blade_right_down_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////
            blade_right_length[0] = dozer_front2_length; blade_right_length[1] = -dozer_blade_length[0]; blade_right_length[2] = -dozer_blade_length[1];

            //front_p_h가 (0, 0, 0) 일때 blade_right_down_p_h - 수정된 부분(0713)
            blade_right_down_p_h[0] =   blade_right_length[0] * Math.Cos(dozer_blade_imu[1]) * Math.Cos(blade_yaw) + blade_right_length[1] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(blade_yaw) - Math.Sin(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Cos(blade_yaw)) - blade_right_length[2] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Cos(blade_yaw) + Math.Sin(dozer_blade_imu[0]) * Math.Sin(blade_yaw));
            blade_right_down_p_h[1] = - blade_right_length[0] * Math.Cos(dozer_blade_imu[1]) * Math.Sin(blade_yaw) + blade_right_length[1] * (Math.Cos(dozer_blade_imu[0]) * Math.Cos(blade_yaw) + Math.Sin(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Sin(blade_yaw)) + blade_right_length[2] * (Math.Cos(dozer_blade_imu[0]) * Math.Sin(dozer_blade_imu[1]) * Math.Sin(blade_yaw) - Math.Sin(dozer_blade_imu[0]) * Math.Cos(blade_yaw));
            blade_right_down_p_h[2] =   blade_right_length[0] * Math.Sin(dozer_blade_imu[1]) + blade_right_length[1] * Math.Sin(dozer_blade_imu[0]) * Math.Cos(dozer_blade_imu[1]) + blade_right_length[2] * Math.Cos(dozer_blade_imu[0]) * Math.Cos(dozer_blade_imu[1]);

            ///////////////////////////////////front_p_h 에서 좌표변환(-90 deg)된 front_p_h_t - 이미 좌표변환 시켰기 때문에 수정된 부분(0713)///////////////////////////////
            //front_p_h_t[0] = -front_p_h[1];
            //front_p_h_t[1] =  front_p_h[0];
            //front_p_h_t[2] =  front_p_h[2];

            front_p_h_t[0] = front_p_h[0];
            front_p_h_t[1] = front_p_h[1];
            front_p_h_t[2] = front_p_h[2];

            ///////////////////////////////////blade_left_down_p_h 에서 좌표변환(-90 deg)된 blade_left_down_p_h_t///////////////////////////////
            blade_left_down_p_h_t[0] = -blade_left_down_p_h[1];
            blade_left_down_p_h_t[1] =  blade_left_down_p_h[0];
            blade_left_down_p_h_t[2] =  blade_left_down_p_h[2];

            ///////////////////////////////////blade_right_down_p_h 에서 좌표변환(-90 deg)된 blade_right_down_p_h_t///////////////////////////////
            blade_right_down_p_h_t[0] = -blade_right_down_p_h[1];
            blade_right_down_p_h_t[1] =  blade_right_down_p_h[0];
            blade_right_down_p_h_t[2] =  blade_right_down_p_h[2];

            ///////////////////////////////////pc = pb + pc_p///////////////////////////////
            pc[0] = pb[0] + pc_p[0];
            pc[1] = pb[1] + pc_p[1];
            pc[2] = pb[2] + pc_p[2];

            ///////////////////////////////////p3 = pb + p3_p///////////////////////////////
            p3[0] = pb[0] + p3_p[0];
            p3[1] = pb[1] + p3_p[1];
            p3[2] = pb[2] + p3_p[2];

            //추가된 부분(0718)
            //p4_left_p 값
            p4_left_p[0] = p4_left_p_h[0] + p3[0];
            p4_left_p[1] = p4_left_p_h[1] + p3[1];
            p4_left_p[2] = p4_left_p_h[2] + p3[2];

            //Debug.WriteLine(" >>>>>>>>>>>>>>>> p4_left_p[0] : " + p4_left_p[0] + " p4_left_p[1] : " + p4_left_p[1] + " p4_left_p[2] : " + p4_left_p[2]);

            //p4_right_p 값
            p4_right_p[0] = p4_right_p_h[0] + p3[0];
            p4_right_p[1] = p4_right_p_h[1] + p3[1];
            p4_right_p[2] = p4_right_p_h[2] + p3[2];

            //Debug.WriteLine(" >>>>>>>>>>>>>>>> p4_right_p[0] : " + p4_right_p[0] + " p4_right_p[1] : " + p4_right_p[1] + " p4_right_p[2] : " + p4_right_p[2]);

            /////////////////////////////////front_p = p3 + front_p_h_t/////////////////////////
            front_p[0] = p3[0] + front_p_h_t[0];
            front_p[1] = p3[1] + front_p_h_t[1];
            front_p[2] = p3[2] + front_p_h_t[2];

            //수식 수정에 따른 수정된 부분(0713)
            /////////////////////////////////blade_left_down_p = front_p + blade_left_down_p_h_t/////////////////////////
            blade_left_down_p[0] = front_p[0] + blade_left_down_p_h_t[0];
            blade_left_down_p[1] = front_p[1] + blade_left_down_p_h_t[1];
            blade_left_down_p[2] = front_p[2] + blade_left_down_p_h_t[2];

            //수식 수정에 따른 수정된 부분(0713)
            ///////////////////////////////////blade_right_down_p = p3 + blade_right_down_p_h_t///////////////////////////
            blade_right_down_p[0] = front_p[0] + blade_right_down_p_h_t[0];
            blade_right_down_p[1] = front_p[1] + blade_right_down_p_h_t[1];
            blade_right_down_p[2] = front_p[2] + blade_right_down_p_h_t[2];

            //**********************************************************************좌표 변환(상대 -> 절대)*************************************************************************
            /////////////////////////////////////////////////////////////////////////상대->임의 좌표계///////////////////////////////////////////////////////////////////
            b_angle_2 = -b_angle;

            //끝점1
            r2_blade_left_down_p[0] = blade_left_down_p[0] * Math.Cos(b_angle_2) + blade_left_down_p[1] * Math.Sin(b_angle_2);
            r2_blade_left_down_p[1] = -blade_left_down_p[0] * Math.Sin(b_angle_2) + blade_left_down_p[1] * Math.Cos(b_angle_2);
            r2_blade_left_down_p[2] = blade_left_down_p[2];

            //끝점2
            r2_blade_right_down_p[0] = blade_right_down_p[0] * Math.Cos(b_angle_2) + blade_right_down_p[1] * Math.Sin(b_angle_2);
            r2_blade_right_down_p[1] = -blade_right_down_p[0] * Math.Sin(b_angle_2) + blade_right_down_p[1] * Math.Cos(b_angle_2);
            r2_blade_right_down_p[2] = blade_right_down_p[2];

            //위치
            r2_pb[0] = pb[0] * Math.Cos(b_angle_2) + pb[1] * Math.Sin(b_angle_2);
            r2_pb[1] = -pb[0] * Math.Sin(b_angle_2) + pb[1] * Math.Cos(b_angle_2);
            r2_pb[2] = pb[2];

            //수신기1
            r_r2_receiving_set[0] = r_receiving_set[0] * Math.Cos(b_angle_2) + r_receiving_set[1] * Math.Sin(b_angle_2);
            r_r2_receiving_set[1] = -r_receiving_set[0] * Math.Sin(b_angle_2) + r_receiving_set[1] * Math.Cos(b_angle_2);
            r_r2_receiving_set[2] = r_receiving_set[2];

            //수신기2
            r_r2_receiving_set[3] = r_receiving_set[3] * Math.Cos(b_angle_2) + r_receiving_set[4] * Math.Sin(b_angle_2);
            r_r2_receiving_set[4] = -r_receiving_set[3] * Math.Sin(b_angle_2) + r_receiving_set[4] * Math.Cos(b_angle_2);
            r_r2_receiving_set[5] = r_receiving_set[5];

            //중심
            r2_pc[0] = pc[0] * Math.Cos(b_angle_2) + pc[1] * Math.Sin(b_angle_2);
            r2_pc[1] = -pc[0] * Math.Sin(b_angle_2) + pc[1] * Math.Cos(b_angle_2);
            r2_pc[2] = pc[2];

            //힌지
            r2_p3[0] = p3[0] * Math.Cos(b_angle_2) + p3[1] * Math.Sin(b_angle_2);
            r2_p3[1] = -p3[0] * Math.Sin(b_angle_2) + p3[1] * Math.Cos(b_angle_2);
            r2_p3[2] = p3[2];

            //front_p
            r2_front_p[0] = front_p[0] * Math.Cos(b_angle_2) + front_p[1] * Math.Sin(b_angle_2);
            r2_front_p[1] = -front_p[0] * Math.Sin(b_angle_2) + front_p[1] * Math.Cos(b_angle_2);
            r2_front_p[2] = front_p[2];

            /////////////////////////////////////////////////////////////////////////임의->절대 좌표계///////////////////////////////////////////////////////////////////

            //끝단1
            abs_e[0] = r2_blade_left_down_p[0] + instrument_station_1[0];
            abs_e[1] = r2_blade_left_down_p[1] + instrument_station_1[1];
            abs_e[2] = r2_blade_left_down_p[2] + instrument_station_1[2];

            //끝단2
            abs_e[3] = r2_blade_right_down_p[0] + instrument_station_1[0];
            abs_e[4] = r2_blade_right_down_p[1] + instrument_station_1[1];
            abs_e[5] = r2_blade_right_down_p[2] + instrument_station_1[2];

            //위치
            abs_p[0] = r2_pb[0] + instrument_station_1[0];
            abs_p[1] = r2_pb[1] + instrument_station_1[1];
            abs_p[2] = r2_pb[2] + instrument_station_1[2];

            //수신기1
            r_r2_abs_receiving_set[0] = r_r2_receiving_set[0] + instrument_station_1[0];
            r_r2_abs_receiving_set[1] = r_r2_receiving_set[1] + instrument_station_1[1];
            r_r2_abs_receiving_set[2] = r_r2_receiving_set[2] + instrument_station_1[2];

            //수신기2
            r_r2_abs_receiving_set[3] = r_r2_receiving_set[3] + instrument_station_1[0];
            r_r2_abs_receiving_set[4] = r_r2_receiving_set[4] + instrument_station_1[1];
            r_r2_abs_receiving_set[5] = r_r2_receiving_set[5] + instrument_station_1[2];

            //중심
            abs_pc[0] = r2_pc[0] + instrument_station_1[0];
            abs_pc[1] = r2_pc[1] + instrument_station_1[1];
            abs_pc[2] = r2_pc[2] + instrument_station_1[2];

            //힌지
            abs_p3[0] = r2_p3[0] + instrument_station_1[0];
            abs_p3[1] = r2_p3[1] + instrument_station_1[1];
            abs_p3[2] = r2_p3[2] + instrument_station_1[2];

            //front_p
            abs_front_p[0] = r2_front_p[0] + instrument_station_1[0];
            abs_front_p[1] = r2_front_p[1] + instrument_station_1[1];
            abs_front_p[2] = r2_front_p[2] + instrument_station_1[2];

            const_becon_p2_dozer = 1;

            //***********************************************return 값 치환 부분**************************************************
            ///////////////////////절대 좌표계////////////////////////

            //수신기1
            result[0] = Math.Round(r_r2_abs_receiving_set[0]);
            result[1] = Math.Round(r_r2_abs_receiving_set[1]);
            result[2] = Math.Round(r_r2_abs_receiving_set[2]);

            //수신기2
            result[3] = Math.Round(r_r2_abs_receiving_set[3]);
            result[4] = Math.Round(r_r2_abs_receiving_set[4]);
            result[5] = Math.Round(r_r2_abs_receiving_set[5]);

            //위치 좌표
            result[6] = Math.Round(abs_p[0]);
            result[7] = Math.Round(abs_p[1]);
            result[8] = Math.Round(abs_p[2]);

            //끝단1 좌표
            result[9] = Math.Round(abs_e[0]);
            result[10] = Math.Round(abs_e[1]);
            result[11] = Math.Round(abs_e[2]);

            //끝단2 좌표
            result[12] = Math.Round(abs_e[3]);
            result[13] = Math.Round(abs_e[4]);
            result[14] = Math.Round(abs_e[5]);

            //중심 좌표
            result[39] = Math.Round(abs_pc[0]);
            result[40] = Math.Round(abs_pc[1]);
            result[41] = Math.Round(abs_pc[2]);

            //힌지 좌표
            result[42] = Math.Round(abs_p3[0]);
            result[43] = Math.Round(abs_p3[1]);
            result[44] = Math.Round(abs_p3[2]);

            //front_p 좌표
            result[64] = Math.Round(abs_front_p[0]);
            result[65] = Math.Round(abs_front_p[1]);
            result[66] = Math.Round(abs_front_p[2]);

            ///////////////////////상대 좌표계////////////////////////

            //위치 좌표
            result[15] = Math.Round(pb[0]);
            result[16] = Math.Round(pb[1]);
            result[17] = Math.Round(pb[2]);

            //끝단1 좌표
            result[18] = Math.Round(blade_left_down_p[0]);
            result[19] = Math.Round(blade_left_down_p[1]);
            result[20] = Math.Round(blade_left_down_p[2]);

            //끝단2 좌표
            result[21] = Math.Round(blade_right_down_p[0]);
            result[22] = Math.Round(blade_right_down_p[1]);
            result[23] = Math.Round(blade_right_down_p[2]);

            //비콘1 좌표
            result[24] = Math.Round(r_becon_1_v[0]);
            result[25] = Math.Round(r_becon_1_v[1]);
            result[26] = Math.Round(r_becon_1_v[2]);

            //비콘2 좌표
            result[27] = Math.Round(r_becon_2_v[0]);
            result[28] = Math.Round(r_becon_2_v[1]);
            result[29] = Math.Round(r_becon_2_v[2]);

            //비콘3 좌표
            result[30] = Math.Round(r_becon_3_v[0]);
            result[31] = Math.Round(r_becon_3_v[1]);
            result[32] = Math.Round(r_becon_3_v[2]);

            //비콘4 좌표
            result[76] = Math.Round(r_becon_4_v[0]);
            result[77] = Math.Round(r_becon_4_v[1]);
            result[78] = Math.Round(r_becon_4_v[2]);

            //수신기1 좌표
            result[33] = Math.Round(r_receiving_set[0]);
            result[34] = Math.Round(r_receiving_set[1]);
            result[35] = Math.Round(r_receiving_set[2]);

            //수신기2 좌표
            result[36] = Math.Round(r_receiving_set[3]);
            result[37] = Math.Round(r_receiving_set[4]);
            result[38] = Math.Round(r_receiving_set[5]);

            //측점2 좌표
            result[45] = Math.Round(r_instrument_station_2[0]);
            result[46] = Math.Round(r_instrument_station_2[1]);
            result[47] = Math.Round(r_instrument_station_2[2]);

            //측점3 좌표
            result[48] = Math.Round(r_instrument_station_3[0]);
            result[49] = Math.Round(r_instrument_station_3[1]);
            result[50] = Math.Round(r_instrument_station_3[2]);

            //중심 좌표
            result[51] = Math.Round(pc[0]);
            result[52] = Math.Round(pc[1]);
            result[53] = Math.Round(pc[2]);

            //----------------------------------------------오리엔테이션 부분 시작 - 추가된 부분(0625)
            result[54] = Math.Round(orientation_o);//orientation 계산 판별에 따른

            //orientation_o_front_L
            //result[] = Math.Round(frontL_ori);

            //orientation_o_front_R
            //result[] = Math.Round(frontR_ori);

            //orientation_o_front
            result[88] = Math.Round(front_ori_unity);//평균으로 변경에 따른

            //orientation_o_blade
            result[75] = Math.Round(blade_ori);
            //------------------------------------------------오리엔테이션 부분 끝

            //힌지 좌표
            result[55] = Math.Round(p3[0]);
            result[56] = Math.Round(p3[1]);
            result[57] = Math.Round(p3[2]);

            //front_p 좌표
            result[67] = Math.Round(front_p[0]);
            result[68] = Math.Round(front_p[1]);
            result[69] = Math.Round(front_p[2]);

            //orientation_front_L
            result[74] = Math.Round(frontL_ori);

            //orientation_front_R
            result[82] = Math.Round(frontR_ori);


            //orientation_blade
            result[75] = Math.Round(blade_ori);

            /////////////////////////imu(rad to deg)  - 보다 정확한 검증을 위한 소수점 1자리 표현으로 수정된 부분(0711)//////////////////////////
            //몸체
            result[58] = Math.Round((dozer_body_imu[0] * (180.0 / Math.PI)), 1);
            result[59] = Math.Round((dozer_body_imu[1] * (180.0 / Math.PI)), 1);

            //rad를 deg으로  - 저장을 위한 각도 수식 변경에 따른 수정된 부분(0709)
            calc_body_y_o *= (180 / Math.PI);

            result[60] = Math.Round(calc_body_y_o, 1);

            //블레이드
            result[61] = Math.Round((dozer_blade_imu[0] * (180.0 / Math.PI)), 1);
            result[62] = Math.Round((dozer_blade_imu[1] * (180.0 / Math.PI)), 1);
            result[63] = Math.Round((blade_yaw * (180.0 / Math.PI)), 1);
            result[73] = Math.Round(ultrasonic_y, 1);

            //프론트_L
            result[70] = Math.Round((dozer_front_imu_L[0] * (180.0 / Math.PI)), 1);
            result[71] = Math.Round((dozer_front_imu_L[1] * (180.0 / Math.PI)), 1);
            result[72] = Math.Round((frontL_yaw * (180.0 / Math.PI)), 1);

            //프론트_R
            result[79] = Math.Round((dozer_front_imu_R[0] * (180.0 / Math.PI)), 1);
            result[80] = Math.Round((dozer_front_imu_R[1] * (180.0 / Math.PI)), 1);
            result[81] = Math.Round((frontR_yaw * (180.0 / Math.PI)), 1);


            //평균값 계산적용
            result[82] = Math.Round(dozer_front_imu_roll_unity, 1);//평균값 계산에 따른 - 추가된 부분(0625)
            result[83] = Math.Round(dozer_front_imu_pitch_unity, 1);//평균값 계산에 따른 - 추가된 부분(0625)

            //////////////////////////////비컨과 수신기 사이의 각도 - 보다 정확한 검증을 위한 소수점 1자리 표현으로 수정된 부분(0711)//////////////////////////////
            result[84] = Math.Round(b1_angle, 1);
            result[85] = Math.Round(b2_angle, 1);
            result[86] = Math.Round(b3_angle, 1);
            result[87] = Math.Round(b4_angle, 1);

            //88 있음

            return result;
        }


    }
}