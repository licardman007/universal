﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.DozerData
{
    class DozerData
    {
        public String Location;
        public String Motion;

        public RelativePointData RelativePoint;
        public AbsolutePointData AbsolutePoint;

        public ReceiverData Receiver;
        public IMUUltrasonicSensorData IMUSensor;
        public RelativeCoordinateData RelativeCoordinate;
        public AbsoluteCoordinateData AbsoluteCoordinate;

        public OriginalData Original;
        public String UnityOrientationBody;
        public String UnityOrientationFront;
        public String UnityOrientationBlade;

        public DozerData()
        {
            RelativePoint = new RelativePointData();
            AbsolutePoint = new AbsolutePointData();
            Receiver = new ReceiverData();
            IMUSensor = new IMUUltrasonicSensorData();
            RelativeCoordinate = new RelativeCoordinateData();
            AbsoluteCoordinate = new AbsoluteCoordinateData();
            Original = new OriginalData();
        }

        public DozerData(String locationData, String motionData, RelativePointData relativePointData, AbsolutePointData absolutePointData,
            ReceiverData receiverData, IMUUltrasonicSensorData iMUSensorData, RelativeCoordinateData relativeCoordinateData, AbsoluteCoordinateData absoluteCoordinateData, OriginalData originalData)
        {
            this.Location = locationData;
            this.Motion = motionData;
            this.RelativePoint = relativePointData;
            this.AbsolutePoint = absolutePointData;
            this.AbsoluteCoordinate = absoluteCoordinateData;
            this.Receiver = receiverData;
            this.IMUSensor = iMUSensorData;
            this.RelativeCoordinate = relativeCoordinateData;
            this.AbsoluteCoordinate = absoluteCoordinateData;
            this.Original = originalData;
        }
    }
}
