﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.DozerData
{
    class IMUUltrasonicSensorData
    {
        public Coordinate Body { get; set; }
        public Coordinate Front { get; set; }
        public Coordinate2 Blade { get; set; }

        public IMUUltrasonicSensorData()
        {
            Body = new Coordinate();
            Front = new Coordinate();
            Blade = new Coordinate2();
        }

        public IMUUltrasonicSensorData(Coordinate Body, Coordinate Front, Coordinate2 Blade)
        {
            this.Body = Body;
            this.Front = Front;
            this.Blade = Blade;
        }
    }
}
