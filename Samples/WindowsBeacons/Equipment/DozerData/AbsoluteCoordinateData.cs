﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.DozerData
{
    class AbsoluteCoordinateData
    {
        public Coordinate Center { get; set; }
        public Coordinate Hinge { get; set; }
        public Coordinate BladeCenter { get; set; }

        public AbsoluteCoordinateData()
        {
            Center = new Coordinate();
            Hinge = new Coordinate();
            BladeCenter = new Coordinate();
        }

        public AbsoluteCoordinateData(Coordinate Center, Coordinate Hinge, Coordinate BladeCenter)
        {
            this.Center = Center;
            this.Hinge = Hinge;
            this.BladeCenter = BladeCenter;
        }
    }
}
