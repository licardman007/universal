﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.MotorGraderData
{
    class IMUSensorData
    {
        public Coordinate Body { get; set; }
        public Coordinate Arm { get; set; }
        public Coordinate2 Blade { get; set; }

        public IMUSensorData()
        {
            Body = new Coordinate();
            Arm = new Coordinate();
            Blade = new Coordinate2();
        }

        public IMUSensorData(Coordinate Body, Coordinate Arm, Coordinate2 Blade)
        {
            this.Body = Body;
            this.Arm = Arm;
            this.Blade = Blade;
        }
    }
}
