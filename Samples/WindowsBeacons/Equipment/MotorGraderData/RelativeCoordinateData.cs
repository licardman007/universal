﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.MotorGraderData
{
    class RelativeCoordinateData
    {
        //public Coordinate MeasuringPoint1 { get; set; }
        public Coordinate MeasuringPoint2 { get; set; }
        public Coordinate MeasuringPoint3 { get; set; }
        public Coordinate Beacon1 { get; set; }
        public Coordinate Beacon2 { get; set; }
        public Coordinate Beacon3 { get; set; }
        public Coordinate Beacon4 { get; set; }
        public Coordinate Center { get; set; }
        public Coordinate Hinge { get; set; }
        public Coordinate ArmEnd { get; set; }
        public Coordinate BladeCenter { get; set; }

        public RelativeCoordinateData()
        {
            //MeasuringPoint1 = new Coordinate();
            MeasuringPoint2 = new Coordinate();
            MeasuringPoint3 = new Coordinate();
            Beacon1 = new Coordinate();
            Beacon2 = new Coordinate();
            Beacon3 = new Coordinate();
            Beacon4 = new Coordinate();
            Center = new Coordinate();
            Hinge = new Coordinate();
            ArmEnd = new Coordinate();
            BladeCenter = new Coordinate();
        }

        public RelativeCoordinateData(/*Coordinate MeasuringPoint1,*/ Coordinate MeasuringPoint2, Coordinate MeasuringPoint3, 
            Coordinate Beacon1, Coordinate Beacon2, Coordinate Beacon3, Coordinate Beacon4, Coordinate Center, Coordinate Hinge, Coordinate ArmEnd, Coordinate BladeCenter)
        {
            //this.MeasuringPoint1 = MeasuringPoint1;
            this.MeasuringPoint2 = MeasuringPoint2;
            this.MeasuringPoint3 = MeasuringPoint3;
            this.Beacon1 = Beacon1;
            this.Beacon2 = Beacon2;
            this.Beacon3 = Beacon3;
            this.Beacon4 = Beacon4;
            this.Center = Center;
            this.Hinge = Hinge;
            this.ArmEnd = ArmEnd;
            this.BladeCenter = BladeCenter;
        }
    }
}
