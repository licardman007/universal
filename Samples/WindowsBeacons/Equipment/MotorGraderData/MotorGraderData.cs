﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.MotorGraderData
{
    class MotorGraderData
    {
        public String Location;
        public String Motion;

        public RelativePointData RelativePoint;
        public AbsolutePointData AbsolutePoint;

        public ReceiverData Receiver;
        public IMUSensorData IMUSensor;
        public RelativeCoordinateData RelativeCoordinate;

        public OriginalData Original;
        public String UnityOrientationBody;
        public String UnityOrientationArm;
        public String UnityOrientationBlade;

        public MotorGraderData()
        {
            RelativePoint = new RelativePointData();
            AbsolutePoint = new AbsolutePointData();
            Receiver = new ReceiverData();
            IMUSensor = new IMUSensorData();
            RelativeCoordinate = new RelativeCoordinateData();
            Original = new OriginalData();
        }

        public MotorGraderData(String locationData, String motionData, RelativePointData relativePointData, AbsolutePointData absolutePointData,
            ReceiverData receiverData, IMUSensorData iMUSensorData, RelativeCoordinateData relativeCoordinateData, OriginalData originalData)
        {
            this.Location = locationData;
            this.Motion = motionData;
            this.RelativePoint = relativePointData;
            this.AbsolutePoint = absolutePointData;
            this.Receiver = receiverData;
            this.IMUSensor = iMUSensorData;
            this.RelativeCoordinate = relativeCoordinateData;
            this.Original = originalData;
        }

    }
}
