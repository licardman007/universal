﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsBeacons.Equipment.CommonData;

namespace WindowsBeacons.Equipment.MotorGraderData
{
    class RelativePointData
    {
        public String Receiver { get; set; }
        public String EndPoint { get; set; }
        public String Position { get; set; }
        public Coordinate BladeLeft { get; set; }
        public Coordinate BladeRight { get; set; }

        public RelativePointData()
        {
            BladeLeft = new Coordinate();
            BladeRight = new Coordinate();
        }

        public RelativePointData(String Receiver, String EndPoint, String Position, Coordinate BladeLeft, Coordinate BladeRight)
        {
            this.Receiver = Receiver;
            this.EndPoint = EndPoint;
            this.Position = Position;
            this.BladeLeft = BladeLeft;
            this.BladeRight = BladeRight;
        }
    }
}
