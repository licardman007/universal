﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using System.Diagnostics;

namespace WindowsBeacons.Equipment
{
    class MotorGraderCalculator
    {
        //calibration
        double[] p1_calibration = new double[4];
        double[] p2_calibration = new double[4]; 

        //좌표
        double[] becon_1 = new double[3];
        double[] becon_2 = new double[3];
        double[] becon_3 = new double[3];
        double[] becon_4 = new double[3];

        double[] r_becon_1 = new double[3];
        double[] r_becon_2 = new double[3];
        double[] r_becon_3 = new double[3];
        double[] r_becon_4 = new double[3];

        //double[] instrument_station_1 = new double[3];
        double[] instrument_station_2 = new double[3];
        double[] instrument_station_3 = new double[3];

        //측점의 상대좌표
        double[] r_instrument_station_2 = new double[3];
        double[] r_instrument_station_3 = new double[3];

        ////수신기
        double[] p_1_u = new double[3]; double[] p_2_u = new double[3];
        double[] p_1_d = new double[3]; double[] p_2_d = new double[3];

        //수신기 계산 판별
        Int16 p_calc_1 = 1;
        Int16 p_calc_2 = 1;

        //다운된 거리값 보정
        double[] d_1_t = new double[3];
        double[] d_2_t = new double[3];

        //-----------------------------------------------------distance fiter 2 : 시간에 따른 거리 튀는값 제거(추가된 부분 0416)

        double time2_constant_number_1 = 0;

        double[] time2_value_1 = new double[6];
        double[] time2_value_2 = new double[6];

        double time2_boundary_constant_up = 333;//333
        double time2_boundary_constant_down = -333;//-333



        //-------------------------------------------------------------------------------------------K.F.(추가된 부분 0412)
        //---------------------------------------verage
        //double kf_constant_number_1 = 0;
        //double kf_constant_number_2 = 0;
        //double[] kf_value_1 = new double[6];
        //double[] kf_value_2 = new double[6];
        //double kf_a_value = 1;
        //double kf_b_value = 2;
        //double[] kf_value_a = new double[6];

        //---------------------------variable
        //double x_est_p1b1 = 2485;
        //double x_est_p1b2 = 2405;
        //double x_est_p1b3 = 1450;
        //double x_est_p2b1 = 2395;
        //double x_est_p2b2 = 2710;
        //double x_est_p2b3 = 1510;

        double x_est_p1b1 = 0;
        double x_est_p1b2 = 0;
        double x_est_p1b3 = 0;
        double x_est_p2b1 = 0;
        double x_est_p2b2 = 0;
        double x_est_p2b3 = 0;


        double x_est_last_p1b1 = 0;
        double x_est_last_p1b2 = 0;
        double x_est_last_p1b3 = 0;
        double x_est_last_p2b1 = 0;
        double x_est_last_p2b2 = 0;
        double x_est_last_p2b3 = 0;

        //double v_value_p1b1 = 1;//1
        //double v_value_p1b2 = 1;//1
        //double v_value_p1b3 = 1;//1
        //double v_value_p2b1 = 1;//1
        //double v_value_p2b2 = 1;//1
        //double v_value_p2b3 = 1;//1

        double P_last = 0;//0, //계속적 감소, 멈춤(update_covariance)

        double Q = 0.0001;//fixing 0.000001, 0.0001
        double R = 0.001;//fixing 0.001, 0.001

        //double[] std_dev = {361.4387, 102.7628, 468.9504, 555.3744, 173.3461, 685.9053}; 
        //double[] std_dev = { 0, 0, 0, 0, 0, 0 };

        //1이면 적용이 안된것임
        double A_value = 1;
        double H_value = 1;

        //-----------------------------------declaration
        //double update_befor_x_p1b1 = 0;
        //double update_befor_x_p1b2 = 0;
        //double update_befor_x_p1b3 = 0;
        //double update_befor_x_p2b1 = 0;
        //double update_befor_x_p2b2 = 0;
        //double update_befor_x_p2b3 = 0;

        double x_temp_est_p1b1 = 0;
        double x_temp_est_p1b2 = 0;
        double x_temp_est_p1b3 = 0;
        double x_temp_est_p2b1 = 0;
        double x_temp_est_p2b2 = 0;
        double x_temp_est_p2b3 = 0;

        double P_temp = 0;
        double K = 0;
        double P = 0;


        ////----------------------------------------모터그레이더----------------------------------------------------

        //imu_calibration
        double[] m_body_imu_calibration  = new double[2];
        double[] m_rotor_imu_calibration = new double[2];
        double[] m_blade_imu_calibration = new double[2];

        //요값 조정
        double m_calc_body_y_c = 0;
        double m_rotor_y_c = 0;
        double m_blade_y_c = 0;

        double[] becon_p1_motor = new double[4];
        double[] becon_p2_motor = new double[4];

        double[] motorgrader_body_imu = new double[3];
        double[] motorgrader_blade_imu = new double[3];
        double[] motorgrader_rotor_imu = new double[3];

        double[] motorgrader_body_length = new double[7];
        double[] motorgrader_rotor_length = new double[3];
        double[] motorgrader_blade_length = new double[4];

        double[] r_motor_tri_point = new double[12];

        double motorgrader_ultrasonic_a = 0;
        double motorgrader_ultrasonic_b = 0;

        double motorgrader_rotor_angle = 0;
        double motorgrader_zero = 0;

        double[] motor_data = new double[82];
        
        public MotorGraderData.MotorGraderData MotorGraderData
        {
            get;
            set;
        }

        public MotorGraderCalculator()
        {
            MotorGraderData = new MotorGraderData.MotorGraderData();
        }

        public void InitMotorGraderCalcData(TextBox ConstCalibration, TextBox ConstIMUCalibration, TextBox ConstAbsolutePoint, TextBox ConstBeaconPoint, TextBox ConstMotorGrader)
        {
            //calibration
            p1_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[0]);
            p1_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[1]);
            p1_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[2]);
            p1_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[3]);

            p2_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[4]);
            p2_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[5]);
            p2_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[6]);
            p2_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[7]);

            ////motor_imu_calibration
            m_body_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[0]);
            m_body_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[1]);

            m_rotor_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[2]);
            m_rotor_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[3]);

            m_blade_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[4]);
            m_blade_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[5]);

            //측점
            instrument_station_2[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[0]);
            instrument_station_2[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[1]);
            instrument_station_2[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[2]);
            instrument_station_3[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[3]);
            instrument_station_3[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[4]);
            instrument_station_3[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[5]);


            //비콘
            becon_1[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[0]);
            becon_1[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[1]);
            becon_1[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[2]);
            becon_2[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[3]);
            becon_2[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[4]);
            becon_2[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[5]);
            becon_3[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[6]);
            becon_3[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[7]);
            becon_3[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[8]);
            becon_4[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[9]);
            becon_4[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[10]);
            becon_4[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[11]);

            //motorgrader
            motorgrader_body_length[0] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[0]);
            motorgrader_body_length[1] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[1]);
            motorgrader_body_length[2] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[2]);
            motorgrader_body_length[3] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[3]);
            motorgrader_body_length[4] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[4]);
            motorgrader_body_length[5] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[5]);
            motorgrader_body_length[6] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[6]);

            motorgrader_rotor_length[2] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[7]);

            motorgrader_blade_length[0] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[8]);
            motorgrader_blade_length[1] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[9]);
            motorgrader_blade_length[2] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[10]);
            motorgrader_blade_length[3] = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[11]);

            motorgrader_ultrasonic_b = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[12]);
            motorgrader_zero         = Convert.ToDouble(ConstMotorGrader.Text.Split('\r')[13]);
            
            m_calc_body_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[6]);//body_y
            m_rotor_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[7]);//rotor_y
            m_blade_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[8]);//blade_y
        }

        public void MotorGraderDataReceived(String lData, String mData)
        {
            MotorGraderData.Location = lData;
            MotorGraderData.Motion = mData;

            becon_p1_motor[0] = Convert.ToDouble(lData.Substring(2, 6));
            becon_p1_motor[1] = Convert.ToDouble(lData.Substring(8, 6));
            becon_p1_motor[2] = Convert.ToDouble(lData.Substring(14, 6));
            becon_p1_motor[3] = 0;

            becon_p2_motor[0] = Convert.ToDouble(lData.Substring(20, 6));
            becon_p2_motor[1] = Convert.ToDouble(lData.Substring(26, 6));
            becon_p2_motor[2] = Convert.ToDouble(lData.Substring(32, 6));
            becon_p2_motor[3] = 0;

            MotorGraderData.Original.Distance = Convert.ToString(becon_p1_motor[0]) + " " + Convert.ToString(becon_p1_motor[1]) + " " + Convert.ToString(becon_p1_motor[2]) + " " + Convert.ToString(becon_p1_motor[3]) + " "
                                                + Convert.ToString(becon_p2_motor[0]) + " " + Convert.ToString(becon_p2_motor[1]) + " " + Convert.ToString(becon_p2_motor[2] + " " + Convert.ToString(becon_p2_motor[3]));

            //calibration
            becon_p1_motor[0] += p1_calibration[0];
            becon_p1_motor[1] += p1_calibration[1];
            becon_p1_motor[2] += p1_calibration[2];
            becon_p1_motor[3] += p1_calibration[3];

            becon_p2_motor[0] += p2_calibration[0];
            becon_p2_motor[1] += p2_calibration[1];
            becon_p2_motor[2] += p2_calibration[2];
            becon_p2_motor[3] += p2_calibration[3];

            Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " P1s: " + becon_p1_motor[0] + "  " + becon_p1_motor[1] + "  " + becon_p1_motor[2] + "  " + becon_p1_motor[3]
            + " P2s: " + becon_p2_motor[0] + "  " + becon_p2_motor[1] + "  " + becon_p2_motor[2] + "  " + becon_p2_motor[3]);


            //-----------------------------------------------------------------distance fiter 2(추가된 부분 0416)-----------------------------------------------

            //time2_constant_number_1 += 1;

            ////if (time2_constant_number_1 < 9)
            ////{
            ////    time2_value_1[0] = becon_p1_backhoe[0];
            ////    time2_value_1[1] = becon_p1_backhoe[1];
            ////    time2_value_1[2] = becon_p1_backhoe[2];

            ////    time2_value_1[3] = becon_p2_backhoe[0];
            ////    time2_value_1[4] = becon_p2_backhoe[1];
            ////    time2_value_1[5] = becon_p2_backhoe[2];


            ////}

            //if (time2_constant_number_1 < 9)
            //{
            //    becon_p1_motor[0] = 2485;
            //    becon_p1_motor[1] = 2405;
            //    becon_p1_motor[2] = 1450;

            //    becon_p2_motor[0] = 2470;
            //    becon_p2_motor[1] = 2710;
            //    becon_p2_motor[2] = 1510;


            //}

            //else if (time2_constant_number_1 == 10)
            //{
            //    time2_value_2[0] = becon_p1_motor[0];
            //    time2_value_2[1] = becon_p1_motor[1];
            //    time2_value_2[2] = becon_p1_motor[2];

            //    time2_value_2[3] = becon_p2_motor[0];
            //    time2_value_2[4] = becon_p2_motor[1];
            //    time2_value_2[5] = becon_p2_motor[2];

            //    //제거 조건
            //    if ((time2_value_2[0] > x_est_p1b1 + time2_boundary_constant_up) || (time2_value_2[0] < x_est_p1b1 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[0] = x_est_p1b1;
            //        becon_p1_motor[0] = time2_value_2[0];
            //    }

            //    if ((time2_value_2[1] > x_est_p1b2 + time2_boundary_constant_up) || (time2_value_2[1] < x_est_p1b2 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[1] = x_est_p1b2;
            //        becon_p1_motor[1] = time2_value_2[1];
            //    }

            //    if ((time2_value_2[2] > x_est_p1b3 + time2_boundary_constant_up) || (time2_value_2[2] < x_est_p1b3 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[2] = x_est_p1b3;
            //        becon_p1_motor[2] = time2_value_2[2];
            //    }

            //    if ((time2_value_2[3] > x_est_p2b1 + time2_boundary_constant_up) || (time2_value_2[3] < x_est_p2b1 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[3] = x_est_p2b1;
            //        becon_p2_motor[0] = time2_value_2[3];
            //    }

            //    if ((time2_value_2[4] > x_est_p2b2 + time2_boundary_constant_up) || (time2_value_2[4] < x_est_p2b2 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[4] = x_est_p2b2;
            //        becon_p2_motor[1] = time2_value_2[4];
            //    }

            //    if ((time2_value_2[5] > x_est_p2b3 + time2_boundary_constant_up) || (time2_value_2[5] < x_est_p2b3 + time2_boundary_constant_down))
            //    {
            //        time2_value_2[5] = x_est_p2b3;
            //        becon_p2_motor[2] = time2_value_2[5];
            //    }

            //    time2_constant_number_1 = 9;

            

            //}

            ////Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " P1t: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2])
            ////     + " P2t: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + " c : " + time2_constant_number_1);



            ////------------------------------------------------------------------------------K.F.(추가된 부분 0412)

            ////-------------------------------------------averaging
            ////kf_constant_number_1 += 1;

            ////if (kf_constant_number_1 == kf_a_value)
            ////{
            ////    kf_value_1[0] = becon_p1_backhoe[0];
            ////    kf_value_1[1] = becon_p1_backhoe[1];
            ////    kf_value_1[2] = becon_p1_backhoe[2];

            ////    kf_value_1[3] = becon_p2_backhoe[0];
            ////    kf_value_1[4] = becon_p2_backhoe[1];
            ////    kf_value_1[5] = becon_p2_backhoe[2];

            ////    //kf_value_a[0] = kf_value_1[0];
            ////    //kf_value_a[1] = kf_value_1[1];
            ////    //kf_value_a[2] = kf_value_1[2];

            ////    //kf_value_a[3] = kf_value_1[3];
            ////    //kf_value_a[4] = kf_value_1[4];
            ////    //kf_value_a[5] = kf_value_1[5];

            ////}


            ////else if (kf_constant_number_1 == kf_b_value)
            ////{
            ////    kf_value_2[0] = becon_p1_backhoe[0];
            ////    kf_value_2[1] = becon_p1_backhoe[1];
            ////    kf_value_2[2] = becon_p1_backhoe[2];

            ////    kf_value_2[3] = becon_p2_backhoe[0];
            ////    kf_value_2[4] = becon_p2_backhoe[1];
            ////    kf_value_2[5] = becon_p2_backhoe[2];

            ////    kf_value_1[0] = (kf_value_1[0] + kf_value_2[0]) / 2;
            ////    kf_value_1[1] = (kf_value_1[1] + kf_value_2[1]) / 2;
            ////    kf_value_1[2] = (kf_value_1[2] + kf_value_2[2]) / 2;

            ////    kf_value_1[3] = (kf_value_1[3] + kf_value_2[3]) / 2;
            ////    kf_value_1[4] = (kf_value_1[4] + kf_value_2[4]) / 2;
            ////    kf_value_1[5] = (kf_value_1[5] + kf_value_2[5]) / 2;

            ////    //std_dev[0] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
            ////    //std_dev[1] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
            ////    //std_dev[2] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);

            ////    //std_dev[3] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
            ////    //std_dev[4] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);
            ////    //std_dev[5] = Math.Sqrt((Math.Pow(kf_value_a[0] - kf_value_1[0], 2) + Math.Pow(kf_value_a[0] - kf_value_1[0], 2)) / 2);

            ////    kf_constant_number_1 = 1;
            ////}

            ////-----------------------------------------------------------------------------prediction
            ////#1
            ////update_befor_x_p1b1 = update_x_p1b1;
            ////predicton_x_p1b1 = update_x_p1b1 + v_value_p1b1;

            ////update_befor_x_p1b2 = update_x_p1b2;
            ////predicton_x_p1b2 = update_x_p1b2 + v_value_p1b2;

            ////update_befor_x_p1b3 = update_x_p1b3;
            ////predicton_x_p1b3 = update_x_p1b3 + v_value_p1b3;

            ////update_befor_x_p2b1 = update_x_p2b1;
            ////predicton_x_p2b1 = update_x_p2b1 + v_value_p2b1;

            ////update_befor_x_p2b2 = update_x_p2b2;
            ////predicton_x_p2b2 = update_x_p2b2 + v_value_p2b2;

            ////update_befor_x_p2b3 = update_x_p2b3;
            ////predicton_x_p2b3 = update_x_p2b3 + v_value_p2b3;

            ////#2
            ////update_x_p1b1 = becon_p1_backhoe[0];
            ////update_x_p1b2 = becon_p1_backhoe[1];
            ////update_x_p1b3 = becon_p1_backhoe[2];
            ////update_x_p2b1 = becon_p2_backhoe[0];
            ////update_x_p2b2 = becon_p2_backhoe[1];
            ////update_x_p2b3 = becon_p2_backhoe[2];

            //x_temp_est_p1b1 = A_value * x_est_last_p1b1;
            //x_temp_est_p1b2 = A_value * x_est_last_p1b2;
            //x_temp_est_p1b3 = A_value * x_est_last_p1b3;
            //x_temp_est_p2b1 = A_value * x_est_last_p2b1;
            //x_temp_est_p2b2 = A_value * x_est_last_p2b2;
            //x_temp_est_p2b3 = A_value * x_est_last_p2b3;

            //P_temp = A_value * P_last * (1 / A_value) + Q;

            ////#3(IMU)
            ////predicton_x_p1b1 = predicton_x_p1b1 + dt * (becon_p1_backhoe[0] - x_bias_p1b1);
            ////predicton_x_p1b2 = predicton_x_p1b2 + dt * (becon_p1_backhoe[1] - x_bias_p1b2);
            ////predicton_x_p1b3 = predicton_x_p1b3 + dt * (becon_p1_backhoe[2] - x_bias_p1b3);
            ////predicton_x_p2b1 = predicton_x_p2b1 + dt * (becon_p2_backhoe[0] - x_bias_p2b1);
            ////predicton_x_p2b2 = predicton_x_p2b2 + dt * (becon_p2_backhoe[1] - x_bias_p2b2);
            ////predicton_x_p2b3 = predicton_x_p2b3 + dt * (becon_p2_backhoe[2] - x_bias_p2b3);

            //////x_bias_p1b1 += 0.014; 
            //////x_bias_p1b2 += 0.014;
            //////x_bias_p1b3 += 0.014;
            //////x_bias_p2b1 += 0.014;
            //////x_bias_p2b2 += 0.014;
            //////x_bias_p2b3 += 0.014;

            ////p_00 = p_00 - dt * (p_10 + p_01) + Math.Pow(dt, 2) * p_11 + predicton_noise_covariance;
            ////p_01 = p_01 - dt * p_11;
            ////p_10 = p_10 - dt * p_11;
            ////p_11 = p_11 + predicton_noise_covariance;

            ////---------------------------------------------------------------------------------------------------------------------------------------------update
            ////#1, 2
            //K = P_temp * (1 / H_value) * (1 / (H_value * P_temp * (1 / H_value) + R));

            ////update_x_p1b1 = predicton_x_p1b1 + kalman_gain * (kf_value_1[0] + std_dev[0] - predicton_x_p1b1);
            //x_est_p1b1 = x_temp_est_p1b1 + K * (becon_p1_motor[0] - H_value * x_temp_est_p1b1);
            ////v_value_p1b1 = update_x_p1b1 - update_befor_x_p1b1;

            ////update_x_p1b2 = predicton_x_p1b2 + kalman_gain * (kf_value_1[1] + std_dev[1] - predicton_x_p1b2);
            //x_est_p1b2 = x_temp_est_p1b2 + K * (becon_p1_motor[1] - H_value * x_temp_est_p1b2);
            ////v_value_p1b2 = update_x_p1b2 - update_befor_x_p1b2;

            ////update_x_p1b3 = predicton_x_p1b3 + kalman_gain * (kf_value_1[2] + std_dev[2] - predicton_x_p1b3);
            //x_est_p1b3 = x_temp_est_p1b3 + K * (becon_p1_motor[2] - H_value * x_temp_est_p1b3);
            ////v_value_p1b3 = update_x_p1b3 - update_befor_x_p1b3;

            ////update_x_p2b1 = predicton_x_p2b1 + kalman_gain * (kf_value_1[3] + std_dev[3] - predicton_x_p2b1);
            //x_est_p2b1 = x_temp_est_p2b1 + K * (becon_p2_motor[0] - H_value * x_temp_est_p2b1);
            //// v_value_p2b1 = update_x_p2b1 - update_befor_x_p2b1;

            ////update_x_p2b2 = predicton_x_p2b2 + kalman_gain * (kf_value_1[4] + std_dev[4] - predicton_x_p2b2);
            //x_est_p2b2 = x_temp_est_p2b2 + K * (becon_p2_motor[1] - H_value * x_temp_est_p2b2);
            //// v_value_p2b2 = update_x_p2b2 - update_befor_x_p2b2;

            ////update_x_p2b3 = predicton_x_p2b3 + kalman_gain * (kf_value_1[5] + std_dev[5] - predicton_x_p2b3);
            //x_est_p2b3 = x_temp_est_p2b3 + K * (becon_p2_motor[2] - H_value * x_temp_est_p2b3);
            //// v_value_p2b3 = update_x_p2b3 - update_befor_x_p2b3;

            //P = P_temp * (1 - K * H_value);

            //P_last = P;
            //x_est_last_p1b1 = x_est_p1b1;
            //x_est_last_p1b2 = x_est_p1b2;
            //x_est_last_p1b3 = x_est_p1b3;
            //x_est_last_p2b1 = x_est_p2b1;
            //x_est_last_p2b2 = x_est_p2b2;
            //x_est_last_p2b3 = x_est_p2b3;

            ////#3(I.M.U.)
            ////y_value_p1b1 = becon_p1_backhoe[0] - predicton_x_p1b1;
            ////y_value_p1b2 = becon_p1_backhoe[1] - predicton_x_p1b2;
            ////y_value_p1b3 = becon_p1_backhoe[2] - predicton_x_p1b3;
            ////y_value_p2b1 = becon_p2_backhoe[0] - predicton_x_p2b1;
            ////y_value_p2b2 = becon_p2_backhoe[1] - predicton_x_p2b2;
            ////y_value_p2b3 = becon_p2_backhoe[2] - predicton_x_p2b3;

            ////s_value = p_00 + measurement_noise_covariance;

            ////predicton_x_p1b1 = predicton_x_p1b1 + p_00 * (y_value_p1b1 / s_value);
            ////predicton_x_p1b2 = predicton_x_p1b2 + p_00 * (y_value_p1b2 / s_value);
            ////predicton_x_p1b3 = predicton_x_p1b3 + p_00 * (y_value_p1b3 / s_value);
            ////predicton_x_p2b1 = predicton_x_p2b1 + p_00 * (y_value_p2b1 / s_value);
            ////predicton_x_p2b2 = predicton_x_p2b2 + p_00 * (y_value_p2b2 / s_value);
            ////predicton_x_p2b3 = predicton_x_p2b3 + p_00 * (y_value_p2b3 / s_value);

            ////x_bias_p1b1 = x_bias_p1b1 + p_10 * (y_value_p1b1 / s_value);
            ////x_bias_p1b2 = x_bias_p1b2 + p_10 * (y_value_p1b2 / s_value);
            ////x_bias_p1b3 = x_bias_p1b3 + p_10 * (y_value_p1b3 / s_value);
            ////x_bias_p2b1 = x_bias_p2b1 + p_10 * (y_value_p2b1 / s_value);
            ////x_bias_p2b2 = x_bias_p2b2 + p_10 * (y_value_p2b2 / s_value);
            ////x_bias_p2b3 = x_bias_p2b3 + p_10 * (y_value_p2b3 / s_value);

            ////p_00 = p_00 - Math.Pow(p_00, 2) * (1 / s_value);
            ////p_01 = p_01 - p_00 * p_01 * (1 / s_value);
            ////p_10 = p_10 - p_10 * p_00 * (1 / s_value);
            ////p_11 = p_11 - p_10 * p_01 * (1 / s_value);



            //becon_p1_motor[0] = Math.Round(x_est_p1b1);
            //becon_p1_motor[1] = Math.Round(x_est_p1b2);
            //becon_p1_motor[2] = Math.Round(x_est_p1b3);

            //becon_p2_motor[0] = Math.Round(x_est_p2b1);
            //becon_p2_motor[1] = Math.Round(x_est_p2b2);
            //becon_p2_motor[2] = Math.Round(x_est_p2b3);

            //// Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " P1k: " + becon_p1_backhoe[0] + "  " + becon_p1_backhoe[1] + "  " + becon_p1_backhoe[2]
            ////+ " P2k: " + becon_p2_backhoe[0] + "  " + becon_p2_backhoe[1] + "  " + becon_p2_backhoe[2]);





            //---------------------------------------------------거리값 검사------------------------------------------
            MotorGraderData.Receiver.P1.DistanceB1 = Convert.ToString(Math.Round(becon_p1_motor[0]));
            MotorGraderData.Receiver.P1.DistanceB2 = Convert.ToString(Math.Round(becon_p1_motor[1]));
            MotorGraderData.Receiver.P1.DistanceB3 = Convert.ToString(Math.Round(becon_p1_motor[2]));

            MotorGraderData.Receiver.P2.DistanceB1 = Convert.ToString(Math.Round(becon_p2_motor[0]));
            MotorGraderData.Receiver.P2.DistanceB2 = Convert.ToString(Math.Round(becon_p2_motor[1]));
            MotorGraderData.Receiver.P2.DistanceB3 = Convert.ToString(Math.Round(becon_p2_motor[2]));
            
            //Debug.WriteLine("----------------------------------------------------------------------------------------------------------------------------");


            motor_data = Calc_motor_point(becon_p1_motor, becon_p2_motor);

            //비콘1 좌표
            MotorGraderData.RelativeCoordinate.Beacon1.X = Convert.ToString(motor_data[36]);
            MotorGraderData.RelativeCoordinate.Beacon1.Y = Convert.ToString(motor_data[37]);
            MotorGraderData.RelativeCoordinate.Beacon1.Z = Convert.ToString(motor_data[38]);

            //비콘2 좌표
            MotorGraderData.RelativeCoordinate.Beacon2.X = Convert.ToString(motor_data[39]);
            MotorGraderData.RelativeCoordinate.Beacon2.Y = Convert.ToString(motor_data[40]);
            MotorGraderData.RelativeCoordinate.Beacon2.Z = Convert.ToString(motor_data[41]);

            //비콘3 좌표
            MotorGraderData.RelativeCoordinate.Beacon3.X = Convert.ToString(motor_data[42]);
            MotorGraderData.RelativeCoordinate.Beacon3.Y = Convert.ToString(motor_data[43]);
            MotorGraderData.RelativeCoordinate.Beacon3.Z = Convert.ToString(motor_data[44]);

            //비콘4 좌표
            MotorGraderData.RelativeCoordinate.Beacon4.X = "0";
            MotorGraderData.RelativeCoordinate.Beacon4.Y = "0";
            MotorGraderData.RelativeCoordinate.Beacon4.Z = "0";
            /*
            //측점1 좌표
            MotorGraderData.RelativeCoordinate.MeasuringPoint1.X = "0";
            MotorGraderData.RelativeCoordinate.MeasuringPoint1.Y = "0";
            MotorGraderData.RelativeCoordinate.MeasuringPoint1.Z = "0";
            */
            //측점2 좌표
            MotorGraderData.RelativeCoordinate.MeasuringPoint2.X = Convert.ToString(motor_data[51]);
            MotorGraderData.RelativeCoordinate.MeasuringPoint2.Y = Convert.ToString(motor_data[52]);
            MotorGraderData.RelativeCoordinate.MeasuringPoint2.Z = Convert.ToString(motor_data[53]);
            
            //측점3 좌표
            MotorGraderData.RelativeCoordinate.MeasuringPoint3.X = Convert.ToString(motor_data[54]);
            MotorGraderData.RelativeCoordinate.MeasuringPoint3.Y = Convert.ToString(motor_data[55]);
            MotorGraderData.RelativeCoordinate.MeasuringPoint3.Z = Convert.ToString(motor_data[56]);

            //중심 좌표
            MotorGraderData.RelativeCoordinate.Center.X = Convert.ToString(motor_data[57]);
            MotorGraderData.RelativeCoordinate.Center.Y = Convert.ToString(motor_data[58]);
            MotorGraderData.RelativeCoordinate.Center.Z = Convert.ToString(motor_data[59]);

            //orientation_body
            MotorGraderData.UnityOrientationBody = Convert.ToString(motor_data[60]);

            //힌지 좌표
            MotorGraderData.RelativeCoordinate.Hinge.X = Convert.ToString(motor_data[61]);
            MotorGraderData.RelativeCoordinate.Hinge.Y = Convert.ToString(motor_data[62]);
            MotorGraderData.RelativeCoordinate.Hinge.Z = Convert.ToString(motor_data[63]);

            //암 끝점
            MotorGraderData.RelativeCoordinate.ArmEnd.X = Convert.ToString(motor_data[64]);
            MotorGraderData.RelativeCoordinate.ArmEnd.Y = Convert.ToString(motor_data[65]);
            MotorGraderData.RelativeCoordinate.ArmEnd.Z = Convert.ToString(motor_data[66]);

            //블레이드 중심 좌표
            MotorGraderData.RelativeCoordinate.BladeCenter.X = Convert.ToString(motor_data[67]);
            MotorGraderData.RelativeCoordinate.BladeCenter.Y = Convert.ToString(motor_data[68]);
            MotorGraderData.RelativeCoordinate.BladeCenter.Z = Convert.ToString(motor_data[69]);

            //orientation_arm
            MotorGraderData.UnityOrientationArm = Convert.ToString(motor_data[70]);

            //orientation_blade
            MotorGraderData.UnityOrientationBlade = Convert.ToString(motor_data[71]);

            //////////////////////////장비의 회전방향/////////////////////////
            //몸체(장비중심)
            MotorGraderData.IMUSensor.Body.X = Convert.ToString(-motor_data[73]) + "(" + Convert.ToString(motor_data[72]) + ")";
            MotorGraderData.IMUSensor.Body.Y = Convert.ToString(motor_data[72]) + "(" + Convert.ToString(motor_data[73]) + ")";
            MotorGraderData.IMUSensor.Body.Z = Convert.ToString(motor_data[74]);

            //암
            MotorGraderData.IMUSensor.Arm.X = Convert.ToString(motor_data[75]);
            MotorGraderData.IMUSensor.Arm.Y = Convert.ToString(motor_data[76]);
            MotorGraderData.IMUSensor.Arm.Z = Convert.ToString(motor_data[77]);

            //블레이드
            MotorGraderData.IMUSensor.Blade.X = Convert.ToString(motor_data[78]);
            MotorGraderData.IMUSensor.Blade.Y = Convert.ToString(motor_data[79]);
            MotorGraderData.IMUSensor.Blade.Z = Convert.ToString(motor_data[80]);
            MotorGraderData.IMUSensor.Blade.Z2 = "(" + Convert.ToString(motor_data[81]) + ")";

            //상대 좌표계(끝단 좌표 값)
            MotorGraderData.RelativePoint.EndPoint = "m_endpoint_1, " + motor_data[30] + ", " + motor_data[31] + ", " + motor_data[32] + ", " + "m_endpoint_2, " + motor_data[33] + ", " + motor_data[34] + ", " + motor_data[35];
            MotorGraderData.RelativePoint.BladeLeft.X = motor_data[30].ToString();
            MotorGraderData.RelativePoint.BladeLeft.Y = motor_data[31].ToString();
            MotorGraderData.RelativePoint.BladeLeft.Z = motor_data[32].ToString();
            MotorGraderData.RelativePoint.BladeRight.X = motor_data[33].ToString();
            MotorGraderData.RelativePoint.BladeRight.Y = motor_data[34].ToString();
            MotorGraderData.RelativePoint.BladeRight.Z = motor_data[35].ToString();

            //상대 좌표계(위치 좌표 값)
            MotorGraderData.RelativePoint.Position = "m_position, " + motor_data[27] + ", " + motor_data[28] + ", " + motor_data[29];

            //상대 좌표계(수신기 좌표 값)
            if (p_calc_1 == 1 && p_calc_2 == 1)
            {
                MotorGraderData.RelativePoint.Receiver = "m_receiver_1(calc), " + motor_data[45] + ", " + motor_data[46] + ", " + motor_data[47] + ", " + "m_receiver_2(calc), " + motor_data[48] + ", " + motor_data[49] + ", " + motor_data[50];
            }

            else if (p_calc_1 == 1 && p_calc_2 == 0)
            {
                MotorGraderData.RelativePoint.Receiver = "m_receiver_1(calc), " + motor_data[45] + ", " + motor_data[46] + ", " + motor_data[47] + ", " + "m_receiver_2, " + motor_data[48] + ", " + motor_data[49] + ", " + motor_data[50];
            }

            else if (p_calc_1 == 0 && p_calc_2 == 1)
            {
                MotorGraderData.RelativePoint.Receiver = "m_receiver_1, " + motor_data[45] + ", " + motor_data[46] + ", " + motor_data[47] + ", " + "m_receiver_2(calc), " + motor_data[48] + ", " + motor_data[49] + ", " + motor_data[50];
            }

            else
            {
                MotorGraderData.RelativePoint.Receiver = "m_receiver_1, " + motor_data[45] + ", " + motor_data[46] + ", " + motor_data[47] + ", " + "m_receiver_2, " + motor_data[48] + ", " + motor_data[49] + ", " + motor_data[50];
            }

            //광파기 좌표계 (끝단 좌표 값)
            MotorGraderData.AbsolutePoint.EndPoint = "m_endpoint";

            //광파기 좌표계 (위치 좌표 값)
            MotorGraderData.AbsolutePoint.Position = "m_position";

            //광파기 좌표계 (수신기 좌표 값)
            MotorGraderData.AbsolutePoint.Receiver = "m_receiver";
        }

        double[] r_Calc_tri(double[] d_1, double[] d_2)
        {
            //-----------원본 계산 변수-------------


            double[] result = new double[20];

            double[] becon_1_1 = new double[3];
            double[] becon_2_1 = new double[3];
            double[] becon_3_1 = new double[3];
            double[] becon_4_1 = new double[3];

            double a_1 = 0, b_1 = 0, c_1 = 0, p_1_w_1 = 0, p_2_w_1 = 0;
            double a_2 = 0, b_2 = 0, c_2 = 0, p_1_w_2 = 0, p_2_w_2 = 0;

            double s_A_1 = 0, p_1_s_B_1 = 0, p_2_s_B_1 = 0;
            double s_A_2 = 0, p_1_s_B_2 = 0, p_2_s_B_2 = 0;

            double Aa = 0, p_1_Bb = 0, p_1_Cc = 0, p_2_Bb = 0, p_2_Cc = 0;

            double[] p_1_t_u = new double[3]; double[] p_2_t_u = new double[3];
            double[] p_1_t_d = new double[3]; double[] p_2_t_d = new double[3];

            //-----------오차 계산 변수-------------
            double B1B2 = 0, B1B3 = 0, B2B3 = 0;//비콘 사이의 거리

            //------수신기 1---------
            double D1T_u = 0, D2T_u = 0;
            double D1T_d = 0, D3T_u = 0;
            double D2T_d = 0, D3T_d = 0;

            double D1T_1 = 0, D2T_1, D3T_1 = 0;
            double D1T_2 = 0, D2T_2, D3T_2 = 0;

            //직선의 방정식
            double A1 = 0, B1 = 0, C1 = 0;
            double A2 = 0, B2 = 0, C2 = 0;
            double A3 = 0, B3 = 0, C3 = 0;

            //직선의 교점
            double x1 = 0, y1 = 0;
            double x2 = 0, y2 = 0;
            double x3 = 0, y3 = 0;

            double cx = 0, cy = 0;//무게중심
            //--------------------------

            //------수신기 2---------
            double D1T_u_2 = 0, D2T_u_2 = 0;
            double D1T_d_2 = 0, D3T_u_2 = 0;
            double D2T_d_2 = 0, D3T_d_2 = 0;

            double D1T_1_2 = 0, D2T_1_2, D3T_1_2 = 0;
            double D1T_2_2 = 0, D2T_2_2, D3T_2_2 = 0;

            //직선의 방정식
            double A1_2 = 0, B1_2 = 0, C1_2 = 0;
            double A2_2 = 0, B2_2 = 0, C2_2 = 0;
            double A3_2 = 0, B3_2 = 0, C3_2 = 0;

            //직선의 교점
            double x1_2 = 0, y1_2 = 0;
            double x2_2 = 0, y2_2 = 0;
            double x3_2 = 0, y3_2 = 0;

            double cx_2 = 0, cy_2 = 0;//무게중심

            //비콘1 = (r_becon_1[0], r_becon_1[1], r_becon_1[2])
            //비콘2 = (r_becon_2[0], r_becon_2[1], r_becon_2[2])
            //비콘3 = (r_becon_3[0], r_becon_3[1], r_becon_3[2])

            //----수신기 1------
            //거리1 = d_1[0]
            //거리2 = d_1[1]
            //거리3 = d_1[2]

            //----수신기 2------
            //거리1 = d_2[0]
            //거리2 = d_2[1]
            //거리3 = d_2[2]

            ////******************************************************************다운된 거리값 보정*************************************************************

            //P1일 경우
            if (d_1[0] == 111111) d_1[0] = d_1_t[0];
            d_1_t[0] = d_1[0];

            if (d_1[1] == 222222) d_1[1] = d_1_t[1];
            d_1_t[1] = d_1[1];

            if (d_1[2] == 333333) d_1[2] = d_1_t[2];
            d_1_t[2] = d_1[2];

            //P2일 경우
            if (d_2[0] == 111111) d_2[0] = d_2_t[0];
            d_2_t[0] = d_2[0];

            if (d_2[1] == 222222) d_2[1] = d_2_t[1];
            d_2_t[1] = d_2[1];

            if (d_2[2] == 333333) d_2[2] = d_2_t[2];
            d_2_t[2] = d_2[2];



            //******************************************************************becon의 변형된 위치*************************************************************

            becon_1_1[0] = r_becon_1[0] - r_becon_1[0]; becon_1_1[1] = r_becon_1[1] - r_becon_1[1]; becon_1_1[2] = r_becon_1[2] - r_becon_1[2];
            becon_2_1[0] = r_becon_2[0] - r_becon_1[0]; becon_2_1[1] = r_becon_2[1] - r_becon_1[1]; becon_2_1[2] = r_becon_2[2] - r_becon_1[2];
            becon_3_1[0] = r_becon_3[0] - r_becon_1[0]; becon_3_1[1] = r_becon_3[1] - r_becon_1[1]; becon_3_1[2] = r_becon_3[2] - r_becon_1[2];
            becon_4_1[0] = r_becon_4[0] - r_becon_1[0]; becon_4_1[1] = r_becon_4[1] - r_becon_1[1]; becon_4_1[2] = r_becon_4[2] - r_becon_1[2];

            //*********************************************************************평면의 방정식*****************************************************************
            a_1 = 2 * becon_2_1[0]; b_1 = 2 * becon_2_1[1]; c_1 = 2 * becon_2_1[2];
            a_2 = 2 * becon_3_1[0]; b_2 = 2 * becon_3_1[1]; c_2 = 2 * becon_3_1[2];

            //////////////////P1일 경우///////////////////
            p_1_w_1 = Math.Pow(d_1[0], 2) - Math.Pow(d_1[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_1_w_2 = Math.Pow(d_1[0], 2) - Math.Pow(d_1[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);

            //////////////////P2일 경우///////////////////
            p_2_w_1 = Math.Pow(d_2[0], 2) - Math.Pow(d_2[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_2_w_2 = Math.Pow(d_2[0], 2) - Math.Pow(d_2[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);

            //*******************************************************************2차 연립방정식******************************************************************
            s_A_1 = (c_1 * a_2 - c_2 * a_1) / (a_1 * b_2 - a_2 * b_1);
            s_A_2 = (c_2 * b_1 - c_1 * b_2) / (a_1 * b_2 - a_2 * b_1);

            //////////////////P1일 경우////////////////////
            p_1_s_B_1 = (a_1 * p_1_w_2 - a_2 * p_1_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_1_s_B_2 = (b_2 * p_1_w_1 - b_1 * p_1_w_2) / (a_1 * b_2 - a_2 * b_1);

            //////////////////P2일 경우////////////////////
            p_2_s_B_1 = (a_1 * p_2_w_2 - a_2 * p_2_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_2_s_B_2 = (b_2 * p_2_w_1 - b_1 * p_2_w_2) / (a_1 * b_2 - a_2 * b_1);

            //******************************************************************z에 관한 2차 방정식****************************************************************
            Aa = Math.Pow(s_A_2, 2) + Math.Pow(s_A_1, 2) + 1;

            //////////////////P1일 경우////////////////////
            p_1_Bb = s_A_2 * p_1_s_B_2 + s_A_1 * p_1_s_B_1;
            p_1_Cc = Math.Pow(p_1_s_B_2, 2) + Math.Pow(p_1_s_B_1, 2) - Math.Pow(d_1_t[0], 2);

            //////////////////P2일 경우////////////////////
            p_2_Bb = s_A_2 * p_2_s_B_2 + s_A_1 * p_2_s_B_1;
            p_2_Cc = Math.Pow(p_2_s_B_2, 2) + Math.Pow(p_2_s_B_1, 2) - Math.Pow(d_2_t[0], 2);

            //******************************************************************becon의 오차 계산*************************************************************
            //수신기 1번 계산 판별
            if (Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc < 0)
            {


                //---------------비콘 사이의 거리 계산-----------------
                B1B2 = Math.Sqrt(Math.Pow(r_becon_2[0] - r_becon_1[0], 2) + Math.Pow(r_becon_2[1] - r_becon_1[1], 2) + Math.Pow(r_becon_2[2] - r_becon_1[2], 2));
                B1B3 = Math.Sqrt(Math.Pow(r_becon_3[0] - r_becon_1[0], 2) + Math.Pow(r_becon_3[1] - r_becon_1[1], 2) + Math.Pow(r_becon_3[2] - r_becon_1[2], 2));
                B2B3 = Math.Sqrt(Math.Pow(r_becon_3[0] - r_becon_2[0], 2) + Math.Pow(r_becon_3[1] - r_becon_2[1], 2) + Math.Pow(r_becon_3[2] - r_becon_2[2], 2));

                //-------------------------------------------------------------------수신기 1 계산------------------------------------------------------
                //---------------구의 반지름 계산(수신기 1)-----------------
                D1T_u = d_1_t[0] + (B1B2 + 2 - d_1_t[0] - d_1_t[1]) / 2;
                D2T_u = d_1_t[1] + (B1B2 + 2 - d_1_t[0] - d_1_t[1]) / 2;

                D1T_d = d_1_t[0] + (B1B3 + 2 - d_1_t[0] - d_1_t[2]) / 2;
                D3T_u = d_1_t[2] + (B1B3 + 2 - d_1_t[0] - d_1_t[2]) / 2;

                D2T_d = d_1_t[1] + (B2B3 + 2 - d_1_t[1] - d_1_t[2]) / 2;
                D3T_d = d_1_t[2] + (B2B3 + 2 - d_1_t[1] - d_1_t[2]) / 2;

                D1T_1 = (D1T_u - D1T_d > 0) ? D1T_u : D1T_d;
                D2T_1 = (D2T_u - D2T_d > 0) ? D2T_u : D2T_d;
                D3T_1 = (D3T_u - D3T_d > 0) ? D3T_u : D3T_d;

                D1T_2 = (d_1_t[0] - D1T_1 > 0) ? d_1_t[0] : D1T_1;
                D2T_2 = (d_1_t[1] - D2T_1 > 0) ? d_1_t[1] : D2T_1;
                D3T_2 = (d_1_t[2] - D3T_1 > 0) ? d_1_t[2] : D3T_1;

                //---------------직선의 방정식(수신기 1)-----------------
                A1 = 2 * (r_becon_2[0] - r_becon_1[0]);
                B1 = 2 * (r_becon_2[1] - r_becon_1[1]);
                C1 = Math.Pow(r_becon_1[0], 2) + Math.Pow(r_becon_1[1], 2) - Math.Pow(r_becon_2[0], 2) - Math.Pow(r_becon_2[1], 2) - Math.Pow(D1T_2, 2) + Math.Pow(D2T_2, 2);

                A2 = 2 * (r_becon_3[0] - r_becon_1[0]);
                B2 = 2 * (r_becon_3[1] - r_becon_1[1]);
                C2 = Math.Pow(r_becon_1[0], 2) + Math.Pow(r_becon_1[1], 2) - Math.Pow(r_becon_3[0], 2) - Math.Pow(r_becon_3[1], 2) - Math.Pow(D1T_2, 2) + Math.Pow(D3T_2, 2);

                A3 = 2 * (r_becon_3[0] - r_becon_2[0]);
                B3 = 2 * (r_becon_3[1] - r_becon_2[1]);
                C3 = Math.Pow(r_becon_2[0], 2) + Math.Pow(r_becon_2[1], 2) - Math.Pow(r_becon_3[0], 2) - Math.Pow(r_becon_3[1], 2) - Math.Pow(D2T_2, 2) + Math.Pow(D3T_2, 2);

                //---------------교점 계산(수신기 1)-----------------
                y1 = (-C1 * A2 + A1 * C2) / (B1 * A2 - A1 * B2);
                x1 = -(C1 + B1 * y1) / A1;

                y2 = (-C1 * A3 + A1 * C3) / (B1 * A3 - A1 * B3);
                x2 = -(C1 + B1 * y2) / A1;

                y3 = (-C2 * A3 + A2 * C3) / (B2 * A3 - A2 * B3);
                x3 = -(C2 + B2 * y3) / A2;

                //--------------삼각형의 무게중심(수신기 1)-------------
                cx = (x1 + x2 + x3) / 3;
                cy = (y1 + y2 + y3) / 3;


                //////임시
                //cx = 1111111111111;
                //cy = 1111111111111;

                p_calc_1 = 1;

                //--------------------------------------------------------------------------------------------------------------------------------------

                //************************************************************************값 출력***********************************************************************
                //////////////////P1일 경우////////////////////
                result[0] = cx; result[1] = cy; result[2] = p_1_u[2];
                result[3] = cx; result[4] = cy; result[5] = p_1_d[2];

            }

            else if (Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc >= 0)
            {
                //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
                //////////////////P1일 경우////////////////////
                p_1_t_u[2] = (-p_1_Bb + Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa; p_1_t_d[2] = (-p_1_Bb - Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa;
                p_1_t_u[0] = s_A_2 * p_1_t_u[2] + p_1_s_B_2; p_1_t_d[0] = s_A_2 * p_1_t_d[2] + p_1_s_B_2;
                p_1_t_u[1] = s_A_1 * p_1_t_u[2] + p_1_s_B_1; p_1_t_d[1] = s_A_1 * p_1_t_d[2] + p_1_s_B_1;

                //*********************************************************************원래 p_1 좌표*******************************************************************
                //////////////////P1일 경우////////////////////
                p_1_u[0] = p_1_t_u[0] + r_becon_1[0]; p_1_u[1] = p_1_t_u[1] + r_becon_1[1]; p_1_u[2] = p_1_t_u[2] + r_becon_1[2];
                p_1_d[0] = p_1_t_d[0] + r_becon_1[0]; p_1_d[1] = p_1_t_d[1] + r_becon_1[1]; p_1_d[2] = p_1_t_d[2] + r_becon_1[2];

                //임시
                //p_1_u[0] = 22222; p_1_u[1] = 22222; p_1_u[2] = p_1_t_u[2] + r_becon_1[2];
                //p_1_d[0] = 22222; p_1_d[1] = 22222; p_1_d[2] = p_1_t_d[2] + r_becon_1[2];

                p_calc_1 = 0;

                //************************************************************************값 출력***********************************************************************
                //////////////////P1일 경우////////////////////
                result[0] = p_1_u[0]; result[1] = p_1_u[1]; result[2] = p_1_u[2];
                result[3] = p_1_d[0]; result[4] = p_1_d[1]; result[5] = p_1_d[2];
            }

            //수신기 2번 계산 판별
            if (Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc < 0)
            {
                //---------------비콘 사이의 거리 계산-----------------
                B1B2 = Math.Sqrt(Math.Pow(r_becon_2[0] - r_becon_1[0], 2) + Math.Pow(r_becon_2[1] - r_becon_1[1], 2) + Math.Pow(r_becon_2[2] - r_becon_1[2], 2));
                B1B3 = Math.Sqrt(Math.Pow(r_becon_3[0] - r_becon_1[0], 2) + Math.Pow(r_becon_3[1] - r_becon_1[1], 2) + Math.Pow(r_becon_3[2] - r_becon_1[2], 2));
                B2B3 = Math.Sqrt(Math.Pow(r_becon_3[0] - r_becon_2[0], 2) + Math.Pow(r_becon_3[1] - r_becon_2[1], 2) + Math.Pow(r_becon_3[2] - r_becon_2[2], 2));

                //-------------------------------------------------------------------수신기 2 계산------------------------------------------------------
                //---------------구의 반지름 계산(수신기 2)-----------------
                D1T_u_2 = d_2_t[0] + (B1B2 + 2 - d_2_t[0] - d_2_t[1]) / 2;
                D2T_u_2 = d_2_t[1] + (B1B2 + 2 - d_2_t[0] - d_2_t[1]) / 2;

                D1T_d_2 = d_2_t[0] + (B1B3 + 2 - d_2_t[0] - d_2_t[2]) / 2;
                D3T_u_2 = d_2_t[2] + (B1B3 + 2 - d_2_t[0] - d_2_t[2]) / 2;

                D2T_d_2 = d_2_t[1] + (B2B3 + 2 - d_2_t[1] - d_2_t[2]) / 2;
                D3T_d_2 = d_2_t[2] + (B2B3 + 2 - d_2_t[1] - d_2_t[2]) / 2;

                D1T_1_2 = (D1T_u_2 - D1T_d_2 > 0) ? D1T_u_2 : D1T_d_2;
                D2T_1_2 = (D2T_u_2 - D2T_d_2 > 0) ? D2T_u_2 : D2T_d_2;
                D3T_1_2 = (D3T_u_2 - D3T_d_2 > 0) ? D3T_u_2 : D3T_d_2;

                D1T_2_2 = (d_2_t[0] - D1T_1_2 > 0) ? d_2_t[0] : D1T_1_2;
                D2T_2_2 = (d_2_t[1] - D2T_1_2 > 0) ? d_2_t[1] : D2T_1_2;
                D3T_2_2 = (d_2_t[2] - D3T_1_2 > 0) ? d_2_t[2] : D3T_1_2;

                //---------------직선의 방정식(수신기 2)-----------------
                A1_2 = 2 * (r_becon_2[0] - r_becon_1[0]);
                B1_2 = 2 * (r_becon_2[1] - r_becon_1[1]);
                C1_2 = Math.Pow(r_becon_1[0], 2) + Math.Pow(r_becon_1[1], 2) - Math.Pow(r_becon_2[0], 2) - Math.Pow(r_becon_2[1], 2) - Math.Pow(D1T_2_2, 2) + Math.Pow(D2T_2_2, 2);

                A2_2 = 2 * (r_becon_3[0] - r_becon_1[0]);
                B2_2 = 2 * (r_becon_3[1] - r_becon_1[1]);
                C2_2 = Math.Pow(r_becon_1[0], 2) + Math.Pow(r_becon_1[1], 2) - Math.Pow(r_becon_3[0], 2) - Math.Pow(r_becon_3[1], 2) - Math.Pow(D1T_2_2, 2) + Math.Pow(D3T_2_2, 2);

                A3_2 = 2 * (r_becon_3[0] - r_becon_2[0]);
                B3_2 = 2 * (r_becon_3[1] - r_becon_2[1]);
                C3_2 = Math.Pow(r_becon_2[0], 2) + Math.Pow(r_becon_2[1], 2) - Math.Pow(r_becon_3[0], 2) - Math.Pow(r_becon_3[1], 2) - Math.Pow(D2T_2_2, 2) + Math.Pow(D3T_2_2, 2);

                //---------------교점 계산(수신기 2)-----------------
                y1_2 = (-C1_2 * A2_2 + A1_2 * C2_2) / (B1_2 * A2_2 - A1_2 * B2_2);
                x1_2 = -(C1_2 + B1_2 * y1_2) / A1_2;

                y2_2 = (-C1_2 * A3_2 + A1_2 * C3_2) / (B1_2 * A3_2 - A1_2 * B3_2);
                x2_2 = -(C1_2 + B1_2 * y2_2) / A1_2;

                y3_2 = (-C2_2 * A3_2 + A2_2 * C3_2) / (B2_2 * A3_2 - A2_2 * B3_2);
                x3_2 = -(C2_2 + B2_2 * y3_2) / A2_2;

                //--------------삼각형의 무게중심(수신기 2)-------------
                cx_2 = (x1_2 + x2_2 + x3_2) / 3;
                cy_2 = (y1_2 + y2_2 + y3_2) / 3;

                //////임시
                //cx_2 = 11111111;
                //cy_2 = 11111111;

                p_calc_2 = 1;

                //---------------------------------------------------------------------------------------------------------------------------------------

                //************************************************************************값 출력***********************************************************************
                //////////////////P2일 경우////////////////////
                result[6] = cx_2; result[7] = cy_2; result[8] = p_2_u[2];
                result[9] = cx_2; result[10] = cy_2; result[11] = p_2_d[2];

            }

            else if (Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc >= 0)
            {

                //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
                //////////////////P2일 경우////////////////////
                p_2_t_u[2] = (-p_2_Bb + Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa; p_2_t_d[2] = (-p_2_Bb - Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa;
                p_2_t_u[0] = s_A_2 * p_2_t_u[2] + p_2_s_B_2; p_2_t_d[0] = s_A_2 * p_2_t_d[2] + p_2_s_B_2;
                p_2_t_u[1] = s_A_1 * p_2_t_u[2] + p_2_s_B_1; p_2_t_d[1] = s_A_1 * p_2_t_d[2] + p_2_s_B_1;

                //*********************************************************************원래 p_1 좌표*******************************************************************
                //////////////////P2일 경우////////////////////
                p_2_u[0] = p_2_t_u[0] + r_becon_1[0]; p_2_u[1] = p_2_t_u[1] + r_becon_1[1]; p_2_u[2] = p_2_t_u[2] + r_becon_1[2];
                p_2_d[0] = p_2_t_d[0] + r_becon_1[0]; p_2_d[1] = p_2_t_d[1] + r_becon_1[1]; p_2_d[2] = p_2_t_d[2] + r_becon_1[2];

                ////임시
                //p_2_u[0] = 22222222; p_2_u[1] = 2222222222; p_2_u[2] = p_2_t_u[2] + r_becon_1[2];
                //p_2_d[0] = 22222222; p_2_d[1] = 2222222222; p_2_d[2] = p_2_t_d[2] + r_becon_1[2];

                p_calc_2 = 0;

                //************************************************************************값 출력***********************************************************************
                //////////////////P2일 경우////////////////////
                result[6] = p_2_u[0]; result[7] = p_2_u[1]; result[8] = p_2_u[2];
                result[9] = p_2_d[0]; result[10] = p_2_d[1]; result[11] = p_2_d[2];

            }

            return result;
        }

        double[] Calc_motor_point(double[] beacon_p1, double[] beacon_p2)
        {
            double[] result = new double[82];

            //length
            double[] pbb_length = new double[3];
            double[] pbpc_length = new double[3];
            double[] pbp4_length = new double[3];
            double[] rotor_length = new double[3];
            double[] center_length = new double[3];
            double[] blade_right_length = new double[3];
            double[] blade_left_length = new double[3];

            //광파기 좌표계
            double[] r_r2_abs_receiving_set = new double[6];//수신기(상대 -> 광파기)

            double[] abs_e = new double[6];//끝단
            double[] abs_p = new double[3];//위치
            double[] abs_pc = new double[3];//중심
            double[] abs_p3 = new double[3];//힌지
            double[] abs_arm_p = new double[3];//암 끝점
            double[] abs_blade_center_p = new double[3];//블레이드 중심

            //위치 좌표계
            double[] pc_p = new double[3];
            double[] p3_p = new double[3];
            double[] pbb_p = new double[3];

            //힌지 좌표계
            double[] rotor_p_h = new double[3];
            double[] blade_center_p_h = new double[3];
            double[] blade_right_down_p_h = new double[3];
            double[] blade_left_down_p_h = new double[3];

            double[] rotor_p_h_t = new double[3];
            double[] blade_center_p_h_t = new double[3];
            double[] blade_left_down_p_h_t = new double[3];
            double[] blade_right_down_p_h_t = new double[3];

            //임의 좌표계
            double[] r2_becon_1 = new double[3];//비콘1
            double[] r2_becon_2 = new double[3];//비콘2
            double[] r2_becon_3 = new double[3];//비콘3
            double[] r2_becon_4 = new double[3];//비콘4

            double[] r2_instrument_station_2 = new double[3];//측점2
            double[] r2_instrument_station_3 = new double[3];//측점3

            double[] r_r2_receiving_set = new double[6];//수신기(상대 -> 임의)

            double b_angle_2 = 0;

            double[] r2_blade_right_down_p = new double[3];//끝단1
            double[] r2_blade_left_down_p = new double[3];//끝단2

            double[] r2_pb = new double[3];//위치
            double[] r2_pc = new double[3];//중심
            double[] r2_p3 = new double[3];//힌지
            double[] r2_rotor_p = new double[3];//arm 끝점
            double[] r2_blade_center_p = new double[3];//블레이드 중심

            //상대 좌표계
            double[] r_receiving_set = new double[6];

            double[] pb = new double[3];
            double[] pc = new double[3];
            double[] p3 = new double[3];
            double[] rotor_p = new double[3];
            double[] blade_center_p = new double[3];
            double[] blade_right_down_p = new double[3];
            double[] blade_left_down_p = new double[3];

            double a_angle = 0, b_angle = 0;
            double[] move_p1 = new double[3];
            double p1_atan = 0;

            //yaw calc
            double calc_body_y = 0;
            double p1_calibration = 0;

            //yaw값 보정
            double body_y_1 = 0;
            double rotor_y_1 = 0, rotor_y_2 = 0;
            double rotor_yaw = 0;

            double rotor_u_x_length = 0;
            double rotor_u_y_length = 0;
            double rotor_u_c_x_length = 0;
            double rotor_u_c_y_length = 0;
            double r_rotor_y = 0;

            double blade_y_1 = 0, blade_y_2 = 0;
            double r_blade_a = 0;
            double multy_b = 0;
            double y_blade_a = 0;
            double blade_yaw = 0;

            //오리엔테이션 보정
            double orientation = 0;
            double rotor_ori = 0;
            double blade_ori = 0;


            double motorgrader_ultrasonic_s = 0;

            //m_imu
            double[] motorgrader_body_imu = new double[3];
            double[] motorgrader_blade_imu = new double[3];
            double[] motorgrader_rotor_imu = new double[3];

            //************************************************************************각도 부분*****************************************************************************

            //imu값 입력
            motorgrader_body_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(10, 4)) / 10 - 200; motorgrader_rotor_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(22, 4)) / 10 - 200; motorgrader_blade_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(34, 4)) / 10 - 200;
            motorgrader_body_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(6, 4)) / 10 - 200;  motorgrader_rotor_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(18, 4)) / 10 - 200; motorgrader_blade_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(30, 4)) / 10 - 200;

            //imu값 입력(시뮬레이션)
            //motorgrader_body_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(10, 4)) / 10 - 200; motorgrader_rotor_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(22, 4)) / 10 - 200; motorgrader_blade_imu[0] = Convert.ToDouble(MotorGraderData.Motion.Substring(34, 4)) / 10 - 200;
            //motorgrader_body_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(6, 4)) / 10 - 200; motorgrader_rotor_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(18, 4)) / 10 - 200; motorgrader_blade_imu[1] = Convert.ToDouble(MotorGraderData.Motion.Substring(30, 4)) / 10 - 200;

            motorgrader_ultrasonic_a = Convert.ToDouble(MotorGraderData.Motion.Substring(38, 4));
            motorgrader_rotor_angle = (Convert.ToDouble(MotorGraderData.Motion.Substring(42, 4)) - 1000) / 10;//원본 로터리 센서값을 1000이 될때까지 돌린다음 센서를 끄면 안됨

            motorgrader_ultrasonic_a += motorgrader_zero;

            motorgrader_ultrasonic_s = motorgrader_ultrasonic_a - motorgrader_ultrasonic_b;

            //출력
            MotorGraderData.Original.IMUSensor = MotorGraderData.Motion.Substring(10, 4) + " " + MotorGraderData.Motion.Substring(6, 4) + " "
                                + MotorGraderData.Motion.Substring(22, 4) + " " + MotorGraderData.Motion.Substring(18, 4) + " "
                                + MotorGraderData.Motion.Substring(34, 4) + " " + MotorGraderData.Motion.Substring(30, 4) + " "
                                + MotorGraderData.Motion.Substring(38, 4) + " " + MotorGraderData.Motion.Substring(42, 4) + " "
                                + Convert.ToString(motorgrader_ultrasonic_s);

            //imu_calibration
            motorgrader_body_imu[0] += m_body_imu_calibration[0];
            motorgrader_body_imu[1] += m_body_imu_calibration[1];

            motorgrader_rotor_imu[0] += m_rotor_imu_calibration[0];
            motorgrader_rotor_imu[1] += m_rotor_imu_calibration[1];

            motorgrader_blade_imu[0] += m_blade_imu_calibration[0];
            motorgrader_blade_imu[1] += m_blade_imu_calibration[1];

            //각도 변경(deg를 rad으로)
            motorgrader_body_imu[0] *= (Math.PI / 180.0); motorgrader_rotor_imu[0] *= (Math.PI / 180.0); motorgrader_blade_imu[0] *= (Math.PI / 180.0);
            motorgrader_body_imu[1] *= (Math.PI / 180.0); motorgrader_rotor_imu[1] *= (Math.PI / 180.0); motorgrader_blade_imu[1] *= (Math.PI / 180.0);

            //**********************************************************************좌표 변환(광파기 -> 상대)*****************************************************************
            ///////////////////////////////////////////////////////////////광파기 -> 임의 좌표계/////////////////////////////////////////////////
            //측점2
            r2_instrument_station_2[0] = instrument_station_2[0];// - instrument_station_1[0];
            r2_instrument_station_2[1] = instrument_station_2[1];// - instrument_station_1[1];
            r2_instrument_station_2[2] = instrument_station_2[2];// - instrument_station_1[2];

            // 측점3
            r2_instrument_station_3[0] = instrument_station_3[0];// - instrument_station_1[0];
            r2_instrument_station_3[1] = instrument_station_3[1];// - instrument_station_1[1];
            r2_instrument_station_3[2] = instrument_station_3[2];// - instrument_station_1[2];

            //비콘1
            r2_becon_1[0] = becon_1[0];// - instrument_station_1[0];
            r2_becon_1[1] = becon_1[1];// - instrument_station_1[1];
            r2_becon_1[2] = becon_1[2];// - instrument_station_1[2];

            //비콘2
            r2_becon_2[0] = becon_2[0];// - instrument_station_1[0];
            r2_becon_2[1] = becon_2[1];// - instrument_station_1[1];
            r2_becon_2[2] = becon_2[2];// - instrument_station_1[2];

            //비콘3
            r2_becon_3[0] = becon_3[0];// - instrument_station_1[0];
            r2_becon_3[1] = becon_3[1];// - instrument_station_1[1];
            r2_becon_3[2] = becon_3[2];// - instrument_station_1[2];

            //비콘4
            r2_becon_4[0] = becon_4[0];// - instrument_station_1[0];
            r2_becon_4[1] = becon_4[1];// - instrument_station_1[1];
            r2_becon_4[2] = becon_4[2];// - instrument_station_1[2];

            ///////////////////////////////////////////////////////////////임의 -> 상대 좌표계//////////////////////////////////////////////////
            a_angle = Math.Atan(r2_instrument_station_2[1] / r2_instrument_station_2[0]) * 180.0 / Math.PI;
            b_angle = ((r2_instrument_station_2[0] < 0) ? 180 + a_angle : ((r2_instrument_station_2[0] > 0) ? ((r2_instrument_station_2[1] >= 0) ? a_angle : 360 + a_angle) : ((r2_instrument_station_2[1] > 0) ? 90 : 270))) * Math.PI / 180.0;

            //비콘1
            r_becon_1[0] = r2_becon_1[0] * Math.Cos(b_angle) + r2_becon_1[1] * Math.Sin(b_angle);
            r_becon_1[1] = -r2_becon_1[0] * Math.Sin(b_angle) + r2_becon_1[1] * Math.Cos(b_angle);
            r_becon_1[2] = r2_becon_1[2];

            //비콘2
            r_becon_2[0] = r2_becon_2[0] * Math.Cos(b_angle) + r2_becon_2[1] * Math.Sin(b_angle);
            r_becon_2[1] = -r2_becon_2[0] * Math.Sin(b_angle) + r2_becon_2[1] * Math.Cos(b_angle);
            r_becon_2[2] = r2_becon_2[2];

            //비콘3
            r_becon_3[0] = r2_becon_3[0] * Math.Cos(b_angle) + r2_becon_3[1] * Math.Sin(b_angle);
            r_becon_3[1] = -r2_becon_3[0] * Math.Sin(b_angle) + r2_becon_3[1] * Math.Cos(b_angle);
            r_becon_3[2] = r2_becon_3[2];

            //비콘4
            r_becon_4[0] = r2_becon_4[0] * Math.Cos(b_angle) + r2_becon_4[1] * Math.Sin(b_angle);
            r_becon_4[1] = -r2_becon_4[0] * Math.Sin(b_angle) + r2_becon_4[1] * Math.Cos(b_angle);
            r_becon_4[2] = r2_becon_4[2];

            //측점2
            r_instrument_station_2[0] = r2_instrument_station_2[0] * Math.Cos(b_angle) + r2_instrument_station_2[1] * Math.Sin(b_angle);
            r_instrument_station_2[1] = -r2_instrument_station_2[0] * Math.Sin(b_angle) + r2_instrument_station_2[1] * Math.Cos(b_angle);
            r_instrument_station_2[2] = r2_instrument_station_2[2];

            //측점3
            r_instrument_station_3[0] = r2_instrument_station_3[0] * Math.Cos(b_angle) + r2_instrument_station_3[1] * Math.Sin(b_angle);
            r_instrument_station_3[1] = -r2_instrument_station_3[0] * Math.Sin(b_angle) + r2_instrument_station_3[1] * Math.Cos(b_angle);
            r_instrument_station_3[2] = r2_instrument_station_3[2];

            //**********************************************************************임의 -> 상대 좌표계 삼각 측량 출력부********************************************************************
            //비콘을 상대좌표로 변환해서 수신기 구함
            r_motor_tri_point = r_Calc_tri(beacon_p1, beacon_p2);

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 위에 있을때)////////////////////
            ////수신기1
            //r_receiving_set[0] = r_motor_tri_point[0];
            //r_receiving_set[1] = r_motor_tri_point[1];
            //r_receiving_set[2] = r_motor_tri_point[2];

            ////수신기2
            //r_receiving_set[3] = r_motor_tri_point[6];
            //r_receiving_set[4] = r_motor_tri_point[7];
            //r_receiving_set[5] = r_motor_tri_point[8];

            ////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 밑에 있을때)////////////////////
            //수신기1
            r_receiving_set[0] = r_motor_tri_point[3];
            r_receiving_set[1] = r_motor_tri_point[4];
            r_receiving_set[2] = r_motor_tri_point[5];

            //수신기2
            r_receiving_set[3] = r_motor_tri_point[9];
            r_receiving_set[4] = r_motor_tri_point[10];
            r_receiving_set[5] = r_motor_tri_point[11];

            //-------------- 상대좌표계에서 수신기 좌표 강제입력 ----------------------
            ////수신기1
            //r_receiving_set[0] = 0;
            //r_receiving_set[1] = 0;
            //r_receiving_set[2] = 0;

            ////수신기2
            //r_receiving_set[3] = 0;
            //r_receiving_set[4] = 0;
            //r_receiving_set[5] = 0;
            //-------------------------------------------------------------------------

            //*************************************************************위치 추적 로직 부분***********************************************************

            /////////////////////////////////////////////////////////////////상대 좌표계 Pb(동일한 높이인 수신기일 경우 적용)//////////////////////////////////////////////////////
            //calc_body_y 적용
            pb[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
            pb[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
            pb[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

            //yaw calc
            move_p1[0] = r_receiving_set[0] - pb[0];
            move_p1[1] = r_receiving_set[1] - pb[1];
            move_p1[2] = 0;

            p1_atan = Math.Atan(move_p1[1] / move_p1[0]) * (180 / Math.PI);
            p1_calibration = Math.Atan((-Math.Sin(motorgrader_body_imu[0]) * Math.Sin(motorgrader_body_imu[1])) / Math.Cos(motorgrader_body_imu[0])) * (180 / Math.PI);

            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) calc_body_y = 90;//deg
                else calc_body_y = -90;//deg
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) calc_body_y = 180;//deg
                    else calc_body_y = 0;//deg
                }

                else
                {
                    if (move_p1[0] < 0) calc_body_y = -p1_atan;//deg
                    else
                    {
                        if (move_p1[1] < 0) calc_body_y = -180 - p1_atan;//deg
                        else calc_body_y = 180 - p1_atan;//deg
                    }
                }
            }

            calc_body_y += m_calc_body_y_c;//몸체 요값 조절

            //------------------------------------rotor yaw calc

            //deg to rad
            motorgrader_rotor_angle *= Math.PI / 180.0;

            rotor_u_x_length = Math.Sin(motorgrader_rotor_angle);
            rotor_u_y_length = Math.Cos(motorgrader_rotor_angle);
            rotor_u_c_x_length = rotor_u_x_length * Math.Cos(motorgrader_rotor_imu[1]) - rotor_u_y_length * Math.Sin(motorgrader_rotor_imu[0]) * Math.Sin(motorgrader_rotor_imu[1]);
            rotor_u_c_y_length = rotor_u_y_length * Math.Cos(motorgrader_rotor_imu[0]);
            r_rotor_y = Math.Atan(rotor_u_c_x_length / rotor_u_c_y_length) * 180.0 / Math.PI;

            //rad to deg
            motorgrader_rotor_angle *= 180.0 / Math.PI;
            rotor_yaw = calc_body_y + r_rotor_y + m_rotor_y_c;

            //-------------------------------------blade yaw calc

            motorgrader_rotor_imu[0] *= -1;

            rotor_y_1 = Math.Pow(Math.Cos(motorgrader_rotor_imu[0]), 2);

            blade_y_1 = Math.Pow(Math.Tan(motorgrader_blade_imu[1]), 2);
            blade_y_2 = Math.Sqrt(blade_y_1 / (rotor_y_1 + blade_y_1 * rotor_y_1));

            if (blade_y_2 > 1) r_blade_a = Math.Asin(2 - blade_y_2);
            else r_blade_a = Math.Asin(blade_y_2);

            multy_b = (motorgrader_rotor_imu[0] * motorgrader_blade_imu[1] < 0) ? 1 : -1;
            y_blade_a = Math.Abs(Math.Atan(Math.Sin(r_blade_a) * Math.Sin(motorgrader_rotor_imu[0]) / Math.Cos(r_blade_a))) * multy_b * 180.0 / Math.PI;
            blade_yaw = (rotor_yaw + y_blade_a + m_blade_y_c) * Math.PI / 180.0;

            motorgrader_rotor_imu[0] *= -1;

            //deg to rad
            calc_body_y *= Math.PI / 180.0;
            rotor_yaw *= Math.PI / 180.0;

            //orientation_body
            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) orientation = 0;
                else orientation = 180;
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) orientation = 270;
                    else orientation = 90;
                }

                else
                {
                    if (move_p1[0] < 0) orientation = 90 + p1_atan;
                    else orientation = 270 + p1_atan;
                }
            }
            
            orientation = 360 - orientation - p1_calibration + m_calc_body_y_c;
            rotor_ori   = orientation + r_rotor_y + m_rotor_y_c;//rotor orientation calc
            blade_ori   = rotor_ori + y_blade_a + m_blade_y_c;//blade orientation calc

            /////////////////////////////////pb가 (0, 0, 0)일때, pbb_p구하기(_p : 위치 좌표계)
            pbb_length[0] = motorgrader_body_length[6]; pbb_length[1] = 0; pbb_length[2] = 0;

            pbb_p[0] = pbb_length[0] * Math.Cos(motorgrader_body_imu[1]) * Math.Cos(calc_body_y);
            pbb_p[1] = -pbb_length[0] * Math.Cos(motorgrader_body_imu[1]) * Math.Sin(calc_body_y);
            pbb_p[2] = pbb_length[0] * Math.Sin(motorgrader_body_imu[1]);

            /////////////////////////////////pbb_p일때, pc_p구하기(_p : 위치 좌표계)
            pbpc_length[0] = 0; pbpc_length[1] = 0; pbpc_length[2] = -motorgrader_body_length[3];

            pc_p[0] = pbb_p[0] - pbpc_length[2] * (Math.Cos(motorgrader_body_imu[0]) * Math.Sin(motorgrader_body_imu[1]) * Math.Cos(calc_body_y) + Math.Sin(motorgrader_body_imu[0]) * Math.Sin(calc_body_y));
            pc_p[1] = pbb_p[1] + pbpc_length[2] * (Math.Cos(motorgrader_body_imu[0]) * Math.Sin(motorgrader_body_imu[1]) * Math.Sin(calc_body_y) - Math.Sin(motorgrader_body_imu[0]) * Math.Cos(calc_body_y));
            pc_p[2] = pbb_p[2] + pbpc_length[2] * Math.Cos(motorgrader_body_imu[0]) * Math.Cos(motorgrader_body_imu[1]);

            //////////////////////////////////////////////////////pc_p일때, p3_p구하기(_p : 위치 좌표계) 여기서 p3는 수식에서 p4를 의미
            pbp4_length[0] = motorgrader_body_length[5]; pbp4_length[1] = motorgrader_body_length[4]; pbp4_length[2] = 0;

            p3_p[0] = pc_p[0] + pbp4_length[0] * Math.Cos(motorgrader_body_imu[1]) * Math.Cos(calc_body_y) + pbp4_length[1] * (Math.Cos(motorgrader_body_imu[0]) * Math.Sin(calc_body_y) - Math.Sin(motorgrader_body_imu[0]) * Math.Sin(motorgrader_body_imu[1]) * Math.Cos(calc_body_y));
            p3_p[1] = pc_p[1] - pbp4_length[0] * Math.Cos(motorgrader_body_imu[1]) * Math.Sin(calc_body_y) + pbp4_length[1] * (Math.Cos(motorgrader_body_imu[0]) * Math.Cos(calc_body_y) + Math.Sin(motorgrader_body_imu[0]) * Math.Sin(motorgrader_body_imu[1]) * Math.Sin(calc_body_y));
            p3_p[2] = pc_p[2] + pbp4_length[0] * Math.Sin(motorgrader_body_imu[1]) + pbp4_length[1] * Math.Sin(motorgrader_body_imu[0]) * Math.Cos(motorgrader_body_imu[1]);

            ///////////////////////////p3_p가 (0, 0, 0)일때, rotor_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님
            rotor_length[0] = -motorgrader_rotor_length[0]; rotor_length[1] = 0; rotor_length[2] = -motorgrader_rotor_length[2];

            rotor_p_h[0] = rotor_length[0] * Math.Cos(motorgrader_rotor_imu[1]) * Math.Cos(rotor_yaw) + rotor_length[1] * Math.Cos(motorgrader_rotor_imu[0]) * Math.Sin(rotor_yaw) - rotor_length[1] * Math.Sin(motorgrader_rotor_imu[0]) * Math.Sin(motorgrader_rotor_imu[1]) * Math.Cos(rotor_yaw) - rotor_length[2] * Math.Cos(motorgrader_rotor_imu[0]) * Math.Sin(motorgrader_rotor_imu[1]) * Math.Cos(rotor_yaw) - rotor_length[2] * Math.Sin(motorgrader_rotor_imu[0]) * Math.Sin(rotor_yaw);
            rotor_p_h[1] = -rotor_length[0] * Math.Cos(motorgrader_rotor_imu[1]) * Math.Sin(rotor_yaw) + rotor_length[1] * Math.Cos(motorgrader_rotor_imu[0]) * Math.Cos(rotor_yaw) + rotor_length[1] * Math.Sin(motorgrader_rotor_imu[0]) * Math.Sin(motorgrader_rotor_imu[1]) * Math.Sin(rotor_yaw) + rotor_length[2] * Math.Cos(motorgrader_rotor_imu[0]) * Math.Sin(motorgrader_rotor_imu[1]) * Math.Sin(rotor_yaw) - rotor_length[2] * Math.Sin(motorgrader_rotor_imu[0]) * Math.Cos(rotor_yaw);
            rotor_p_h[2] = rotor_length[0] * Math.Sin(motorgrader_rotor_imu[1]) + rotor_length[1] * Math.Sin(motorgrader_rotor_imu[0]) * Math.Cos(motorgrader_rotor_imu[1]) + rotor_length[2] * Math.Cos(motorgrader_rotor_imu[0]) * Math.Cos(motorgrader_rotor_imu[1]);

            /////////////////////////rotor_p_h일때, blade_center_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님
            center_length[0] = motorgrader_blade_length[0]; center_length[1] = motorgrader_ultrasonic_s; center_length[2] = 0;//수정된 부분(20180302)

            blade_center_p_h[0] = rotor_p_h[0] + center_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw) + center_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(blade_yaw) - Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw));
            blade_center_p_h[1] = rotor_p_h[1] - center_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw) + center_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Cos(blade_yaw) + Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw));
            blade_center_p_h[2] = rotor_p_h[2] + center_length[0] * Math.Sin(motorgrader_blade_imu[1]) + center_length[1] * Math.Sin(motorgrader_blade_imu[0]) * Math.Cos(motorgrader_blade_imu[1]);

            ////////////////////////rotor_p_h일때, blade_left_down_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님
            blade_left_length[0] = motorgrader_blade_length[0] + motorgrader_blade_length[1]; blade_left_length[1] = motorgrader_blade_length[2] + motorgrader_ultrasonic_s; blade_left_length[2] = -motorgrader_blade_length[3];

            blade_left_down_p_h[0] = rotor_p_h[0] + blade_left_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw) + blade_left_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(blade_yaw) - Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw)) - blade_left_length[2] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw) + Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(blade_yaw));
            blade_left_down_p_h[1] = rotor_p_h[1] - blade_left_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw) + blade_left_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Cos(blade_yaw) + Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw)) + blade_left_length[2] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw) - Math.Sin(motorgrader_blade_imu[0]) * Math.Cos(blade_yaw));
            blade_left_down_p_h[2] = rotor_p_h[2] + blade_left_length[0] * Math.Sin(motorgrader_blade_imu[1]) + blade_left_length[1] * Math.Sin(motorgrader_blade_imu[0]) * Math.Cos(motorgrader_blade_imu[1]) + blade_left_length[2] * Math.Cos(motorgrader_blade_imu[0]) * Math.Cos(motorgrader_blade_imu[1]);

            /////////////////////////rotor_p_h일때, blade_right_down_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님
            blade_right_length[0] = motorgrader_blade_length[0] + motorgrader_blade_length[1]; blade_right_length[1] = -motorgrader_blade_length[2] + motorgrader_ultrasonic_s; blade_right_length[2] = -motorgrader_blade_length[3];

            blade_right_down_p_h[0] = rotor_p_h[0] + blade_right_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw) + blade_right_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(blade_yaw) - Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw)) - blade_right_length[2] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Cos(blade_yaw) + Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(blade_yaw));
            blade_right_down_p_h[1] = rotor_p_h[1] - blade_right_length[0] * Math.Cos(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw) + blade_right_length[1] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Cos(blade_yaw) + Math.Sin(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw)) + blade_right_length[2] * (Math.Cos(motorgrader_blade_imu[0]) * Math.Sin(motorgrader_blade_imu[1]) * Math.Sin(blade_yaw) - Math.Sin(motorgrader_blade_imu[0]) * Math.Cos(blade_yaw));
            blade_right_down_p_h[2] = rotor_p_h[2] + blade_right_length[0] * Math.Sin(motorgrader_blade_imu[1]) + blade_right_length[1] * Math.Sin(motorgrader_blade_imu[0]) * Math.Cos(motorgrader_blade_imu[1]) + blade_right_length[2] * Math.Cos(motorgrader_blade_imu[0]) * Math.Cos(motorgrader_blade_imu[1]);

            ///////////////////////////////////rotor_p_h에서 좌표변환(-90 deg)된 rotor_p_h_t
            rotor_p_h_t[0] = -rotor_p_h[1];
            rotor_p_h_t[1] = rotor_p_h[0];
            rotor_p_h_t[2] = rotor_p_h[2];

            ///////////////////////////////////blade_center_p_h에서 좌표변환(-90 deg)된 blade_center_p_h_t///////////////////////////////
            blade_center_p_h_t[0] = -blade_center_p_h[1];
            blade_center_p_h_t[1] = blade_center_p_h[0];
            blade_center_p_h_t[2] = blade_center_p_h[2];

            ///////////////////////////////////blade_left_down_p_h에서 좌표변환(-90 deg)된 blade_left_down_p_h_t///////////////////////////////
            blade_left_down_p_h_t[0] = -blade_left_down_p_h[1];
            blade_left_down_p_h_t[1] = blade_left_down_p_h[0];
            blade_left_down_p_h_t[2] = blade_left_down_p_h[2];

            ///////////////////////////////////blade_right_down_p_h에서 좌표변환(-90 deg)된 blade_right_down_p_h_t///////////////////////////////
            blade_right_down_p_h_t[0] = -blade_right_down_p_h[1];
            blade_right_down_p_h_t[1] = blade_right_down_p_h[0];
            blade_right_down_p_h_t[2] = blade_right_down_p_h[2];

            ///////////////////////////////////pc = pb + pc_p///////////////////////////////
            pc[0] = pb[0] + pc_p[0];
            pc[1] = pb[1] + pc_p[1];
            pc[2] = pb[2] + pc_p[2];

            ///////////////////////////////////p3 = pb + p3_p///////////////////////////////
            p3[0] = pb[0] + p3_p[0];
            p3[1] = pb[1] + p3_p[1];
            p3[2] = pb[2] + p3_p[2];

            ///////////////////////////////////rotor_p = p3 + rotor_p_h_t///////////////////////////////
            rotor_p[0] = p3[0] + rotor_p_h_t[0];
            rotor_p[1] = p3[1] + rotor_p_h_t[1];
            rotor_p[2] = p3[2] + rotor_p_h_t[2];

            //////////////////////////////////blade_center_p = p3 + blade_center_p_h_t////////////////////////
            blade_center_p[0] = p3[0] + blade_center_p_h_t[0];
            blade_center_p[1] = p3[1] + blade_center_p_h_t[1];
            blade_center_p[2] = p3[2] + blade_center_p_h_t[2];

            ////////////////////////////////////blade_left_down_p = p3 + blade_left_down_p_h_t////////////////////////
            blade_left_down_p[0] = p3[0] + blade_left_down_p_h_t[0];
            blade_left_down_p[1] = p3[1] + blade_left_down_p_h_t[1];
            blade_left_down_p[2] = p3[2] + blade_left_down_p_h_t[2];

            //////////////////////////////////blade_right_down_p = p3 + blade_right_down_p_h_t////////////////////////
            blade_right_down_p[0] = p3[0] + blade_right_down_p_h_t[0];
            blade_right_down_p[1] = p3[1] + blade_right_down_p_h_t[1];
            blade_right_down_p[2] = p3[2] + blade_right_down_p_h_t[2];

            //**********************************************************************좌표 변환(상대 -> 광파기)*****************************************************************
            /////////////////////////////////////////////////광파기 -> 임의 좌표계/////////////////////////////////////////////////
            b_angle_2 = -b_angle;

            //끝단1
            r2_blade_left_down_p[0] = blade_left_down_p[0] * Math.Cos(b_angle_2) + blade_left_down_p[1] * Math.Sin(b_angle_2);
            r2_blade_left_down_p[1] = -blade_left_down_p[0] * Math.Sin(b_angle_2) + blade_left_down_p[1] * Math.Cos(b_angle_2);
            r2_blade_left_down_p[2] = blade_left_down_p[2];

            //끝단2
            r2_blade_right_down_p[0] = blade_right_down_p[0] * Math.Cos(b_angle_2) + blade_right_down_p[1] * Math.Sin(b_angle_2);
            r2_blade_right_down_p[1] = -blade_right_down_p[0] * Math.Sin(b_angle_2) + blade_right_down_p[1] * Math.Cos(b_angle_2);
            r2_blade_right_down_p[2] = blade_right_down_p[2];

            //위치
            r2_pb[0] = pb[0] * Math.Cos(b_angle_2) + pb[1] * Math.Sin(b_angle_2);
            r2_pb[1] = -pb[0] * Math.Sin(b_angle_2) + pb[1] * Math.Cos(b_angle_2);
            r2_pb[2] = pb[2];

            //수신기1
            r_r2_receiving_set[0] = r_receiving_set[0] * Math.Cos(b_angle_2) + r_receiving_set[1] * Math.Sin(b_angle_2);
            r_r2_receiving_set[1] = -r_receiving_set[0] * Math.Sin(b_angle_2) + r_receiving_set[1] * Math.Cos(b_angle_2);
            r_r2_receiving_set[2] = r_receiving_set[2];

            //수신기2
            r_r2_receiving_set[3] = r_receiving_set[3] * Math.Cos(b_angle_2) + r_receiving_set[4] * Math.Sin(b_angle_2);
            r_r2_receiving_set[4] = -r_receiving_set[3] * Math.Sin(b_angle_2) + r_receiving_set[4] * Math.Cos(b_angle_2);
            r_r2_receiving_set[5] = r_receiving_set[5];

            //중심
            r2_pc[0] = pc[0] * Math.Cos(b_angle_2) + pc[1] * Math.Sin(b_angle_2);
            r2_pc[1] = -pc[0] * Math.Sin(b_angle_2) + pc[1] * Math.Cos(b_angle_2);
            r2_pc[2] = pc[2];

            //힌지
            r2_p3[0] = p3[0] * Math.Cos(b_angle_2) + p3[1] * Math.Sin(b_angle_2);
            r2_p3[1] = -p3[0] * Math.Sin(b_angle_2) + p3[1] * Math.Cos(b_angle_2);
            r2_p3[2] = p3[2];

            //로터 끝점
            r2_rotor_p[0] = rotor_p[0] * Math.Cos(b_angle_2) + rotor_p[1] * Math.Sin(b_angle_2);
            r2_rotor_p[1] = -rotor_p[0] * Math.Sin(b_angle_2) + rotor_p[1] * Math.Cos(b_angle_2);
            r2_rotor_p[2] = rotor_p[2];

            //블레이드 중심
            r2_blade_center_p[0] = blade_center_p[0] * Math.Cos(b_angle_2) + blade_center_p[1] * Math.Sin(b_angle_2);
            r2_blade_center_p[1] = -blade_center_p[0] * Math.Sin(b_angle_2) + blade_center_p[1] * Math.Cos(b_angle_2);
            r2_blade_center_p[2] = blade_center_p[2];

            ////////////////////////////////////////////////임의->광파기 좌표계///////////////////////////////////////////////////
            //끝단1
            abs_e[0] = r2_blade_left_down_p[0];// + instrument_station_1[0];
            abs_e[1] = r2_blade_left_down_p[1];// + instrument_station_1[1];
            abs_e[2] = r2_blade_left_down_p[2];// + instrument_station_1[2];

            //끝단2
            abs_e[3] = r2_blade_right_down_p[0];// + instrument_station_1[0];
            abs_e[4] = r2_blade_right_down_p[1];// + instrument_station_1[1];
            abs_e[5] = r2_blade_right_down_p[2];// + instrument_station_1[2];

            //위치
            abs_p[0] = r2_pb[0];// + instrument_station_1[0];
            abs_p[1] = r2_pb[1];// + instrument_station_1[1];
            abs_p[2] = r2_pb[2];// + instrument_station_1[2];

            //수신기1
            r_r2_abs_receiving_set[0] = r_r2_receiving_set[0];// + instrument_station_1[0];
            r_r2_abs_receiving_set[1] = r_r2_receiving_set[1];// + instrument_station_1[1];
            r_r2_abs_receiving_set[2] = r_r2_receiving_set[2];// + instrument_station_1[2];

            //수신기2
            r_r2_abs_receiving_set[3] = r_r2_receiving_set[3];// + instrument_station_1[0];
            r_r2_abs_receiving_set[4] = r_r2_receiving_set[4];// + instrument_station_1[1];
            r_r2_abs_receiving_set[5] = r_r2_receiving_set[5];// + instrument_station_1[2];

            //중심
            abs_pc[0] = r2_pc[0];// + instrument_station_1[0];
            abs_pc[1] = r2_pc[1];// + instrument_station_1[1];
            abs_pc[2] = r2_pc[2];// + instrument_station_1[2];

            //힌지
            abs_p3[0] = r2_p3[0];// + instrument_station_1[0];
            abs_p3[1] = r2_p3[1];// + instrument_station_1[1];
            abs_p3[2] = r2_p3[2];// + instrument_station_1[2];

            //로터 끝점
            abs_arm_p[0] = r2_rotor_p[0];// + instrument_station_1[0];
            abs_arm_p[1] = r2_rotor_p[1];// + instrument_station_1[1];
            abs_arm_p[2] = r2_rotor_p[2];// + instrument_station_1[2];

            //블레이드 중심
            abs_blade_center_p[0] = r2_blade_center_p[0];// + instrument_station_1[0];
            abs_blade_center_p[1] = r2_blade_center_p[1];// + instrument_station_1[1];
            abs_blade_center_p[2] = r2_blade_center_p[2];// + instrument_station_1[2];

            //**********************************************************************return 값 치환 부분****************************************************************
            /////////////////////////광파기 좌표계////////////////////////

            //수신기1
            result[0] = r_r2_abs_receiving_set[0];
            result[1] = r_r2_abs_receiving_set[1];
            result[2] = r_r2_abs_receiving_set[2];

            //수신기2
            result[3] = r_r2_abs_receiving_set[3];
            result[4] = r_r2_abs_receiving_set[4];
            result[5] = r_r2_abs_receiving_set[5];

            //위치 좌표
            result[6] = abs_p[0];
            result[7] = abs_p[1];
            result[8] = abs_p[2];

            //끝단1 좌표
            result[9] = abs_e[0];
            result[10] = abs_e[1];
            result[11] = abs_e[2];

            //끝단2 좌표
            result[12] = abs_e[3];
            result[13] = abs_e[4];
            result[14] = abs_e[5];

            //중심 좌표
            result[15] = Math.Round(abs_pc[0]);
            result[16] = Math.Round(abs_pc[1]);
            result[17] = Math.Round(abs_pc[2]);

            //힌지 좌표
            result[18] = Math.Round(abs_p3[0]);
            result[19] = Math.Round(abs_p3[1]);
            result[20] = Math.Round(abs_p3[2]);

            //암 끝점 좌표
            result[21] = Math.Round(abs_arm_p[0]);
            result[22] = Math.Round(abs_arm_p[1]);
            result[23] = Math.Round(abs_arm_p[2]);

            //블레이드 중심
            result[24] = Math.Round(abs_blade_center_p[0]);
            result[25] = Math.Round(abs_blade_center_p[1]);
            result[26] = Math.Round(abs_blade_center_p[2]);

            ///////////////////////상대 좌표계////////////////////////

            //위치 좌표
            result[27] = Math.Round(pb[0]);
            result[28] = Math.Round(pb[1]);
            result[29] = Math.Round(pb[2]);

            //끝단1 좌표
            result[30] = Math.Round(blade_left_down_p[0]);
            result[31] = Math.Round(blade_left_down_p[1]);
            result[32] = Math.Round(blade_left_down_p[2]);

            //끝단2 좌표
            result[33] = Math.Round(blade_right_down_p[0]);
            result[34] = Math.Round(blade_right_down_p[1]);
            result[35] = Math.Round(blade_right_down_p[2]);

            //비콘1 좌표
            result[36] = Math.Round(r_becon_1[0]);
            result[37] = Math.Round(r_becon_1[1]);
            result[38] = Math.Round(r_becon_1[2]);

            //비콘2 좌표
            result[39] = Math.Round(r_becon_2[0]);
            result[40] = Math.Round(r_becon_2[1]);
            result[41] = Math.Round(r_becon_2[2]);

            //비콘3 좌표
            result[42] = Math.Round(r_becon_3[0]);
            result[43] = Math.Round(r_becon_3[1]);
            result[44] = Math.Round(r_becon_3[2]);

            //수신기1 좌표(비콘을 상대좌표로 변환)
            result[45] = Math.Round(r_receiving_set[0]);
            result[46] = Math.Round(r_receiving_set[1]);
            result[47] = Math.Round(r_receiving_set[2]);

            //수신기2 좌표(비콘을 상대좌표로 변환)
            result[48] = Math.Round(r_receiving_set[3]);
            result[49] = Math.Round(r_receiving_set[4]);
            result[50] = Math.Round(r_receiving_set[5]);

            //측점2 좌표
            result[51] = Math.Round(r_instrument_station_2[0]);
            result[52] = Math.Round(r_instrument_station_2[1]);
            result[53] = Math.Round(r_instrument_station_2[2]);

            //측점3 좌표
            result[54] = Math.Round(r_instrument_station_3[0]);
            result[55] = Math.Round(r_instrument_station_3[1]);
            result[56] = Math.Round(r_instrument_station_3[2]);

            //중심 좌표
            result[57] = Math.Round(pc[0]);
            result[58] = Math.Round(pc[1]);
            result[59] = Math.Round(pc[2]);

            //orientation_body
            result[60] = Math.Round(orientation);

            //힌지 좌표
            result[61] = Math.Round(p3[0]);
            result[62] = Math.Round(p3[1]);
            result[63] = Math.Round(p3[2]);

            //로터 끝점
            result[64] = Math.Round(rotor_p[0]);
            result[65] = Math.Round(rotor_p[1]);
            result[66] = Math.Round(rotor_p[2]);

            //블레이드 중심 좌표
            result[67] = Math.Round(blade_center_p[0]);
            result[68] = Math.Round(blade_center_p[1]);
            result[69] = Math.Round(blade_center_p[2]);

            //orientation_arm
            result[70] = Math.Round(rotor_ori);

            //orientation_blade`
            result[71] = Math.Round(blade_ori);

            /////////////////////////imu(rad to deg)//////////////////////////
            //몸체
            result[72] = Math.Round((motorgrader_body_imu[0] * (180.0 / Math.PI)), 1);
            result[73] = Math.Round((motorgrader_body_imu[1] * (180.0 / Math.PI)), 1);
            result[74] = Math.Round((calc_body_y * (180.0 / Math.PI)), 1);

            //암
            result[75] = Math.Round((motorgrader_rotor_imu[0] * (180.0 / Math.PI)), 1);
            result[76] = Math.Round((motorgrader_rotor_imu[1] * (180.0 / Math.PI)), 1);
            result[77] = Math.Round((rotor_yaw * (180.0 / Math.PI)), 1);
             
            //블레이드
            result[78] = Math.Round((motorgrader_blade_imu[0] * (180.0 / Math.PI)), 1);
            result[79] = Math.Round((motorgrader_blade_imu[1] * (180.0 / Math.PI)), 1);
            result[80] = Math.Round((blade_yaw * (180.0 / Math.PI)), 1);
            result[81] = Math.Round(motorgrader_rotor_angle, 1);

            return result;
        }
    }
}
