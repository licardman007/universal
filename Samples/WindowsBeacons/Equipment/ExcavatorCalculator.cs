﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using WindowsBeacons.Equipment.ExcavatorData;
using System.Diagnostics;

namespace WindowsBeacons.Component
{
    class ExcavatorCalculator
    {
        ////상수입력
        //수신기 좌표이동거리
        //사잇거리
        //커팅거리

        ////const_name
        //@_1
        //@_2
        //@_3
        
            
        // 요값 흔들림 +-2


        //calibration
        double[] p1_calibration = new double[4];
        double[] p2_calibration = new double[4];

        //좌표
        double[] becon_1 = new double[3];
        double[] becon_2 = new double[3];
        double[] becon_3 = new double[3];
        double[] becon_4 = new double[3];//비컨이 4개일 경우

        double[] r_becon_1 = new double[3];
        double[] r_becon_2 = new double[3];
        double[] r_becon_3 = new double[3];
        double[] r_becon_4 = new double[3];//비컨이 4개일 경우

        double[] instrument_station_1 = new double[3];
        double[] instrument_station_2 = new double[3];
        double[] instrument_station_3 = new double[3];

        //측점의 상대좌표
        double[] r_instrument_station_2 = new double[3];
        double[] r_instrument_station_3 = new double[3];

        //수신기
        double[] p_1_u = new double[3];    double[] p_2_u = new double[3];
        double[] p_1_d = new double[3];    double[] p_2_d = new double[3];

        double[] p_1_u_o = new double[3];  double[] p_2_u_o = new double[3];
        double[] p_1_d_o = new double[3];  double[] p_2_d_o = new double[3];

        //수신기 계산 판별
        Int16 p_calc_1 = 1;
        Int16 p_calc_2 = 1;

        //다운된 거리값 보정
        double[] d_1_t = new double[3];
        double[] d_2_t = new double[3];

        double[] d_1_t_o = new double[3];
        double[] d_2_t_o = new double[3];

        //imu_calibration
        double[] e_body_imu_calibration = new double[2];
        double[] e_boom_imu_calibration = new double[2];
        double[] e_arm_imu_calibration = new double[2];
        double[] e_bucket_imu_calibration = new double[2];

        //요값 조절
        double e_body_y_c = 0;
        double e_boom_y_c = 0;
        double e_arm_y_c = 0;

        //수신기와 장비 바닥까지의 높이
        double e_reciver_down = 0;

        //값파기값 0을 막가위해
        double[] pb = new double[3];
        double pb_s = 0;

        double yawCalcPass = 0; // yaw값 계산 확인 부분 - 추가된 부분(1221)

        //---------------------------------------------------평균화 시킨값과 비교--------------------------------------------------------
        double[] value_1 = new double[8];
        double[] value_2 = new double[8];

        //const_name
        //칼만값 초기화 커팅, 초기화 근접 상수 /50, 
        Int16 K_time2_boundary_constant_up = 50;
        Int16 K_time2_boundary_constant_down = -50;

        //평균화값과 칼만필터값과의 비교를 위한 변수
        UInt16 p1b1_K = 0;
        UInt16 p1b2_K = 0;
        UInt16 p1b3_K = 0;
        UInt16 p1b4_K = 0;

        UInt16 p2b1_K = 0;
        UInt16 p2b2_K = 0;
        UInt16 p2b3_K = 0;
        UInt16 p2b4_K = 0;

        UInt16 P1P2_distance_signal = 0;//초기 상태로 돌아갈수있는지 확인을 위한 부분

        double[] save_value_2 = new double[8];//초기화단계에서 거리값 저장





        //-------------------------------------------------------안정화 구간----------------------------------------------------
        UInt16 stop_selection = 0;//멈춘상태로 가는중인지 파악
        UInt16 move_point = 0;//이동안된 상태 선언

        //const_name - 커팅범위
        double time2_boundary_constant_up = 2000;
        double time2_boundary_constant_down = -2000;

        //const_name
        double move_up = 2000;
        double move_down = -2000;

        double debug_count = 0;



        
        //----------------------------------------------- @_1 ---------------------------------------------------
        UInt16 no_cut_save = 0;//이전 칼만 필터로 보낼수 있는지 확인부분

        //컷팅한값 저장
        double[] save_cut_becon_p1_backhoe = new double[4];
        double[] save_cut_becon_p2_backhoe = new double[4];

        //로우값으로 계산된 수신기좌표
        double[] c_becon_p1_backhoe = new double[4];
        double[] c_becon_p2_backhoe = new double[4];

        UInt16 const_becon_p2_backhoe = 0;

        double[] backhoe_data_1 = new double[87];

        double P1P2_absolute = 0;

        //const_name
        UInt16 P1P2_absolute_constat = 200;//수신기 좌표이동 범위/500, 100, 50, 

        UInt16 no_p12_1 = 0;//수신기 좌표이동 튀는값

        UInt32 p_move_count = 0;//수신기 좌표이동 true 카운트

        //위치거리값 저장을 위한 선언
        double[] becon_p1_backhoe_d_a = new double[4];
        double[] becon_p2_backhoe_d_a = new double[4];

        UInt16 stabilization_position_cut_save = 0;//이전값으로 부내는 부분

        //----두 수신기의 이동된 거리를 가지고 오차를 판단
        double[] r_receiving_set_k_s1 = new double[6];
        double[] r_receiving_set_k_s2 = new double[6];
        
        double P1bP1a_distance = 0;
        double P2bP2a_distance = 0;




        //------------------------------------------------ @_2 --------------------------------------------------
        double[] backhoe_data_2 = new double[100]; // - 수정된 부분(1214)
        
        double P1P2_distance_o = 0;

        //const_name
        double const_distance = 986;//거리값 범위 결정을 위한 부분/1225(스티로폼), 986(수정된 도저모형)
        double distance_value_const = 200;//사잇거리 허용 범위 /50, 30, 100, 200, 150, 

        //사이거리값 저장을 위한 선언
        double[] becon_p1_backhoe_a = new double[4];
        double[] becon_p2_backhoe_a = new double[4];

        UInt16 const_becon_p_backhoe_a = 0;//사이거리 이전 이후를 구분하기 위해

        UInt32 p12_distance_count = 0;//사잇거리 true 카운트

        //커팅 부분이 적용 가능한지 구분
        UInt16 no_distance_1 = 0;






        //------------------------------------------------ @_3 --------------------------------------------------
        Int16 constant_number = 0;

        //로우값 끼리 비교할때 필요한 변수
        double[] cut_value_1 = new double[8];
        double[] cut_value_2 = new double[8];

        Int16 constant_number_2 = 0;

        UInt16 cut_output = 0;//커팅부분 이전 수신기값 구분해서 디버그

        UInt16 cut_signal = 0;//커팅통과시 구분을 위한 변수

        UInt32 cut_count = 0;//커팅 true 카운트





        //-------------------------------------------------------------------K.F.-------------------------------------------------------------
        //---------------------------------------verage
        double[] kf_value_1 = new double[8];//비컨이 4개
        double[] kf_value_2 = new double[8];//비컨이 4개
         
        //----------------------------------------variable
        
        double x_est_p1b1 = 0;
        double x_est_p1b2 = 0;
        double x_est_p1b3 = 0;
        double x_est_p1b4 = 0;//비컨이 4개
        
        double x_est_p2b1 = 0;
        double x_est_p2b2 = 0;
        double x_est_p2b3 = 0;
        double x_est_p2b4 = 0;//비컨이 4개

        double x_est_last_p1b1 = 0;
        double x_est_last_p1b2 = 0;
        double x_est_last_p1b3 = 0;
        double x_est_last_p1b4 = 0;//비컨이 4개

        double x_est_last_p2b1 = 0;
        double x_est_last_p2b2 = 0;
        double x_est_last_p2b3 = 0;
        double x_est_last_p2b4 = 0;//비컨이 4개
        
        double P_last = 0;//0, //계속적 감소, 멈춤(update_covariance)
        
        //반응속도 (R = 0.0001, Q = 0.0075 확장칼만보다 더빠름)x_수신기 4개

        //반응속도 (R = 0.0001, Q = 0.005 확장칼만보다 더빠름)x_수신기 4개

        //반응속도 (R = 0.001, Q = 0.0001 확장칼만보다 더느림)_수신기 2개

        //반응속도 (R = 0.001, Q = 0.0005 확장칼만보다 더느림)_수신기 4개

        //반응속도 (R = 0.001, Q = 0.001 확장칼만보다 더느림)_수신기 4개

        //반응속도 (R = 0.0001, Q = 0.001 확장칼만보다 더빠름)_수신기 4개
        
        //const_name
        double Q = 0.0001;//fixing 0.000001,  0.001, 0.0001, 
        double R = 0.001;// fixing 0.001,     0.001, 0.001, 

        double stop_signal_Q = 0.0001;//안정화 단계에서 Q값
        
        //1이면 적용이 안된것임
        double A_value = 1;
        double H_value = 1;

        //---------------------------------------------------------------declaration
        double x_temp_est_p1b1 = 0;
        double x_temp_est_p1b2 = 0;
        double x_temp_est_p1b3 = 0;
        double x_temp_est_p1b4 = 0;//비컨4개
        //----------------------------------------------------------------------------

        double x_temp_est_p2b1 = 0;
        double x_temp_est_p2b2 = 0;
        double x_temp_est_p2b3 = 0;
        double x_temp_est_p2b4 = 0;//비컨4개

        double P_temp = 0;
        double K = 0;
        double P = 0;

        UInt16 k_a_signal = 0;//안정화 후 칼만 정상화 확인 부분

        //------------------------------------------------------------------------------------백호
        double[] excavator_constant = { 0, 0, 0 };// 백호 {Boom, Arm, buckit}
        
        //비컨 4개
        double[] becon_p1_backhoe = new double[4];
        double[] becon_p2_backhoe = new double[4];

        //최종거리값 표현을 위한 부분
        double[] f_becon_p1_backhoe = new double[4];
        double[] f_becon_p2_backhoe = new double[4];
        
        double[] excavator_body_imu = new double[2];
        double[] excavator_boom_imu = new double[2];
        double[] excavator_arm_imu = new double[2];
        double[] excavator_arm_imu_s = new double[2];
        double[] excavator_bucket_imu = new double[2];
        double[] excavator_body_length = new double[7];

        double[] backhoe_data = new double[100]; // 추가된 부분(1214)

        double[] excavator_body_imu_a = new double[2];
        double[] excavator_boom_imu_a = new double[2];
        double[] excavator_arm_imu_a = new double[2];
        double[] excavator_bucket_imu_a = new double[2];
        
        double[] excavator_bucket_constant = new double[4];
        double excavator_bucket_angle = 0;

        //수신기 좌표계산 1
        double[] r_backhoe_tri_point = new double[12];
        double[] r_receiving_set_k = new double[6];

        //수신기 좌표계산 2
        double[] r_backhoe_tri_point_o = new double[12];
        double[] r_receiving_set_o = new double[6];

        //arm 각도  보정
        UInt16 state = 1;
        UInt16 state_routine = 1;

        //버킷 각도 보정
        UInt16 state_bucket = 1;
        UInt16 state_routine_bucket = 1;
        
        UInt16 stop_signal    = 0;//멈춰있는지의 유무
        
        //yaw 값 판별
        //------calc_imu
        double calc_body_y = 0;//저장을 위한 전역 변수 설정
        double calc_body_y_o = 0;
        double calc_body_y_a = 0;//요값 저장을 위한
        UInt16 NaN_yaw_count = 0;//계산 실패한 yaw 카운트
        
        //중점 가지고 거리계산
        double[] pb_o   = new double[3];
        double[] pb_o_2 = new double[3];
        double[] pb_o_1 = new double[3];//초기 거리값 저장 부분
        
        double[] r_receiving_set = new double[6];//수신기값 저장을 위한 전역변수 선언



        //-------칼만필터 통과후 위치좌표 이동된 거리 표시를 위한-------
        UInt16 pb_signal = 0;
        UInt16 pb_count = 0;
        double pb_distance = 0;

        double[] pb_a = new double[3];
        double[] pb_b = new double[3];


        //----------------거리 중간값 계산(시작)--------------------
        Int16 number_standard_P1 = 0;
        Int16 number_standard_P2 = 0;

        Int16 number_divide_P1 = 0;
        Int16 number_divide_P2 = 0;



        //-----정렬할 갯수 입력 - 비컨 4개
        //배열의 총수 6/99
        double[] real_data_d_p1_b1 = new double[99];
        double[] real_data_d_p1_b2 = new double[99];
        double[] real_data_d_p1_b3 = new double[99];
        double[] real_data_d_p1_b4 = new double[99];
                                              
        double[] real_data_d_p2_b1 = new double[99];
        double[] real_data_d_p2_b2 = new double[99];
        double[] real_data_d_p2_b3 = new double[99];
        double[] real_data_d_p2_b4 = new double[99];

        double[] temp_array_sort_d_P1_b1 = new double[99];
        double[] temp_array_sort_d_P1_b2 = new double[99];
        double[] temp_array_sort_d_P1_b3 = new double[99];
        double[] temp_array_sort_d_P1_b4 = new double[99];
                                                    
        double[] temp_array_sort_d_P2_b1 = new double[99];
        double[] temp_array_sort_d_P2_b2 = new double[99];
        double[] temp_array_sort_d_P2_b3 = new double[99];
        double[] temp_array_sort_d_P2_b4 = new double[99];
        
        Int16 constant_number_d_P1 = 0;
        Int16 constant_number_d_P2 = 0;

        Int32 i = 0, k = 0;

        double data_value_align = 0;
    
        //평균계산 부분
        Int16 const_array = 0;
        
        double sum_P1b1_1 = 0;
        double sum_P1b2_1 = 0;
        double sum_P1b3_1 = 0;
        double sum_P1b4_1 = 0;

        double sum_P1b1_2 = 0;
        double sum_P1b2_2 = 0;
        double sum_P1b3_2 = 0;
        double sum_P1b4_2 = 0;

        double sum_P2b1_1 = 0;
        double sum_P2b2_1 = 0;
        double sum_P2b3_1 = 0;
        double sum_P2b4_1 = 0;
                    
        double sum_P2b1_2 = 0;
        double sum_P2b2_2 = 0;
        double sum_P2b3_2 = 0;
        double sum_P2b4_2 = 0;

        double[] a_becon_p1_backhoe = new double[4];
        double[] a_becon_p2_backhoe = new double[4];

        //------------------거리 중간값 계산(끝)--------------------


        //------------------ z 중간값 계산(시작) --------------------
        
        Int16 number_standard_P1_z = 0;
        Int16 number_standard_P2_z = 0;

        //각 구간의 배열 갯수(총 9개일때)
        Int16 number_divide_P1_z = 3;
        Int16 number_divide_P2_z = 3;

        //------음수값 비교를 위한 -------
        double[] real_data_d_p1_z = { -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999 };
        double[] real_data_d_p2_z = { -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999, -99999999999999 };

        double[] temp_array_sort_d_P1_z = new double[9];
        double[] temp_array_sort_d_P2_z = new double[9];
        //---------------------------------------------------

        Int16 constant_number_d_P1_z = 0;
        Int16 constant_number_d_P2_z = 0;

        Int32 iz = 0, kz = 0;

        Int32 jz = 0;//초기화를 위한 변수


        double data_value_align_z = 0;

        Int16 center_position_p1 = 0;
        Int16 center_position_p2 = 0;

        double calc_center_p1 = 0;
        double calc_center_p2 = 0;
        //------------------z 중간값 계산(끝)--------------------
        



        //통합된 변수선언
        double[] receiver_p1_data = new double[4];
        double[] receiver_p2_data = new double[4];

        Int16 average_count = 0;
        Int16 average_stop = 0;
        
        //--------------------비컨 각도를 위한 - 시작-----------------
        //비컨 2개 계산 결정 준비
        Int16 beacon_2_calc = 0;

        //비컨 선택시 사용될 변수 선언
        Int16 s_beacon_0 = 0;
        Int16 s_beacon_1 = 0;
        Int16 s_beacon_2 = 0;
        Int16 s_beacon_3 = 0;

        //나머지 각도인 비컨의 좌표에 사용될 부분
        double angle_s_b1_x = 0;
        double angle_s_b1_y = 0;
        double angle_s_b1_z = 0;

        double angle_s_b2_x = 0;
        double angle_s_b2_y = 0;
        double angle_s_b2_z = 0;

        double angle_s_b3_x = 0;
        double angle_s_b3_y = 0;
        double angle_s_b3_z = 0;

        //각도가 가장 큰 비컨의 좌표에 사용될 부분
        double angle_s_b0_x = 0;
        double angle_s_b0_y = 0;
        double angle_s_b0_z = 0;

        //--------------------비컨 각도를 위한 추가 - 끝-----------------

        Int16 first_const = 0;//처음값 확인용
        Int16 kalman_after = 0;//칼만통과 재확인용

        //---------수신기 계산에서의 값 저장 부분 시작 ---------

        //지역변수에서 전역변수로
        double[] r_becon_1_c = new double[3];
        double[] r_becon_2_c = new double[3];
        double[] r_becon_3_c = new double[3];
        


        //----계산_1
        //거리값 저장
        double save_p1b1 = 0;
        double save_p1b2 = 0;
        double save_p1b3 = 0;
                         
        double save_p2b1 = 0;
        double save_p2b2 = 0;
        double save_p2b3 = 0;

        
        //비컨값 저장
        double save_b1_x = 0;
        double save_b1_y = 0;
        double save_b1_z = 0;
                           
        double save_b2_x = 0;
        double save_b2_y = 0;
        double save_b2_z = 0;
                           
        double save_b3_x = 0;
        double save_b3_y = 0;
        double save_b3_z = 0;

        Int16 save_id_b1 = 0;
        Int16 save_id_b2 = 0;
        Int16 save_id_b3 = 0;

        //-----계산_2
        //거리값 저장
        double save_p1b1_o = 0;
        double save_p1b2_o = 0;
        double save_p1b3_o = 0;
                       
        double save_p2b1_o = 0;
        double save_p2b2_o = 0;
        double save_p2b3_o = 0;
                        
        //비컨값 저장   
        double save_b1_x_o = 0;
        double save_b1_y_o = 0;
        double save_b1_z_o = 0;
                        
        double save_b2_x_o = 0;
        double save_b2_y_o = 0;
        double save_b2_z_o = 0;
                        
        double save_b3_x_o = 0;
        double save_b3_y_o = 0;
        double save_b3_z_o = 0;

        Int16 save_id_b1_o = 0;
        Int16 save_id_b2_o = 0;
        Int16 save_id_b3_o = 0;

        //이전거리값을 사용 해야할경우
        Int16 check_calc = 0;
        Int16 before_d_value = 0;

        // 임시 z값
        double tempZ = 0.0f;
        
        //거리값이 999999 일경우 비컨표시를 위한 변수 선언
        Int16 b1_sign = 0;
        Int16 b2_sign = 0;
        Int16 b3_sign = 0;
        Int16 b4_sign = 0;

        //-------------------------- p&t 관련부분 ----------------------------------
        // 타겟의 광파기 좌표값 - 추가된 부분(1214)
        // ##0
        double[] targetPoint = {2700, 450, 1450};
        
        double calcDownPz = 0; // P&T에서 받아온 수정된 바닥의 z값 - 수정된 부분(1218)
        public string arduinoSend = "";

        double saveYaw = 0; // 요값을 저장하는 변수 - 추가된 부분(1222)
        double saveRoll = 0; // 롤값을 저장하는 변수 - 추가된 부분(1226)
        double savePitch = 0; // 피치값을 저장하는 변수 - 추가된 부분(1226)

        public Int16 arduinoSendCounter = 0; // 아두이노 1번 보내기 - 추가된 부분(1214)
        Int16 arduinoNum = 0; // 아두이노 보내는 갯수 부분 - 추가된 부분(1223)

        public ExcavatorData ExcavatorData
        {
            get;set;
        }
        

        //--------------------------------------------------시리얼 포트 수신 부분-------------------------------------------------------------

        public ExcavatorCalculator()
        {
            ExcavatorData = new ExcavatorData();
        }

        //UI 와 다른 부분
        public void InitExcavatorCalcData(TextBox ConstCalibration, TextBox ConstIMUCalibration, TextBox ConstAbsolutePoint, TextBox ConstBeaconPoint, TextBox ConstExcavator)
        {
            //calibration
            p1_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[0]);
            p1_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[1]);
            p1_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[2]);
            p1_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[3]);


            p2_calibration[0] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[4]);
            p2_calibration[1] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[5]);
            p2_calibration[2] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[6]);
            p2_calibration[3] = Convert.ToDouble(ConstCalibration.Text.Split('\r')[7]);
            
            e_body_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[0]);//body_r
            e_body_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[1]);//body_p
            e_body_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[2]);//body_y

            e_boom_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[3]);//boom_r
            e_boom_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[4]);//boom_p
            e_boom_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[5]);//boom_y

            e_arm_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[6]);//arm_r
            e_arm_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[7]);//arm_p
            e_arm_y_c = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[8]);//arm_y

            e_bucket_imu_calibration[0] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[9]);//bucket_r
            e_bucket_imu_calibration[1] = Convert.ToDouble(ConstIMUCalibration.Text.Split('\r')[10]);//bucket_p

            

            //측점
            instrument_station_1[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[0]);
            instrument_station_1[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[1]);
            instrument_station_1[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[2]); 
            instrument_station_2[0] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[3]);
            instrument_station_2[1] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[4]);
            instrument_station_2[2] = Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[5]);
            instrument_station_3[0] = 0;//Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[6]);
            instrument_station_3[1] = 0;//Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[7]);
            instrument_station_3[2] = 0;//Convert.ToDouble(ConstAbsolutePoint.Text.Split('\r')[8]);

            ////측점 직접입력
            //instrument_station_1[0] = 0;
            //instrument_station_1[1] = 0;
            //instrument_station_1[2] = 0; 
            //instrument_station_2[0] = 0;
            //instrument_station_2[1] = 0;
            //instrument_station_2[2] = 0;
            //instrument_station_3[1] = 0;
            //instrument_station_3[0] = 0;
            //instrument_station_3[2] = 0;


            //비콘
            becon_1[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[0]);
            becon_1[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[1]);
            becon_1[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[2]);
            becon_2[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[3]);
            becon_2[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[4]);
            becon_2[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[5]);
            becon_3[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[6]);
            becon_3[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[7]);
            becon_3[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[8]);
            becon_4[0] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[9]);//비컨이 4개일 경우 추가
            becon_4[1] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[10]); 
            becon_4[2] = Convert.ToDouble(ConstBeaconPoint.Text.Split('\r')[11]);

            ////비콘 직접입력
            //becon_1[0] = 0;
            //becon_1[1] = 0;
            //becon_1[2] = 1515;
            //becon_2[0] = 3150;
            //becon_2[1] = 0;
            //becon_2[2] = 1515;
            //becon_3[0] = 2;
            //becon_3[1] = 2700;
            //becon_3[2] = 1515;
            //becon_4[0] = 3150;
            //becon_4[1] = 2700; 
            //becon_4[2] = 1515;

            //excavator
            excavator_body_length[0] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[0]);//두 수신기 사이의 길이값
            excavator_body_length[1] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[1]);//광파기 z보정값
            excavator_body_length[2] = 0;//Convert.ToDouble(ConstExcavator.Text.Split('\r')[2]);
            excavator_body_length[3] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[3]);
            excavator_body_length[4] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[4]);

            excavator_constant[0] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[5]);
            excavator_constant[1] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[6]);
            excavator_constant[2] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[7]);

            excavator_bucket_constant[0] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[8]);
            excavator_bucket_constant[1] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[9]);
            excavator_bucket_constant[2] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[10]);
            excavator_bucket_constant[3] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[11]);

            excavator_bucket_angle = Convert.ToDouble(ConstExcavator.Text.Split('\r')[12]);
            excavator_body_length[5] = 0;//Convert.ToDouble(ConstExcavator.Text.Split('\r')[13]);//f
            excavator_body_length[6] = Convert.ToDouble(ConstExcavator.Text.Split('\r')[14]);

            e_reciver_down = Convert.ToDouble(ConstExcavator.Text.Split('\r')[15]);//수신기와 장비 바닥까지의 높이

            
        }



        public void ExcavatorDataReceived(String lData, String mData, double tempZ)
        {
            this.tempZ = tempZ;
            calcDownPz = tempZ; // P&T에서 받아온 수정된 바닥의 z값 - 수정된 부분(1218)

            ExcavatorData.Location = lData;
            
            ExcavatorData.Motion = mData;
            
            const_distance = excavator_body_length[0];//두 수신기 사이의 거리 입력
            
            //비컨 4개
            becon_p1_backhoe[0] = Convert.ToDouble(lData.Substring(2, 6));
            becon_p1_backhoe[1] = Convert.ToDouble(lData.Substring(8, 6));
            becon_p1_backhoe[2] = Convert.ToDouble(lData.Substring(14, 6));
            becon_p1_backhoe[3] = Convert.ToDouble(lData.Substring(20, 6));

            becon_p2_backhoe[0] = Convert.ToDouble(lData.Substring(26, 6));
            becon_p2_backhoe[1] = Convert.ToDouble(lData.Substring(32, 6));
            becon_p2_backhoe[2] = Convert.ToDouble(lData.Substring(38, 6));
            becon_p2_backhoe[3] = Convert.ToDouble(lData.Substring(44, 6));

            //출력
            ExcavatorData.Original.Distance = Convert.ToString(becon_p1_backhoe[0]) + " " + Convert.ToString(becon_p1_backhoe[1]) + " " + Convert.ToString(becon_p1_backhoe[2]) + " " + Convert.ToString(becon_p1_backhoe[3]) + " "
                                + Convert.ToString(becon_p2_backhoe[0]) + " " + Convert.ToString(becon_p2_backhoe[1]) + " " + Convert.ToString(becon_p2_backhoe[2]) + " " + Convert.ToString(becon_p2_backhoe[3]);



            //거리값이 999999일 경우 해당되는 비컨 표시
            if (becon_p1_backhoe[0] == 999999 || becon_p2_backhoe[0] == 999999) b1_sign = 1;
            else if (becon_p1_backhoe[0] < 999999 && becon_p2_backhoe[0] < 999999) b1_sign = 0;

            if (becon_p1_backhoe[1] == 999999 || becon_p2_backhoe[1] == 999999) b2_sign = 1;
            else if (becon_p1_backhoe[1] < 999999 && becon_p2_backhoe[1] < 999999) b2_sign = 0;

            if (becon_p1_backhoe[2] == 999999 || becon_p2_backhoe[2] == 999999) b3_sign = 1;
            else if (becon_p1_backhoe[2] < 999999 && becon_p2_backhoe[2] < 999999) b3_sign = 0;

            if (becon_p1_backhoe[3] == 999999 || becon_p2_backhoe[3] == 999999) b4_sign = 1;
            else if (becon_p1_backhoe[3] < 999999 && becon_p2_backhoe[3] < 999999) b4_sign = 0;
            
            //수신기 케이블 연장에 따른 캘리브레이션 상수 추가
            becon_p1_backhoe[0] = Math.Abs(becon_p1_backhoe[0] + p1_calibration[0]);
            becon_p1_backhoe[1] = Math.Abs(becon_p1_backhoe[1] + p1_calibration[1]);
            becon_p1_backhoe[2] = Math.Abs(becon_p1_backhoe[2] + p1_calibration[2]);
            becon_p1_backhoe[3] = Math.Abs(becon_p1_backhoe[3] + p1_calibration[3]);

            becon_p2_backhoe[0] = Math.Abs(becon_p2_backhoe[0] + p2_calibration[0]);
            becon_p2_backhoe[1] = Math.Abs(becon_p2_backhoe[1] + p2_calibration[1]);
            becon_p2_backhoe[2] = Math.Abs(becon_p2_backhoe[2] + p2_calibration[2]);
            becon_p2_backhoe[3] = Math.Abs(becon_p2_backhoe[3] + p2_calibration[3]);
            
            becon_p1_backhoe[0] = Math.Round(becon_p1_backhoe[0]);
            becon_p1_backhoe[1] = Math.Round(becon_p1_backhoe[1]);
            becon_p1_backhoe[2] = Math.Round(becon_p1_backhoe[2]);
            becon_p1_backhoe[3] = Math.Round(becon_p1_backhoe[3]);

            becon_p2_backhoe[0] = Math.Round(becon_p2_backhoe[0]);
            becon_p2_backhoe[1] = Math.Round(becon_p2_backhoe[1]);
            becon_p2_backhoe[2] = Math.Round(becon_p2_backhoe[2]);
            becon_p2_backhoe[3] = Math.Round(becon_p2_backhoe[3]);

            // 디버그 간소화로 인한 - 수정된 부분(1214)
            //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1s: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
            //+ " eP2s: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 원본 거리값 ");




            //--------------중간값 계산식 시작-----------
            if (average_stop == 0)//평균화 작동이 멈출경우 1
            {

                average_count++;//카운트를 셈



                //P1
                real_data_d_p1_b1[constant_number_d_P1] = becon_p1_backhoe[0];
                real_data_d_p1_b2[constant_number_d_P1] = becon_p1_backhoe[1];
                real_data_d_p1_b3[constant_number_d_P1] = becon_p1_backhoe[2];
                real_data_d_p1_b4[constant_number_d_P1] = becon_p1_backhoe[3];

                //P2
                real_data_d_p2_b1[constant_number_d_P2] = becon_p2_backhoe[0];
                real_data_d_p2_b2[constant_number_d_P2] = becon_p2_backhoe[1];
                real_data_d_p2_b3[constant_number_d_P2] = becon_p2_backhoe[2];
                real_data_d_p2_b4[constant_number_d_P2] = becon_p2_backhoe[3];

                //각 구간의 배열 갯수 /2/33
                number_divide_P1 = 33;
                number_divide_P2 = 33;
                
                //중앙구간에서의 첫번째수(배열 맨처음에 0부터 시작이므로)
                number_standard_P1 = number_divide_P1;
                number_standard_P2 = number_divide_P2;
                
                //P1
                //P1b1
                temp_array_sort_d_P1_b1 = real_data_d_p1_b1;//가상의 공간을 생성 
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)//5
                {
                    for (k = i + 1; k < number_divide_P1 * 3; k++)//6
                    {
                        if (temp_array_sort_d_P1_b1[i] > temp_array_sort_d_P1_b1[k])//i 가 0 ~ 4 까지일때 각각 + 1 부터 5까지 비교(처음에는 0 배열을 제외하고 전부 0임)
                        {
                            //크기별로 순서화
                            data_value_align = temp_array_sort_d_P1_b1[i];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P1_b1[i] = temp_array_sort_d_P1_b1[k];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P1_b1[k] = data_value_align;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }

                ////Debug.WriteLine(" data_value_align " + data_value_align);

                data_value_align = 0;
                ////-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b1_1 = temp_array_sort_d_P1_b1[number_standard_P1];
                        
                    }

                    else if (const_array > 0)
                    {
                        sum_P1b1_2 = temp_array_sort_d_P1_b1[number_standard_P1 + const_array];

                        sum_P1b1_1 = sum_P1b1_1 + sum_P1b1_2;
                    }
                }

                a_becon_p1_backhoe[0] = sum_P1b1_1 / number_divide_P1;//평균화



                //P1b2
                temp_array_sort_d_P1_b2 = real_data_d_p1_b2;
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b2[i] > temp_array_sort_d_P1_b2[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b2[i];
                            temp_array_sort_d_P1_b2[i] = temp_array_sort_d_P1_b2[k];
                            temp_array_sort_d_P1_b2[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b2_1 = temp_array_sort_d_P1_b2[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b2_2 = temp_array_sort_d_P1_b2[number_standard_P1 + const_array];

                        sum_P1b2_1 = sum_P1b2_1 + sum_P1b2_2;
                    }
                }

                a_becon_p1_backhoe[1] = sum_P1b2_1 / number_divide_P1;//평균화

                //P1b3
                temp_array_sort_d_P1_b3 = real_data_d_p1_b3;
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b3[i] > temp_array_sort_d_P1_b3[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b3[i];
                            temp_array_sort_d_P1_b3[i] = temp_array_sort_d_P1_b3[k];
                            temp_array_sort_d_P1_b3[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b3_1 = temp_array_sort_d_P1_b3[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b3_2 = temp_array_sort_d_P1_b3[number_standard_P1 + const_array];

                        sum_P1b3_1 = sum_P1b3_1 + sum_P1b3_2;
                    }
                }

                a_becon_p1_backhoe[2] = sum_P1b3_1 / number_divide_P1;//평균화

                //P1b4
                temp_array_sort_d_P1_b4 = real_data_d_p1_b4;
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P1 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P1 * 3; k++)
                    {
                        if (temp_array_sort_d_P1_b4[i] > temp_array_sort_d_P1_b4[k])
                        {
                            data_value_align = temp_array_sort_d_P1_b4[i];
                            temp_array_sort_d_P1_b4[i] = temp_array_sort_d_P1_b4[k];
                            temp_array_sort_d_P1_b4[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P1; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P1b4_1 = temp_array_sort_d_P1_b4[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P1b4_2 = temp_array_sort_d_P1_b4[number_standard_P1 + const_array];

                        sum_P1b4_1 = sum_P1b4_1 + sum_P1b4_2;
                    }
                }

                a_becon_p1_backhoe[3] = sum_P1b4_1 / number_divide_P1;//평균화

                //P2
                //P2b1
                temp_array_sort_d_P2_b1 = real_data_d_p2_b1;//가상의 공간을 생성
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b1[i] > temp_array_sort_d_P2_b1[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b1[i];
                            temp_array_sort_d_P2_b1[i] = temp_array_sort_d_P2_b1[k];
                            temp_array_sort_d_P2_b1[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b1_1 = temp_array_sort_d_P2_b1[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b1_2 = temp_array_sort_d_P2_b1[number_standard_P1 + const_array];

                        sum_P2b1_1 = sum_P2b1_1 + sum_P2b1_2;
                    }
                }

                a_becon_p2_backhoe[0] = sum_P2b1_1 / number_divide_P2;//평균화

                //P2b2
                temp_array_sort_d_P2_b2 = real_data_d_p2_b2;
               
                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b2[i] > temp_array_sort_d_P2_b2[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b2[i];
                            temp_array_sort_d_P2_b2[i] = temp_array_sort_d_P2_b2[k];
                            temp_array_sort_d_P2_b2[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b2_1 = temp_array_sort_d_P2_b2[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b2_2 = temp_array_sort_d_P2_b2[number_standard_P1 + const_array];

                        sum_P2b2_1 = sum_P2b2_1 + sum_P2b2_2;
                    }
                }

                a_becon_p2_backhoe[1] = sum_P2b2_1 / number_divide_P2;//평균화

                //P2b3
                temp_array_sort_d_P2_b3 = real_data_d_p2_b3;
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b3[i] > temp_array_sort_d_P2_b3[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b3[i];
                            temp_array_sort_d_P2_b3[i] = temp_array_sort_d_P2_b3[k];
                            temp_array_sort_d_P2_b3[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b3_1 = temp_array_sort_d_P2_b3[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b3_2 = temp_array_sort_d_P2_b3[number_standard_P1 + const_array];

                        sum_P2b3_1 = sum_P2b3_1 + sum_P2b3_2;
                    }
                }

                a_becon_p2_backhoe[2] = sum_P2b3_1 / number_divide_P2;//평균화

                //P2b4
                temp_array_sort_d_P2_b4 = real_data_d_p2_b4;
                
                //------직접계산 시작
                for (i = 0; i < number_divide_P2 * 3 - 1; i++)
                {
                    for (k = 1 + i; k < number_divide_P2 * 3; k++)
                    {
                        if (temp_array_sort_d_P2_b4[i] > temp_array_sort_d_P2_b4[k])
                        {
                            data_value_align = temp_array_sort_d_P2_b4[i];
                            temp_array_sort_d_P2_b4[i] = temp_array_sort_d_P2_b4[k];
                            temp_array_sort_d_P2_b4[k] = data_value_align;
                        }
                    }
                }

                data_value_align = 0;
                //-------직접계산 끝
                
                //for문 수정으로 인한
                for (const_array = 0; const_array < number_divide_P2; const_array++)
                {
                    if (const_array == 0)
                    {
                        sum_P2b4_1 = temp_array_sort_d_P2_b4[number_standard_P1];

                    }

                    else if (const_array > 0)
                    {
                        sum_P2b4_2 = temp_array_sort_d_P2_b4[number_standard_P1 + const_array];

                        sum_P2b4_1 = sum_P2b4_1 + sum_P2b4_2;
                    }
                }

                a_becon_p2_backhoe[3] = sum_P2b4_1 / number_divide_P2;//평균화
                
                //에러로 인한 추가
                ExcavatorData.Original.IMUSensor = ExcavatorData.Motion.Substring(10, 4) + " " + ExcavatorData.Motion.Substring(6, 4) + " " + ExcavatorData.Motion.Substring(22, 4) + " "
                                + ExcavatorData.Motion.Substring(18, 4) + " " + ExcavatorData.Motion.Substring(34, 4) + " " + ExcavatorData.Motion.Substring(30, 4) + " " + ExcavatorData.Motion.Substring(46, 4) + " " + ExcavatorData.Motion.Substring(42, 4);


                Debug.WriteLine(" average_count " + average_count);//배열카운트 위치 이동

               
            }
            
            //배열이 차면 작동 중지(배열의 갯수 입력) - 평균화
            if (average_count == 99 && stop_signal == 0)//초기화 단계에서만 사용
            {
                
                average_stop = 1;//작동중지 신호(0 일때 평균화 작동)
                
                // 시간부분 제거 - 수정된 부분(1217)
                Debug.WriteLine(" eP1a: " + Math.Round(a_becon_p1_backhoe[0]) + "  " + Math.Round(a_becon_p1_backhoe[1]) + "  " + Math.Round(a_becon_p1_backhoe[2]) + "  " + Math.Round(a_becon_p1_backhoe[3]) + " eP2a: " + Math.Round(a_becon_p2_backhoe[0]) + "  " + Math.Round(a_becon_p2_backhoe[1]) + "  " + Math.Round(a_becon_p2_backhoe[2]) + "  " + Math.Round(a_becon_p2_backhoe[3]) + " 중간값을 평균시킨값");
            }



            if (average_stop == 1)//배열이 작동 중일때는 다른 필터 계산을 하지 않는다(평균화 작동 0) - 평균화
            {
                if (stop_signal == 0)//초기화 단계일때(0)
                {
                    //평균화 시킨값과 비교하는 부분
                    value_1[0] = becon_p1_backhoe[0];
                    value_1[1] = becon_p1_backhoe[1];
                    value_1[2] = becon_p1_backhoe[2];
                    value_1[3] = becon_p1_backhoe[3];

                    value_1[4] = becon_p2_backhoe[0];
                    value_1[5] = becon_p2_backhoe[1];
                    value_1[6] = becon_p2_backhoe[2];
                    value_1[7] = becon_p2_backhoe[3];

                    if ((value_1[0] > a_becon_p1_backhoe[0] + K_time2_boundary_constant_up) || (value_1[0] < a_becon_p1_backhoe[0] + K_time2_boundary_constant_down))
                    {
                        value_1[0] = a_becon_p1_backhoe[0];
                        becon_p1_backhoe[0] = value_1[0];
                    }


                    if ((value_1[1] > a_becon_p1_backhoe[1] + K_time2_boundary_constant_up) || (value_1[1] < a_becon_p1_backhoe[1] + K_time2_boundary_constant_down))
                    {
                        value_1[1] = a_becon_p1_backhoe[1];
                        becon_p1_backhoe[1] = value_1[1];
                    }

                    if ((value_1[2] > a_becon_p1_backhoe[2] + K_time2_boundary_constant_up) || (value_1[2] < a_becon_p1_backhoe[2] + K_time2_boundary_constant_down))
                    {
                        value_1[2] = a_becon_p1_backhoe[2];
                        becon_p1_backhoe[2] = value_1[2];
                    }

                    if ((value_1[3] > a_becon_p1_backhoe[3] + K_time2_boundary_constant_up) || (value_1[3] < a_becon_p1_backhoe[3] + K_time2_boundary_constant_down))
                    {
                        value_1[3] = a_becon_p1_backhoe[3];
                        becon_p1_backhoe[3] = value_1[3];
                    }

                    //---------------------------------------------------------------------------------------
                    if ((value_1[4] > a_becon_p2_backhoe[0] + K_time2_boundary_constant_up) || (value_1[4] < a_becon_p2_backhoe[0] + K_time2_boundary_constant_down))
                    {
                        value_1[4] = a_becon_p2_backhoe[0];
                        becon_p2_backhoe[0] = value_1[4];
                    }
                    if ((value_1[5] > a_becon_p2_backhoe[1] + K_time2_boundary_constant_up) || (value_1[5] < a_becon_p2_backhoe[1] + K_time2_boundary_constant_down))
                    {
                        value_1[5] = a_becon_p2_backhoe[1];
                        becon_p2_backhoe[1] = value_1[5];
                    }

                    if ((value_1[6] > a_becon_p2_backhoe[2] + K_time2_boundary_constant_up) || (value_1[6] < a_becon_p2_backhoe[2] + K_time2_boundary_constant_down))
                    {
                        value_1[6] = a_becon_p2_backhoe[2];
                        becon_p2_backhoe[2] = value_1[6];
                    }   

                    if ((value_1[7] > a_becon_p2_backhoe[3] + K_time2_boundary_constant_up) || (value_1[7] < a_becon_p2_backhoe[3] + K_time2_boundary_constant_down))
                    {
                        value_1[7] = a_becon_p2_backhoe[3];
                        becon_p2_backhoe[3] = value_1[7];
                    }

                    // 시간부분 제거 - 수정된 부분(1217)
                    Debug.WriteLine(" eP1c: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                        + " eP2c: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 평균값 비교후 컷트 시키고 난후 거리값 ");



                    //평균화시킨값과 칼만필터값과의 비교 부분

                    //평균화가 완료되었을때의 칼만필터값과 비교
                    value_2[0] = becon_p1_backhoe[0];
                    value_2[1] = becon_p1_backhoe[1];
                    value_2[2] = becon_p1_backhoe[2];
                    value_2[3] = becon_p1_backhoe[3];

                    value_2[4] = becon_p2_backhoe[0];
                    value_2[5] = becon_p2_backhoe[1];
                    value_2[6] = becon_p2_backhoe[2];
                    value_2[7] = becon_p2_backhoe[3];

                    if ((value_1[0] < x_est_p1b1 + K_time2_boundary_constant_up) && (value_1[0] > x_est_p1b1 + K_time2_boundary_constant_down))
                    {
                        //P1B1에 해당 되는 값들을 비교후 오차 범위 이내에 값이 들어온다면 표시
                        p1b1_K = 1;
                    }


                    if ((value_1[1] < x_est_p1b2 + K_time2_boundary_constant_up) && (value_1[1] > x_est_p1b2 + K_time2_boundary_constant_down))
                    {
                        p1b2_K = 1;
                    }

                    if ((value_1[2] < x_est_p1b3 + K_time2_boundary_constant_up) && (value_1[2] > x_est_p1b3 + K_time2_boundary_constant_down))
                    {
                        p1b3_K = 1;
                    }

                    if ((value_1[3] < x_est_p1b4 + K_time2_boundary_constant_up) && (value_1[3] > x_est_p1b4 + K_time2_boundary_constant_down))
                    {
                        p1b4_K = 1;
                    }

                    //---------------------------------------------------------------------------------------
                    if ((value_1[4] < x_est_p2b1 + K_time2_boundary_constant_up) && (value_1[4] > x_est_p2b1 + K_time2_boundary_constant_down))
                    {
                        p2b1_K = 1;
                    }
                    if ((value_1[5] < x_est_p2b2 + K_time2_boundary_constant_up) && (value_1[5] > x_est_p2b2 + K_time2_boundary_constant_down))
                    {
                        p2b2_K = 1;
                    }

                    if ((value_1[6] < x_est_p2b3 + K_time2_boundary_constant_up) && (value_1[6] > x_est_p2b3 + K_time2_boundary_constant_down))
                    {
                        p2b3_K = 1;
                    }

                    if ((value_1[7] < x_est_p2b4 + K_time2_boundary_constant_up) && (value_1[7] > x_est_p2b4 + K_time2_boundary_constant_down))
                    {
                        p2b4_K = 1;
                    }

                    if (p1b1_K == 1 && p1b2_K == 1 && p1b3_K == 1 && p1b4_K == 1 && p2b1_K == 1 && p2b2_K == 1 && p2b3_K == 1 && p2b4_K == 1)
                    {
                        P1P2_distance_signal = 1;//만족하면 안정화 단계 진입

                        //안정화 되기전의 칼만필터값 저장 - 초기값 적용할때 이 거리값을 사용 한다
                        save_value_2[0] = value_2[0];
                        save_value_2[1] = value_2[1];
                        save_value_2[2] = value_2[2];                                                                                                                                                       
                        save_value_2[3] = value_2[3];

                        save_value_2[4] = value_2[4];
                        save_value_2[5] = value_2[5];
                        save_value_2[6] = value_2[6];
                        save_value_2[7] = value_2[7];

                    }

                    //처음 일치하지않을경우 다시 비교하기위한 초기화 부분
                    p1b1_K = 0;
                    p1b2_K = 0;
                    p1b3_K = 0;
                    p1b4_K = 0;

                    p2b1_K = 0;
                    p2b2_K = 0;
                    p2b3_K = 0;
                    p2b4_K = 0;
                    
                }
                




                //--------------------------------------------------------------------------------안정화 구간---------------------------------------------------------------------------------------
                
                if (P1P2_distance_signal >= 1)//안정화 상태
                {
                    P1P2_distance_signal = 1;//안정화 신호를 유지하기위해

                    stop_selection = 2;//한번 값이 들어오면 유지 하기위해

                    if (move_point == 0)//이동안된 상태 - 한번들어가면 적용
                    {
                        

                        time2_boundary_constant_up = move_up;
                        time2_boundary_constant_down = move_down;

                        Q = stop_signal_Q;


                        stop_signal = 1;
                        
                        debug_count++;
                    }
                    
                }






                if (stop_signal == 1) // 안정화 단계 - 평균카운터가 끝난 상태
                {
                    //@_1
                    // -------------------  이 부분은 두 수신기의 좌표이동된 거리를 확인하는 구간임(입출력 : becon_p12_backhoe)------------------------------
                    //커팅된 거리값을 가지고 수신기의 좌표를 구한다.(입력 부분)

                    becon_p1_backhoe[0] = Math.Round(becon_p1_backhoe[0]);
                    becon_p1_backhoe[1] = Math.Round(becon_p1_backhoe[1]);
                    becon_p1_backhoe[2] = Math.Round(becon_p1_backhoe[2]);
                    becon_p1_backhoe[3] = Math.Round(becon_p1_backhoe[3]);

                    becon_p2_backhoe[0] = Math.Round(becon_p2_backhoe[0]);
                    becon_p2_backhoe[1] = Math.Round(becon_p2_backhoe[1]);
                    becon_p2_backhoe[2] = Math.Round(becon_p2_backhoe[2]);
                    becon_p2_backhoe[3] = Math.Round(becon_p2_backhoe[3]);

                    //이 부분은 수신기의 비교할 두개의 좌표를 계산하기 위한 구간이다
                    //조건문을 걸어서 초기단계와 안정화단계를 구분한다
                    if (no_cut_save > 0)//안정화 수신 데이터가 존재할때
                    {
                        //이전 수신 데이터가 커팅 통과한값을 적용 시킬때)
                        receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                        receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                        receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                        receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                        receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                        receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                        receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                        receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                        c_becon_p1_backhoe[0] = receiver_p1_data[0];
                        c_becon_p1_backhoe[1] = receiver_p1_data[1];
                        c_becon_p1_backhoe[2] = receiver_p1_data[2];
                        c_becon_p1_backhoe[3] = receiver_p1_data[3];

                        c_becon_p2_backhoe[0] = receiver_p2_data[0];
                        c_becon_p2_backhoe[1] = receiver_p2_data[1];
                        c_becon_p2_backhoe[2] = receiver_p2_data[2];
                        c_becon_p2_backhoe[3] = receiver_p2_data[3];

                        const_becon_p2_backhoe = 2;//안정화 된후 거리값 가지고 사이값이 만족 했을때 수신기 좌표값 구분

                        first_const = 1;//처음으로 값이 들어옴

                        backhoe_data_1 = Calc_backhoe_point(becon_p1_backhoe, becon_p2_backhoe, c_becon_p1_backhoe, c_becon_p2_backhoe);//2로 들어가서 1로 나온다 becon_p12_backhoe 입력으로 들어감

                        first_const = 0;//처음으로 값이 들어온것을 리셋

                    }

                    else if (no_cut_save == 0)//초가화 수신 데이터가 존재할때
                    {

                        //이전 초기화단계 수신 데이터 거리값(이전 수신데이터가 중간값 적용 할때) 
                        receiver_p1_data[0] = a_becon_p1_backhoe[0];
                        receiver_p1_data[1] = a_becon_p1_backhoe[1];
                        receiver_p1_data[2] = a_becon_p1_backhoe[2];
                        receiver_p1_data[3] = a_becon_p1_backhoe[3];

                        receiver_p2_data[0] = a_becon_p2_backhoe[0];
                        receiver_p2_data[1] = a_becon_p2_backhoe[1];
                        receiver_p2_data[2] = a_becon_p2_backhoe[2];
                        receiver_p2_data[3] = a_becon_p2_backhoe[3];


                        c_becon_p1_backhoe[0] = receiver_p1_data[0];
                        c_becon_p1_backhoe[1] = receiver_p1_data[1];
                        c_becon_p1_backhoe[2] = receiver_p1_data[2];
                        c_becon_p1_backhoe[3] = receiver_p1_data[3];

                        c_becon_p2_backhoe[0] = receiver_p2_data[0];
                        c_becon_p2_backhoe[1] = receiver_p2_data[1];
                        c_becon_p2_backhoe[2] = receiver_p2_data[2];
                        c_becon_p2_backhoe[3] = receiver_p2_data[3];

                        const_becon_p2_backhoe = 2;//안정화 된후(평균 거친후) 거리값 가지고 사이값이 만족 했을때 수신기 좌표값 구분

                        first_const = 1;//처음으로 값이 들어옴

                        backhoe_data_1 = Calc_backhoe_point(becon_p1_backhoe, becon_p2_backhoe, c_becon_p1_backhoe, c_becon_p2_backhoe);//2로 들어가서 1로 나온다 becon_p12_backhoe 입력으로 들어감

                        first_const = 0;//처음으로 값이 들어온것을 리셋

                    }





                    if (P1P2_absolute > P1P2_absolute_constat)//튄값일때
                    {
                        
                        //--------------------------------------튄값일 경우 이전 칼만필터로 보낸 데이터 전달----------------------------

                        Debug.WriteLine(" P1P2_absolute(이동거리) " + P1P2_absolute + " false ");

                        // 디버그 간소화에 따른 수정된 부분(1214)
                        //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                        //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 좌표이동거리(false) ");


                        //false 일때
                        no_p12_1 = 2;
                    }

                    else if (P1P2_absolute <= P1P2_absolute_constat)//맞는 값일때
                    {
                        //맞을경우 카운팅
                        p_move_count++;

                        //현재값을 저장한다
                        becon_p1_backhoe_d_a[0] = becon_p1_backhoe[0];
                        becon_p1_backhoe_d_a[1] = becon_p1_backhoe[1];
                        becon_p1_backhoe_d_a[2] = becon_p1_backhoe[2];
                        becon_p1_backhoe_d_a[3] = becon_p1_backhoe[3];

                        becon_p2_backhoe_d_a[0] = becon_p2_backhoe[0];
                        becon_p2_backhoe_d_a[1] = becon_p2_backhoe[1];
                        becon_p2_backhoe_d_a[2] = becon_p2_backhoe[2];
                        becon_p2_backhoe_d_a[3] = becon_p2_backhoe[3];
                        
                        //true 일 경우 커팅으로 전달
                        no_p12_1 = 1;

                        Debug.WriteLine(" P1P2_absolute(이동거리) " + P1P2_absolute + " true ");

                        // 디버그 간소화에 따른 수정된 부분(1214)
                        //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                        //   + " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 좌표이동거리(true) ");
                        
                        //Debug.WriteLine("                                                                                                                                                                                                                        move_count " + p_move_count);

                        //값이 저장되었다는 것을 표시(계속적 1)
                        stabilization_position_cut_save = 1;//(save = 1)
                        
                    }

                    else//NaN 이 나올경우
                    {
                        Debug.WriteLine(" P1P2_absolute(이동거리) " + P1P2_absolute + " false(NaN) ");

                        // 디버그 간소화에 따른 수정된 부분(1214)
                        //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                        //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 좌표이동거리(false_NaN) ");



                        //false 일때
                        no_p12_1 = 2;

                    }




                    //@_2
                    //------------------------------안정화된후 사잇거리값 적용(becon_p12_backhoe 입력)----------------------------
                    if (no_p12_1 == 2)//수신기 좌표이동 거리가 false 되는경우
                    {

                        //수신기 좌표이동 통과 확인 초기화(true 2, false 1, 초기값 0)
                        no_p12_1 = 0;

                        //사잇거리 계산을 위한 입력 becon_p12_backhoe
                        c_becon_p1_backhoe[0] = Math.Round(becon_p1_backhoe[0]);
                        c_becon_p1_backhoe[1] = Math.Round(becon_p1_backhoe[1]);
                        c_becon_p1_backhoe[2] = Math.Round(becon_p1_backhoe[2]);
                        c_becon_p1_backhoe[3] = Math.Round(becon_p1_backhoe[3]);

                        c_becon_p2_backhoe[0] = Math.Round(becon_p2_backhoe[0]);
                        c_becon_p2_backhoe[1] = Math.Round(becon_p2_backhoe[1]);
                        c_becon_p2_backhoe[2] = Math.Round(becon_p2_backhoe[2]);
                        c_becon_p2_backhoe[3] = Math.Round(becon_p2_backhoe[3]);

                        const_becon_p2_backhoe = 0;
                        
                        backhoe_data_2 = Calc_backhoe_point(becon_p1_backhoe, becon_p2_backhoe, c_becon_p1_backhoe, c_becon_p2_backhoe);//0으로 들어가서 1로 나온다 그리고 나오는 값은 커트된 거리값(_o) 이다
                        




                        //------------------------------------------------이 부분은 안정화 된후 사잇거리값 비교후 커트 시키는 구간이다(입력 : becon_p12_backhoe)---------------------------------------------
                        
                        if ((P1P2_distance_o <= (const_distance + distance_value_const)) && (P1P2_distance_o >= (const_distance - distance_value_const)))
                        {
                            // 디버그 간소화에 따른 수정된 부분(0905)
                            //Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " true ");//P1P2_distance_o 는 안정화 된후 커트 시키고난후의 사이거리값

                            becon_p1_backhoe_a[0] = becon_p1_backhoe[0];
                            becon_p1_backhoe_a[1] = becon_p1_backhoe[1];
                            becon_p1_backhoe_a[2] = becon_p1_backhoe[2];
                            becon_p1_backhoe_a[3] = becon_p1_backhoe[3];

                            becon_p2_backhoe_a[0] = becon_p2_backhoe[0];
                            becon_p2_backhoe_a[1] = becon_p2_backhoe[1];
                            becon_p2_backhoe_a[2] = becon_p2_backhoe[2];
                            becon_p2_backhoe_a[3] = becon_p2_backhoe[3];
                            
                            const_becon_p_backhoe_a = 1;//초기 값이 저장되었는지 확인 하기위해
                            
                            p12_distance_count++;//사잇거리 true 카운트

                            // 디버그 간소화에 따른 수정된 부분(1214)
                            //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                            // + " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 사잇거리(true) ");
                            //    Debug.WriteLine("                                                                                                                                                                                                                       p12_distance_count " + p12_distance_count);
                            
                            no_distance_1 = 1;//커팅부분으로 적용된다(커팅적용 1, 미적용 2, 초기값 0)
                           
                        }
                        
                        else//false 인 경우
                        {
                            
                            ////------------------------- 이 영역은 false 인 경우 이전에 칼만필터로 넘어간 거리값을 사용한다----------------------------------
                            if (const_becon_p_backhoe_a == 1)//초기값 저장이 된 경우라도 이전 칼만필터값을 사용 하므로 의미없음
                            {
                                // 디버그 간소화에 따른 수정된 부분(0905)
                                //Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");

                                if (no_cut_save == 1)//커팅후 거리값이 저장됨(조건문 적용)
                                {
                                    receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                    receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                    receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                    receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                    receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                    receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                    receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                    receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];


                                    becon_p1_backhoe[0] = receiver_p1_data[0];
                                    becon_p1_backhoe[1] = receiver_p1_data[1];
                                    becon_p1_backhoe[2] = receiver_p1_data[2];
                                    becon_p1_backhoe[3] = receiver_p1_data[3];

                                    becon_p2_backhoe[0] = receiver_p2_data[0];
                                    becon_p2_backhoe[1] = receiver_p2_data[1];
                                    becon_p2_backhoe[2] = receiver_p2_data[2];
                                    becon_p2_backhoe[3] = receiver_p2_data[3];


                                    // 디버그 간소화에 따른 수정된 부분(1214)
                             //       Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                             //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 사잇거리(이전 수신 데이터) ");// + before_id_b1_save + " " + before_id_b2_save + " " + before_id_b3_save);//사잇거리값 비교후 거리값(false) - 거리값에 해당되는 비컨좌표 출력으로 수정  - 알고리즘 꼬이므로 주석

                                    check_calc = 1;//이전 수신 데이터를 사용할 경우 1

                                }

                                else if (no_cut_save == 0)//커팅후 거리값이 없을경우 초기화 단계에서의 중간 거리값을 가져온다(조건문 적용)
                                {

                                    receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                    receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                    receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                    receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                    receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                    receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                    receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                    receiver_p2_data[3] = a_becon_p2_backhoe[3];



                                    becon_p1_backhoe[0] = receiver_p1_data[0];
                                    becon_p1_backhoe[1] = receiver_p1_data[1];
                                    becon_p1_backhoe[2] = receiver_p1_data[2];
                                    becon_p1_backhoe[3] = receiver_p1_data[3];

                                    becon_p2_backhoe[0] = receiver_p2_data[0];
                                    becon_p2_backhoe[1] = receiver_p2_data[1];
                                    becon_p2_backhoe[2] = receiver_p2_data[2];
                                    becon_p2_backhoe[3] = receiver_p2_data[3];


                                    // 디버그 간소화에 따른 수정된 부분(1214)
                             //       Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                             //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 사잇거리(이전 수신 데이터_x) ");//사잇거리값 비교후 거리값(false)

                                }

                                //이 거리값은 커팅 부분에 적용되면 안됨(칼만필터로 보냄) - 이후 다시 0 으로 초기화가 됨
                                no_distance_1 = 2;
                            }





                            else if (const_becon_p_backhoe_a == 0)//이전 값이 저장 되지 않더라도 이전 칼만필터값을 사용 하므로 의미없음
                            {
                                //디버그 간소화에 따른 수정된 부분(0905)
                                //Debug.WriteLine(" P1P2_distance_o : " + Math.Round(P1P2_distance_o) + " false ");

                                if (no_cut_save == 1)//커팅후 거리값이 저장됨(조건문 적용)
                                {
                                    receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                    receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                    receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                    receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                    receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                    receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                    receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                    receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];


                                    becon_p1_backhoe[0] = receiver_p1_data[0];
                                    becon_p1_backhoe[1] = receiver_p1_data[1];
                                    becon_p1_backhoe[2] = receiver_p1_data[2];
                                    becon_p1_backhoe[3] = receiver_p1_data[3];

                                    becon_p2_backhoe[0] = receiver_p2_data[0];
                                    becon_p2_backhoe[1] = receiver_p2_data[1];
                                    becon_p2_backhoe[2] = receiver_p2_data[2];
                                    becon_p2_backhoe[3] = receiver_p2_data[3];

                                    // 디버그 간소화에 따른 수정된 부분(1214)
                             //       Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                             //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 사잇거리(이전 수신 데이터) ");// + before_id_b1_save + " " + before_id_b2_save + " " + before_id_b3_save);//사잇거리값 비교후 거리값(false) - 거리값에 해당되는 비컨좌표 출력으로 수정  - 알고리즘 꼬이므로 주석

                                    check_calc = 1;//이전 수신 데이터를 사용할 경우 1
                                }

                                else if (no_cut_save == 0)//커팅후 거리값이 없을경우 초기화 단계에서의 중간 거리값을 가져온다(조건문 적용)
                                {

                                    receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                    receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                    receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                    receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                    receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                    receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                    receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                    receiver_p2_data[3] = a_becon_p2_backhoe[3];



                                    becon_p1_backhoe[0] = receiver_p1_data[0];
                                    becon_p1_backhoe[1] = receiver_p1_data[1];
                                    becon_p1_backhoe[2] = receiver_p1_data[2];
                                    becon_p1_backhoe[3] = receiver_p1_data[3];

                                    becon_p2_backhoe[0] = receiver_p2_data[0];
                                    becon_p2_backhoe[1] = receiver_p2_data[1];
                                    becon_p2_backhoe[2] = receiver_p2_data[2];
                                    becon_p2_backhoe[3] = receiver_p2_data[3];


                                    // 디버그 간소화에 따른 수정된 부분(1214)
                             //       Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1d: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                             //+ " eP2d: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 사잇거리(이전 수신 데이터_x) ");//사잇거리값 비교후 거리값(false)

                                }

                                //이 거리값은 커팅 부분에 적용되면 안됨(칼만필터로 보냄) - 이후 다시 0 으로 초기화가 됨
                                no_distance_1 = 2;
                                
                            }
                            //---------------------------------------------------------------------------------------------------------------------------------------------------------------

                        }

                    }



                    //@_3
                    //----------------------------------------------------안정화단계에서 커팅하는 부분(입출력 부분 becon_p12_backhoe)-----------------------------------
                    if (no_distance_1 == 1 || no_p12_1 == 1)// "사잇거리값" 과 "좌표이동거리값" 중 하나라도 true 이면 통과
                    {

                        //초기화 부분
                        no_distance_1 = 0;
                        no_p12_1 = 0;

                        //////-----------------------------------------------------------------distance fiter 1 - 로우값끼리 비교-----------------------------------------------
                        constant_number += 1;

                        //현재 수신 데이터
                        if (constant_number == 1)//처음값은 다음값을 필터할때의 기준이 된다 따라서 그대로 넘어감
                        {
                            cut_value_1[0] = becon_p1_backhoe[0];
                            cut_value_1[1] = becon_p1_backhoe[1];
                            cut_value_1[2] = becon_p1_backhoe[2];
                            cut_value_1[3] = becon_p1_backhoe[3];

                            cut_value_1[4] = becon_p2_backhoe[0];
                            cut_value_1[5] = becon_p2_backhoe[1];
                            cut_value_1[6] = becon_p2_backhoe[2];
                            cut_value_1[7] = becon_p2_backhoe[3];


                            // 디버그 간략화로 인한 주석처리 - 수정된 부분(1214)
                            //if (constant_number_2 == 0)
                            //{
                            //    //필터링된 4개 거리
                            //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1c: " + Math.Round(cut_value_1[0]) + "  " + Math.Round(cut_value_1[1]) + "  " + Math.Round(cut_value_1[2]) + "  " + Math.Round(cut_value_1[3])
                            //     + " eP2c: " + Math.Round(cut_value_1[4]) + "  " + Math.Round(cut_value_1[5]) + "  " + Math.Round(cut_value_1[6]) + "  " + Math.Round(cut_value_1[7]) + " 비교입력_(초기값) ");
                            //}

                            //else if (constant_number_2 == 1)
                            //{
                            //    Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1c: " + Math.Round(cut_value_1[0]) + "  " + Math.Round(cut_value_1[1]) + "  " + Math.Round(cut_value_1[2]) + "  " + Math.Round(cut_value_1[3])
                            //          + " eP2c: " + Math.Round(cut_value_1[4]) + "  " + Math.Round(cut_value_1[5]) + "  " + Math.Round(cut_value_1[6]) + "  " + Math.Round(cut_value_1[7]) + " 비교입력_2(이전 값과 비교를 한다.) ");
                            //}


                            //이 부분은 카운터가 1번돌 경우임 이전 수신 데이터인 중간값 평균을 적용 시켜야된다.(초기값 0, 이후 1, 2)
                            if (constant_number_2 == 0 && (cut_value_1[0] > a_becon_p1_backhoe[0] + time2_boundary_constant_up || cut_value_1[0] < a_becon_p1_backhoe[0] + time2_boundary_constant_down))
                            {
                               
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[1] > a_becon_p1_backhoe[1] + time2_boundary_constant_up || cut_value_1[1] < a_becon_p1_backhoe[1] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[2] > a_becon_p1_backhoe[2] + time2_boundary_constant_up || cut_value_1[2] < a_becon_p1_backhoe[2] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[3] > a_becon_p1_backhoe[3] + time2_boundary_constant_up || cut_value_1[3] < a_becon_p1_backhoe[3] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //---------------------------------------------------------------------------------------
                            if (constant_number_2 == 0 && (cut_value_1[4] > a_becon_p2_backhoe[0] + time2_boundary_constant_up || cut_value_1[4] < a_becon_p2_backhoe[0] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            if (constant_number_2 == 0 && (cut_value_1[5] > a_becon_p2_backhoe[1] + time2_boundary_constant_up || cut_value_1[5] < a_becon_p2_backhoe[1] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[6] > a_becon_p2_backhoe[2] + time2_boundary_constant_up || cut_value_1[6] < a_becon_p2_backhoe[2] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 0 && (cut_value_1[7] > a_becon_p2_backhoe[3] + time2_boundary_constant_up || cut_value_1[7] < a_becon_p2_backhoe[3] + time2_boundary_constant_down))
                            {
                                
                                //전체 수정
                                receiver_p1_data[0] = a_becon_p1_backhoe[0];
                                receiver_p1_data[1] = a_becon_p1_backhoe[1];
                                receiver_p1_data[2] = a_becon_p1_backhoe[2];
                                receiver_p1_data[3] = a_becon_p1_backhoe[3];

                                receiver_p2_data[0] = a_becon_p2_backhoe[0];
                                receiver_p2_data[1] = a_becon_p2_backhoe[1];
                                receiver_p2_data[2] = a_becon_p2_backhoe[2];
                                receiver_p2_data[3] = a_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }






                            //이 부분은 카운터가 2번 이상 돌았을경우(1번 돌경우 이전버젼일 경우그냥 넘어감) 적용되기 때문에 카운터가 1번돌 경우 이전 수신 데이터인 중간값 평균을 적용 시켜야된다.
                            //이 부분은 카운터가 2번 이상 돌았을경우 적용된다 따라서 커팅된 값은 저장되어져 있다
                            if (constant_number_2 == 1 && (cut_value_1[0] > cut_value_2[0] + time2_boundary_constant_up || cut_value_1[0] < cut_value_2[0] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[1] > cut_value_2[1] + time2_boundary_constant_up || cut_value_1[1] < cut_value_2[1] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[2] > cut_value_2[2] + time2_boundary_constant_up || cut_value_1[2] < cut_value_2[2] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[3] > cut_value_2[3] + time2_boundary_constant_up || cut_value_1[3] < cut_value_2[3] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //---------------------------------------------------------------------------------------
                            if (constant_number_2 == 1 && (cut_value_1[4] > cut_value_2[4] + time2_boundary_constant_up || cut_value_1[4] < cut_value_2[4] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            if (constant_number_2 == 1 && (cut_value_1[5] > cut_value_2[5] + time2_boundary_constant_up || cut_value_1[5] < cut_value_2[5] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[6] > cut_value_2[6] + time2_boundary_constant_up || cut_value_1[6] < cut_value_2[6] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (constant_number_2 == 1 && (cut_value_1[7] > cut_value_2[7] + time2_boundary_constant_up || cut_value_1[7] < cut_value_2[7] + time2_boundary_constant_down))
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            
                            no_cut_save = 1;//커팅 안정화(한번 들어오면 계속 1인 상태)

                        }

                        //현재 수신 데이터
                        if (constant_number == 2)
                        {
                            cut_value_2[0] = becon_p1_backhoe[0];
                            cut_value_2[1] = becon_p1_backhoe[1];
                            cut_value_2[2] = becon_p1_backhoe[2];
                            cut_value_2[3] = becon_p1_backhoe[3];

                            cut_value_2[4] = becon_p2_backhoe[0];
                            cut_value_2[5] = becon_p2_backhoe[1];
                            cut_value_2[6] = becon_p2_backhoe[2];
                            cut_value_2[7] = becon_p2_backhoe[3];

                            // 디버그 간소화로 인한 - 수정된 부분(1214)
                            //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1c: " + Math.Round(cut_value_2[0]) + "  " + Math.Round(cut_value_2[1]) + "  " + Math.Round(cut_value_2[2]) + "  " + Math.Round(cut_value_2[3])
                            //          + " eP2c: " + Math.Round(cut_value_2[4]) + "  " + Math.Round(cut_value_2[5]) + "  " + Math.Round(cut_value_2[6]) + "  " + Math.Round(cut_value_2[7]) + " 비교입력_1(이전 값과 비교를 한다.) ");


                            if (cut_value_2[0] > cut_value_1[0] + time2_boundary_constant_up || cut_value_2[0] < cut_value_1[0] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[1] > cut_value_1[1] + time2_boundary_constant_up || cut_value_2[1] < cut_value_1[1] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[2] > cut_value_1[2] + time2_boundary_constant_up || cut_value_2[2] < cut_value_1[2] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[3] > cut_value_1[3] + time2_boundary_constant_up || cut_value_2[3] < cut_value_1[3] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }
                            //----------------------------------------------------------------------------
                            if (cut_value_2[4] > cut_value_1[4] + time2_boundary_constant_up || cut_value_2[4] < cut_value_1[4] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[5] > cut_value_1[5] + time2_boundary_constant_up || cut_value_2[5] < cut_value_1[5] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[6] > cut_value_1[6] + time2_boundary_constant_up || cut_value_2[6] < cut_value_1[6] + time2_boundary_constant_down)
                            {
                               
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            if (cut_value_2[7] > cut_value_1[7] + time2_boundary_constant_up || cut_value_2[7] < cut_value_1[7] + time2_boundary_constant_down)
                            {
                                
                                receiver_p1_data[0] = save_cut_becon_p1_backhoe[0];
                                receiver_p1_data[1] = save_cut_becon_p1_backhoe[1];
                                receiver_p1_data[2] = save_cut_becon_p1_backhoe[2];
                                receiver_p1_data[3] = save_cut_becon_p1_backhoe[3];

                                receiver_p2_data[0] = save_cut_becon_p2_backhoe[0];
                                receiver_p2_data[1] = save_cut_becon_p2_backhoe[1];
                                receiver_p2_data[2] = save_cut_becon_p2_backhoe[2];
                                receiver_p2_data[3] = save_cut_becon_p2_backhoe[3];

                                becon_p1_backhoe[0] = receiver_p1_data[0];
                                becon_p1_backhoe[1] = receiver_p1_data[1];
                                becon_p1_backhoe[2] = receiver_p1_data[2];
                                becon_p1_backhoe[3] = receiver_p1_data[3];

                                becon_p2_backhoe[0] = receiver_p2_data[0];
                                becon_p2_backhoe[1] = receiver_p2_data[1];
                                becon_p2_backhoe[2] = receiver_p2_data[2];
                                becon_p2_backhoe[3] = receiver_p2_data[3];

                                //출력값 표시
                                cut_output = 1;
                            }

                            //비교를 하기위한 카운트 초기화
                            constant_number = 0;
                            constant_number_2 = 1;
                            
                        }


                        //커팅된값 저장
                        save_cut_becon_p1_backhoe[0] = becon_p1_backhoe[0];
                        save_cut_becon_p1_backhoe[1] = becon_p1_backhoe[1];
                        save_cut_becon_p1_backhoe[2] = becon_p1_backhoe[2];
                        save_cut_becon_p1_backhoe[3] = becon_p1_backhoe[3];

                        save_cut_becon_p2_backhoe[0] = becon_p2_backhoe[0];
                        save_cut_becon_p2_backhoe[1] = becon_p2_backhoe[1];
                        save_cut_becon_p2_backhoe[2] = becon_p2_backhoe[2];
                        save_cut_becon_p2_backhoe[3] = becon_p2_backhoe[3];

                        //커팅된값 저장되었을때의 비컨 id를 저장시키기 위한 구문 - 추가된 부분(0706)
                        before_d_value = 1;

                        if (cut_output == 0) cut_count++;

                        // 필터링된 4개 거리
                        // 디버그 간소화로 인한 - 수정된 부분(1214)
                        //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1c: " + Math.Round(becon_p1_backhoe[0]) + "  " + Math.Round(becon_p1_backhoe[1]) + "  " + Math.Round(becon_p1_backhoe[2]) + "  " + Math.Round(becon_p1_backhoe[3])
                        //  + " eP2c: " + Math.Round(becon_p2_backhoe[0]) + "  " + Math.Round(becon_p2_backhoe[1]) + "  " + Math.Round(becon_p2_backhoe[2]) + "  " + Math.Round(becon_p2_backhoe[3]) + " 컷팅 비교후 거리값(1 = 이전값, 0 = 통과) " + cut_output);

                        //Debug.WriteLine("                                                                                                                                                                                                                        cut_count " + cut_count);

                         //출력값 최기화
                        cut_output = 0;

                         //커팅이 됨으로 해서 칼만으로 신호를 보내기 위한 변수(칼만 통과시 다시 0 이 된다)
                         cut_signal = 1;
                        

                    }
                    
                }





                //---------------------------------------------------------------------------------------- 이부분은 칼만필터 구간 -----------------------------------------------------------------------------
                
                if (stop_signal == 0 || cut_signal == 1 || no_distance_1 == 2)//칼만필터는 "초기화", "커팅", "사잇거리  false" 일경우만 통과 시킨다.
                {
                    //안정화후 칼만 정상화 확인 조건문(계속 1)
                    if (cut_signal == 1 || no_distance_1 == 2)
                    {
                        k_a_signal = 1; // 필터를 전부 거친후의 값
                    }

                    //커팅 통과 변수 초기화
                    cut_signal = 0;

                    //사잇거리 false 신호 초기화
                    no_distance_1 = 0;

                    

                    //-----------------------------------------------------------------------------------------------------------K.F.
                    

                    //-----------------------------------------------------------------------------prediction
                    

                    x_temp_est_p1b1 = A_value * x_est_last_p1b1;
                    x_temp_est_p1b2 = A_value * x_est_last_p1b2;
                    x_temp_est_p1b3 = A_value * x_est_last_p1b3;
                    x_temp_est_p1b4 = A_value * x_est_last_p1b4;//비컨이 4개로 인한
                                                                //--------
                    x_temp_est_p2b1 = A_value * x_est_last_p2b1;
                    x_temp_est_p2b2 = A_value * x_est_last_p2b2;
                    x_temp_est_p2b3 = A_value * x_est_last_p2b3;
                    x_temp_est_p2b4 = A_value * x_est_last_p2b4;//비컨이 4개로 인한

                    P_temp = A_value * P_last * (1 / A_value) + Q;

                    
                    //#1, 2
                    K = P_temp * (1 / H_value) * (1 / (H_value * P_temp * (1 / H_value) + R));
                    
                    x_est_p1b1 = x_temp_est_p1b1 + K * (becon_p1_backhoe[0] - H_value * x_temp_est_p1b1);
                    
                    x_est_p1b2 = x_temp_est_p1b2 + K * (becon_p1_backhoe[1] - H_value * x_temp_est_p1b2);
                    
                    x_est_p1b3 = x_temp_est_p1b3 + K * (becon_p1_backhoe[2] - H_value * x_temp_est_p1b3);

                    x_est_p1b4 = x_temp_est_p1b4 + K * (becon_p1_backhoe[3] - H_value * x_temp_est_p1b4);//비컨이 4개로 인한

                    //-----------------------------------------------------------------
                    
                    x_est_p2b1 = x_temp_est_p2b1 + K * (becon_p2_backhoe[0] - H_value * x_temp_est_p2b1);
                    
                    x_est_p2b2 = x_temp_est_p2b2 + K * (becon_p2_backhoe[1] - H_value * x_temp_est_p2b2);
                    
                    x_est_p2b3 = x_temp_est_p2b3 + K * (becon_p2_backhoe[2] - H_value * x_temp_est_p2b3);

                    x_est_p2b4 = x_temp_est_p2b4 + K * (becon_p2_backhoe[3] - H_value * x_temp_est_p2b4);//비컨이 4개로 인한

                    P = P_temp * (1 - K * H_value);

                    P_last = P;
                    x_est_last_p1b1 = x_est_p1b1;
                    x_est_last_p1b2 = x_est_p1b2;
                    x_est_last_p1b3 = x_est_p1b3;
                    x_est_last_p1b4 = x_est_p1b4;//비컨이 4개로 인한
                                                 //----------------------

                    x_est_last_p2b1 = x_est_p2b1;
                    x_est_last_p2b2 = x_est_p2b2;
                    x_est_last_p2b3 = x_est_p2b3;
                    x_est_last_p2b4 = x_est_p2b4;//비컨이 4개로 인한 
                    
                    //계산편의상 정수로 표현
                    becon_p1_backhoe[0] = Math.Round(x_est_p1b1);
                    becon_p1_backhoe[1] = Math.Round(x_est_p1b2);
                    becon_p1_backhoe[2] = Math.Round(x_est_p1b3);
                    becon_p1_backhoe[3] = Math.Round(x_est_p1b4);//비컨이 4개로 인한

                    becon_p2_backhoe[0] = Math.Round(x_est_p2b1);
                    becon_p2_backhoe[1] = Math.Round(x_est_p2b2);
                    becon_p2_backhoe[2] = Math.Round(x_est_p2b3);
                    becon_p2_backhoe[3] = Math.Round(x_est_p2b4);//비컨이 4개로 인한 

                    //마지막 필터링된 4개 거리 - 비컨이 4개로 인한

                    // 디버그 간소화에 따른 수정된 부분(1214)
                    //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " eP1k: " + becon_p1_backhoe[0] + "  " + becon_p1_backhoe[1] + "  " + becon_p1_backhoe[2] + "  " + becon_p1_backhoe[3]
                    //     + " eP2k: " + becon_p2_backhoe[0] + "  " + becon_p2_backhoe[1] + "  " + becon_p2_backhoe[2] + "  " + becon_p2_backhoe[3] + " 칼만필터 거리값 ");//칼만필터 통과후 거리값

                    
                    kalman_after = 1;//칼만통과 재확인용

                    backhoe_data = Calc_backhoe_point(becon_p1_backhoe, becon_p2_backhoe, c_becon_p1_backhoe, c_becon_p2_backhoe);//칼만필터 통과한 값만 계산적용을 위한

                    kalman_after = 0;//칼만통과 재확인용

                    //최종거리값 표현을 위한 부분
                    f_becon_p1_backhoe[0] = becon_p1_backhoe[0];
                    f_becon_p1_backhoe[1] = becon_p1_backhoe[1];
                    f_becon_p1_backhoe[2] = becon_p1_backhoe[2];
                    f_becon_p1_backhoe[3] = becon_p1_backhoe[3];
                                                         
                    f_becon_p2_backhoe[0] = becon_p2_backhoe[0];
                    f_becon_p2_backhoe[1] = becon_p2_backhoe[1];
                    f_becon_p2_backhoe[2] = becon_p2_backhoe[2];
                    f_becon_p2_backhoe[3] = becon_p2_backhoe[3];

                }
                Debug.WriteLine(" c: " + debug_count + " Q: " + Q + " 칼만필터와 비교후 근접: " + stop_signal + " 수신기 좌표이동 정상화: " + stabilization_position_cut_save + " 사잇거리 정상화: " + const_becon_p_backhoe_a + " 커팅 정상화: " + no_cut_save + " 안정화된 후 칼만 정상화: " + k_a_signal + " 아두이노:" + arduinoNum);


            }

            //---------------------------------------------------거리값 검사 - 최종 거리값 표현을 위한-----------------------------------------
                ExcavatorData.Receiver.P1.DistanceB1 = Convert.ToString(f_becon_p1_backhoe[0]);
                ExcavatorData.Receiver.P1.DistanceB2 = Convert.ToString(f_becon_p1_backhoe[1]);
                ExcavatorData.Receiver.P1.DistanceB3 = Convert.ToString(f_becon_p1_backhoe[2]);
                ExcavatorData.Receiver.P1.DistanceB4 = Convert.ToString(f_becon_p1_backhoe[3]);//비컨이 4개로 인한
                                                                       
                ExcavatorData.Receiver.P2.DistanceB1 = Convert.ToString(f_becon_p2_backhoe[0]);
                ExcavatorData.Receiver.P2.DistanceB2 = Convert.ToString(f_becon_p2_backhoe[1]);
                ExcavatorData.Receiver.P2.DistanceB3 = Convert.ToString(f_becon_p2_backhoe[2]);
                ExcavatorData.Receiver.P2.DistanceB4 = Convert.ToString(f_becon_p2_backhoe[3]);//비컨이 4개로 인한
            
                /////////////////////////상대 좌표계/////////////////////////

                //비콘1 좌표 - 각도 평균으로 수정
                ExcavatorData.RelativeCoordinate.Beacon1.X = Convert.ToString(backhoe_data[24]);
                ExcavatorData.RelativeCoordinate.Beacon1.Y = Convert.ToString(backhoe_data[25]);
                ExcavatorData.RelativeCoordinate.Beacon1.Z = Convert.ToString(backhoe_data[26]) + " (" + Convert.ToString(backhoe_data[87]) + ")";//비컨과 수신기 사이각으로 인한

                //비콘2 좌표 - 각도 평균으로 수정
                ExcavatorData.RelativeCoordinate.Beacon2.X = Convert.ToString(backhoe_data[27]);
                ExcavatorData.RelativeCoordinate.Beacon2.Y = Convert.ToString(backhoe_data[28]);
                ExcavatorData.RelativeCoordinate.Beacon2.Z = Convert.ToString(backhoe_data[29]) + " (" + Convert.ToString(backhoe_data[88]) + ")";//비컨과 수신기 사이각으로 인한

                //비콘3 좌표 - 각도 평균으로 수정
                ExcavatorData.RelativeCoordinate.Beacon3.X = Convert.ToString(backhoe_data[30]);
                ExcavatorData.RelativeCoordinate.Beacon3.Y = Convert.ToString(backhoe_data[31]);
                ExcavatorData.RelativeCoordinate.Beacon3.Z = Convert.ToString(backhoe_data[32]) + " (" + Convert.ToString(backhoe_data[89]) + ")";//비컨과 수신기 사이각으로 인한

                //비콘4 좌표 - 비컨이 4개로 인한 추가 - 각도 평균으로 수정
                ExcavatorData.RelativeCoordinate.Beacon4.X = Convert.ToString(backhoe_data[84]);
                ExcavatorData.RelativeCoordinate.Beacon4.Y = Convert.ToString(backhoe_data[85]);
                ExcavatorData.RelativeCoordinate.Beacon4.Z = Convert.ToString(backhoe_data[86]) + " (" + Convert.ToString(backhoe_data[90]) + ")";//비컨과 수신기 사이각으로 인한


                //측점1 좌표
                ExcavatorData.RelativeCoordinate.MeasuringPoint1.X = "0";
                ExcavatorData.RelativeCoordinate.MeasuringPoint1.Y = "0";
                ExcavatorData.RelativeCoordinate.MeasuringPoint1.Z = "0";

                //측점2 좌표
                ExcavatorData.RelativeCoordinate.MeasuringPoint2.X = Convert.ToString(backhoe_data[42]);
                ExcavatorData.RelativeCoordinate.MeasuringPoint2.Y = Convert.ToString(backhoe_data[43]);
                ExcavatorData.RelativeCoordinate.MeasuringPoint2.Z = Convert.ToString(backhoe_data[44]);
            
                //중심 좌표(e)
                ExcavatorData.RelativeCoordinate.Center.X = Convert.ToString(backhoe_data[64]);
                ExcavatorData.RelativeCoordinate.Center.Y = Convert.ToString(backhoe_data[65]);
                ExcavatorData.RelativeCoordinate.Center.Z = Convert.ToString(backhoe_data[66]);

                //orientation(e)
                ExcavatorData.Angle.UnityOrientation = Convert.ToString(backhoe_data[67]);

                //힌지 좌표(e)
                ExcavatorData.RelativeCoordinate.Hinge.X = Convert.ToString(backhoe_data[68]);
                ExcavatorData.RelativeCoordinate.Hinge.Y = Convert.ToString(backhoe_data[69]);
                ExcavatorData.RelativeCoordinate.Hinge.Z = Convert.ToString(backhoe_data[70]);

                //boom
                ExcavatorData.RelativeCoordinate.Boom.X = Convert.ToString(backhoe_data[80]);
                ExcavatorData.RelativeCoordinate.Boom.Y = Convert.ToString(backhoe_data[81]);
                ExcavatorData.RelativeCoordinate.Boom.Z = Convert.ToString(backhoe_data[82]);

                //arm
                ExcavatorData.RelativeCoordinate.Arm.X = Convert.ToString(backhoe_data[45]);
                ExcavatorData.RelativeCoordinate.Arm.Y = Convert.ToString(backhoe_data[46]);
                ExcavatorData.RelativeCoordinate.Arm.Z = Convert.ToString(backhoe_data[47]);

                /////////////////////////절대 좌표계(e)/////////////////////////
                

                //중심 좌표
                ExcavatorData.AbsoluteCoordinate.Center.X = Convert.ToString(backhoe_data[61]);
                ExcavatorData.AbsoluteCoordinate.Center.Y = Convert.ToString(backhoe_data[62]);
                ExcavatorData.AbsoluteCoordinate.Center.Z = Convert.ToString(backhoe_data[63]);

                //힌지 좌표
                ExcavatorData.AbsoluteCoordinate.Hinge.X = Convert.ToString(backhoe_data[71]);
                ExcavatorData.AbsoluteCoordinate.Hinge.Y = Convert.ToString(backhoe_data[72]);
                ExcavatorData.AbsoluteCoordinate.Hinge.Z = Convert.ToString(backhoe_data[73]);

                //boom
                ExcavatorData.AbsoluteCoordinate.Boom.X = Convert.ToString(backhoe_data[74]);
                ExcavatorData.AbsoluteCoordinate.Boom.Y = Convert.ToString(backhoe_data[75]);
                ExcavatorData.AbsoluteCoordinate.Boom.Z = Convert.ToString(backhoe_data[76]);

                //arm
                ExcavatorData.AbsoluteCoordinate.Arm.X = Convert.ToString(backhoe_data[77]);
                ExcavatorData.AbsoluteCoordinate.Arm.Y = Convert.ToString(backhoe_data[78]);
                ExcavatorData.AbsoluteCoordinate.Arm.Z = Convert.ToString(backhoe_data[79]);

                //////////////////////////imu/////////////////////////
                //몸체
                ExcavatorData.IMUSensor.Body.X = Convert.ToString(-backhoe_data[49]) + "(" + Convert.ToString(backhoe_data[48]) + ")";
                ExcavatorData.IMUSensor.Body.Y = Convert.ToString(backhoe_data[48]) + "(" + Convert.ToString(backhoe_data[49]) + ")";
                ExcavatorData.IMUSensor.Body.Z = Convert.ToString(backhoe_data[50]);

                //붐
                ExcavatorData.IMUSensor.Boom.X = Convert.ToString(backhoe_data[51]);
                ExcavatorData.IMUSensor.Boom.Y = Convert.ToString(backhoe_data[52]);
                ExcavatorData.IMUSensor.Boom.Z = Convert.ToString(backhoe_data[53]);

                //암
                ExcavatorData.IMUSensor.Arm.X = Convert.ToString(backhoe_data[54]);
                ExcavatorData.IMUSensor.Arm.Y = Convert.ToString(backhoe_data[55]) + "(" + Convert.ToString(state) + ")";//state 설정에 따른
                ExcavatorData.IMUSensor.Arm.Z = Convert.ToString(backhoe_data[56]);

                //버킷
                ExcavatorData.IMUSensor.Bucket.X = Convert.ToString(backhoe_data[57]);
                ExcavatorData.IMUSensor.Bucket.Y = Convert.ToString(backhoe_data[58]);
                ExcavatorData.IMUSensor.Bucket.Z = Convert.ToString("0");

                /////////////////////////////////사이각//////////////////////////////
                ExcavatorData.Angle.BodyBoom = Convert.ToString(backhoe_data[83]);
                ExcavatorData.Angle.BoomArm = Convert.ToString(backhoe_data[59]);
                ExcavatorData.Angle.ArmBucket = Convert.ToString(backhoe_data[60]);

                //////////////////////////////// arduino로 전송용 - 추가된 부분(1214) ///////////////////////////////////
                // P&T 좌표값
                string ptStringX = "B" + Convert.ToString(backhoe_data[91]);
                string ptStringY = "N" + Convert.ToString(backhoe_data[92]);
                string ptStringZ = "M" + Convert.ToString(backhoe_data[93]);

                // 타겟 좌표값
                string targetPointStringX = "J" + Convert.ToString(targetPoint[0]);
                string targetPointStringY = "K" + Convert.ToString(targetPoint[1]);
                string targetPointStringZ = "L" + Convert.ToString(targetPoint[2]);

                // yaw값
                string calc_body_y_o_string = "Y" + Convert.ToString(backhoe_data[94]);
            
                arduinoSend = targetPointStringX + targetPointStringY + targetPointStringZ + ptStringX + ptStringY + ptStringZ + calc_body_y_o_string; // 계속 업데이터로 수정 - 수정된 부분(1224)
                
                // yaw값이 계산 된 경우 보냄, pt_z값은 항상 양수 - 수정된 부분(1221)
                // roll, pitch 저장기능 - 추가된 부분(1226)
                if (arduinoSendCounter == 0 && yawCalcPass == 1 && backhoe_data[93] > 0) {
                    // arduino로 보내는 
                    Debug.WriteLine(">>>>>>> : " + arduinoSend);

                    // 1번 보내기
                    arduinoSendCounter = 1;

                    saveYaw   = backhoe_data[94]; // 요값 저장
                    saveRoll  = backhoe_data[95]; // 롤값 저장
                    savePitch = backhoe_data[96]; // 피치값 저장

                    // 아두이노로 몇번 보낸는지 확인 - 추가된 부분(1223)
                    arduinoNum++;
                }

                if (Math.Abs(backhoe_data[94] - saveYaw) > 5 || Math.Abs(backhoe_data[95] - saveRoll) > 5 || Math.Abs(backhoe_data[96] - savePitch) > 5) {
                    arduinoSendCounter = 0; // 요값, 롤값, 피치값이 차이 많이 나는 경우 다시 전송을 위한 부분임 - 추가된 부분(1222)
                }

                //상대 좌표계(끝단 좌표 값)
                ExcavatorData.RelativePoint.EndPoint = "e_endpoint_1, " + backhoe_data[18] + ", " + backhoe_data[19] + ", " + backhoe_data[20];

                //상대 좌표계(위치 좌표 값)
                ExcavatorData.RelativePoint.Position = "e_position, " + backhoe_data[15] + ", " + backhoe_data[16] + ", " + backhoe_data[17];
                

                //상대 좌표계(수신기 좌표 값)
                if (p_calc_1 == 1 && p_calc_2 == 1)
                {
                    ExcavatorData.RelativePoint.Receiver = "e_receiver_1(c), " + backhoe_data[33] + ", " + backhoe_data[34] + ", " + backhoe_data[35] + ", " + "e_receiver_2(c), " + backhoe_data[36] + ", " + backhoe_data[37] + ", " + backhoe_data[38];
                }

                else if (p_calc_1 == 1 && p_calc_2 == 0)
                {
                    ExcavatorData.RelativePoint.Receiver = "e_receiver_1(c), " + backhoe_data[33] + ", " + backhoe_data[34] + ", " + backhoe_data[35] + ", " + "e_receiver_2, " + backhoe_data[36] + ", " + backhoe_data[37] + ", " + backhoe_data[38];
                }

                else if (p_calc_1 == 0 && p_calc_2 == 1)
                {
                    ExcavatorData.RelativePoint.Receiver = "e_receiver_1, " + backhoe_data[33] + ", " + backhoe_data[34] + ", " + backhoe_data[35] + ", " + "e_receiver_2(c), " + backhoe_data[36] + ", " + backhoe_data[37] + ", " + backhoe_data[38];
                }

                else if (p_calc_1 == 0 && p_calc_2 == 0)
                {
                    ExcavatorData.RelativePoint.Receiver = "e_receiver_1, " + backhoe_data[33] + ", " + backhoe_data[34] + ", " + backhoe_data[35] + ", " + "e_receiver_2, " + backhoe_data[36] + ", " + backhoe_data[37] + ", " + backhoe_data[38];
                }
                
                //절대 좌표계 (끝단 좌표 값)
                ExcavatorData.AbsolutePoint.EndPoint = "e_endpoint_1, " + backhoe_data[9] + ", " + backhoe_data[10] + ", " + backhoe_data[11];

                //절대 좌표계 (위치 좌표 값)
                ExcavatorData.AbsolutePoint.Position = "e_position, " + backhoe_data[6] + ", " + backhoe_data[7] + ", " + backhoe_data[8];

                //절대 좌표계 (수신기 좌표 값)
                ExcavatorData.AbsolutePoint.Receiver = "e_receiver_1, " + backhoe_data[0] + ", " + backhoe_data[1] + ", " + backhoe_data[2] + ", " + "e_receiver_2, " + backhoe_data[3] + ", " + backhoe_data[4] + ", " + backhoe_data[5];

                Debug.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + "\r\n");

            
            
        }


        //---------------------------------------------------------------- 수학 공식 부분-----------------------------------------------------------------

        //==========================상대 좌표계의 삼각 측량 계산부=================================

        double[] r_Calc_tri(double[] d_1, double[] d_2)
        {
            //-------비컨이 4개 사용시 추가---------
            double[] d_1_c = new double[3];
            double[] d_2_c = new double[3];
            
            //큰수
            double p1_dN1_o = 0;
            double p1_dN2 = 0;
            double p1_dN3 = 0;

            //수신기 평균 거리값
            double[] d_12 = new double[4];

            //-----------원본 계산 변수-------------
            double[] result = new double[20];

            double[] becon_1_1 = new double[3];
            double[] becon_2_1 = new double[3];
            double[] becon_3_1 = new double[3];

            double a_1 = 0, b_1 = 0, c_1 = 0, p_1_w_1 = 0, p_2_w_1 = 0;
            double a_2 = 0, b_2 = 0, c_2 = 0, p_1_w_2 = 0, p_2_w_2 = 0;

            double s_A_1 = 0, p_1_s_B_1 = 0, p_2_s_B_1 = 0;
            double s_A_2 = 0, p_1_s_B_2 = 0, p_2_s_B_2 = 0;

            double Aa = 0, p_1_Bb = 0, p_1_Cc = 0, p_2_Bb = 0, p_2_Cc = 0;

            double[] p_1_t_u = new double[3]; double[] p_2_t_u = new double[3];
            double[] p_1_t_d = new double[3]; double[] p_2_t_d = new double[3];
            
            //로우값 계산을 위한
            double[] r_becon_1_m = new double[3];
            double[] r_becon_2_m = new double[3];
            double[] r_becon_3_m = new double[3];

            //배열에서 변수로 변환을 위한
            double d_1_c_0 = 0;
            double d_1_c_1 = 0;
            double d_1_c_2 = 0;

            double d_2_c_0 = 0;
            double d_2_c_1 = 0;
            double d_2_c_2 = 0;




            //----------비컨 각도 적용에 따른 - 시작-------------
            //각도가 가장 높은것과 나머지 비컨의 좌표를 위한 부분
            double beacon_2_calc_p1b0 = 0;
            double beacon_2_calc_p1b1 = 0;
            double beacon_2_calc_p1b2 = 0;
            double beacon_2_calc_p1b3 = 0;

            double beacon_2_calc_p2b0 = 0;
            double beacon_2_calc_p2b1 = 0;
            double beacon_2_calc_p2b2 = 0;
            double beacon_2_calc_p2b3 = 0;

            //평균 거리값 계산을 위한 부분
            double beacon_2_calc_a_b1 = 0;
            double beacon_2_calc_a_b2 = 0;
            double beacon_2_calc_a_b3 = 0;

            //가장 큰 평균 거리값 계산을 위한 부분
            double angle_dN1 = 0;
            double angle_dN2 = 0;

            //비컨 id 확인을 위한 부분
            Int16 b1_id = 0;
            Int16 b2_id = 0;
            Int16 b3_id = 0;
            //-------------------------------------------------------끝-------------



            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 0)
            {

                //---------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘1 = (r_becon_1[0], r_becon_1[1], r_becon_1[2])
                //비콘2 = (r_becon_2[0], r_becon_2[1], r_becon_2[2])
                //비콘3 = (r_becon_3[0], r_becon_3[1], r_becon_3[2])
                //비콘4 = (r_becon_4[0], r_becon_4[1], r_becon_4[2])//비컨이 4개일 경우

                //------------계산에 사용되는 비컨값이 결정된후 비컨값
                //비콘1 = (r_becon_1_c[0], r_becon_1_c[1], r_becon_1_c[2])
                //비콘2 = (r_becon_2_c[0], r_becon_2_c[1], r_becon_2_c[2])
                //비콘3 = (r_becon_3_c[0], r_becon_3_c[1], r_becon_3_c[2])

                //-------------계산에 사용되는 거리값이 결정되기전 거리값
                //----수신기 1------
                //거리1 = d_1[0]
                //거리2 = d_1[1]
                //거리3 = d_1[2]
                //거리4 = d_1[3]//비컨이 4개일 경우

                //----수신기 2------
                //거리1 = d_2[0]
                //거리2 = d_2[1]
                //거리3 = d_2[2]
                //거리4 = d_2[3]//비컨이 4개일 경우

                //-----수신기 평균 거리값
                //거리1 = d_12[0]
                //거리2 = d_12[1]
                //거리3 = d_12[2]
                //거리4 = d_12[3]

                //--------------계산에 사용되는 거리값이 결정된후 거리값
                //----수신기 1------
                //거리1 = d_1_c[0]
                //거리2 = d_1_c[1]
                //거리3 = d_1_c[2]

                //----수신기 2------
                //거리1 = d_2_c[0]
                //거리2 = d_2_c[1]
                //거리3 = d_2_c[2]

                ////******************************************************************결정 코드 부분 비컨이 4개일경우*************************************************************



                //수신기의 평균 거리값 계산
                d_12[0] = (d_1[0] + d_2[0]) / 2;
                d_12[1] = (d_1[1] + d_2[1]) / 2;
                d_12[2] = (d_1[2] + d_2[2]) / 2;
                d_12[3] = (d_1[3] + d_2[3]) / 2;

                //거리값이 같아지는경우는 고려 안함(비컨 번호 가장 작은것 우선)
                //가장긴거리값 결정(dN3이 가장김)
                if (d_12[0] < d_12[1]) p1_dN1_o = d_12[1];
                else p1_dN1_o = d_12[0];

                if (p1_dN1_o < d_12[2]) p1_dN2 = d_12[2];
                else p1_dN2 = p1_dN1_o;

                if (p1_dN2 < d_12[3]) p1_dN3 = d_12[3];
                else p1_dN3 = p1_dN2;
                

                //계산되는 비컨과 거리값 결정
                if (p1_dN3 == d_12[0])
                {
                    d_1_c[0] = d_1[1];
                    d_1_c[1] = d_1[2];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[1];
                    d_2_c[1] = d_2[2];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_2[0];
                    r_becon_1_c[1] = r_becon_2[1];
                    r_becon_1_c[2] = r_becon_2[2];

                    r_becon_2_c[0] = r_becon_3[0];
                    r_becon_2_c[1] = r_becon_3[1];
                    r_becon_2_c[2] = r_becon_3[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    //if ((const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2) && beacon_2_calc == 0) Debug.WriteLine(" B2  B3  B4 ");

                }

                else if (p1_dN3 == d_12[1])
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[2];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[2];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_3[0];
                    r_becon_2_c[1] = r_becon_3[1];
                    r_becon_2_c[2] = r_becon_3[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    //if ((const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2) && beacon_2_calc == 0) Debug.WriteLine(" B1  B3  B4 ");
                }

                else if (p1_dN3 == d_12[2])
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[1];
                    d_1_c[2] = d_1[3];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[1];
                    d_2_c[2] = d_2[3];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_2[0];
                    r_becon_2_c[1] = r_becon_2[1];
                    r_becon_2_c[2] = r_becon_2[2];

                    r_becon_3_c[0] = r_becon_4[0];
                    r_becon_3_c[1] = r_becon_4[1];
                    r_becon_3_c[2] = r_becon_4[2];

                    //if ((const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2) && beacon_2_calc == 0) Debug.WriteLine(" B1  B2  B4 ");
                }

                else
                {
                    d_1_c[0] = d_1[0];
                    d_1_c[1] = d_1[1];
                    d_1_c[2] = d_1[2];

                    d_2_c[0] = d_2[0];
                    d_2_c[1] = d_2[1];
                    d_2_c[2] = d_2[2];

                    r_becon_1_c[0] = r_becon_1[0];
                    r_becon_1_c[1] = r_becon_1[1];
                    r_becon_1_c[2] = r_becon_1[2];

                    r_becon_2_c[0] = r_becon_2[0];
                    r_becon_2_c[1] = r_becon_2[1];
                    r_becon_2_c[2] = r_becon_2[2];

                    r_becon_3_c[0] = r_becon_3[0];
                    r_becon_3_c[1] = r_becon_3[1];
                    r_becon_3_c[2] = r_becon_3[2];

                    //if ((const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2) && beacon_2_calc == 0) Debug.WriteLine(" B1  B2  B3 ");
                }
            }
            

            //-------------비컨 각도 적용에 따른 계산부----------------
            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 1)
            {
                
                //------------------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘0 = (angle_s_b0_x, angle_s_b0_y, angle_s_b0_z)
                //비콘1 = (angle_s_b1_x, angle_s_b1_y, angle_s_b1_z)
                //비콘2 = (angle_s_b2_x, angle_s_b2_y, angle_s_b2_z)
                //비콘3 = (angle_s_b3_x, angle_s_b3_y, angle_s_b3_z)

                //------------계산에 사용되는 비컨값이 결정된후 비컨값
                //비콘1 = (r_becon_1_c[0], r_becon_1_c[1], r_becon_1_c[2])
                //비콘2 = (r_becon_2_c[0], r_becon_2_c[1], r_becon_2_c[2])
                //비콘3 = (r_becon_3_c[0], r_becon_3_c[1], r_becon_3_c[2]) <----- 무조건 비컨0 부분

                ////-------------계산에 사용되는 거리값이 결정되기전 거리값
                //비컨번호를 확인해서 해당되는 거리값 선택(각각 비컨 1, 2, 3, 4 사이의 거리값임)
                //----수신기 1------
                //거리1 = d_1[0]
                //거리2 = d_1[1]
                //거리3 = d_1[2]
                //거리4 = d_1[3]

                //----수신기 2------
                //거리1 = d_2[0]
                //거리2 = d_2[1]
                //거리3 = d_2[2]
                //거리4 = d_2[3]

                //비컨번호(예를 들어서, s_beacon_0 = 1 일 경우 거리1 은 각각 d_1[0], d_2[0] 가 된다.)
                //s_beacon_0
                //s_beacon_1
                //s_beacon_2
                //s_beacon_3

                //--------------계산에 사용되는 거리값이 결정된후 거리값
                //----수신기 1------
                //거리1 = d_1_c[0]
                //거리2 = d_1_c[1]
                //거리3 = d_1_c[2]    <-------- 무조건 비컨0과 수신기1번 사이의 거리

                //----수신기 2------
                //거리1 = d_2_c[0]
                //거리2 = d_2_c[1]
                //거리3 = d_2_c[2]    <-------- 무조건 비컨0과 수신기2번 사이의 거리



                //-------------------------------------거리값 계산 부분--------------------------------------
                //------비컨0에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b0)
                if (s_beacon_0 == 1) beacon_2_calc_p1b0 = d_1[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p1b0 = d_1[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p1b0 = d_1[2];
                else beacon_2_calc_p1b0 = d_1[3];

                //p2(beacon_2_calc_p2b0)
                if (s_beacon_0 == 1) beacon_2_calc_p2b0 = d_2[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p2b0 = d_2[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p2b0 = d_2[2];
                else beacon_2_calc_p2b0 = d_2[3];

                //--------비컨1에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b1)
                if (s_beacon_1 == 1) beacon_2_calc_p1b1 = d_1[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p1b1 = d_1[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p1b1 = d_1[2];
                else beacon_2_calc_p1b1 = d_1[3];

                //p2(beacon_2_calc_p2b1)
                if (s_beacon_1 == 1) beacon_2_calc_p2b1 = d_2[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p2b1 = d_2[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p2b1 = d_2[2];
                else beacon_2_calc_p2b1 = d_2[3];

                //---------비컨2에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b2)
                if (s_beacon_2 == 1) beacon_2_calc_p1b2 = d_1[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p1b2 = d_1[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p1b2 = d_1[2];
                else beacon_2_calc_p1b2 = d_1[3];

                //p2(beacon_2_calc_p2b2)
                if (s_beacon_2 == 1) beacon_2_calc_p2b2 = d_2[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p2b2 = d_2[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p2b2 = d_2[2];
                else beacon_2_calc_p2b2 = d_2[3];

                //---------비컨3에 관련된 거리값 계산 - ID 오류로 인한 수정
                //p1(beacon_2_calc_p1b2)
                if (s_beacon_3 == 1) beacon_2_calc_p1b3 = d_1[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p1b3 = d_1[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p1b3 = d_1[2];
                else beacon_2_calc_p1b3 = d_1[3];

                //p2(beacon_2_calc_p2b2)
                if (s_beacon_3 == 1) beacon_2_calc_p2b3 = d_2[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p2b3 = d_2[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p2b3 = d_2[2];
                else beacon_2_calc_p2b3 = d_2[3];


                //비컨1, 2, 3을 가지고 계산을 해서 결정된 비컨1, 2가되고 비컨0은 결정된 비컨 3이 됨 
                //수신기의 평균 거리값 계산
                beacon_2_calc_a_b1 = (beacon_2_calc_p1b1 + beacon_2_calc_p2b1) / 2;
                beacon_2_calc_a_b2 = (beacon_2_calc_p1b2 + beacon_2_calc_p2b2) / 2;
                beacon_2_calc_a_b3 = (beacon_2_calc_p1b3 + beacon_2_calc_p2b3) / 2;

                //가장 큰 평균 거리값 결정 부분
                if (beacon_2_calc_a_b1 < beacon_2_calc_a_b2) angle_dN1 = beacon_2_calc_a_b2;
                else angle_dN1 = beacon_2_calc_a_b1;

                if (angle_dN1 < beacon_2_calc_a_b3) angle_dN2 = beacon_2_calc_a_b3;
                else angle_dN2 = angle_dN1;


                //비컨 2개의 거리값과 좌표값 결정

                //------거리값 결정
                //p1(d_1_c[0])_b1
                if (angle_dN2 == beacon_2_calc_a_b1) d_1_c[0] = beacon_2_calc_p1b2;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_1_c[0] = beacon_2_calc_p1b1;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_1_c[0] = beacon_2_calc_p1b1;
                else d_1_c[0] = beacon_2_calc_p1b1;

                //p1(d_1_c[1])_b2
                if (angle_dN2 == beacon_2_calc_a_b1) d_1_c[1] = beacon_2_calc_p1b3;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_1_c[1] = beacon_2_calc_p1b3;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_1_c[1] = beacon_2_calc_p1b2;
                else d_1_c[1] = beacon_2_calc_p1b2;

                //p2(d_1_c[2])_b3
                d_1_c[2] = beacon_2_calc_p1b0;

                //p2(d_2_c[0])_b1
                if (angle_dN2 == beacon_2_calc_a_b1) d_2_c[0] = beacon_2_calc_p2b2;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_2_c[0] = beacon_2_calc_p2b1;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_2_c[0] = beacon_2_calc_p2b1;
                else d_2_c[0] = beacon_2_calc_p2b1;

                //p2(d_2_c[1])_b2
                if (angle_dN2 == beacon_2_calc_a_b1) d_2_c[1] = beacon_2_calc_p2b3;
                else if (angle_dN2 == beacon_2_calc_a_b2) d_2_c[1] = beacon_2_calc_p2b3;
                else if (angle_dN2 == beacon_2_calc_a_b3) d_2_c[1] = beacon_2_calc_p2b2;
                else d_2_c[1] = beacon_2_calc_p2b2;

                //p2(d_2_c[2])_b3
                d_2_c[2] = beacon_2_calc_p2b0;



                //-------좌표값 결정
                //----------b1
                //x
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[0] = angle_s_b2_x;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[0] = angle_s_b1_x;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[0] = angle_s_b1_x;
                else r_becon_1_c[0] = angle_s_b1_x;

                //y
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[1] = angle_s_b2_y;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[1] = angle_s_b1_y;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[1] = angle_s_b1_y;
                else r_becon_1_c[1] = angle_s_b1_y;

                //z
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_1_c[2] = angle_s_b2_z;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_1_c[2] = angle_s_b1_z;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_1_c[2] = angle_s_b1_z;
                else r_becon_1_c[2] = angle_s_b1_z;

                //------------b2
                //x
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[0] = angle_s_b3_x;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[0] = angle_s_b3_x;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[0] = angle_s_b2_x;
                else r_becon_2_c[0] = angle_s_b2_x;
                
                //y
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[1] = angle_s_b3_y;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[1] = angle_s_b3_y;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[1] = angle_s_b2_y;
                else r_becon_2_c[1] = angle_s_b2_y;

                //z
                if (angle_dN2 == beacon_2_calc_a_b1) r_becon_2_c[2] = angle_s_b3_z;
                else if (angle_dN2 == beacon_2_calc_a_b2) r_becon_2_c[2] = angle_s_b3_z;
                else if (angle_dN2 == beacon_2_calc_a_b3) r_becon_2_c[2] = angle_s_b2_z;
                else r_becon_2_c[2] = angle_s_b2_z;

                //-----------------b3
                r_becon_3_c[0] = angle_s_b0_x;
                r_becon_3_c[1] = angle_s_b0_y;
                r_becon_3_c[2] = angle_s_b0_z;

                //좌표값 결정에 대한 비컨 id 표현 - 수식 오류로 인한 수정
                //b1에 대한 id
                if (angle_s_b1_x == r_becon_1_c[0] && angle_s_b1_y == r_becon_1_c[1] && angle_s_b1_z == r_becon_1_c[2]) b1_id = s_beacon_1;
                else b1_id = s_beacon_2;

                //b2에 대한 id
                if (angle_s_b2_x == r_becon_2_c[0] && angle_s_b2_y == r_becon_2_c[1] && angle_s_b2_z == r_becon_2_c[2]) b2_id = s_beacon_2;
                else b2_id = s_beacon_3;

                //b3에 대한 id
                b3_id = s_beacon_0;
                
                //처음 값이 들어온경우 값 저장 부분(이 부분은 계산도중 비컨의 좌표가 바뀌는 것을 방지하기 위함이다)
                if (first_const == 1)
                {
                    

                    //좌표값 저장
                    save_b1_x = r_becon_1_c[0];
                    save_b1_y = r_becon_1_c[1];
                    save_b1_z = r_becon_1_c[2];
                    
                    save_b2_x = r_becon_2_c[0];
                    save_b2_y = r_becon_2_c[1];
                    save_b2_z = r_becon_2_c[2];
                    
                    save_b3_x = r_becon_3_c[0];
                    save_b3_y = r_becon_3_c[1];
                    save_b3_z = r_becon_3_c[2];

                    //비컨 아이디 저장
                    save_id_b1 = b1_id;
                    save_id_b2 = b2_id;
                    save_id_b3 = b3_id;
                    
                    if (const_becon_p2_backhoe == 1 || const_becon_p2_backhoe == 2 || const_becon_p2_backhoe == 0)//보기(0 -> 1, 2 -> 1)
                    {
                        //사용된 비컨 출력부분 - 수정된 부분(1217)
                        Debug.WriteLine(save_id_b1 + " x " + r_becon_1_c[0] + " y " + r_becon_1_c[1] + " z " + r_becon_1_c[2]);
                        Debug.WriteLine(save_id_b2 + " x " + r_becon_2_c[0] + " y " + r_becon_2_c[1] + " z " + r_becon_2_c[2]);
                        Debug.WriteLine(save_id_b3 + " x " + r_becon_3_c[0] + " y " + r_becon_3_c[1] + " z " + r_becon_3_c[2]);

                        //사용된 거리값 확인
                        //디버그 간소화에 따른 수정된 부분(1214)
                        //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> 1_1 " + d_1_c[0] + " " + d_1_c[1] + " " + d_1_c[2] + " " + d_2_c[0] + " " + d_2_c[1] + " " + d_2_c[2]);
                    }

                }
                else if(first_const == 0)//처음이 아닐경우
                {
                    
                    //거리값 결정
                    //pb1
                    if (save_id_b1 == 1)
                    {
                        save_p1b1 = d_1[0];
                        save_p2b1 = d_2[0];
                    }
                    else if (save_id_b1 == 2)
                    {
                        save_p1b1 = d_1[1];
                        save_p2b1 = d_2[1];
                    }
                    else if (save_id_b1 == 3)
                    {
                        save_p1b1 = d_1[2];
                        save_p2b1 = d_2[2];
                    }

                    //pb2
                    if (save_id_b2 == 2)
                    {
                        save_p1b2 = d_1[1];
                        save_p2b2 = d_2[1];
                    }
                    else if (save_id_b2 == 3)
                    {
                        save_p1b2 = d_1[2];
                        save_p2b2 = d_2[2];
                    }
                    else if (save_id_b2 == 4)
                    {
                        save_p1b2 = d_1[3];
                        save_p2b2 = d_2[3];
                    }

                    //pb3
                    if (save_id_b3 == 1)
                    {
                        save_p1b3 = d_1[0];
                        save_p2b3 = d_2[0];
                    }
                    else if (save_id_b3 == 2)
                    {
                        save_p1b3 = d_1[1];
                        save_p2b3 = d_2[1];
                    }
                    else if (save_id_b3 == 3)
                    {
                        save_p1b3 = d_1[2];
                        save_p2b3 = d_2[2];
                    }
                    else if (save_id_b3 == 4)
                    {
                        save_p1b3 = d_1[3];
                        save_p2b3 = d_2[3];
                    }

                    //해당 거리값 출력
                    d_1_c[0] = save_p1b1;
                    d_1_c[1] = save_p1b2;
                    d_1_c[2] = save_p1b3;
                              
                    d_2_c[0] = save_p2b1;
                    d_2_c[1] = save_p2b2;
                    d_2_c[2] = save_p2b3;


                    //비컨 저장된값 출력
                    r_becon_1_c[0] = save_b1_x;
                    r_becon_1_c[1] = save_b1_y;
                    r_becon_1_c[2] = save_b1_z;

                    r_becon_2_c[0] = save_b2_x;
                    r_becon_2_c[1] = save_b2_y;
                    r_becon_2_c[2] = save_b2_z;

                    r_becon_3_c[0] = save_b3_x;
                    r_becon_3_c[1] = save_b3_y;
                    r_becon_3_c[2] = save_b3_z;

                    
                }

            }


            ////******************************************************************다운된 거리값 보정*************************************************************
            //----변환 부분 시작 비컨이 4개일 경우------
            //거리
            d_1_c_0 = d_1_c[0];
            d_1_c_1 = d_1_c[1];
            d_1_c_2 = d_1_c[2];
                    
            d_2_c_0 = d_2_c[0];
            d_2_c_1 = d_2_c[1];
            d_2_c_2 = d_2_c[2];

            // 디버그 간소화에 따른 수정된 부분(1214)
            //if (kalman_after == 1) Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>> kalman_after_d " + d_1_c_0 + " " + d_1_c_1 + " " + d_1_c_2 + " " + d_2_c_0 + " " + d_2_c_1 + " " + d_2_c_2);
            
            //비컨 - 로우값 계산으로 인한
            r_becon_1_m[0] = r_becon_1_c[0];
            r_becon_1_m[1] = r_becon_1_c[1];
            r_becon_1_m[2] = r_becon_1_c[2];
                      
            r_becon_2_m[0] = r_becon_2_c[0];
            r_becon_2_m[1] = r_becon_2_c[1];
            r_becon_2_m[2] = r_becon_2_c[2];
                      
            r_becon_3_m[0] = r_becon_3_c[0];
            r_becon_3_m[1] = r_becon_3_c[1];
            r_becon_3_m[2] = r_becon_3_c[2];
            
            //----변환 부분 끝------


            //라즈베리파이 연결로 인한 수정
            //P1일 경우
            d_1_t[0] = d_1_c_0;
            
            d_1_t[1] = d_1_c_1;
            
            d_1_t[2] = d_1_c_2;
            


            //P2일 경우
            d_2_t[0] = d_2_c_0;
            
            d_2_t[1] = d_2_c_1;
            
            d_2_t[2] = d_2_c_2;
            
            //******************************************************************becon의 변형된 위치*************************************************************

            becon_1_1[0] = r_becon_1_m[0] - r_becon_1_m[0];          becon_1_1[1] = r_becon_1_m[1] - r_becon_1_m[1];            becon_1_1[2] = r_becon_1_m[2] - r_becon_1_m[2];//비컨1
            becon_2_1[0] = r_becon_2_m[0] - r_becon_1_m[0];          becon_2_1[1] = r_becon_2_m[1] - r_becon_1_m[1];            becon_2_1[2] = r_becon_2_m[2] - r_becon_1_m[2];//비컨2
            becon_3_1[0] = r_becon_3_m[0] - r_becon_1_m[0];          becon_3_1[1] = r_becon_3_m[1] - r_becon_1_m[1];            becon_3_1[2] = r_becon_3_m[2] - r_becon_1_m[2];//비컨3
            
            //*********************************************************************평면의 방정식*****************************************************************
            a_1 = 2 * becon_2_1[0];      b_1 = 2 * becon_2_1[1];      c_1 = 2 * becon_2_1[2];
            a_2 = 2 * becon_3_1[0];      b_2 = 2 * becon_3_1[1];      c_2 = 2 * becon_3_1[2];
            
            //////////////////P1일 경우///////////////////
            p_1_w_1 = Math.Pow(d_1_t[0], 2) - Math.Pow(d_1_t[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_1_w_2 = Math.Pow(d_1_t[0], 2) - Math.Pow(d_1_t[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);
            
            //////////////////P2일 경우///////////////////
            p_2_w_1 = Math.Pow(d_2_t[0], 2) - Math.Pow(d_2_t[1], 2) + Math.Pow(becon_2_1[0], 2) + Math.Pow(becon_2_1[1], 2) + Math.Pow(becon_2_1[2], 2);
            p_2_w_2 = Math.Pow(d_2_t[0], 2) - Math.Pow(d_2_t[2], 2) + Math.Pow(becon_3_1[0], 2) + Math.Pow(becon_3_1[1], 2) + Math.Pow(becon_3_1[2], 2);

            //*******************************************************************2차 연립방정식******************************************************************
            s_A_1 = (c_1 * a_2 - c_2 * a_1) / (a_1 * b_2 - a_2 * b_1);
            s_A_2 = (c_2 * b_1 - c_1 * b_2) / (a_1 * b_2 - a_2 * b_1);
            
            //////////////////P1일 경우////////////////////
            p_1_s_B_1 = (a_1 * p_1_w_2 - a_2 * p_1_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_1_s_B_2 = (b_2 * p_1_w_1 - b_1 * p_1_w_2) / (a_1 * b_2 - a_2 * b_1);
            
            //////////////////P2일 경우////////////////////
            p_2_s_B_1 = (a_1 * p_2_w_2 - a_2 * p_2_w_1) / (a_1 * b_2 - a_2 * b_1);
            p_2_s_B_2 = (b_2 * p_2_w_1 - b_1 * p_2_w_2) / (a_1 * b_2 - a_2 * b_1);

            //******************************************************************z에 관한 2차 방정식****************************************************************
            Aa = Math.Pow(s_A_2, 2) + Math.Pow(s_A_1, 2) + 1;

            //////////////////P1일 경우////////////////////
            p_1_Bb = s_A_2 * p_1_s_B_2 + s_A_1 * p_1_s_B_1;
            p_1_Cc = Math.Pow(p_1_s_B_2, 2) + Math.Pow(p_1_s_B_1, 2) - Math.Pow(d_1_t[0], 2);
           
            //////////////////P2일 경우////////////////////
            p_2_Bb = s_A_2 * p_2_s_B_2 + s_A_1 * p_2_s_B_1;
            p_2_Cc = Math.Pow(p_2_s_B_2, 2) + Math.Pow(p_2_s_B_1, 2) - Math.Pow(d_2_t[0], 2);

            //******************************************************************becon의 오차 계산*************************************************************
            ////수신기 1번 계산 판별
            
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P1일 경우////////////////////
            p_1_t_u[2] = (-p_1_Bb + Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa; p_1_t_d[2] = (-p_1_Bb - Math.Sqrt(Math.Pow(p_1_Bb, 2) - Aa * p_1_Cc)) / Aa;
            p_1_t_u[0] = s_A_2 * p_1_t_u[2] + p_1_s_B_2; p_1_t_d[0] = s_A_2 * p_1_t_d[2] + p_1_s_B_2;
            p_1_t_u[1] = s_A_1 * p_1_t_u[2] + p_1_s_B_1; p_1_t_d[1] = s_A_1 * p_1_t_d[2] + p_1_s_B_1;
            
            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P1일 경우////////////////////
            p_1_u[0] = p_1_t_u[0] + r_becon_1_m[0]; p_1_u[1] = p_1_t_u[1] + r_becon_1_m[1]; p_1_u[2] = p_1_t_u[2] + r_becon_1_m[2];
            p_1_d[0] = p_1_t_d[0] + r_becon_1_m[0]; p_1_d[1] = p_1_t_d[1] + r_becon_1_m[1]; p_1_d[2] = p_1_t_d[2] + r_becon_1_m[2];
            
            p_calc_1 = 0;

            //************************************************************************값 출력***********************************************************************
            //////////////////P1일 경우////////////////////
            result[0] = p_1_u[0];      result[1] = p_1_u[1];      result[2] = p_1_u[2];
            result[3] = p_1_d[0];      result[4] = p_1_d[1];      result[5] = p_1_d[2];
            
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P2일 경우////////////////////
            p_2_t_u[2] = (-p_2_Bb + Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa; p_2_t_d[2] = (-p_2_Bb - Math.Sqrt(Math.Pow(p_2_Bb, 2) - Aa * p_2_Cc)) / Aa;
            p_2_t_u[0] = s_A_2 * p_2_t_u[2] + p_2_s_B_2; p_2_t_d[0] = s_A_2 * p_2_t_d[2] + p_2_s_B_2;
            p_2_t_u[1] = s_A_1 * p_2_t_u[2] + p_2_s_B_1; p_2_t_d[1] = s_A_1 * p_2_t_d[2] + p_2_s_B_1;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P2일 경우////////////////////
            p_2_u[0] = p_2_t_u[0] + r_becon_1_m[0]; p_2_u[1] = p_2_t_u[1] + r_becon_1_m[1]; p_2_u[2] = p_2_t_u[2] + r_becon_1_m[2];
            p_2_d[0] = p_2_t_d[0] + r_becon_1_m[0]; p_2_d[1] = p_2_t_d[1] + r_becon_1_m[1]; p_2_d[2] = p_2_t_d[2] + r_becon_1_m[2];
            
            p_calc_2 = 0;

            //************************************************************************값 출력***********************************************************************
            //////////////////P2일 경우////////////////////
            result[6] = p_2_u[0];            result[7] = p_2_u[1];                      result[8] = p_2_u[2];
            result[9] = p_2_d[0];            result[10] = p_2_d[1];                     result[11] = p_2_d[2];
            
            return result;
        }

        double[] r_Calc_tri_o(double[] d_1_o, double[] d_2_o)
        {
            //-------비컨이 4개---------
            double[] d_1_c_o = new double[3];
            double[] d_2_c_o = new double[3];

            double[] r_becon_1_c_o = new double[3];
            double[] r_becon_2_c_o = new double[3];
            double[] r_becon_3_c_o = new double[3];

            //큰수
            double p1_dN1_o = 0;
            double p1_dN2_o = 0;
            double p1_dN3_o = 0;

            //수신기 평균 거리값
            double[] d_12_o = new double[4];

            //-----------원본 계산 변수-------------
            double[] result_1 = new double[20];

            double[] becon_1_1_o = new double[3];
            double[] becon_2_1_o = new double[3];
            double[] becon_3_1_o = new double[3];

            double a_1_o = 0, b_1_o = 0, c_1_o = 0, p_1_w_1_o = 0, p_2_w_1_o = 0;
            double a_2_o = 0, b_2_o = 0, c_2_o = 0, p_1_w_2_o = 0, p_2_w_2_o = 0;

            double s_a_1_o = 0, p_1_s_b_1_o = 0, p_2_s_b_1_o = 0;
            double s_a_2_o = 0, p_1_s_b_2_o = 0, p_2_s_b_2_o = 0;

            double Aa_o = 0, p_1_Bb_o = 0, p_1_Cc_o = 0, p_2_Bb_o = 0, p_2_Cc_o = 0;

            double[] p_1_t_u_o = new double[3]; double[] p_2_t_u_o = new double[3];
            double[] p_1_t_d_o = new double[3]; double[] p_2_t_d_o = new double[3];
            
            //로우값 계산
            double[] r_becon_1_m_o = new double[3];
            double[] r_becon_2_m_o = new double[3];
            double[] r_becon_3_m_o = new double[3];

            // 배열에서 변수로 변환
            double d_1_c_o_0 = 0;
            double d_1_c_o_1 = 0;
            double d_1_c_o_2 = 0;
                        
            double d_2_c_o_0 = 0;
            double d_2_c_o_1 = 0;
            double d_2_c_o_2 = 0;


            //----------비컨 각도 적용에 따른 - 시작-------------
            //각도가 가장 높은것과 나머지 비컨의 좌표를 위한 부분
            double beacon_2_calc_p1b0_o = 0;
            double beacon_2_calc_p1b1_o = 0;
            double beacon_2_calc_p1b2_o = 0;
            double beacon_2_calc_p1b3_o = 0;

            double beacon_2_calc_p2b0_o = 0;
            double beacon_2_calc_p2b1_o = 0;
            double beacon_2_calc_p2b2_o = 0;
            double beacon_2_calc_p2b3_o = 0;

            //평균 거리값 계산을 위한 부분
            double beacon_2_calc_a_b1_o = 0;
            double beacon_2_calc_a_b2_o = 0;
            double beacon_2_calc_a_b3_o = 0;

            //가장 큰 평균 거리값 계산을 위한 부분
            double angle_dN1_o = 0;
            double angle_dN2_o = 0;

            //비컨 id 확인을 위한 부분
            Int16 b1_id_o = 0;
            Int16 b2_id_o = 0;
            Int16 b3_id_o = 0;
            //-------------------------------------------------------끝-------------


            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 0)
            {

                //---------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘1 = (r_becon_1[0], r_becon_1[1], r_becon_1[2])
                //비콘2 = (r_becon_2[0], r_becon_2[1], r_becon_2[2])
                //비콘3 = (r_becon_3[0], r_becon_3[1], r_becon_3[2])
                //비콘4 = (r_becon_4[0], r_becon_4[1], r_becon_4[2])//비컨이 4개일 경우

                //------------계산에 사용되는 비컨값이 결정된후 비컨값 비컨이 4개일 경우
                //비콘1 = (r_becon_1_c_o[0], r_becon_1_c_o[1], r_becon_1_c_o[2])
                //비콘2 = (r_becon_2_c_o[0], r_becon_2_c_o[1], r_becon_2_c_o[2])
                //비콘3 = (r_becon_3_c_o[0], r_becon_3_c_o[1], r_becon_3_c_o[2])

                //-------------계산에 사용되는 거리값이 결정되기전 거리값
                //----수신기 1------
                //거리1 = d_1_o[0]
                //거리2 = d_1_o[1]
                //거리3 = d_1_o[2]
                //거리4 = d_1_o[3]//비컨이 4개일 경우

                //----수신기 2------
                //거리1 = d_2_o[0]
                //거리2 = d_2_o[1]
                //거리3 = d_2_o[2]
                //거리4 = d_2_o[3]//비컨이 4개일 경우

                //-----수신기 평균 거리값
                //거리1 = d_12_o[0]
                //거리2 = d_12_o[1]
                //거리3 = d_12_o[2]
                //거리4 = d_12_o[3]

                //--------------계산에 사용되는 거리값이 결정된후 거리값 비컨이 4개일 경우
                //----수신기 1------
                //거리1 = d_1_c_o[0]
                //거리2 = d_1_c_o[1]
                //거리3 = d_1_c_o[2]

                //----수신기 2------
                //거리1 = d_2_c_o[0]
                //거리2 = d_2_c_o[1]
                //거리3 = d_2_c_o[2]

                ////******************************************************************결정 코드 부분 비컨이 4개일경우************************************************************

                //수신기의 평균 거리값 계산
                d_12_o[0] = (d_1_o[0] + d_2_o[0]) / 2;
                d_12_o[1] = (d_1_o[1] + d_2_o[1]) / 2;
                d_12_o[2] = (d_1_o[2] + d_2_o[2]) / 2;
                d_12_o[3] = (d_1_o[3] + d_2_o[3]) / 2;

                //거리값이 같아지는경우는 고려 안함
                //가장긴거리값 결정(dN3이 가장김)
                if (d_12_o[0] < d_12_o[1]) p1_dN1_o = d_12_o[1];
                else p1_dN1_o = d_12_o[0];

                if (p1_dN1_o < d_12_o[2]) p1_dN2_o = d_12_o[2];
                else p1_dN2_o = p1_dN1_o;

                if (p1_dN2_o < d_12_o[3]) p1_dN3_o = d_12_o[3];
                else p1_dN3_o = p1_dN2_o;
                
                //계산되는 비컨과 거리값 결정
                if (p1_dN3_o == d_12_o[0])
                {
                    d_1_c_o[0] = d_1_o[1];
                    d_1_c_o[1] = d_1_o[2];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[1];
                    d_2_c_o[1] = d_2_o[2];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_2[0];
                    r_becon_1_c_o[1] = r_becon_2[1];
                    r_becon_1_c_o[2] = r_becon_2[2];

                    r_becon_2_c_o[0] = r_becon_3[0];
                    r_becon_2_c_o[1] = r_becon_3[1];
                    r_becon_2_c_o[2] = r_becon_3[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

                   // if (const_becon_p2_backhoe == 0 && beacon_2_calc == 0) Debug.WriteLine(" B2_o B3_o B4_o ");//비컨 각도 계산으로 인한 수정
                }

                else if (p1_dN3_o == d_12_o[1])
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[2];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[2];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_3[0];
                    r_becon_2_c_o[1] = r_becon_3[1];
                    r_becon_2_c_o[2] = r_becon_3[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

                  //  if (const_becon_p2_backhoe == 0 && beacon_2_calc == 0) Debug.WriteLine(" B1_o B3_o B4_o ");//비컨 각도 계산으로 인한 수정
                }

                else if (p1_dN3_o == d_12_o[2])
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[1];
                    d_1_c_o[2] = d_1_o[3];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[1];
                    d_2_c_o[2] = d_2_o[3];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_2[0];
                    r_becon_2_c_o[1] = r_becon_2[1];
                    r_becon_2_c_o[2] = r_becon_2[2];

                    r_becon_3_c_o[0] = r_becon_4[0];
                    r_becon_3_c_o[1] = r_becon_4[1];
                    r_becon_3_c_o[2] = r_becon_4[2];

             //       if (const_becon_p2_backhoe == 0 && beacon_2_calc == 0) Debug.WriteLine(" B1_o B2_o B4_o ");//비컨 각도 계산으로 인한 수정
                }

                else
                {
                    d_1_c_o[0] = d_1_o[0];
                    d_1_c_o[1] = d_1_o[1];
                    d_1_c_o[2] = d_1_o[2];

                    d_2_c_o[0] = d_2_o[0];
                    d_2_c_o[1] = d_2_o[1];
                    d_2_c_o[2] = d_2_o[2];

                    r_becon_1_c_o[0] = r_becon_1[0];
                    r_becon_1_c_o[1] = r_becon_1[1];
                    r_becon_1_c_o[2] = r_becon_1[2];

                    r_becon_2_c_o[0] = r_becon_2[0];
                    r_becon_2_c_o[1] = r_becon_2[1];
                    r_becon_2_c_o[2] = r_becon_2[2];

                    r_becon_3_c_o[0] = r_becon_3[0];
                    r_becon_3_c_o[1] = r_becon_3[1];
                    r_becon_3_c_o[2] = r_becon_3[2];

           //         if (const_becon_p2_backhoe == 0 && beacon_2_calc == 0) Debug.WriteLine(" B1_o B2_o B3_o ");//비컨 각도 계산으로 인한 수정
                }

            }




            //-------------비컨 각도 적용에 따른----------------
            //비컨 2개 결정 준비 확인부분
            if (beacon_2_calc == 1)
            {
                //------------------------설명문----------------------
                //------------계산에 사용되는 비컨값이 결정되기전 비컨값
                //비콘0 = (angle_s_b0_x, angle_s_b0_y, angle_s_b0_z)
                //비콘1 = (angle_s_b1_x, angle_s_b1_y, angle_s_b1_z)
                //비콘2 = (angle_s_b2_x, angle_s_b2_y, angle_s_b2_z)
                //비콘3 = (angle_s_b3_x, angle_s_b3_y, angle_s_b3_z)

                //------------계산에 사용되는 비컨값이 결정된후 비컨값
                //비콘1 = (r_becon_1_c_o[0], r_becon_1_c_o[1], r_becon_1_c_o[2])
                //비콘2 = (r_becon_2_c_o[0], r_becon_2_c_o[1], r_becon_2_c_o[2])
                //비콘3 = (r_becon_3_c_o[0], r_becon_3_c_o[1], r_becon_3_c_o[2]) <----- 무조건 비컨0 부분

                ////-------------계산에 사용되는 거리값이 결정되기전 거리값
                //비컨번호를 확인해서 해당되는 거리값 선택(각각 비컨 1, 2, 3, 4 사이의 거리값임)
                //----수신기 1------
                //거리1 = d_1_o[0]
                //거리2 = d_1_o[1]
                //거리3 = d_1_o[2]
                //거리4 = d_1_o[3]

                //----수신기 2------
                //거리1 = d_2_o[0]
                //거리2 = d_2_o[1]
                //거리3 = d_2_o[2]
                //거리4 = d_2_o[3]

                //비컨번호(예를 들어서, s_beacon_0 = 1 일 경우 거리1 은 각각 d_1[0], d_2[0] 가 된다.)
                //s_beacon_0
                //s_beacon_1
                //s_beacon_2
                //s_beacon_3

                //--------------계산에 사용되는 거리값이 결정된후 거리값
                //----수신기 1------
                //거리1 = d_1_c_o[0]
                //거리2 = d_1_c_o[1]
                //거리3 = d_1_c_o[2]    <-------- 무조건 비컨0과 수신기1번 사이의 거리

                //----수신기 2------
                //거리1 = d_2_c_o[0]
                //거리2 = d_2_c_o[1]
                //거리3 = d_2_c_o[2]    <-------- 무조건 비컨0과 수신기2번 사이의 거리



                //-------------------------------------거리값 계산 부분--------------------------------------
                //------비컨0에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b0_o)
                if (s_beacon_0 == 1) beacon_2_calc_p1b0_o = d_1_o[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p1b0_o = d_1_o[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p1b0_o = d_1_o[2];
                else beacon_2_calc_p1b0_o = d_1_o[3];

                //p2(beacon_2_calc_p2b0_o)
                if (s_beacon_0 == 1) beacon_2_calc_p2b0_o = d_2_o[0];
                else if (s_beacon_0 == 2) beacon_2_calc_p2b0_o = d_2_o[1];
                else if (s_beacon_0 == 3) beacon_2_calc_p2b0_o = d_2_o[2];
                else beacon_2_calc_p2b0_o = d_2_o[3];

                //--------비컨1에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b1_o)
                if (s_beacon_1 == 1) beacon_2_calc_p1b1_o = d_1_o[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p1b1_o = d_1_o[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p1b1_o = d_1_o[2];
                else beacon_2_calc_p1b1_o = d_1_o[3];

                //p2(beacon_2_calc_p2b1_o)
                if (s_beacon_1 == 1) beacon_2_calc_p2b1_o = d_2_o[0];
                else if (s_beacon_1 == 2) beacon_2_calc_p2b1_o = d_2_o[1];
                else if (s_beacon_1 == 3) beacon_2_calc_p2b1_o = d_2_o[2];
                else beacon_2_calc_p2b1_o = d_2_o[3];

                //---------비컨2에 관련된 거리값 계산
                //p1(beacon_2_calc_p1b2_o)
                if (s_beacon_2 == 1) beacon_2_calc_p1b2_o = d_1_o[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p1b2_o = d_1_o[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p1b2_o = d_1_o[2];
                else beacon_2_calc_p1b2_o = d_1_o[3];

                //p2(beacon_2_calc_p2b2_o)
                if (s_beacon_2 == 1) beacon_2_calc_p2b2_o = d_2_o[0];
                else if (s_beacon_2 == 2) beacon_2_calc_p2b2_o = d_2_o[1];
                else if (s_beacon_2 == 3) beacon_2_calc_p2b2_o = d_2_o[2];
                else beacon_2_calc_p2b2_o = d_2_o[3];

                //---------비컨3에 관련된 거리값 계산 - ID 오류로 인한 수정
                //p1(beacon_2_calc_p1b2_o)
                if (s_beacon_3 == 1) beacon_2_calc_p1b3_o = d_1_o[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p1b3_o = d_1_o[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p1b3_o = d_1_o[2];
                else beacon_2_calc_p1b3_o = d_1_o[3];

                //p2(beacon_2_calc_p2b2_o)
                if (s_beacon_3 == 1) beacon_2_calc_p2b3_o = d_2_o[0];
                else if (s_beacon_3 == 2) beacon_2_calc_p2b3_o = d_2_o[1];
                else if (s_beacon_3 == 3) beacon_2_calc_p2b3_o = d_2_o[2];
                else beacon_2_calc_p2b3_o = d_2_o[3];


                //비컨1, 2, 3을 가지고 계산을 해서 결정된 비컨1, 2가되고 비컨0은 결정된 비컨 3이 됨 
                //수신기의 평균 거리값 계산
                beacon_2_calc_a_b1_o = (beacon_2_calc_p1b1_o + beacon_2_calc_p2b1_o) / 2;
                beacon_2_calc_a_b2_o = (beacon_2_calc_p1b2_o + beacon_2_calc_p2b2_o) / 2;
                beacon_2_calc_a_b3_o = (beacon_2_calc_p1b3_o + beacon_2_calc_p2b3_o) / 2;

                //가장 큰 평균 거리값 결정 부분
                if (beacon_2_calc_a_b1_o < beacon_2_calc_a_b2_o) angle_dN1_o = beacon_2_calc_a_b2_o;
                else angle_dN1_o = beacon_2_calc_a_b1_o;

                if (angle_dN1_o < beacon_2_calc_a_b3_o) angle_dN2_o = beacon_2_calc_a_b3_o;
                else angle_dN2_o = angle_dN1_o;


                //비컨 2개의 거리값과 좌표값 결정

                //------거리값 결정
                //p1(d_1_c_o[0])_b1
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_1_c_o[0] = beacon_2_calc_p1b2_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_1_c_o[0] = beacon_2_calc_p1b1_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_1_c_o[0] = beacon_2_calc_p1b1_o;
                else d_1_c_o[0] = beacon_2_calc_p1b1_o;

                //p1(d_1_c_o[1])_b2
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_1_c_o[1] = beacon_2_calc_p1b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_1_c_o[1] = beacon_2_calc_p1b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_1_c_o[1] = beacon_2_calc_p1b2_o;
                else d_1_c_o[1] = beacon_2_calc_p1b2_o;

                //p2(d_1_c_o[2])_b3
                d_1_c_o[2] = beacon_2_calc_p1b0_o;

                //p2(d_2_c_o[0])_b1
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_2_c_o[0] = beacon_2_calc_p2b2_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_2_c_o[0] = beacon_2_calc_p2b1_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_2_c_o[0] = beacon_2_calc_p2b1_o;
                else d_2_c_o[0] = beacon_2_calc_p2b1_o;

                //p2(d_2_c_o[1])_b2
                if (angle_dN2_o == beacon_2_calc_a_b1_o) d_2_c_o[1] = beacon_2_calc_p2b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) d_2_c_o[1] = beacon_2_calc_p2b3_o;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) d_2_c_o[1] = beacon_2_calc_p2b2_o;
                else d_2_c_o[1] = beacon_2_calc_p2b2_o;

                //p2(d_2_c_o[2])_b3
                d_2_c_o[2] = beacon_2_calc_p2b0_o;



                //-------좌표값 결정
                //----------b1
                //x
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[0] = angle_s_b2_x;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[0] = angle_s_b1_x;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[0] = angle_s_b1_x;
                else r_becon_1_c_o[0] = angle_s_b1_x;

                //y
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[1] = angle_s_b2_y;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[1] = angle_s_b1_y;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[1] = angle_s_b1_y;
                else r_becon_1_c_o[1] = angle_s_b1_y;

                //z
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_1_c_o[2] = angle_s_b2_z;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_1_c_o[2] = angle_s_b1_z;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_1_c_o[2] = angle_s_b1_z;
                else r_becon_1_c_o[2] = angle_s_b1_z;

                //------------b2
                //x
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[0] = angle_s_b3_x;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[0] = angle_s_b3_x;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[0] = angle_s_b2_x;
                else r_becon_2_c_o[0] = angle_s_b2_x;

                //y
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[1] = angle_s_b3_y;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[1] = angle_s_b3_y;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[1] = angle_s_b2_y;
                else r_becon_2_c_o[1] = angle_s_b2_y;

                //z
                if (angle_dN2_o == beacon_2_calc_a_b1_o) r_becon_2_c_o[2] = angle_s_b3_z;
                else if (angle_dN2_o == beacon_2_calc_a_b2_o) r_becon_2_c_o[2] = angle_s_b3_z;
                else if (angle_dN2_o == beacon_2_calc_a_b3_o) r_becon_2_c_o[2] = angle_s_b2_z;
                else r_becon_2_c_o[2] = angle_s_b2_z;

                //-----------------b3
                r_becon_3_c_o[0] = angle_s_b0_x;
                r_becon_3_c_o[1] = angle_s_b0_y;
                r_becon_3_c_o[2] = angle_s_b0_z;

                //beacon_2_calc = 0;

                //좌표값 결정에 대한 비컨 id 표현 - 수식 오류로 인한 수정
                //b1에 대한 id
                if (angle_s_b1_x == r_becon_1_c_o[0] && angle_s_b1_y == r_becon_1_c_o[1] && angle_s_b1_z == r_becon_1_c_o[2]) b1_id_o = s_beacon_1;
                else b1_id_o = s_beacon_2;

                //b2에 대한 id
                if (angle_s_b2_x == r_becon_2_c_o[0] && angle_s_b2_y == r_becon_2_c_o[1] && angle_s_b2_z == r_becon_2_c_o[2]) b2_id_o = s_beacon_2;
                else b2_id_o = s_beacon_3;

                //b3에 대한 id
                b3_id_o = s_beacon_0;
                
                //처음 값이 들어온경우 값 저장 부분
                if (first_const == 1)
                {


                    //좌표값 저장 - 첫번째 계산과 동일한 값 적용
                    save_b1_x_o = r_becon_1_c[0];
                    save_b1_y_o = r_becon_1_c[1];
                    save_b1_z_o = r_becon_1_c[2];
                                             
                    save_b2_x_o = r_becon_2_c[0];
                    save_b2_y_o = r_becon_2_c[1];
                    save_b2_z_o = r_becon_2_c[2];
                                             
                    save_b3_x_o = r_becon_3_c[0];
                    save_b3_y_o = r_becon_3_c[1];
                    save_b3_z_o = r_becon_3_c[2];

                    //비컨 아이디 저장
                    save_id_b1_o = save_id_b1;
                    save_id_b2_o = save_id_b2;
                    save_id_b3_o = save_id_b3;

                    //해당되는 거리값
                    //pb1
                    if (save_id_b1_o == 1)
                    {
                        save_p1b1_o = d_1_o[0];
                        save_p2b1_o = d_2_o[0];
                    }
                    else if (save_id_b1_o == 2)
                    {
                        save_p1b1_o = d_1_o[1];
                        save_p2b1_o = d_2_o[1];
                    }
                    else if (save_id_b1_o == 3)
                    {
                        save_p1b1_o = d_1_o[2];
                        save_p2b1_o = d_2_o[2];
                    }

                    //pb2
                    if (save_id_b2_o == 2)
                    {
                        save_p1b2_o = d_1_o[1];
                        save_p2b2_o = d_2_o[1];
                    }
                    else if (save_id_b2_o == 3)
                    {
                        save_p1b2_o = d_1_o[2];
                        save_p2b2_o = d_2_o[2];
                    }
                    else if (save_id_b2_o == 4)
                    {
                        save_p1b2_o = d_1_o[3];
                        save_p2b2_o = d_2_o[3];
                    }

                    //pb3
                    if (save_id_b3_o == 1)
                    {
                        save_p1b3_o = d_1_o[0];
                        save_p2b3_o = d_2_o[0];
                    }
                    else if (save_id_b3_o == 2)
                    {
                        save_p1b3_o = d_1_o[1];
                        save_p2b3_o = d_2_o[1];
                    }
                    else if (save_id_b3_o == 3)
                    {
                        save_p1b3_o = d_1_o[2];
                        save_p2b3_o = d_2_o[2];
                    }
                    else if (save_id_b3_o == 4)
                    {
                        save_p1b3_o = d_1_o[3];
                        save_p2b3_o = d_2_o[3];
                    }
                    
                }
                else if (first_const == 0)//처음이 아닐경우
                {
                    
                    //거리값 결정
                    //pb1
                    if (save_id_b1_o == 1)
                    {
                        save_p1b1_o = d_1_o[0];
                        save_p2b1_o = d_2_o[0];
                    }
                    else if (save_id_b1_o == 2)
                    {
                        save_p1b1_o = d_1_o[1];
                        save_p2b1_o = d_2_o[1];
                    }
                    else if (save_id_b1_o == 3)
                    {
                        save_p1b1_o = d_1_o[2];
                        save_p2b1_o = d_2_o[2];
                    }

                    //pb2
                    if (save_id_b2_o == 2)
                    {
                        save_p1b2_o = d_1_o[1];
                        save_p2b2_o = d_2_o[1];
                    }
                    else if (save_id_b2_o == 3)
                    {
                        save_p1b2_o = d_1_o[2];
                        save_p2b2_o = d_2_o[2];
                    }
                    else if (save_id_b2_o == 4)
                    {
                        save_p1b2_o = d_1_o[3];
                        save_p2b2_o = d_2_o[3];
                    }

                    //pb3
                    if (save_id_b3_o == 1)
                    {
                        save_p1b3_o = d_1_o[0];
                        save_p2b3_o = d_2_o[0];
                    }
                    else if (save_id_b3_o == 2)
                    {
                        save_p1b3_o = d_1_o[1];
                        save_p2b3_o = d_2_o[1];
                    }
                    else if (save_id_b3_o == 3)
                    {
                        save_p1b3_o = d_1_o[2];
                        save_p2b3_o = d_2_o[2];
                    }
                    else if (save_id_b3_o == 4)
                    {
                        save_p1b3_o = d_1_o[3];
                        save_p2b3_o = d_2_o[3];
                    }

                    //해당 거리값 출력
                    d_1_c_o_0 = save_p1b1_o;
                    d_1_c_o_1 = save_p1b2_o;
                    d_1_c_o_2 = save_p1b3_o;
                        
                    d_2_c_o_0 = save_p2b1_o;
                    d_2_c_o_1 = save_p2b2_o;
                    d_2_c_o_2 = save_p2b3_o;

                    //비컨 저장된값 출력
                    r_becon_1_c_o[0] = save_b1_x_o;
                    r_becon_1_c_o[1] = save_b1_y_o;
                    r_becon_1_c_o[2] = save_b1_z_o;
                                              
                    r_becon_2_c_o[0] = save_b2_x_o;
                    r_becon_2_c_o[1] = save_b2_y_o;
                    r_becon_2_c_o[2] = save_b2_z_o;
                                              
                    r_becon_3_c_o[0] = save_b3_x_o;
                    r_becon_3_c_o[1] = save_b3_y_o;
                    r_becon_3_c_o[2] = save_b3_z_o;
                    
                }
            }

            ////******************************************************************다운된 거리값 보정*************************************************************
            //----변환 부분 시작 비컨이 4개일 경우------
            //거리
            d_1_c_o_0 = d_1_c_o[0];
            d_1_c_o_1 = d_1_c_o[1];
            d_1_c_o_2 = d_1_c_o[2];
                           
            d_2_c_o_0 = d_2_c_o[0];
            d_2_c_o_1 = d_2_c_o[1];
            d_2_c_o_2 = d_2_c_o[2];

            //비컨 - 로우값 계산
            r_becon_1_m_o[0] = r_becon_1_c_o[0];
            r_becon_1_m_o[1] = r_becon_1_c_o[1];
            r_becon_1_m_o[2] = r_becon_1_c_o[2];

            r_becon_2_m_o[0] = r_becon_2_c_o[0];
            r_becon_2_m_o[1] = r_becon_2_c_o[1];
            r_becon_2_m_o[2] = r_becon_2_c_o[2];

            r_becon_3_m_o[0] = r_becon_3_c_o[0];
            r_becon_3_m_o[1] = r_becon_3_c_o[1];
            r_becon_3_m_o[2] = r_becon_3_c_o[2];
            
            //----변환 부분 끝------



            //라즈베리파이 연결로 인한 수정
            //P1일 경우
            d_1_t_o[0] = d_1_c_o_0;
            
            d_1_t_o[1] = d_1_c_o_1;
            
            d_1_t_o[2] = d_1_c_o_2;

            //P2일 경우
            d_2_t_o[0] = d_2_c_o_0;
            
            d_2_t_o[1] = d_2_c_o_1;
            
            d_2_t_o[2] = d_2_c_o_2;

            //******************************************************************becon의 변형된 위치*************************************************************

            becon_1_1_o[0] = r_becon_1_m_o[0] - r_becon_1_m_o[0];        becon_1_1_o[1] = r_becon_1_m_o[1] - r_becon_1_m_o[1];         becon_1_1_o[2] = r_becon_1_m_o[2] - r_becon_1_m_o[2];//비컨1
            becon_2_1_o[0] = r_becon_2_m_o[0] - r_becon_1_m_o[0];        becon_2_1_o[1] = r_becon_2_m_o[1] - r_becon_1_m_o[1];         becon_2_1_o[2] = r_becon_2_m_o[2] - r_becon_1_m_o[2];//비컨2
            becon_3_1_o[0] = r_becon_3_m_o[0] - r_becon_1_m_o[0];        becon_3_1_o[1] = r_becon_3_m_o[1] - r_becon_1_m_o[1];         becon_3_1_o[2] = r_becon_3_m_o[2] - r_becon_1_m_o[2];//비컨3

            //*********************************************************************평면의 방정식*****************************************************************
            a_1_o = 2 * becon_2_1_o[0]; b_1_o = 2 * becon_2_1_o[1]; c_1_o = 2 * becon_2_1_o[2];
            a_2_o = 2 * becon_3_1_o[0]; b_2_o = 2 * becon_3_1_o[1]; c_2_o = 2 * becon_3_1_o[2];

            //////////////////P1일 경우///////////////////
            p_1_w_1_o = Math.Pow(d_1_t_o[0], 2) - Math.Pow(d_1_t_o[1], 2) + Math.Pow(becon_2_1_o[0], 2) + Math.Pow(becon_2_1_o[1], 2) + Math.Pow(becon_2_1_o[2], 2);
            p_1_w_2_o = Math.Pow(d_1_t_o[0], 2) - Math.Pow(d_1_t_o[2], 2) + Math.Pow(becon_3_1_o[0], 2) + Math.Pow(becon_3_1_o[1], 2) + Math.Pow(becon_3_1_o[2], 2);

            //////////////////P2일 경우///////////////////
            p_2_w_1_o = Math.Pow(d_2_t_o[0], 2) - Math.Pow(d_2_t_o[1], 2) + Math.Pow(becon_2_1_o[0], 2) + Math.Pow(becon_2_1_o[1], 2) + Math.Pow(becon_2_1_o[2], 2);
            p_2_w_2_o = Math.Pow(d_2_t_o[0], 2) - Math.Pow(d_2_t_o[2], 2) + Math.Pow(becon_3_1_o[0], 2) + Math.Pow(becon_3_1_o[1], 2) + Math.Pow(becon_3_1_o[2], 2);

            //*******************************************************************2차 연립방정식******************************************************************
            s_a_1_o = (c_1_o * a_2_o - c_2_o * a_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            s_a_2_o = (c_2_o * b_1_o - c_1_o * b_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //////////////////P1일 경우////////////////////
            p_1_s_b_1_o = (a_1_o * p_1_w_2_o - a_2_o * p_1_w_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            p_1_s_b_2_o = (b_2_o * p_1_w_1_o - b_1_o * p_1_w_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //////////////////P2일 경우////////////////////
            p_2_s_b_1_o = (a_1_o * p_2_w_2_o - a_2_o * p_2_w_1_o) / (a_1_o * b_2_o - a_2_o * b_1_o);
            p_2_s_b_2_o = (b_2_o * p_2_w_1_o - b_1_o * p_2_w_2_o) / (a_1_o * b_2_o - a_2_o * b_1_o);

            //******************************************************************z에 관한 2차 방정식****************************************************************
            Aa_o = Math.Pow(s_a_2_o, 2) + Math.Pow(s_a_1_o, 2) + 1;

            //////////////////P1일 경우////////////////////
            p_1_Bb_o = s_a_2_o * p_1_s_b_2_o + s_a_1_o * p_1_s_b_1_o;
            p_1_Cc_o = Math.Pow(p_1_s_b_2_o, 2) + Math.Pow(p_1_s_b_1_o, 2) - Math.Pow(d_1_t_o[0], 2);

            //////////////////P2일 경우////////////////////
            p_2_Bb_o = s_a_2_o * p_2_s_b_2_o + s_a_1_o * p_2_s_b_1_o;
            p_2_Cc_o = Math.Pow(p_2_s_b_2_o, 2) + Math.Pow(p_2_s_b_1_o, 2) - Math.Pow(d_2_t_o[0], 2);
            
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P1일 경우////////////////////
            p_1_t_u_o[2] = (-p_1_Bb_o + Math.Sqrt(Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o)) / Aa_o; p_1_t_d_o[2] = (-p_1_Bb_o - Math.Sqrt(Math.Pow(p_1_Bb_o, 2) - Aa_o * p_1_Cc_o)) / Aa_o;
            p_1_t_u_o[0] = s_a_2_o * p_1_t_u_o[2] + p_1_s_b_2_o; p_1_t_d_o[0] = s_a_2_o * p_1_t_d_o[2] + p_1_s_b_2_o;
            p_1_t_u_o[1] = s_a_1_o * p_1_t_u_o[2] + p_1_s_b_1_o; p_1_t_d_o[1] = s_a_1_o * p_1_t_d_o[2] + p_1_s_b_1_o;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P1일 경우////////////////////
            p_1_u_o[0] = p_1_t_u_o[0] + r_becon_1_m_o[0];          p_1_u_o[1] = p_1_t_u_o[1] + r_becon_1_m_o[1];          p_1_u_o[2] = p_1_t_u_o[2] + r_becon_1_m_o[2];
            p_1_d_o[0] = p_1_t_d_o[0] + r_becon_1_m_o[0];          p_1_d_o[1] = p_1_t_d_o[1] + r_becon_1_m_o[1];          p_1_d_o[2] = p_1_t_d_o[2] + r_becon_1_m_o[2];
            
            //************************************************************************값 출력***********************************************************************
            //////////////////P1일 경우////////////////////
            result_1[0] = p_1_u_o[0]; result_1[1] = p_1_u_o[1]; result_1[2] = p_1_u_o[2];
            result_1[3] = p_1_d_o[0]; result_1[4] = p_1_d_o[1]; result_1[5] = p_1_d_o[2];
           
            //****************************************************************변형된 p_1 좌표인 p_1_t*************************************************************
            //////////////////P2일 경우////////////////////
            p_2_t_u_o[2] = (-p_2_Bb_o + Math.Sqrt(Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o)) / Aa_o; p_2_t_d_o[2] = (-p_2_Bb_o - Math.Sqrt(Math.Pow(p_2_Bb_o, 2) - Aa_o * p_2_Cc_o)) / Aa_o;
            p_2_t_u_o[0] = s_a_2_o * p_2_t_u_o[2] + p_2_s_b_2_o; p_2_t_d_o[0] = s_a_2_o * p_2_t_d_o[2] + p_2_s_b_2_o;
            p_2_t_u_o[1] = s_a_1_o * p_2_t_u_o[2] + p_2_s_b_1_o; p_2_t_d_o[1] = s_a_1_o * p_2_t_d_o[2] + p_2_s_b_1_o;

            //*********************************************************************원래 p_1 좌표*******************************************************************
            //////////////////P2일 경우////////////////////
            p_2_u_o[0] = p_2_t_u_o[0] + r_becon_1_m_o[0]; p_2_u_o[1] = p_2_t_u_o[1] + r_becon_1_m_o[1]; p_2_u_o[2] = p_2_t_u_o[2] + r_becon_1_m_o[2];
            p_2_d_o[0] = p_2_t_d_o[0] + r_becon_1_m_o[0]; p_2_d_o[1] = p_2_t_d_o[1] + r_becon_1_m_o[1]; p_2_d_o[2] = p_2_t_d_o[2] + r_becon_1_m_o[2];
            
            //************************************************************************값 출력***********************************************************************
            //////////////////P2일 경우////////////////////
            result_1[6] = p_2_u_o[0]; result_1[7] = p_2_u_o[1]; result_1[8] = p_2_u_o[2];
            result_1[9] = p_2_d_o[0]; result_1[10] = p_2_d_o[1]; result_1[11] = p_2_d_o[2];
            
            return result_1;
        }

        double[] Calc_backhoe_point(double[] beacon_p1, double[] beacon_p2, double[] c_beacon_p1, double[] c_beacon_p2)// - 로우값으로 계산된 수신기 좌표를 구하기 위한
        {
            double[] result = new double[100];

            

            //length
            double[] pbb_length = new double[3];
            double[] pbpc_length = new double[3];
            double[] pcp4_length = new double[3];
            double[] boom_length = new double[3];
            double[] arm_length = new double[3];
            double[] buckit_length = new double[3];

            double[] pt_length = new double[3]; // p3와 pt사이의 xyz축상 길이 - 추가된 부분(1214)

            //절대 좌표계
            double[] r_r2_abs_receiving_set = new double[6];//수신기(상대 -> 절대)

            double[] abs_e = new double[3];
            double[] abs_p = new double[3];
            double[] abs_pc = new double[3];//pc
            double[] abs_p3 = new double[3];//p3
            double[] abs_boom = new double[3];//boom
            double[] abs_arm = new double[3];//arm

            //위치 좌표계
            double[] p3_p = new double[3];
            double[] pc_p = new double[3];
            double[] pbb_p = new double[3];

            //힌지 좌표계
            double[] boom_p_h = new double[3];
            double[] arm_p_h = new double[3];
            double[] buckit_p_h = new double[3];

            double[] boom_p_h_t = new double[3];//-90도 회전된 좌표
            double[] arm_p_h_t = new double[3];//-90도 회전된 좌표
            double[] buckit_p_h_t = new double[3];//-90도 회전된 좌표

            //임의 좌표계
            double[] r2_becon_1 = new double[3];//비콘1
            double[] r2_becon_2 = new double[3];//비콘2
            double[] r2_becon_3 = new double[3];//비콘3
            double[] r2_becon_4 = new double[3];//비콘4  - 비컨이 4개일경우

            double[] r2_instrument_station_2 = new double[3];//측점2
            double[] r2_instrument_station_3 = new double[3];//측점3(e)

            double b_angle_2 = 0;
            double[] r2_buckit_p = new double[3];//끝단1
            double[] r2_pb = new double[3];//위치
            double[] r_r2_receiving_set = new double[6];//수신기
            double[] r2_pc = new double[3];//중심
            double[] r2_p3 = new double[3];//힌지
            double[] r2_boom = new double[3];//boom
            double[] r2_arm = new double[3];//arm



            //-----상대 좌표계
            
            double[] pc = new double[3];
            double[] p3 = new double[3];
            double[] buckit_p = new double[3];
            double[] arm_p = new double[3];
            double[] boom_p = new double[3];

            double[] pt = new double[3]; // P&T의 바닥 좌표값 - 추가된 부분(1214)

            double a_angle = 0, b_angle = 0;

            //비컨이 4개로 인한
            double[] r_becon_1_v = new double[3];
            double[] r_becon_2_v = new double[3];
            double[] r_becon_3_v = new double[3];
            double[] r_becon_4_v = new double[3];

            //-------bucket
            double ec_angle_cos;//버킷각도 수식 수정에 따른
            double bucket_angle_ab;
            double bucket_angle_ac_deg;
            double bucket_angle_ec_deg;
            double bucket_c_length;
            double bucket_angle_calc;
            double bucket_imu_calc;
            double bucket_a_angle;
            


            

            double[] move_p1 = new double[3];
            double p1_atan = 0;
            double p1_calibration = 0;

            //-------orientation
            double orientation = 0;

            //--------수신기 출력1
            double[] find_p1 = new double[3];
            double[] find_p2 = new double[3];

            //---------수신기 출력2
            double[] find_p1_2 = new double[3];
            double[] find_p2_2 = new double[3];

            //----------사이각
            double[] hinge_t_p = new double[3];
            double[] boom_t_p = new double[3];
            double[] arm_t_p = new double[3];

            double body_boom_angle = 0;
            double boom_arm_angle = 0;
            double arm_bucket_angle = 0;

            //------------boom_arm
            double a_length = 0, b_length = 0;
            double[] boom_arm_length = new double[3];

            //-------------------yaw값 보정------------------
            double body_y_1 = 0;

            //boom 부분
            double multy_a = 0;
            double boom_y_1 = 0;
            double boom_y_2 = 0;
            double r_boom_a = 0;
            double y_boom_a = 0;
            double boom_yaw = 0;

            //arm 부분
            double multy_b = 0;
            double arm_y_1 = 0;
            double arm_y_2 = 0;
            double r_arm_a = 0;
            double y_arm_a = 0;
            double arm_yaw = 0;

            //------------------------------------------------

            string real_time = System.DateTime.Now.ToString("HH:mm:ss");

            //---------각도 추가에 따른-----------
            //수평거리 부분
            double p1b1_flat_distance = 0;
            double p1b2_flat_distance = 0;
            double p1b3_flat_distance = 0;
            double p1b4_flat_distance = 0;

            double p2b1_flat_distance = 0;
            double p2b2_flat_distance = 0;
            double p2b3_flat_distance = 0;
            double p2b4_flat_distance = 0;

            //각도 부분
            double p1b1_angle = 0;
            double p1b2_angle = 0;
            double p1b3_angle = 0;
            double p1b4_angle = 0;

            double p2b1_angle = 0;
            double p2b2_angle = 0;
            double p2b3_angle = 0;
            double p2b4_angle = 0;

            //각도 평균화
            double b1_angle = 0;
            double b2_angle = 0;
            double b3_angle = 0;
            double b4_angle = 0;

            //가장 큰 각도 계산
            double a_angle_N1 = 0;
            double a_angle_N2 = 0;
            double a_angle_N3 = 0;
            

            //아크코사인의 정의역이 범위를 벗어나는경우
            double p1b1_angle_value = 0;
            double p1b2_angle_value = 0;
            double p1b3_angle_value = 0;
            double p1b4_angle_value = 0;

            double p2b1_angle_value = 0;
            double p2b2_angle_value = 0;
            double p2b3_angle_value = 0;
            double p2b4_angle_value = 0;




            //**********************************************************************각도 부분(압축 e)*************************************************************************

            //--------------------------------- 실제 imu센서 -------------------------------------------
            excavator_body_imu_a[0] = Convert.ToDouble(ExcavatorData.Motion.Substring(10, 4)) / 10 - 200; excavator_boom_imu_a[0] = Convert.ToDouble(ExcavatorData.Motion.Substring(22, 4)) / 10 - 200; excavator_arm_imu_a[0] = Convert.ToDouble(ExcavatorData.Motion.Substring(34, 4)) / 10 - 200; excavator_bucket_imu_a[0] = Convert.ToDouble(ExcavatorData.Motion.Substring(46, 4)) / 10 - 200;
            excavator_body_imu_a[1] = Convert.ToDouble(ExcavatorData.Motion.Substring(6, 4)) / 10 - 200;  excavator_boom_imu_a[1] = Convert.ToDouble(ExcavatorData.Motion.Substring(18, 4)) / 10 - 200; excavator_arm_imu_a[1] = Convert.ToDouble(ExcavatorData.Motion.Substring(30, 4)) / 10 - 200; excavator_bucket_imu_a[1] = Convert.ToDouble(ExcavatorData.Motion.Substring(42, 4)) / 10 - 200;

            //출력
            ExcavatorData.Original.IMUSensor = ExcavatorData.Motion.Substring(10, 4) + " " + ExcavatorData.Motion.Substring(6, 4) + " " + ExcavatorData.Motion.Substring(22, 4) + " "
                                + ExcavatorData.Motion.Substring(18, 4) + " " + ExcavatorData.Motion.Substring(34, 4) + " " + ExcavatorData.Motion.Substring(30, 4) + " " + ExcavatorData.Motion.Substring(46, 4) + " " + ExcavatorData.Motion.Substring(42, 4);

            ////----------------------------------튀는값 검출 안할때
            excavator_body_imu[0] = excavator_body_imu_a[0];
            excavator_body_imu[1] = excavator_body_imu_a[1];

            excavator_boom_imu[0] = excavator_boom_imu_a[0];
            excavator_boom_imu[1] = excavator_boom_imu_a[1];

            excavator_arm_imu[0] = excavator_arm_imu_a[0];
            excavator_arm_imu[1] = excavator_arm_imu_a[1];

            excavator_bucket_imu[0] = excavator_bucket_imu_a[0];
            excavator_bucket_imu[1] = excavator_bucket_imu_a[1];
            
            //e_imu_calibration 
            excavator_body_imu[0] += e_body_imu_calibration[0];
            excavator_body_imu[1] += e_body_imu_calibration[1];

            excavator_boom_imu[0] += e_boom_imu_calibration[0];
            excavator_boom_imu[1] += e_boom_imu_calibration[1];

            excavator_arm_imu[0] += e_arm_imu_calibration[0];
            excavator_arm_imu[1] += e_arm_imu_calibration[1];

            excavator_bucket_imu[0] += e_bucket_imu_calibration[0];
            excavator_bucket_imu[1] += e_bucket_imu_calibration[1];
            
            excavator_arm_imu_s[1] = excavator_arm_imu[1];

            // imu 센서값 강제입력 - 추가된 부분(1214) - ##1
            //excavator_body_imu[0] = 0;
            //excavator_body_imu[1] = 0;

            // 각도 변경(deg to rad)
            excavator_body_imu[0] *= (Math.PI / 180.0);     excavator_boom_imu[0] *= (Math.PI / 180.0);      excavator_bucket_imu[0] *= (Math.PI / 180.0);
            excavator_body_imu[1] *= (Math.PI / 180.0);     excavator_boom_imu[1] *= (Math.PI / 180.0);      excavator_arm_imu[1] *= (Math.PI / 180.0);             excavator_bucket_imu[1] *= (Math.PI / 180.0);
            
            bucket_angle_ab = (excavator_bucket_imu[1] * (180.0 / Math.PI) - excavator_arm_imu_s[1]) * (Math.PI / 180.0);//deg to rad
            
            bucket_c_length = Math.Sqrt(Math.Pow(excavator_bucket_constant[0] * Math.Sin(bucket_angle_ab), 2) + Math.Pow(excavator_bucket_constant[1] - excavator_bucket_constant[0] * Math.Cos(bucket_angle_ab), 2));
            bucket_angle_ac_deg = Math.Acos((Math.Pow(bucket_c_length, 2) + Math.Pow(excavator_bucket_constant[0], 2) - Math.Pow(excavator_bucket_constant[1], 2)) / (2 * excavator_bucket_constant[0] * bucket_c_length)) * (180.0 / Math.PI);

            //버킷 계산식 수정에 따른
            ec_angle_cos = (Math.Pow(bucket_c_length, 2) + Math.Pow(excavator_bucket_constant[3], 2) - Math.Pow(excavator_bucket_constant[2], 2)) / (2 * excavator_bucket_constant[3] * bucket_c_length);
            if (ec_angle_cos > 1) ec_angle_cos = 2 - ec_angle_cos;

            bucket_angle_ec_deg = Math.Acos(ec_angle_cos) * (180.0 / Math.PI);//rad to deg
            bucket_angle_calc = 180 - (bucket_angle_ac_deg + bucket_angle_ec_deg);

            bucket_imu_calc = (excavator_arm_imu[1] * (180.0 / Math.PI) + bucket_angle_calc - excavator_bucket_angle) * (Math.PI / 180.0);

            bucket_a_angle = (bucket_angle_ac_deg + bucket_angle_ec_deg + excavator_bucket_angle - 180) * -(Math.PI / 180.0);


            //**********************************************************************좌표 변환(절대 -> 상대)*************************************************************************
            /////////////////////////////////////////////////////////////////////////절대->임의 좌표계///////////////////////////////////////////////////////////////////
            //측점2
            r2_instrument_station_2[0] = instrument_station_2[0] - instrument_station_1[0];
            r2_instrument_station_2[1] = instrument_station_2[1] - instrument_station_1[1];
            r2_instrument_station_2[2] = instrument_station_2[2] - instrument_station_1[2];

            // 측점3
            r2_instrument_station_3[0] = instrument_station_3[0] - instrument_station_1[0];
            r2_instrument_station_3[1] = instrument_station_3[1] - instrument_station_1[1];
            r2_instrument_station_3[2] = instrument_station_3[2] - instrument_station_1[2];

            //비콘1
            r2_becon_1[0] = becon_1[0] - instrument_station_1[0];
            r2_becon_1[1] = becon_1[1] - instrument_station_1[1];
            r2_becon_1[2] = becon_1[2] - instrument_station_1[2];

            //비콘2
            r2_becon_2[0] = becon_2[0] - instrument_station_1[0];
            r2_becon_2[1] = becon_2[1] - instrument_station_1[1];
            r2_becon_2[2] = becon_2[2] - instrument_station_1[2];

            //비콘3
            r2_becon_3[0] = becon_3[0] - instrument_station_1[0];
            r2_becon_3[1] = becon_3[1] - instrument_station_1[1];
            r2_becon_3[2] = becon_3[2] - instrument_station_1[2];

            //비콘4  - 비컨이 4개일경우
            r2_becon_4[0] = becon_4[0] - instrument_station_1[0];
            r2_becon_4[1] = becon_4[1] - instrument_station_1[1];
            r2_becon_4[2] = becon_4[2] - instrument_station_1[2];

            /////////////////////////////////////////////////////////////////////////임의 -> 상대 좌표계///////////////////////////////////////////////////////////////////
            a_angle = Math.Atan(r2_instrument_station_2[1] / r2_instrument_station_2[0]) * 180.0 / Math.PI;
            b_angle = ((r2_instrument_station_2[0] < 0) ? 180 + a_angle : ((r2_instrument_station_2[0] > 0) ? ((r2_instrument_station_2[1] >= 0) ? a_angle : 360 + a_angle) : ((r2_instrument_station_2[1] > 0) ? 90 : 270))) * Math.PI / 180.0;
            
            //비콘1
            r_becon_1[0] = r2_becon_1[0] * Math.Cos(b_angle) + r2_becon_1[1] * Math.Sin(b_angle);
            r_becon_1[1] = -r2_becon_1[0] * Math.Sin(b_angle) + r2_becon_1[1] * Math.Cos(b_angle);
            r_becon_1[2] = r2_becon_1[2];

            //비콘2
            r_becon_2[0] = r2_becon_2[0] * Math.Cos(b_angle) + r2_becon_2[1] * Math.Sin(b_angle);
            r_becon_2[1] = -r2_becon_2[0] * Math.Sin(b_angle) + r2_becon_2[1] * Math.Cos(b_angle);
            r_becon_2[2] = r2_becon_2[2];

            //비콘3
            r_becon_3[0] = r2_becon_3[0] * Math.Cos(b_angle) + r2_becon_3[1] * Math.Sin(b_angle);
            r_becon_3[1] = -r2_becon_3[0] * Math.Sin(b_angle) + r2_becon_3[1] * Math.Cos(b_angle);
            r_becon_3[2] = r2_becon_3[2];

            //비콘4 - 비컨이 4개일경우
            r_becon_4[0] =  r2_becon_4[0] * Math.Cos(b_angle) + r2_becon_4[1] * Math.Sin(b_angle);
            r_becon_4[1] = -r2_becon_4[0] * Math.Sin(b_angle) + r2_becon_4[1] * Math.Cos(b_angle);
            r_becon_4[2] =  r2_becon_4[2];

            //비컨의 좌표 UI상에 표시 - 비컨이 4개일경우
            r_becon_1_v[0] = r_becon_1[0];
            r_becon_1_v[1] = r_becon_1[1];
            r_becon_1_v[2] = r_becon_1[2];

            r_becon_2_v[0] = r_becon_2[0];
            r_becon_2_v[1] = r_becon_2[1];          
            r_becon_2_v[2] = r_becon_2[2];

            r_becon_3_v[0] = r_becon_3[0];
            r_becon_3_v[1] = r_becon_3[1];
            r_becon_3_v[2] = r_becon_3[2];

            r_becon_4_v[0] = r_becon_4[0];
            r_becon_4_v[1] = r_becon_4[1];
            r_becon_4_v[2] = r_becon_4[2];
            
            //측점2
            r_instrument_station_2[0] = r2_instrument_station_2[0] * Math.Cos(b_angle) + r2_instrument_station_2[1] * Math.Sin(b_angle);
            r_instrument_station_2[1] = -r2_instrument_station_2[0] * Math.Sin(b_angle) + r2_instrument_station_2[1] * Math.Cos(b_angle);
            r_instrument_station_2[2] = r2_instrument_station_2[2];

            //측점3
            r_instrument_station_3[0] = r2_instrument_station_3[0] * Math.Cos(b_angle) + r2_instrument_station_3[1] * Math.Sin(b_angle);
            r_instrument_station_3[1] = -r2_instrument_station_3[0] * Math.Sin(b_angle) + r2_instrument_station_3[1] * Math.Cos(b_angle);
            r_instrument_station_3[2] = r2_instrument_station_3[2];

            //**********************************************************************임의 -> 상대 좌표계 삼각 측량 출력부********************************************************************

            

            //비콘을 상대좌표로 변환해서 수신기 구함
            r_backhoe_tri_point = r_Calc_tri(beacon_p1, beacon_p2);
            
            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 위에 있을때)////////////////////
            ////수신기1
            //r_receiving_set_k[0] = r_backhoe_tri_point[0];
            //r_receiving_set_k[1] = r_backhoe_tri_point[1];
            //r_receiving_set_k[2] = r_backhoe_tri_point[2];

            ////수신기2      
            //r_receiving_set_k[3] = r_backhoe_tri_point[6];
            //r_receiving_set_k[4] = r_backhoe_tri_point[7];
            //r_receiving_set_k[5] = r_backhoe_tri_point[8];

            ////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 밑에 있을때)////////////////////
            //수신기1
            r_receiving_set_k[0] =  r_backhoe_tri_point[3];
            r_receiving_set_k[1] =  r_backhoe_tri_point[4];
            r_receiving_set_k[2] =  r_backhoe_tri_point[5];

            //수신기2              
            r_receiving_set_k[3] =  r_backhoe_tri_point[9];
            r_receiving_set_k[4] =  r_backhoe_tri_point[10];
            r_receiving_set_k[5] =  r_backhoe_tri_point[11];

            //-------------- 상대좌표계에서 수신기 좌표 강제입력 ----------------------
            ////수신기1
            //r_receiving_set_k[0] = 0;
            //r_receiving_set_k[1] = 0;
            //r_receiving_set_k[2] = 0;

            ////수신기2      
            //r_receiving_set_k[3] = 0;
            //r_receiving_set_k[4] = 0;
            //r_receiving_set_k[5] = 0;
            //--------------------------------------------------------------------------

            // 디버그 간소화에 따른 수정된 부분(0905)
            //이부분은 칼만필터 통과후 좌표값이다
            //if (const_becon_p2_backhoe == 1) Debug.Write(" K P1x " + r_receiving_set_k[0] + " P1y " + r_receiving_set_k[1] + " P1z " + r_receiving_set_k[2] + " P2x " + r_receiving_set_k[3] + " P2y " + r_receiving_set_k[4] + " P2z " + r_receiving_set_k[5] + " 칼만필터 좌표값" + "\r\n");//칼만필터 값으로 좌표값 계산



            //-----------------------------비콘을 상대좌표로 변환해서 수신기 구함(로우값으로 계산된 수신기 좌표)----------------------------
            
            r_backhoe_tri_point_o = r_Calc_tri_o(c_beacon_p1, c_beacon_p2);

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 위에 있을때)////////////////////
            ////수신기1
            //r_receiving_set_o[0] = r_backhoe_tri_point_o[0];
            //r_receiving_set_o[1] = r_backhoe_tri_point_o[1];
            //r_receiving_set_o[2] = r_backhoe_tri_point_o[2];

            ////수신기2                                 
            //r_receiving_set_o[3] = r_backhoe_tri_point_o[6];
            //r_receiving_set_o[4] = r_backhoe_tri_point_o[7];
            //r_receiving_set_o[5] = r_backhoe_tri_point_o[8];

            //////////////////////수신기 출력 (수신기가 무조건 비콘평면보다 밑에 있을때)///////////////////////
            //수신기1
            r_receiving_set_o[0] = r_backhoe_tri_point_o[3];
            r_receiving_set_o[1] = r_backhoe_tri_point_o[4];
            r_receiving_set_o[2] = r_backhoe_tri_point_o[5];
                                   
            //수신기2              
            r_receiving_set_o[3] = r_backhoe_tri_point_o[9];
            r_receiving_set_o[4] = r_backhoe_tri_point_o[10];
            r_receiving_set_o[5] = r_backhoe_tri_point_o[11];
            
            // -------------------  이 부분은 두 수신기의 이동된 거리를 가지고 오차를 판단하는 구간임(꼬리표 설정 해야됨) ------------------------------
            if (const_becon_p2_backhoe == 2)//안정화된 커팅후의 사이거리값이 적용된 상태
            {

                //이 부분은 현재값과 이전 수신된 데이터와 비교한다

                //현재 로값
                //수신기1
                r_receiving_set_k_s1[0] = r_receiving_set_k[0];
                r_receiving_set_k_s1[1] = r_receiving_set_k[1];
                r_receiving_set_k_s1[2] = r_receiving_set_k[2];

                //수신기2                                  
                r_receiving_set_k_s1[3] = r_receiving_set_k[3];
                r_receiving_set_k_s1[4] = r_receiving_set_k[4];
                r_receiving_set_k_s1[5] = r_receiving_set_k[5];

                //이전 수신 데이터값
                //수신기1
                r_receiving_set_k_s2[0] = r_receiving_set_o[0];
                r_receiving_set_k_s2[1] = r_receiving_set_o[1];
                r_receiving_set_k_s2[2] = r_receiving_set_o[2];

                //수신기2                                     
                r_receiving_set_k_s2[3] = r_receiving_set_o[3];
                r_receiving_set_k_s2[4] = r_receiving_set_o[4];
                r_receiving_set_k_s2[5] = r_receiving_set_o[5];

                // 디버그 간소화에 따른 수정된 부분(1214)
                //Debug.Write(" K P1x " + r_receiving_set_k_s1[0] + " P1y " + r_receiving_set_k_s1[1] + " P1z " + r_receiving_set_k_s1[2] + " P2x " + r_receiving_set_k_s1[3] + " P2y " + r_receiving_set_k_s1[4] + " P2z " + r_receiving_set_k_s1[5] + " 현재 수신 데이터 좌표값(좌표이동거리) " + "\r\n");
                //Debug.Write(" K P1x " + r_receiving_set_k_s2[0] + " P1y " + r_receiving_set_k_s2[1] + " P1z " + r_receiving_set_k_s2[2] + " P2x " + r_receiving_set_k_s2[3] + " P2y " + r_receiving_set_k_s2[4] + " P2z " + r_receiving_set_k_s2[5] + " 이전 수신 데이터 좌표값(좌표이동거리) " + "\r\n");

                //--------------------------------두 좌표사이의 거리를 가지고 튀는값 잡기(튀지않으면 칼만필터로 전달되고 튄값이면 이전값을 칼만필터로 던진다) - 비율로 수정-----------------
                //P1 과 P1' 거리값 계산
                P1bP1a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[0] - r_receiving_set_k_s2[0], 2) + Math.Pow(r_receiving_set_k_s1[1] - r_receiving_set_k_s2[1], 2) + Math.Pow(r_receiving_set_k_s1[2] - r_receiving_set_k_s2[2], 2));

                //P2 과 P2' 거리값 계산
                P2bP2a_distance = Math.Sqrt(Math.Pow(r_receiving_set_k_s1[3] - r_receiving_set_k_s2[3], 2) + Math.Pow(r_receiving_set_k_s1[4] - r_receiving_set_k_s2[4], 2) + Math.Pow(r_receiving_set_k_s1[5] - r_receiving_set_k_s2[5], 2));

                P1P2_absolute = Math.Abs(P1bP1a_distance - P2bP2a_distance);

                
            }
            
            if (const_becon_p2_backhoe == 0 && P1P2_distance_signal == 1)//0 부분은 안정화된 처음값을 의미
            {
                // 디버그 간소화에 따른 수정된 부분(1214)
                // 처음 들어온값을 가지고 바로 좌표 계산 따라서 == 0 (이부분은 안정화된 거리값을 가지고 좌표를 계산한것임)
                // Debug.Write(" o P1x " + r_receiving_set_o[0] + " P1y " + r_receiving_set_o[1] + " P1z " + r_receiving_set_o[2] + " P2x " + r_receiving_set_o[3] + " P2y " + r_receiving_set_o[4] + " P2z " + r_receiving_set_o[5] + " 수신 데이터 좌표값(사잇거리)" + "\r\n");//안정화 된 후 원본거리값으로 계산된 좌표값

                //두 수신기 사이의 거리값 계산(안정화된 커팅후의 거리값을 가지고 사이거리값 계산)
                P1P2_distance_o = Math.Sqrt(Math.Pow(r_receiving_set_o[0] - r_receiving_set_o[3], 2) + Math.Pow(r_receiving_set_o[1] - r_receiving_set_o[4], 2) + Math.Pow(r_receiving_set_o[2] - r_receiving_set_o[5], 2));
                
            }
            
            //**************************************************************************위치 추적 로직 부분************************************************************************

            //----------------------------------수신기 z값 수식적용-----------------------------
            //처음 9개의 z값을 받은 후 중간값을 찾는다
            //입력된 z값
            //p1 z값 : r_receiving_set_k[2];
            //p2 z값 : r_receiving_set_k[5];

            if (const_becon_p2_backhoe == 1 && stop_signal == 1)//칼만근접후 계산
            {
                //-------테스트용 - 추가된 부분(0729)-------
                //p1
                //r_receiving_set_k[2] -= 2000;

                //p2
                //r_receiving_set_k[5] -= 2000;
                //-----------------------

                //P1
                real_data_d_p1_z[constant_number_d_P1_z] = r_receiving_set_k[2];
                
                //P2
                real_data_d_p2_z[constant_number_d_P2_z] = r_receiving_set_k[5];
                
                //중앙구간에서의 첫번째수(배열 맨처음에 0부터 시작이므로)
                number_standard_P1_z = number_divide_P1_z;//3
                number_standard_P2_z = number_divide_P2_z;

                //P1z
                temp_array_sort_d_P1_z = real_data_d_p1_z;//가상의 공간을 생성 
                
                //------직접계산 시작
                for (iz = 0; iz < number_divide_P1_z * 3 - 1; iz++)// 0부터 7
                {
                    for (kz = iz + 1; kz < number_divide_P1_z * 3; kz++)//1 부터 8(예 : iz가 0일때 kz는 1부터 8까지 포문, iz가 1일때 kz는 2부터 8까지 포문)
                    {
                        if (temp_array_sort_d_P1_z[iz] >= temp_array_sort_d_P1_z[kz])//iz 가 0 ~ 7 까지일때 각각 + 1 부터 8까지 비교(처음에는 0 배열을 제외하고 전부 0)
                        {                                                            //iz를 가지고 크기비교를 한뒤 큰값이면 뒤로 쉬프트
                            
                            //크기별로 순서화
                            data_value_align_z = temp_array_sort_d_P1_z[iz];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P1_z[iz] = temp_array_sort_d_P1_z[kz];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P1_z[kz] = data_value_align_z;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }
                
                data_value_align_z = 0;

                ////-------직접계산 끝



                //P2z
                temp_array_sort_d_P2_z = real_data_d_p2_z;//가상의 공간을 생성 

                //------직접계산 시작
                for (iz = 0; iz < number_divide_P2_z * 3 - 1; iz++)//5
                {
                    for (kz = iz + 1; kz < number_divide_P2_z * 3; kz++)//6
                    {
                        if (temp_array_sort_d_P2_z[iz] >= temp_array_sort_d_P2_z[kz])//iz 가 0 ~ 4 까지일때 각각 + 1 부터 5까지 비교(처음에는 0 배열을 제외하고 전부 0임)
                        {
                            //크기별로 순서화
                            data_value_align_z = temp_array_sort_d_P2_z[iz];//첫번째 베열이 두번째 배열 보다 클때, 첫번째 배열을 따로 값을 저장
                            temp_array_sort_d_P2_z[iz] = temp_array_sort_d_P2_z[kz];//두번째 배열 값을, 첫번째 배열값에다 저장
                            temp_array_sort_d_P2_z[kz] = data_value_align_z;//따로 저장된 첫번째 배열값을 두번째 배열에 저장
                        }
                    }
                }

                data_value_align_z = 0;
                ////-------직접계산 끝
                
                //중간값 위치 계산
                calc_center_p1 = number_divide_P1_z / 2;
                calc_center_p2 = number_divide_P2_z / 2;

                center_position_p1 = Convert.ToInt16(Math.Round(calc_center_p1));
                center_position_p2 = Convert.ToInt16(Math.Round(calc_center_p2));

                if (temp_array_sort_d_P1_z[0] > -99999999999999)//음수값 설정을 위한
                {
                    r_receiving_set[2] = temp_array_sort_d_P1_z[number_standard_P1_z + center_position_p1];//구간의 범위가 3개일때 중간값(012 345 678)
                    for (jz = 0; jz < number_divide_P1_z * 3; jz++) temp_array_sort_d_P1_z[jz] = -99999999999999;

                }

                if (temp_array_sort_d_P2_z[0] > -99999999999999)//음수값 설정을 위한
                {
                    r_receiving_set[5] = temp_array_sort_d_P2_z[number_standard_P2_z + center_position_p2];//구간의 범위가 3개일때 중간값(012 345 678)
                    for (jz = 0; jz < number_divide_P2_z * 3; jz++) temp_array_sort_d_P2_z[jz] = -99999999999999;

                }


                //최종 출력된 수신기 좌표
                r_receiving_set[0] = r_receiving_set_k[0];
                r_receiving_set[1] = r_receiving_set_k[1];
                

                r_receiving_set[3] = r_receiving_set_k[3];
                r_receiving_set[4] = r_receiving_set_k[4];

                // 디버그 간소화에 따른 수정된 부분(1214)
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1 z : " + r_receiving_set[2]);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2 z : " + r_receiving_set[5]);


                //----------------------------------------------비컨과 수신기 사이의 각도 계산부------------------------------------------------


                //비컨 좌표
                //r_becon_1_v[0]
                //r_becon_1_v[1]
                //r_becon_1_v[2]
                //    
                //r_becon_2_v[0]
                //r_becon_2_v[1]
                //r_becon_2_v[2]
                //    
                //r_becon_3_v[0]
                //r_becon_3_v[1]
                //r_becon_3_v[2]
                //    
                //r_becon_4_v[0]
                //r_becon_4_v[1]
                //r_becon_4_v[2]

                //----------------------수평거리 계산(평면 상에서  비컨과 수신기 사이의 거리)--------------------
                //p1b1
                p1b1_flat_distance = Math.Sqrt(Math.Pow(r_becon_1_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_1_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b1_down : " + p1b1_flat_distance);

                //p1b2
                p1b2_flat_distance = Math.Sqrt(Math.Pow(r_becon_2_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_2_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b2_down : " + p1b2_flat_distance);

                //p1b3
                p1b3_flat_distance = Math.Sqrt(Math.Pow(r_becon_3_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_3_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b3_down : " + p1b3_flat_distance);

                //p1b4
                p1b4_flat_distance = Math.Sqrt(Math.Pow(r_becon_4_v[0] - r_receiving_set[0], 2) + Math.Pow(r_becon_4_v[1] - r_receiving_set[1], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b4_down : " + p1b4_flat_distance);

                //------------------------

                //p2b1    
                p2b1_flat_distance = Math.Sqrt(Math.Pow(r_becon_1_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_1_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b1_down : " + p2b1_flat_distance);

                //p2b2
                p2b2_flat_distance = Math.Sqrt(Math.Pow(r_becon_2_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_2_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b2_down : " + p2b2_flat_distance);

                //p2b3
                p2b3_flat_distance = Math.Sqrt(Math.Pow(r_becon_3_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_3_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b3_down : " + p2b3_flat_distance);

                //p2b4
                p2b4_flat_distance = Math.Sqrt(Math.Pow(r_becon_4_v[0] - r_receiving_set[3], 2) + Math.Pow(r_becon_4_v[1] - r_receiving_set[4], 2));
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b4_down : " + p2b4_flat_distance);


                //-------------센서거리---------------
                //p1b1
                //beacon_p1[0]

                //p1b2
                //beacon_p1[1]

                //p1b3
                //beacon_p1[2]

                //p1b4
                //beacon_p1[3]

                //------------------------

                //p2b1
                //beacon_p2[0]

                //p2b2
                //beacon_p2[1]

                //p2b3
                //beacon_p2[2]

                //p2b4
                //beacon_p3[3]

                //------------------------각도계산----------------------

                //아크코사인 정의역이 1보다 클경우 추가

                //p1b1
                if (p1b1_flat_distance / beacon_p1[0] > 1) p1b1_angle_value = 2 - (p1b1_flat_distance / beacon_p1[0]);
                else if (p1b1_flat_distance / beacon_p1[0] <= 1) p1b1_angle_value = p1b1_flat_distance / beacon_p1[0];
                p1b1_angle = Math.Acos(p1b1_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b1_angle : " + p1b1_angle);

                //p1b2
                if (p1b2_flat_distance / beacon_p1[1] > 1) p1b2_angle_value = 2 - (p1b2_flat_distance / beacon_p1[1]);
                else if (p1b2_flat_distance / beacon_p1[1] <= 1) p1b2_angle_value = p1b2_flat_distance / beacon_p1[1];
                p1b2_angle = Math.Acos(p1b2_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b2_angle : " + p1b2_angle);

                //p1b3
                if (p1b3_flat_distance / beacon_p1[2] > 1) p1b3_angle_value = 2 - (p1b3_flat_distance / beacon_p1[2]);
                else if (p1b3_flat_distance / beacon_p1[2] <= 1) p1b3_angle_value = p1b3_flat_distance / beacon_p1[2];
                p1b3_angle = Math.Acos(p1b3_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b3_angle : " + p1b3_angle);
                
                //p1b4
                if (p1b4_flat_distance / beacon_p1[3] > 1) p1b4_angle_value = 2 - (p1b4_flat_distance / beacon_p1[3]);
                else if (p1b4_flat_distance / beacon_p1[3] <= 1) p1b4_angle_value = p1b4_flat_distance / beacon_p1[3];
                p1b4_angle = Math.Acos(p1b4_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p1b4_angle : " + p1b4_angle);



                //--------------------------------------------------



                //p2b1
                if (p2b1_flat_distance / beacon_p2[0] > 1) p2b1_angle_value = 2 - (p2b1_flat_distance / beacon_p2[0]);
                else if (p2b1_flat_distance / beacon_p2[0] <= 1) p2b1_angle_value = p2b1_flat_distance / beacon_p2[0];
                p2b1_angle = Math.Acos(p2b1_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b1_angle : " + p2b1_angle);

                //p2b2
                if (p2b2_flat_distance / beacon_p2[1] > 1) p2b2_angle_value = 2 - (p2b2_flat_distance / beacon_p2[1]);
                else if (p2b2_flat_distance / beacon_p2[1] <= 1) p2b2_angle_value = p2b2_flat_distance / beacon_p2[1];
                p2b2_angle = Math.Acos(p2b2_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b2_angle : " + p2b2_angle);

                //p2b3
                if (p2b3_flat_distance / beacon_p2[2] > 1) p2b3_angle_value = 2 - (p2b3_flat_distance / beacon_p2[2]);
                else if (p2b3_flat_distance / beacon_p2[2] <= 1) p2b3_angle_value = p2b3_flat_distance / beacon_p2[2];
                p2b3_angle = Math.Acos(p2b3_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b3_angle : " + p2b3_angle);

                //p2b4
                if (p2b4_flat_distance / beacon_p2[3] > 1) p2b4_angle_value = 2 - (p2b4_flat_distance / beacon_p2[3]);
                else if (p2b4_flat_distance / beacon_p2[3] <= 1) p2b4_angle_value = p2b4_flat_distance / beacon_p2[3];
                p2b4_angle = Math.Acos(p2b4_angle_value) * (180 / Math.PI);
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> p2b4_angle : " + p2b4_angle);




                //------------------각도 평균화----------------
                //b1
                b1_angle = (p1b1_angle + p2b1_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b1_angle : " + b1_angle);
                
                //b2
                b2_angle = (p1b2_angle + p2b2_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b2_angle : " + b2_angle);

                //b3
                b3_angle = (p1b3_angle + p2b3_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b3_angle : " + b3_angle);

                //b4
                b4_angle = (p1b4_angle + p2b4_angle) / 2;
                //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>> b4_angle : " + b4_angle);


                //거리값이 999999일 경우 해당되는 비컨의 각도를 0으로 표시
                if (b1_sign == 1) b1_angle = 0;
                else if (b2_sign == 1) b2_angle = 0;
                else if (b3_sign == 1) b3_angle = 0;
                else if (b4_sign == 1) b4_angle = 0;


                //---------------가장 큰 각도 계산--------------
                //가장 큰 각도 값 결정(a_angle_N3가 가장큼)
                if (b1_angle < b2_angle) a_angle_N1 = b2_angle;
                else a_angle_N1 = b1_angle;

                if (a_angle_N1 < b3_angle) a_angle_N2 = b3_angle;
                else a_angle_N2 = a_angle_N1;

                if (a_angle_N2 < b4_angle) a_angle_N3 = b4_angle;
                else a_angle_N3 = a_angle_N2;

                a_angle_N3 = Math.Round(a_angle_N3, 4);//수식 오류로 인한 수정
                
                //수식 오류로 인한 수정
                b1_angle = Math.Round(b1_angle, 4);
                b2_angle = Math.Round(b2_angle, 4);
                b3_angle = Math.Round(b3_angle, 4);
                b4_angle = Math.Round(b4_angle, 4);

                //--------가장 큰 각도인 비컨 좌표값- 수식 오류로 수정--------
                //-----비컨 좌표 계산 
                //x 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_x = r_becon_1_v[0];
                else if (a_angle_N3 == b2_angle) angle_s_b0_x = r_becon_2_v[0];
                else if (a_angle_N3 == b3_angle) angle_s_b0_x = r_becon_3_v[0];
                else angle_s_b0_x = r_becon_4_v[0];

                //y 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_y = r_becon_1_v[1];
                else if (a_angle_N3 == b2_angle) angle_s_b0_y = r_becon_2_v[1];
                else if (a_angle_N3 == b3_angle) angle_s_b0_y = r_becon_3_v[1];
                else angle_s_b0_y = r_becon_4_v[1];

                //z 좌표 계산
                if (a_angle_N3 == b1_angle) angle_s_b0_z = r_becon_1_v[2];
                else if (a_angle_N3 == b2_angle) angle_s_b0_z = r_becon_2_v[2];
                else if (a_angle_N3 == b3_angle) angle_s_b0_z = r_becon_3_v[2];
                else angle_s_b0_z = r_becon_4_v[2];

                //----비컨 번호 선택(s_beacon_0)
                if (angle_s_b0_x == r_becon_1_v[0] && angle_s_b0_y == r_becon_1_v[1] && angle_s_b0_z == r_becon_1_v[2]) s_beacon_0 = 1;
                else if (angle_s_b0_x == r_becon_2_v[0] && angle_s_b0_y == r_becon_2_v[1] && angle_s_b0_z == r_becon_2_v[2]) s_beacon_0 = 2;
                else if (angle_s_b0_x == r_becon_3_v[0] && angle_s_b0_y == r_becon_3_v[1] && angle_s_b0_z == r_becon_3_v[2]) s_beacon_0 = 3;
                else s_beacon_0 = 4;

                //----------- 나머지 비컨 좌표값 계산--------------
                //------s_beacon_1
                //x 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_x = r_becon_2_v[0];
                else angle_s_b1_x = r_becon_1_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_y = r_becon_2_v[1];
                else angle_s_b1_y = r_becon_1_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 1) angle_s_b1_z = r_becon_2_v[2];
                else angle_s_b1_z = r_becon_1_v[2];

                //비컨 번호 선택(s_beacon_1)
                if (s_beacon_0 == 1) s_beacon_1 = 2;
                else s_beacon_1 = 1;

                //-------s_beacon_2
                //x 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_x = r_becon_3_v[0];
                else if (s_beacon_0 == 2) angle_s_b2_x = r_becon_3_v[0];
                else if (s_beacon_0 == 3) angle_s_b2_x = r_becon_2_v[0];
                else angle_s_b2_x = r_becon_2_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_y = r_becon_3_v[1];
                else if (s_beacon_0 == 2) angle_s_b2_y = r_becon_3_v[1];
                else if (s_beacon_0 == 3) angle_s_b2_y = r_becon_2_v[1];
                else angle_s_b2_y = r_becon_2_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 1) angle_s_b2_z = r_becon_3_v[2];
                else if (s_beacon_0 == 2) angle_s_b2_z = r_becon_3_v[2];
                else if (s_beacon_0 == 3) angle_s_b2_z = r_becon_2_v[2];
                else angle_s_b2_z = r_becon_2_v[2];

                //비컨 번호 선택(s_beacon_2)
                if (s_beacon_0 == 1) s_beacon_2 = 3;
                else if (s_beacon_0 == 2) s_beacon_2 = 3;
                else if (s_beacon_0 == 3) s_beacon_2 = 2;
                else s_beacon_2 = 2;

                //--------s_beacon_3
                //x 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_x = r_becon_3_v[0];
                else angle_s_b3_x = r_becon_4_v[0];

                //y 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_y = r_becon_3_v[1];
                else angle_s_b3_y = r_becon_4_v[1];

                //z 좌표 계산
                if (s_beacon_0 == 4) angle_s_b3_z = r_becon_3_v[2];
                else angle_s_b3_z = r_becon_4_v[2];

                //비컨 번호 선택(s_beacon_3)
                if (s_beacon_0 == 4) s_beacon_3 = 3;
                else s_beacon_3 = 4;

                //------ 비컨 2개 결정 준비
                beacon_2_calc = 1;


            }


            ////////////////////////////////////////////////////////////////////////상대 좌표계 Pb(동일한 높이인 수신기일 경우 적용) - e///////////////////////////////////////////////////////////////////

            //위치좌표 이전 이후값 구분(카운트로 구분)
            if (const_becon_p2_backhoe == 1)
            {
                pb_count++;

                if (pb_count == 1)
                {
                    //이전값

                    //calc_body_y 적용
                    pb_b[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
                    pb_b[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
                    pb_b[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

                    //디버그 간소화에 따른 수정된 부분(1214)
                    //Debug.WriteLine(" 위치좌표_1: " + " x " + pb_b[0] + " y " + pb_b[1] + " z " + pb_b[2]);

                    if (pb_signal == 1)
                    {
                        //이전값과 이후값 거리 구하기
                        pb_distance = Math.Sqrt(Math.Pow(pb_b[0] - pb_a[0], 2) + Math.Pow(pb_b[1] - pb_a[1], 2) + Math.Pow(pb_b[2] - pb_a[2], 2));
                    }
                }

                else if (pb_count > 1)
                {
                    //이후값

                    //calc_body_y 적용
                    pb_a[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
                    pb_a[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
                    pb_a[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

                    //디버그 간소화에 따른 수정된 부분(1214)
                    //Debug.WriteLine(" 위치좌표_2: " + " x " + pb_a[0] + " y " + pb_a[1] + " z " + pb_a[2]);

                    pb_count = 0;
                    pb_signal = 1;

                    //이전값과 이후값 거리 구하기
                    pb_distance = Math.Sqrt(Math.Pow(pb_b[0] - pb_a[0], 2) + Math.Pow(pb_b[1] - pb_a[1], 2) + Math.Pow(pb_b[2] - pb_a[2], 2));
                }

                //디버그 간소화에 따른 수정된 부분(1214)
                //Debug.WriteLine(" 위치 좌표 계산후 튀는 거리 " + pb_distance);

            }


            // tempZ는 사용하지 않으므로 주석처리 - 수정된 부분(1219)
            // 0일때 이전값 출력
            // if (tempZ > 0) pb_s = tempZ;
            // else if (tempZ == 0) tempZ = pb_s;


            pb[0] = (r_receiving_set[0] + r_receiving_set[3]) / 2;
            pb[1] = (r_receiving_set[1] + r_receiving_set[4]) / 2;
            pb[2] = (r_receiving_set[2] + r_receiving_set[5]) / 2;

            // - tempZ는 사용하지 않으므로 주석처리 - 수정된 부분(1219)
            // pb[2] = tempZ * 1000; // 광파기 z값으로 수정
            // pb[2] += excavator_body_length[1]; // 광파기 z값 보정 - 1538, 1544, 1644, 1594, 1624, 1544

            //yaw calc e
            move_p1[0] = r_receiving_set[0] - pb[0];
            move_p1[1] = r_receiving_set[1] - pb[1];    
            move_p1[2] = 0;

            p1_atan = Math.Atan(move_p1[1] / move_p1[0]) * (180 / Math.PI);
            p1_calibration = Math.Atan((-Math.Sin(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1])) / Math.Cos(excavator_body_imu[0])) * (180 / Math.PI);

            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) calc_body_y = 90;
                else calc_body_y = -90;
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) calc_body_y = 180;
                    else calc_body_y = 0;
                }

                else
                {
                    if (move_p1[0] < 0) calc_body_y = -p1_atan;
                    else
                    {
                        if (move_p1[1] < 0) calc_body_y = -180 - p1_atan;
                        else calc_body_y = 180 - p1_atan;
                    }
                }
            }

            //요값 강제입력 - 추가된 부분(1214) - ##2
            //calc_body_y = 0;

            //yaw 값 계산 판별
            if ((calc_body_y > 0 || calc_body_y < 0 || calc_body_y == 0) && const_becon_p2_backhoe == 1 && stop_signal == 1)//계산이 잘될때
            {
                calc_body_y_o = calc_body_y;//값이 잘 나올때 저장
                calc_body_y_a = calc_body_y;//요값 저장을 위한
                Debug.WriteLine(" yaw_o " + calc_body_y);//값이 잘 나올때 출력

                yawCalcPass = 1; // yaw 계산이 잘나올 경우 표시 - 추가된 부분(1221)
            }
            else//계산이 안될때
            {
                if (const_becon_p2_backhoe == 1 && stop_signal == 1)
                {
                    NaN_yaw_count++;//정상화 이후 카운팅 시작
                    calc_body_y_o = calc_body_y_a;//저장된 요값 사용을 위한

                    //디버그 간소화에 따른 수정된 부분(1214)
                    //Debug.WriteLine("                                                                                                                                                                                                                        NaN_yaw_count " + NaN_yaw_count);
                    Debug.Write(" yaw_c " + calc_body_y_o);//계산이 안될경우 저장된값 출력
                    Debug.WriteLine(" yaw_o " + calc_body_y);//값이 안 나올때 원본값 출력 
                }

            }

            


            calc_body_y_o += e_body_y_c;//body의 yaw값 조절
           
            body_y_1 = Math.Pow(Math.Cos(excavator_body_imu[0]), 2);

            //boom yaw calc e
            boom_y_1 = Math.Pow(Math.Tan(excavator_boom_imu[1]), 2);
            boom_y_2 = Math.Sqrt(boom_y_1 / (body_y_1 + boom_y_1 * body_y_1));

            if (boom_y_2 > 1) r_boom_a = Math.Asin(2 - boom_y_2);
            else r_boom_a = Math.Asin(boom_y_2);

            multy_a = (excavator_body_imu[1] * excavator_boom_imu[1] < 0) ? 1 : -1;
            y_boom_a = Math.Abs(Math.Atan(Math.Sin(r_boom_a) * Math.Sin(excavator_body_imu[1]) / Math.Cos(r_boom_a))) * multy_a * 180.0 / Math.PI;
            boom_yaw = (calc_body_y_o + y_boom_a + e_boom_y_c) * Math.PI / 180.0; //-2.34 

            //arm yaw calc e
            arm_y_1 = Math.Pow(Math.Tan(excavator_arm_imu[1]), 2);
            arm_y_2 = Math.Sqrt(arm_y_1 / (body_y_1 + arm_y_1 * body_y_1));

            if (arm_y_2 > 1) r_arm_a = Math.Asin(2 - arm_y_2);
            else r_arm_a = Math.Asin(arm_y_2);

            multy_b = (excavator_body_imu[1] * excavator_arm_imu[1] < 0) ? 1 : -1;
            y_arm_a = Math.Abs(Math.Atan(Math.Sin(r_arm_a) * Math.Sin(excavator_body_imu[1]) / Math.Cos(r_arm_a))) * multy_b * 180.0 / Math.PI;
            arm_yaw = ((excavator_arm_imu_s[1] < -90) ? calc_body_y_o + y_arm_a + 180 + e_arm_y_c : calc_body_y_o + y_arm_a + e_arm_y_c) * Math.PI / 180.0;

            //deg를 rad으로 - 수식 변경에 따른 수정
            calc_body_y_o *= (Math.PI / 180);

            //orientation e
            if (move_p1[0] == 0)
            {
                if (move_p1[1] > 0) orientation = 0;
                else orientation = 180;
            }

            else
            {
                if (move_p1[1] == 0)
                {
                    if (move_p1[0] > 0) orientation = 270;
                    else orientation = 90;
                }

                else
                {
                    if (move_p1[0] < 0) orientation = 90 + p1_atan;
                    else orientation = 270 + p1_atan;
                }
            }

            orientation = 360 - orientation - p1_calibration + e_body_y_c;

            //////////////////////////////////////////////////////pb가 (0, 0, 0)일때, pbb_p구하기(_p : 위치 좌표계)////////////////////////////////////
            // roll = excavator_body_imu[0], pitch = excavator_body_imu[1], yaw = calc_body_y_o

            pbb_length[0] = excavator_body_length[6];
            pbb_length[1] = 0;
            pbb_length[2] = 0;
            
            pbb_p[0]  = pbb_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Cos(calc_body_y_o);
            pbb_p[1] = -pbb_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Sin(calc_body_y_o);
            pbb_p[2]  = pbb_length[0] * Math.Sin(excavator_body_imu[1]);     

            //////////////////////////////////////////////////////pbb_p일때, pc_p구하기(_p : 위치 좌표계)////////////////////////////////////
            pbpc_length[0] = 0; pbpc_length[1] = 0; pbpc_length[2] = -excavator_body_length[3];

            pc_p[0] = pbb_p[0] - pbpc_length[2] * (Math.Cos(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Cos(calc_body_y_o) + Math.Sin(excavator_body_imu[0]) * Math.Sin(calc_body_y_o));
            pc_p[1] = pbb_p[1] + pbpc_length[2] * (Math.Cos(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Sin(calc_body_y_o) - Math.Sin(excavator_body_imu[0]) * Math.Cos(calc_body_y_o));
            pc_p[2] = pbb_p[2] + pbpc_length[2] * Math.Cos(excavator_body_imu[0]) * Math.Cos(excavator_body_imu[1]);
            
            //////////////////////////////////////////////////////pc_p일때, p3_p구하기(_p : 위치 좌표계) - e(여기서 p3는 수식에서 p4를 의미)////////////////////////////////////
            pcp4_length[0] = excavator_body_length[5]; pcp4_length[1] = excavator_body_length[4]; pcp4_length[2] = 0;

            p3_p[0] = pc_p[0] + pcp4_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Cos(calc_body_y_o) + pcp4_length[1] * (Math.Cos(excavator_body_imu[0]) * Math.Sin(calc_body_y_o) - Math.Sin(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Cos(calc_body_y_o));
            p3_p[1] = pc_p[1] - pcp4_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Sin(calc_body_y_o) + pcp4_length[1] * (Math.Cos(excavator_body_imu[0]) * Math.Cos(calc_body_y_o) + Math.Sin(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Sin(calc_body_y_o));
            p3_p[2] = pc_p[2] + pcp4_length[0] * Math.Sin(excavator_body_imu[1]) + pcp4_length[1] * Math.Sin(excavator_body_imu[0]) * Math.Cos(excavator_body_imu[1]);
            

            ////////////////////////////////////p3_p이 (0, 0, 0)일때, boom_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님/////////////////////////
            boom_length[0] = excavator_constant[0]; boom_length[1] = 0; boom_length[2] = 0;

            boom_p_h[0] = boom_length[0] * Math.Cos(excavator_boom_imu[1]) * Math.Cos(boom_yaw);
            boom_p_h[1] = -boom_length[0] * Math.Cos(excavator_boom_imu[1]) * Math.Sin(boom_yaw);
            boom_p_h[2] = boom_length[0] * Math.Sin(excavator_boom_imu[1]);

            /////////////////////////////////////boom_p_h일때, arm_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님/////////////////////////
            arm_length[0] = excavator_constant[1]; arm_length[1] = 0; arm_length[2] = 0;

            arm_p_h[0] = boom_p_h[0] + arm_length[0] * Math.Cos(excavator_arm_imu[1]) * Math.Cos(arm_yaw);
            arm_p_h[1] = boom_p_h[1] - arm_length[0] * Math.Cos(excavator_arm_imu[1]) * Math.Sin(arm_yaw);
            arm_p_h[2] = boom_p_h[2] + arm_length[0] * Math.Sin(excavator_arm_imu[1]);

            //////////////////////////////boom_p_h일때, buckit_p_h구하기(_h : 힌지 좌표계) - 좌표변환된것 아님////////////////////////
            arm_length[0] = excavator_constant[1]; arm_length[1] = 0; arm_length[2] = 0;
            buckit_length[0] = excavator_constant[2]; buckit_length[1] = 0; buckit_length[2] = 0;

            a_length = buckit_length[0] * Math.Cos(bucket_a_angle); b_length = buckit_length[0] * Math.Sin(bucket_a_angle);
            boom_arm_length[0] = arm_length[0] + a_length;
            boom_arm_length[1] = 0;
            boom_arm_length[2] = b_length;

            if (excavator_arm_imu_s[1] < -90) excavator_arm_imu[0] = excavator_arm_imu[0] - 180;
            excavator_arm_imu[0] *= Math.PI / 180.0;

            buckit_p_h[0] = boom_p_h[0] + boom_arm_length[0] * Math.Cos(excavator_arm_imu[1]) * Math.Cos(arm_yaw) - boom_arm_length[2] * (Math.Cos(excavator_arm_imu[0]) * Math.Sin(excavator_arm_imu[1]) * Math.Cos(arm_yaw) + Math.Sin(excavator_arm_imu[0]) * Math.Sin(arm_yaw));
            buckit_p_h[1] = boom_p_h[1] - boom_arm_length[0] * Math.Cos(excavator_arm_imu[1]) * Math.Sin(arm_yaw) + boom_arm_length[2] * (Math.Cos(excavator_arm_imu[0]) * Math.Sin(excavator_arm_imu[1]) * Math.Sin(arm_yaw) - Math.Sin(excavator_arm_imu[0]) * Math.Cos(arm_yaw));
            buckit_p_h[2] = boom_p_h[2] + boom_arm_length[0] * Math.Sin(excavator_arm_imu[1]) + boom_arm_length[2] * Math.Cos(excavator_arm_imu[0]) * Math.Cos(excavator_arm_imu[1]);

            ///////////////////////////////////boom_p_h에서 좌표변환(-90 deg)된 boom_p_h_t///////////////////////////////
            boom_p_h_t[0] = -boom_p_h[1];
            boom_p_h_t[1] = boom_p_h[0];
            boom_p_h_t[2] = boom_p_h[2];

            ///////////////////////////////////arm_p_h에서 좌표변환(-90 deg)된 arm_p_h_t///////////////////////////////
            arm_p_h_t[0] = -arm_p_h[1];
            arm_p_h_t[1] = arm_p_h[0];
            arm_p_h_t[2] = arm_p_h[2];

            ///////////////////////////////////buckit_p_h에서 좌표변환(-90 deg)된 buckit_p_h_t///////////////////////////////
            buckit_p_h_t[0] = -buckit_p_h[1];
            buckit_p_h_t[1] = buckit_p_h[0];
            buckit_p_h_t[2] = buckit_p_h[2];

            ///////////////////////////////////pc = pb + ppredicton_covariance(e)///////////////////////////////
            pc[0] = pb[0] + pc_p[0];
            pc[1] = pb[1] + pc_p[1];
            pc[2] = pb[2] + pc_p[2];

            ///////////////////////////////////p3 = pb + p3_p(e)///////////////////////////////
            p3[0] = pb[0] + p3_p[0];
            p3[1] = pb[1] + p3_p[1];
            p3[2] = pb[2] + p3_p[2];
            
            /////////////////////////////////// pt /////////////////////////////////////////////
            // p3 좌표값 가지고 바닥좌표값 계산 - 추가된 부분(1214)
            // 바닥좌표점(pt[0], pt[1], pt[2])
            // p3와 pt사이의 x, y, z 축상의 길이 
            pt_length[0] = 0;
            pt_length[1] = 0;
            pt_length[2] = 0;

            pt[0] = p3[0] + pt_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Cos(calc_body_y_o) + pt_length[1] * Math.Cos(excavator_body_imu[0]) * Math.Sin(calc_body_y_o) - pt_length[1] * Math.Sin(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Cos(calc_body_y_o)
                - pt_length[2] * Math.Cos(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Cos(calc_body_y_o) - pt_length[2] * Math.Sin(excavator_body_imu[0]) * Math.Sin(calc_body_y_o);
            pt[1] = p3[1] - pt_length[0] * Math.Cos(excavator_body_imu[1]) * Math.Sin(calc_body_y_o) + pt_length[1] * Math.Cos(excavator_body_imu[0]) * Math.Cos(calc_body_y_o) + pt_length[1] * Math.Sin(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Sin(calc_body_y_o)
                + pt_length[2] * Math.Cos(excavator_body_imu[0]) * Math.Sin(excavator_body_imu[1]) * Math.Sin(calc_body_y_o) - pt_length[2] * Math.Sin(excavator_body_imu[0]) * Math.Cos(calc_body_y_o);
            pt[2] = p3[2] + pt_length[0] * Math.Sin(excavator_body_imu[1]) + pt_length[1] * Math.Sin(excavator_body_imu[0]) * Math.Cos(excavator_body_imu[1]) + pt_length[2] * Math.Cos(excavator_body_imu[0]) * Math.Cos(excavator_body_imu[1]);

            // ##3
            // pt 강제입력 - 수정된 부분(1218)
            //pt[0] = 3;
            //pt[1] = 450;
            //pt[2] = 8;

            //Debug.WriteLine(">>>>>>> P&T : " + pt[0] + " " + pt[1] + " " + pt[2]);

            if (kalman_after == 1) // 칼만필터 통과된 값만 계산 - 추가된 부분(1226)
            {
                Debug.WriteLine(">>>>>>> P3 Z_1 : " + p3[2]);

                if (yawCalcPass == 1) // 요값이 계산이 되면 시작
                {


                    // 아두이노에서 값이 들어올경우 보정
                    if (calcDownPz != 0)
                    {
                        double deltaZ = pt[2] - calcDownPz;
                        p3[2] -= deltaZ;
                    }


                }

                Debug.WriteLine(">>>>>>> P3 Z_2 : " + p3[2]);
            }

            ///////////////////////////////////boom_p = p3 + boom_p_h_t///////////////////////////////
            boom_p[0] = p3[0] + boom_p_h_t[0];
            boom_p[1] = p3[1] + boom_p_h_t[1];
            boom_p[2] = p3[2] + boom_p_h_t[2];

            ///////////////////////////////////arm_p = p3 + arm_p_h_t///////////////////////////////
            arm_p[0] = p3[0] + arm_p_h_t[0];
            arm_p[1] = p3[1] + arm_p_h_t[1];
            arm_p[2] = p3[2] + arm_p_h_t[2];

            ///////////////////////////////////buckit_p = p3 + buckit_p_h_t///////////////////////////////
            buckit_p[0] = p3[0] + buckit_p_h_t[0];
            buckit_p[1] = p3[1] + buckit_p_h_t[1];
            buckit_p[2] = p3[2] + buckit_p_h_t[2];

            //**************************************************************************************사이각 계산 부분******************************************************************************
            //body와 boom의 사이각
            //pc가 원점으로 이동된 p4
            hinge_t_p[0] = p3[0] - pc[0];
            hinge_t_p[1] = p3[1] - pc[1];
            hinge_t_p[2] = p3[2] - pc[2];

            //p4가 원점으로 이동된 boomp
            boom_t_p[0] = boom_p[0] - p3[0];
            boom_t_p[1] = boom_p[1] - p3[1];
            boom_t_p[2] = boom_p[2] - p3[2];

            body_boom_angle = 180 - Math.Acos((boom_t_p[0] * hinge_t_p[0] + boom_t_p[1] * hinge_t_p[1] + boom_t_p[2] * hinge_t_p[2]) / (boom_length[0] * excavator_body_length[4])) * (180.0 / Math.PI);

            //boom과 arm의 사이각
            arm_t_p[0] = arm_p[0] - boom_p[0];
            arm_t_p[1] = arm_p[1] - boom_p[1];
            arm_t_p[2] = arm_p[2] - boom_p[2];

            boom_arm_angle = 180 - Math.Acos((boom_t_p[0] * arm_t_p[0] + boom_t_p[1] * arm_t_p[1] + boom_t_p[2] * arm_t_p[2]) / (boom_length[0] * arm_length[0])) * (180.0 / Math.PI);

            //boom_arm_angle 각도 제한에 따른
            if (boom_arm_angle > 165) boom_arm_angle = 165;

            //arm과 bucket의 사이각
            arm_bucket_angle = 360 - (bucket_angle_ac_deg + bucket_angle_ec_deg + excavator_bucket_angle);

            //*******************************************************************************좌표 변환(상대 -> 절대)***********************************************************************************
            /////////////////////////////////////////////////////////////////////////상대->임의 좌표계///////////////////////////////////////////////////////////////////
            b_angle_2 = -b_angle;

            //끝단1
            r2_buckit_p[0] = buckit_p[0] * Math.Cos(b_angle_2) + buckit_p[1] * Math.Sin(b_angle_2);
            r2_buckit_p[1] = -buckit_p[0] * Math.Sin(b_angle_2) + buckit_p[1] * Math.Cos(b_angle_2);
            r2_buckit_p[2] = buckit_p[2];

            //위치
            r2_pb[0] = pb[0] * Math.Cos(b_angle_2) + pb[1] * Math.Sin(b_angle_2);
            r2_pb[1] = -pb[0] * Math.Sin(b_angle_2) + pb[1] * Math.Cos(b_angle_2);
            r2_pb[2] = pb[2];

            //수신기1
            r_r2_receiving_set[0] = r_receiving_set[0] * Math.Cos(b_angle_2) + r_receiving_set[1] * Math.Sin(b_angle_2);
            r_r2_receiving_set[1] = -r_receiving_set[0] * Math.Sin(b_angle_2) + r_receiving_set[1] * Math.Cos(b_angle_2);
            r_r2_receiving_set[2] = r_receiving_set[2];

            //수신기2
            r_r2_receiving_set[3] = r_receiving_set[3] * Math.Cos(b_angle_2) + r_receiving_set[4] * Math.Sin(b_angle_2);
            r_r2_receiving_set[4] = -r_receiving_set[3] * Math.Sin(b_angle_2) + r_receiving_set[4] * Math.Cos(b_angle_2);
            r_r2_receiving_set[5] = r_receiving_set[5];

            //중심
            r2_pc[0] = pc[0] * Math.Cos(b_angle_2) + pc[1] * Math.Sin(b_angle_2);
            r2_pc[1] = -pc[0] * Math.Sin(b_angle_2) + pc[1] * Math.Cos(b_angle_2);
            r2_pc[2] = pc[2];

            //힌지
            r2_p3[0] = p3[0] * Math.Cos(b_angle_2) + p3[1] * Math.Sin(b_angle_2);
            r2_p3[1] = -p3[0] * Math.Sin(b_angle_2) + p3[1] * Math.Cos(b_angle_2);
            r2_p3[2] = p3[2];

            //boom
            r2_boom[0] = boom_p[0] * Math.Cos(b_angle_2) + boom_p[1] * Math.Sin(b_angle_2);
            r2_boom[1] = -boom_p[0] * Math.Sin(b_angle_2) + boom_p[1] * Math.Cos(b_angle_2);
            r2_boom[2] = boom_p[2];

            //arm
            r2_arm[0] = arm_p[0] * Math.Cos(b_angle_2) + arm_p[1] * Math.Sin(b_angle_2);
            r2_arm[1] = -arm_p[0] * Math.Sin(b_angle_2) + arm_p[1] * Math.Cos(b_angle_2);
            r2_arm[2] = arm_p[2];

            /////////////////////////////////////////////////////////////////////////임의->절대 좌표계///////////////////////////////////////////////////////////////////
            //끝단1
            abs_e[0] = r2_buckit_p[0] + instrument_station_1[0];
            abs_e[1] = r2_buckit_p[1] + instrument_station_1[1];
            abs_e[2] = r2_buckit_p[2] + instrument_station_1[2];

            //위치
            abs_p[0] = r2_pb[0] + instrument_station_1[0];
            abs_p[1] = r2_pb[1] + instrument_station_1[1];
            abs_p[2] = r2_pb[2] + instrument_station_1[2];

            //수신기1
            r_r2_abs_receiving_set[0] = r_r2_receiving_set[0] + instrument_station_1[0];
            r_r2_abs_receiving_set[1] = r_r2_receiving_set[1] + instrument_station_1[1];
            r_r2_abs_receiving_set[2] = r_r2_receiving_set[2] + instrument_station_1[2];

            //수신기2
            r_r2_abs_receiving_set[3] = r_r2_receiving_set[3] + instrument_station_1[0];
            r_r2_abs_receiving_set[4] = r_r2_receiving_set[4] + instrument_station_1[1];
            r_r2_abs_receiving_set[5] = r_r2_receiving_set[5] + instrument_station_1[2];

            //중심
            abs_pc[0] = r2_pc[0] + instrument_station_1[0];
            abs_pc[1] = r2_pc[1] + instrument_station_1[1];
            abs_pc[2] = r2_pc[2] + instrument_station_1[2];

            //힌지
            abs_p3[0] = r2_p3[0] + instrument_station_1[0];
            abs_p3[1] = r2_p3[1] + instrument_station_1[1];
            abs_p3[2] = r2_p3[2] + instrument_station_1[2];

            //boom
            abs_boom[0] = r2_boom[0] + instrument_station_1[0];
            abs_boom[1] = r2_boom[1] + instrument_station_1[1];
            abs_boom[2] = r2_boom[2] + instrument_station_1[2];

            //arm
            abs_arm[0] = r2_arm[0] + instrument_station_1[0];
            abs_arm[1] = r2_arm[1] + instrument_station_1[1];
            abs_arm[2] = r2_arm[2] + instrument_station_1[2];

            const_becon_p2_backhoe = 1;

            //**************************************************return 값 치환 부분 - e*****************************************************
            //////////////////////////절대 좌표계///////////////////////////
            
            //수신기1
            result[0] = Math.Round(r_r2_abs_receiving_set[0]);
            result[1] = Math.Round(r_r2_abs_receiving_set[1]);
            result[2] = Math.Round(r_r2_abs_receiving_set[2]);

            //수신기2
            result[3] = Math.Round(r_r2_abs_receiving_set[3]);
            result[4] = Math.Round(r_r2_abs_receiving_set[4]);
            result[5] = Math.Round(r_r2_abs_receiving_set[5]);

            //위치 좌표
            result[6] = Math.Round(abs_p[0]);
            result[7] = Math.Round(abs_p[1]);
            result[8] = Math.Round(abs_p[2]);

            //끝단1 좌표
            result[9] = Math.Round(abs_e[0]);
            result[10] = Math.Round(abs_e[1]);
            result[11] = Math.Round(abs_e[2]);

            //끝단2 좌표
            result[12] = 0;
            result[13] = 0;
            result[14] = 0;

            //중심 좌표
            result[61] = Math.Round(abs_pc[0]);
            result[62] = Math.Round(abs_pc[1]);
            result[63] = Math.Round(abs_pc[2]);

            //힌지 좌표
            result[71] = Math.Round(abs_p3[0]);
            result[72] = Math.Round(abs_p3[1]);
            result[73] = Math.Round(abs_p3[2]);

            //boom 좌표
            result[74] = Math.Round(abs_boom[0]);
            result[75] = Math.Round(abs_boom[1]);
            result[76] = Math.Round(abs_boom[2]);

            //arm 좌표                          
            result[77] = Math.Round(abs_arm[0]);
            result[78] = Math.Round(abs_arm[1]);
            result[79] = Math.Round(abs_arm[2]);

            //////////////////////////상대 좌표계////////////////////////////

            //위치 좌표
            result[15] = Math.Round(pb[0]);
            result[16] = Math.Round(pb[1]);
            result[17] = Math.Round(pb[2]);

            //끝단1 좌표
            result[18] = Math.Round(buckit_p[0]);
            result[19] = Math.Round(buckit_p[1]);
            result[20] = Math.Round(buckit_p[2]);

            //끝단2 좌표
            result[21] = 0;
            result[22] = 0;
            result[23] = 0;

            //비콘1 좌표
            result[24] = Math.Round(r_becon_1_v[0]);
            result[25] = Math.Round(r_becon_1_v[1]);
            result[26] = Math.Round(r_becon_1_v[2]);

            //비콘2 좌표
            result[27] = Math.Round(r_becon_2_v[0]);
            result[28] = Math.Round(r_becon_2_v[1]);
            result[29] = Math.Round(r_becon_2_v[2]);

            //비콘3 좌표
            result[30] = Math.Round(r_becon_3_v[0]);
            result[31] = Math.Round(r_becon_3_v[1]);
            result[32] = Math.Round(r_becon_3_v[2]);

            //비콘4 좌표 - 비컨이 4개
            result[84] = Math.Round(r_becon_4_v[0]);
            result[85] = Math.Round(r_becon_4_v[1]);
            result[86] = Math.Round(r_becon_4_v[2]);


            //측점2 좌표
            result[42] = Math.Round(r_instrument_station_2[0]);
            result[43] = Math.Round(r_instrument_station_2[1]);
            result[44] = Math.Round(r_instrument_station_2[2]);

            //측점3 좌표
            result[39] = Math.Round(r_instrument_station_3[0]);
            result[40] = Math.Round(r_instrument_station_3[1]);
            result[41] = Math.Round(r_instrument_station_3[2]);

            //수신기1 좌표
            result[33] = Math.Round(r_receiving_set[0]);
            result[34] = Math.Round(r_receiving_set[1]);
            result[35] = Math.Round(r_receiving_set[2]);

            //수신기2 좌표
            result[36] = Math.Round(r_receiving_set[3]);
            result[37] = Math.Round(r_receiving_set[4]);
            result[38] = Math.Round(r_receiving_set[5]);

            //중심 좌표
            result[64] = Math.Round(pc[0]);
            result[65] = Math.Round(pc[1]);
            result[66] = Math.Round(pc[2]);

            //orientation
            result[67] = Math.Round(orientation);

            //힌지 좌표
            result[68] = Math.Round(p3[0]);
            result[69] = Math.Round(p3[1]);
            result[70] = Math.Round(p3[2]);

            //boom 좌표
            result[80] = Math.Round(boom_p[0]);
            result[81] = Math.Round(boom_p[1]);
            result[82] = Math.Round(boom_p[2]);

            //arm 좌표
            result[45] = Math.Round(arm_p[0]);
            result[46] = Math.Round(arm_p[1]);
            result[47] = Math.Round(arm_p[2]);

            /////////////////////////imu(rad to deg) - 보다 정확한 검증을 위한 소수점 1자리 표현으로//////////////////////////
            //몸체
            result[48] = Math.Round((excavator_body_imu[0] * (180.0 / Math.PI)), 1);
            result[49] = Math.Round((excavator_body_imu[1] * (180.0 / Math.PI)), 1);

            //rad를 deg으로  - 저장을 위한 각도 수식 변경에 따른
            calc_body_y_o *= (180 / Math.PI);

            result[50] = Math.Round(calc_body_y_o, 1);

            //붐
            result[51] = Math.Round((excavator_boom_imu[0] * (180.0 / Math.PI)), 1);
            result[52] = Math.Round((excavator_boom_imu[1] * (180.0 / Math.PI)), 1);
            result[53] = Math.Round((boom_yaw * (180.0 / Math.PI)), 1);

            //암
            result[54] = Math.Round((excavator_arm_imu[0] * (180.0 / Math.PI)), 1);
            result[55] = Math.Round((excavator_arm_imu_s[1]), 1);
            result[56] = Math.Round((arm_yaw * (180.0 / Math.PI)), 1);

            //버킷
            result[57] = Math.Round((excavator_bucket_imu[0] * (180.0 / Math.PI)), 1);
            result[58] = Math.Round((excavator_bucket_imu[1] * (180.0 / Math.PI)), 1);

            //////////////////////////////사이각 - 보다 정확한 검증을 위한 소수점 1자리 표현으로/////////////////////////////////////////////////////////
            result[83] = Math.Round(body_boom_angle, 1);
            result[59] = Math.Round(boom_arm_angle, 1);
            result[60] = Math.Round(arm_bucket_angle, 1);

            //////////////////////////////비컨과 수신기 사이의 각도  - 보다 정확한 검증을 위한 소수점 1자리 표현으로///////////////////////////////
            result[87] = Math.Round(b1_angle, 1);
            result[88] = Math.Round(b2_angle, 1);
            result[89] = Math.Round(b3_angle, 1);
            result[90] = Math.Round(b4_angle, 1);

            /////////////////////////////// arduino 전송용 P&T 바닥의 좌표값 - 추가된 부분(1214) ///////////////////////////////
            result[91] = Math.Round(pt[0], 0);
            result[92] = Math.Round(pt[1], 0);
            result[93] = Math.Round(pt[2], 0);

            /////////////////////////////// arduino 전송용 yaw, roll, pitch값 - 수정된 부분(1226) ////////////////////////////
            result[94] = Math.Round(calc_body_y_o, 0);
            result[95] = Math.Round((excavator_body_imu[0] * (180.0 / Math.PI)), 0);
            result[96] = Math.Round((excavator_body_imu[1] * (180.0 / Math.PI)), 0);

            //96

            return result;
        }

    }
}