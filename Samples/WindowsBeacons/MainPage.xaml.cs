﻿// Copyright 2015 - 2017 Andreas Jakl. All rights reserved.
// https://github.com/andijakl/universal-beacon 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Resources;
using Windows.Foundation.Metadata;
using Windows.Globalization;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using UniversalBeacon.Library.Core.Entities;
using UniversalBeacon.Library.Core.Interop;
using UniversalBeacon.Library.UWP;
using UniversalBeaconLibrary;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Storage.Streams;
using System.Text;
using WindowsBeacons.Component;
using System.Collections.Generic;
using Windows.Networking.Sockets;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Storage;
using System.Collections;
using WindowsBeacons.Data;
using Windows.UI.Xaml.Media;
using Newtonsoft.Json.Linq;
using WindowsBeacons.Equipment;
using Newtonsoft.Json;
using Windows.UI.Popups;

namespace WindowsBeacons
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private readonly WindowsBluetoothPacketProvider _provider;
        private readonly BeaconManager _beaconManager;
        private Dictionary<String, String> locationData;
        private Dictionary<String, String> motionData;
        StreamSocket _socket;
        //private String _ip = "164.125.170.125";
        //private String _port = "6999";
        private String _ip = "127.0.0.1";
        private String _port = "5999";

        private int currentEquipIndex = 0;
        DispatcherTimer _timer;
        private int _interval = 300;
        private Boolean isConfig = true;
        //private Sqlite3Manager sqlite3;
        private List<KeyValue<int>> excavatorConst;
        private List<KeyValue<int>> dozerConst;
        private List<KeyValue<int>> motorGraderConst;
        private List<KeyValue<int>> absolutePoint;
        private List<KeyValue<int>> beaconPoint;
        private List<KeyValue<int>> calibration;
        private List<KeyValue<int>> imuCalibration;

        private ExcavatorCalculator excavator;
        private DozerCalculator dozer;
        private MotorGraderCalculator motorGrader;
        List<String> EquipList = new List<string>();

        // UI
        private CoreDispatcher _dispatcher;

        public static readonly DependencyProperty LeftColumnWidthProperty = DependencyProperty.Register(
            "LeftColumnWidth", typeof(int), typeof(MainPage), new PropertyMetadata(default(int)));

        private ResourceLoader _resourceLoader;

        public int LeftColumnWidth
        {
            get => (int)GetValue(LeftColumnWidthProperty);
            set => SetValue(LeftColumnWidthProperty, value);
        }

        private string _statusText;
        private string _locationDataText;
        private string _motionDataText;
        private bool _restartingBeaconWatch;

        StorageFile logFile;
        StorageFile constFile;

        public string LocationDataText
        {
            get => _locationDataText;
            set
            {
                if (_locationDataText == value) return;
                _locationDataText = value;
                OnPropertyChanged();
            }
        }

        public string MotionDataText
        {
            get => _motionDataText;
            set
            {
                if (_motionDataText == value) return;
                _motionDataText = value;
                OnPropertyChanged();
            }
        }

        public string StatusText
        {
            get => _statusText;
            set
            {
                if (_statusText == value) return;
                _statusText = value;
                OnPropertyChanged();
            }
        }
        
        List<string> calibs = new List<string>();

        public MainPage()
        {
            InitializeComponent();
            DataContext = this;
            _dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            
            InitializeConstUI();
            if (currentEquipIndex == 0)
            {
                InitializeExcavatorData();
            }
            else if (currentEquipIndex == 1)
            {
                InitializeDozerData();
            }

            EquipPivot.SelectedIndex = currentEquipIndex;

            // Construct the Universal Bluetooth Beacon manager
            _provider = new WindowsBluetoothPacketProvider();
            _provider.AdvertisementPacketReceived += AdvertisementOnPacketReceived;
            //Task.Run(async () => await this.connect(_ip, _port, "client connected."));
            //var t = Task.Run(async () => await this.test());

            //SQLite3 Use
            //sqlite3 = new Sqlite3Manager();
            //////String lData = locationData["prefix"] + locationData["p1"] + locationData["p2"];
            ////String mData = motionData["prefix"] + motionData["body"] + motionData["boom"] + motionData["arm"] + motionData["bucket"];
            //Task.Run(async () => await sqlite3.InitSqlite3("", ""));

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(_interval);
            _timer.Tick += SendDataToUnity;
            //_timer.Start();

            SetStatusOutput("Stopped detecting ble signal.");
            //SetDataOutput(lData, mData);
            //var t = Task.Run(async () => await SendToUnity());

            _beaconManager = new BeaconManager(_provider, async (action) =>
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { action(); });
            });
            // Subscribe to status change events of the provider
            _provider.WatcherStopped += WatcherOnStopped;
            _beaconManager.BeaconAdded += BeaconManagerOnBeaconAdded;

            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
            {
                // Make sure the status bar is visible also in the light mode on Windows 10 Mobile
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().BackgroundColor = Color.FromArgb(0, 255, 255, 255);
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().BackgroundOpacity = 1;
                Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ForegroundColor = Colors.Black;
            }

            Task.Run(async () => await initializeConfigFile());
            //Loaded += MainPage_Loaded;//(수전된 부분 0517)
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            var dialog1 = new LoginPopup();
            var result = await dialog1.ShowAsync();
        }

        private async Task initializeConfigFile()
        {

            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;

            String fileName = DateTime.Now.ToString("yyyyMMdd HHmmss") + ".txt";
            logFile = await storageFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            
            var item = await storageFolder.TryGetItemAsync("const_file.txt");
            
            if (item != null)
            {
                constFile = await storageFolder.GetFileAsync("const_file.txt");
                String constStr = await FileIO.ReadTextAsync(constFile);
                JObject constJson = JObject.Parse(constStr);
                calibs = JsonConvert.DeserializeObject<List<string>>(constJson["imuCalibration"].ToString());
                   
                await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    ConstExcavator.Text = constJson["excavator"].ToString();
                    ConstDozer.Text = constJson["dozer"].ToString();
                    ConstMotorGrader.Text = constJson["motorGrader"].ToString();
                    ConstAbsolutePoint.Text = constJson["absolutePoint"].ToString();
                    ConstBeaconPoint.Text = constJson["beaconPoint"].ToString();
                    ConstCalibration.Text = constJson["calibration"].ToString();
                    ConstIMUCalibration.Text = calibs[currentEquipIndex];
                });
            }
            else
            {
                constFile = await storageFolder.CreateFileAsync("const_file.txt", CreationCollisionOption.ReplaceExisting);
                JObject json = new JObject();
                

                json.Add("topic", "const");
                json.Add("excavator", "0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0");
                json.Add("dozer", "0\r0\r0\r0\r0\r0\r0\r0\r0");
                json.Add("motorGrader", "0\r0\r0\r0\r0\r0\r0\r0");
                json.Add("absolutePoint", "0\r0\r0\r0\r0\r0");
                json.Add("beaconPoint", "0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0");
                json.Add("calibration", "0\r0\r0\r0\r0\r0\r0\r0\r0");
                json.Add("imuCalibration", JsonConvert.SerializeObject(calibs, Formatting.None));

                String jsonStr = json.ToString();
                await FileIO.WriteTextAsync(constFile, jsonStr);
            }
        }

        private void InitializeExcavatorData()
        {
            locationData = new Dictionary<String, String>();
            locationData.Add("prefix", "00");
            locationData.Add("machine_type", "0");
            locationData.Add("machine_id", "0");
            locationData.Add("p1", "000000000000000000000000");
            locationData.Add("p2", "000000000000000000000000");

            motionData = new Dictionary<String, String>();
            motionData.Add("prefix", "00");
            motionData.Add("machine_type", "0");
            motionData.Add("machine_id", "0");
            motionData.Add("body", "000000000000");
            motionData.Add("boom", "000000000000");
            motionData.Add("arm", "000000000000");
            motionData.Add("bucket", "000000000000");

            excavator = new ExcavatorCalculator();
        }

        private void InitializeDozerData()
        {
            locationData = new Dictionary<String, String>();
            locationData.Add("prefix", "00");
            locationData.Add("machine_type", "1");
            locationData.Add("machine_id", "0");
            locationData.Add("p1", "000000000000000000000000");
            locationData.Add("p2", "000000000000000000000000");


            motionData = new Dictionary<String, String>();
            motionData.Add("prefix", "00");
            motionData.Add("machine_type", "1");
            motionData.Add("machine_id", "0");
            motionData.Add("body", "000000000000");
            motionData.Add("blade", "000000000000");
            //motionData.Add("front", "000000000000");
            motionData.Add("frontL", "000000000000");//imu 센서 추가
            motionData.Add("frontR", "000000000000");//imu 센서 추가
            motionData.Add("ultra_sonic_left", "0000");
            motionData.Add("ultra_sonic_right", "0000");

            dozer = new DozerCalculator();
        }

        private void InitializeMotorGraderData()
        {
            locationData = new Dictionary<String, String>();
            locationData.Add("prefix", "00");
            locationData.Add("machine_type", "2");
            locationData.Add("machine_id", "0");
            locationData.Add("p1", "000000000000000000");
            locationData.Add("p2", "000000000000000000");

            motionData = new Dictionary<String, String>();
            motionData.Add("prefix", "00");
            motionData.Add("machine_type", "2");
            motionData.Add("machine_id", "0");
            motionData.Add("body", "000000000000");
            motionData.Add("arm", "000000000000");
            motionData.Add("blade", "000000000000");
            motionData.Add("ultra_sonic", "0000");
            motionData.Add("angle", "0000");

            motorGrader = new MotorGraderCalculator();
        }

        private void InitializeConstUI()
        {
            calibs = new List<string>();
            calibs.Add("0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0");
            //calibs.Add("0\r0\r0\r0\r0\r0\r0\r0\r0");
            calibs.Add("0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0\r0");//도저 imu 추가에 따른 수정된 부분(0720)
            calibs.Add("0\r0\r0\r0\r0\r0\r0\r0\r0");

            EquipList.Add("굴삭기");
            EquipList.Add("도저");
            EquipList.Add("모터 그레이더");
            EquipComboBox.ItemsSource = EquipList;
            EquipComboBox.SelectedIndex = 0;
            //굴삭기
            excavatorConst = new List<KeyValue<int>>();
            excavatorConst.Add(new KeyValue<int>("const a", 0));
            excavatorConst.Add(new KeyValue<int>("const b", 0));
            excavatorConst.Add(new KeyValue<int>("const c", 0));
            excavatorConst.Add(new KeyValue<int>("const d", 0));
            excavatorConst.Add(new KeyValue<int>("const e", 0));
            excavatorConst.Add(new KeyValue<int>("const boom", 0));
            excavatorConst.Add(new KeyValue<int>("const arm", 0));
            excavatorConst.Add(new KeyValue<int>("const bucket", 0));
            excavatorConst.Add(new KeyValue<int>("bucket_a", 0));
            excavatorConst.Add(new KeyValue<int>("bucket_b", 0));
            excavatorConst.Add(new KeyValue<int>("bucket_d", 0));
            excavatorConst.Add(new KeyValue<int>("bucket_e", 0));
            excavatorConst.Add(new KeyValue<int>("bucket_angle", 0));
            excavatorConst.Add(new KeyValue<int>("const f", 0));
            excavatorConst.Add(new KeyValue<int>("const g", 0));
            excavatorConst.Add(new KeyValue<int>("const h", 0));//수신기와 장비 바닥까지의 높이 추가로 인한 추가된 부분(0714)

            for (int i = 0; i < excavatorConst.Count; i++)
            {
                ExcavatorLabel.Text += excavatorConst[i].key + "\r\n";
                ConstExcavator.Text += excavatorConst[i].value + "\r\n";
            }
            //도저
            dozerConst = new List<KeyValue<int>>();
            dozerConst.Add(new KeyValue<int>("const a", 0));
            dozerConst.Add(new KeyValue<int>("const b", 0));
            dozerConst.Add(new KeyValue<int>("const c", 0));
            dozerConst.Add(new KeyValue<int>("const d", 0));
            dozerConst.Add(new KeyValue<int>("const e", 0));
            dozerConst.Add(new KeyValue<int>("const f", 0));
            dozerConst.Add(new KeyValue<int>("const front1", 0));
            dozerConst.Add(new KeyValue<int>("const front2", 0));
            dozerConst.Add(new KeyValue<int>("const right", 0));
            dozerConst.Add(new KeyValue<int>("const down", 0));
            dozerConst.Add(new KeyValue<int>("const between", 0));
            dozerConst.Add(new KeyValue<int>("span_left", 0));
            dozerConst.Add(new KeyValue<int>("zero_left", 0));
            dozerConst.Add(new KeyValue<int>("span_right", 0));
            dozerConst.Add(new KeyValue<int>("zero_right", 0));
            dozerConst.Add(new KeyValue<int>("const g", 0));
            dozerConst.Add(new KeyValue<int>("const p4_length", 0));
            dozerConst.Add(new KeyValue<int>("const h", 0));//수신기와 장비 바닥까지의 높이 추가로 인한 추가된 부분(0714)

            for (int i = 0; i < dozerConst.Count; i++)
            {
                DozerLabel.Text += dozerConst[i].key + "\r\n";
                ConstDozer.Text += dozerConst[i].value + "\r\n";
            }
            //모터 그레이더
            motorGraderConst = new List<KeyValue<int>>();
            motorGraderConst.Add(new KeyValue<int>("const a", 0));
            motorGraderConst.Add(new KeyValue<int>("const b", 0));
            motorGraderConst.Add(new KeyValue<int>("const c", 0));
            motorGraderConst.Add(new KeyValue<int>("const d", 0));
            motorGraderConst.Add(new KeyValue<int>("const e", 0));
            motorGraderConst.Add(new KeyValue<int>("const f", 0));
            motorGraderConst.Add(new KeyValue<int>("const g", 0));
            motorGraderConst.Add(new KeyValue<int>("const rotor_z", 0));
            motorGraderConst.Add(new KeyValue<int>("const front_1", 0));
            motorGraderConst.Add(new KeyValue<int>("const front_2", 0));
            motorGraderConst.Add(new KeyValue<int>("const left", 0));
            motorGraderConst.Add(new KeyValue<int>("const down", 0));
            motorGraderConst.Add(new KeyValue<int>("const us", 0));
            motorGraderConst.Add(new KeyValue<int>("const zero", 0));

            for (int i = 0; i < motorGraderConst.Count; i++)
            {
                MotorGraderLabel.Text += motorGraderConst[i].key + "\r\n";
                ConstMotorGrader.Text += motorGraderConst[i].value + "\r\n";
            }
            //측점의 절대 좌표값
            absolutePoint = new List<KeyValue<int>>();
            absolutePoint.Add(new KeyValue<int>("측점1 x", 0));
            absolutePoint.Add(new KeyValue<int>("측점1 y", 0));
            absolutePoint.Add(new KeyValue<int>("측점1 z", 0));
            absolutePoint.Add(new KeyValue<int>("측점2 x", 0));
            absolutePoint.Add(new KeyValue<int>("측점2 y", 0));
            absolutePoint.Add(new KeyValue<int>("측점2 z", 0));

            for (int i = 0; i < absolutePoint.Count; i++)
            {
                AbsolutePointLabel.Text += absolutePoint[i].key + "\r\n";
                ConstAbsolutePoint.Text += absolutePoint[i].value + "\r\n";
            }
            //비콘의 절대 좌표값
            beaconPoint = new List<KeyValue<int>>();
            beaconPoint.Add(new KeyValue<int>("비콘1 x", 0));
            beaconPoint.Add(new KeyValue<int>("비콘1 y", 0));
            beaconPoint.Add(new KeyValue<int>("비콘1 z", 0));
            beaconPoint.Add(new KeyValue<int>("비콘2 x", 0));
            beaconPoint.Add(new KeyValue<int>("비콘2 y", 0));
            beaconPoint.Add(new KeyValue<int>("비콘2 z", 0));
            beaconPoint.Add(new KeyValue<int>("비콘3 x", 0));
            beaconPoint.Add(new KeyValue<int>("비콘3 y", 0));
            beaconPoint.Add(new KeyValue<int>("비콘3 z", 0));
            beaconPoint.Add(new KeyValue<int>("비콘4 x", 0));
            beaconPoint.Add(new KeyValue<int>("비콘4 y", 0));
            beaconPoint.Add(new KeyValue<int>("비콘4 z", 0));

            for (int i = 0; i < beaconPoint.Count; i++)
            {
                BeaconPointLabel.Text += beaconPoint[i].key + "\r\n";
                ConstBeaconPoint.Text += beaconPoint[i].value + "\r\n";
            }
            //캘리브레이션
            calibration = new List<KeyValue<int>>();
            calibration.Add(new KeyValue<int>("p1b1", 0));
            calibration.Add(new KeyValue<int>("p1b2", 0));
            calibration.Add(new KeyValue<int>("p1b3", 0));
            calibration.Add(new KeyValue<int>("p1b4", 0));
            calibration.Add(new KeyValue<int>("p2b1", 0));
            calibration.Add(new KeyValue<int>("p2b2", 0));
            calibration.Add(new KeyValue<int>("p2b3", 0));
            calibration.Add(new KeyValue<int>("p2b4", 0));

            for (int i = 0; i < calibration.Count; i++)
            {
                CalibrationLabel.Text += calibration[i].key + "\r\n";
                ConstCalibration.Text += calibration[i].value + "\r\n";
            }

            ChangedIMUCalibration(currentEquipIndex);
        }

        private void ChangedIMUCalibration(int equipIndex)
        {
            IMUCalibrationLabel.Text = "";
            ConstIMUCalibration.Text = "";

            imuCalibration = new List<KeyValue<int>>();

            //광파기 캘리브레이션

            List<int> list = new List<int>();
            list = calibs[equipIndex].Split(new string[] { "\r" }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

            if (equipIndex == 0)
            {
                //순서가 바뀜에 따른 수정된 부분(0628)
                //imuCalibration.Add(new KeyValue<int>("body_r", list[0]));
                //imuCalibration.Add(new KeyValue<int>("body_p", list[1]));
                //imuCalibration.Add(new KeyValue<int>("boom_r", list[2]));
                //imuCalibration.Add(new KeyValue<int>("boom_p", list[3]));
                //imuCalibration.Add(new KeyValue<int>("arm_r", list[4]));
                //imuCalibration.Add(new KeyValue<int>("arm_p", list[5]));
                //imuCalibration.Add(new KeyValue<int>("bucket_r", list[6]));
                //imuCalibration.Add(new KeyValue<int>("bucket_p", list[7]));
                //imuCalibration.Add(new KeyValue<int>("body_y", 0));
                //imuCalibration.Add(new KeyValue<int>("boom_y", 0));
                //imuCalibration.Add(new KeyValue<int>("arm_y", 0));
                //롤피치 반대 표시로 인한 추가된 부분(0818)
                imuCalibration.Add(new KeyValue<int>("body_r(p)", list[0]));
                imuCalibration.Add(new KeyValue<int>("body_p(r)", list[1]));
                imuCalibration.Add(new KeyValue<int>("body_y", list[2]));

                imuCalibration.Add(new KeyValue<int>("boom_r", list[3]));
                imuCalibration.Add(new KeyValue<int>("boom_p", list[4]));
                imuCalibration.Add(new KeyValue<int>("boom_y", list[5]));

                imuCalibration.Add(new KeyValue<int>("arm_r", list[6]));
                imuCalibration.Add(new KeyValue<int>("arm_p", list[7]));
                imuCalibration.Add(new KeyValue<int>("arm_y", list[8]));

                imuCalibration.Add(new KeyValue<int>("bucket_r", list[9]));
                imuCalibration.Add(new KeyValue<int>("bucket_p", list[10]));

            }
            else if (equipIndex == 1)
            {

                //순서가 바뀜에 따른 수정된 부분(0628)
                //imuCalibration.Add(new KeyValue<int>("body_r", list[0]));
                //imuCalibration.Add(new KeyValue<int>("body_p", list[1]));
                ////imuCalibration.Add(new KeyValue<int>("front_r", list[2]));
                ////imuCalibration.Add(new KeyValue<int>("front_p", list[3]));
                //imuCalibration.Add(new KeyValue<int>("frontL_r", list[2]));//imu 추가 
                //imuCalibration.Add(new KeyValue<int>("frontL_p", list[3]));//imu 추가
                //imuCalibration.Add(new KeyValue<int>("blade_r", list[4]));
                //imuCalibration.Add(new KeyValue<int>("blade_p", list[5]));
                //imuCalibration.Add(new KeyValue<int>("body_y", list[6]));
                ////imuCalibration.Add(new KeyValue<int>("front_y", 0));
                //imuCalibration.Add(new KeyValue<int>("frontL_y", list[7]));//imu 추가
                //imuCalibration.Add(new KeyValue<int>("blade_y", list[8]));
                //imuCalibration.Add(new KeyValue<int>("frontR_r", list[9]));
                //imuCalibration.Add(new KeyValue<int>("frontR_p", list[10]));
                //imuCalibration.Add(new KeyValue<int>("frontR_y", list[11]));

                imuCalibration.Add(new KeyValue<int>("body_r", list[0]));
                imuCalibration.Add(new KeyValue<int>("body_p", list[1]));
                imuCalibration.Add(new KeyValue<int>("body_y", list[2]));

                imuCalibration.Add(new KeyValue<int>("frontL_r", list[3]));//imu 추가 
                imuCalibration.Add(new KeyValue<int>("frontL_p", list[4]));//imu 추가
                imuCalibration.Add(new KeyValue<int>("frontL_y", list[5]));//imu 추가

                imuCalibration.Add(new KeyValue<int>("frontR_r", list[6]));
                imuCalibration.Add(new KeyValue<int>("frontR_p", list[7]));
                imuCalibration.Add(new KeyValue<int>("frontR_y", list[8]));

                imuCalibration.Add(new KeyValue<int>("blade_r", list[9]));
                imuCalibration.Add(new KeyValue<int>("blade_p", list[10]));
                imuCalibration.Add(new KeyValue<int>("blade_y", list[11]));

            }
            else if (equipIndex == 2)
            {
                
                imuCalibration.Add(new KeyValue<int>("body_r", list[0]));
                imuCalibration.Add(new KeyValue<int>("body_p", list[1]));
                imuCalibration.Add(new KeyValue<int>("rotor_r", list[2]));
                imuCalibration.Add(new KeyValue<int>("rotor_p", list[3]));
                imuCalibration.Add(new KeyValue<int>("blade_r", list[4]));
                imuCalibration.Add(new KeyValue<int>("blade_p", list[5]));
                imuCalibration.Add(new KeyValue<int>("body_y", 0));
                imuCalibration.Add(new KeyValue<int>("rotor_y", 0));
                imuCalibration.Add(new KeyValue<int>("blade_y", 0));

            }

            for (int i = 0; i < imuCalibration.Count; i++)
            {
                IMUCalibrationLabel.Text += imuCalibration[i].key + "\r\n";
                ConstIMUCalibration.Text += imuCalibration[i].value + "\r\n";
            }
        }


        private async Task saveConfigFile()
        {

            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                String excavator = ConstExcavator.Text;
                String dozer = ConstDozer.Text;
                String motorGrader = ConstMotorGrader.Text;
                String absolutePoint = ConstAbsolutePoint.Text;
                String beaconPoint = ConstBeaconPoint.Text;
                String calibration = ConstCalibration.Text;
                calibs[currentEquipIndex] = ConstIMUCalibration.Text;
                String imuCalibration = JsonConvert.SerializeObject(calibs);
                JObject json = new JObject();
                

                json.Add("topic", "const");
                json.Add("machine_type", currentEquipIndex);
                json.Add("machine_id", locationData["machine_id"]);
                json.Add("excavator", excavator);
                json.Add("dozer", dozer);
                json.Add("motorGrader", motorGrader);
                json.Add("absolutePoint", absolutePoint);
                json.Add("beaconPoint", beaconPoint);
                json.Add("calibration", calibration);
                json.Add("imuCalibration", imuCalibration);
                String constJson = json.ToString();
                Task.Run(async () => await FileIO.WriteTextAsync(constFile, constJson));
                Task.Run(async () => await connect(_ip, _port, constJson));
            });
        }

        private string SetText() {
            String constJson = null;


            String excavator = ConstExcavator.Text;
            String dozer = ConstDozer.Text;
            String motorGrader = ConstMotorGrader.Text;
            String absolutePoint = ConstAbsolutePoint.Text;
            String beaconPoint = ConstBeaconPoint.Text;
            String calibration = ConstCalibration.Text;
            calibs[currentEquipIndex] = ConstIMUCalibration.Text;
            String imuCalibration = JsonConvert.SerializeObject(calibs);
            JObject json = new JObject();
            json.Add("topic", "const");
            json.Add("machine_type", currentEquipIndex);
            json.Add("machine_id", locationData["machine_id"]);
            json.Add("excavator", excavator);
            json.Add("dozer", dozer);
            json.Add("motorGrader", motorGrader);
            json.Add("absolutePoint", absolutePoint);
            json.Add("beaconPoint", beaconPoint);
            json.Add("calibration", calibration);
            json.Add("imuCalibration", imuCalibration);
            constJson = json.ToString();
            return constJson;
        }

        private bool Validation() {
            bool isChecked = false;



            return isChecked;
        }

        private void UpdateExcavatorResult(String lData, String mData)
        {
            //SetDataOutput(lData, mData);
            //수신받은 데이터
            LocationDataText = lData;
            MotionDataText = mData;
            //상대 좌표계(끝단, 위치, 수신기)
            RelativePointEnd.Text = excavator.ExcavatorData.RelativePoint.EndPoint;
            RelativePointLocation.Text = excavator.ExcavatorData.RelativePoint.Position;
            RelativePointReceiver.Text = excavator.ExcavatorData.RelativePoint.Receiver;
            //절대 좌표계(끝단, 위치, 수신기)
            AbsolutePointEnd.Text = excavator.ExcavatorData.AbsolutePoint.EndPoint;
            AbsolutePointLocation.Text = excavator.ExcavatorData.AbsolutePoint.Position;
            AbsolutePointReceiver.Text = excavator.ExcavatorData.AbsolutePoint.Receiver;

            //거리값(e)
            ExcavatorP1Distance.Text = excavator.ExcavatorData.Receiver.P1.DistanceB1 + " " + excavator.ExcavatorData.Receiver.P1.DistanceB2 + " " + excavator.ExcavatorData.Receiver.P1.DistanceB3 + " " + excavator.ExcavatorData.Receiver.P1.DistanceB4;
            ExcavatorP2Distance.Text = excavator.ExcavatorData.Receiver.P2.DistanceB1 + " " + excavator.ExcavatorData.Receiver.P2.DistanceB2 + " " + excavator.ExcavatorData.Receiver.P2.DistanceB3 + " " + excavator.ExcavatorData.Receiver.P2.DistanceB4;
            //굴삭기 IMU
            ExcavatorIMUBody.Text = excavator.ExcavatorData.IMUSensor.Body.X + " " + excavator.ExcavatorData.IMUSensor.Body.Y + " " + excavator.ExcavatorData.IMUSensor.Body.Z;
            ExcavatorIMUBoom.Text = excavator.ExcavatorData.IMUSensor.Boom.X + " " + excavator.ExcavatorData.IMUSensor.Boom.Y + " " + excavator.ExcavatorData.IMUSensor.Boom.Z;
            ExcavatorIMUArm.Text = excavator.ExcavatorData.IMUSensor.Arm.X + " " + excavator.ExcavatorData.IMUSensor.Arm.Y + " " + excavator.ExcavatorData.IMUSensor.Arm.Z;
            ExcavatorIMUBucket.Text = excavator.ExcavatorData.IMUSensor.Bucket.X + " " + excavator.ExcavatorData.IMUSensor.Bucket.Y + " " + excavator.ExcavatorData.IMUSensor.Bucket.Z;
            //상대 좌표계
            ExcavatorRelativePoint1.Text = excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint1.X + " " + excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint1.Y + " " + excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint1.Z;
            ExcavatorRelativePoint2.Text = excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint2.X + " " + excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint2.Y + " " + excavator.ExcavatorData.RelativeCoordinate.MeasuringPoint2.Z;
            

            ExcavatorRelativeBeacon1.Text = excavator.ExcavatorData.RelativeCoordinate.Beacon1.X + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon1.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon1.Z;
            ExcavatorRelativeBeacon2.Text = excavator.ExcavatorData.RelativeCoordinate.Beacon2.X + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon2.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon2.Z;
            ExcavatorRelativeBeacon3.Text = excavator.ExcavatorData.RelativeCoordinate.Beacon3.X + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon3.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon3.Z;
            ExcavatorRelativeBeacon4.Text = excavator.ExcavatorData.RelativeCoordinate.Beacon4.X + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon4.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Beacon4.Z;

            ExcavatorRelativeCenter.Text = excavator.ExcavatorData.RelativeCoordinate.Center.X + " " + excavator.ExcavatorData.RelativeCoordinate.Center.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Center.Z;
            ExcavatorRelativeHinge.Text = excavator.ExcavatorData.RelativeCoordinate.Hinge.X + " " + excavator.ExcavatorData.RelativeCoordinate.Hinge.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Hinge.Z;
            ExcavatorRelativeBoom.Text = excavator.ExcavatorData.RelativeCoordinate.Boom.X + " " + excavator.ExcavatorData.RelativeCoordinate.Boom.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Boom.Z;
            ExcavatorRelativeArm.Text = excavator.ExcavatorData.RelativeCoordinate.Arm.X + " " + excavator.ExcavatorData.RelativeCoordinate.Arm.Y + " " + excavator.ExcavatorData.RelativeCoordinate.Arm.Z;
            //절대 좌표계
            ExcavatorAbsoluteCenter.Text = excavator.ExcavatorData.AbsoluteCoordinate.Center.X + " " + excavator.ExcavatorData.AbsoluteCoordinate.Center.Y + " " + excavator.ExcavatorData.AbsoluteCoordinate.Center.Z;
            ExcavatorAbsoluteHinge.Text = excavator.ExcavatorData.AbsoluteCoordinate.Hinge.X + " " + excavator.ExcavatorData.AbsoluteCoordinate.Hinge.Y + " " + excavator.ExcavatorData.AbsoluteCoordinate.Hinge.Z;
            ExcavatorAbsoluteBoom.Text = excavator.ExcavatorData.AbsoluteCoordinate.Boom.X + " " + excavator.ExcavatorData.AbsoluteCoordinate.Boom.Y + " " + excavator.ExcavatorData.AbsoluteCoordinate.Boom.Z;
            ExcavatorAbsoluteArm.Text = excavator.ExcavatorData.AbsoluteCoordinate.Arm.X + " " + excavator.ExcavatorData.AbsoluteCoordinate.Arm.Y + " " + excavator.ExcavatorData.AbsoluteCoordinate.Arm.Z;

            //사이각
            ExcavatorAngleBodyBoom.Text = excavator.ExcavatorData.Angle.BodyBoom;
            ExcavatorAngleBoomArm.Text = excavator.ExcavatorData.Angle.BoomArm;
            ExcavatorAngleArmBucket.Text = excavator.ExcavatorData.Angle.ArmBucket;
            //ETC
            ExcavatorOriginalDistance.Text = excavator.ExcavatorData.Original.Distance;
            ExcavatorOriginalIMU.Text = excavator.ExcavatorData.Original.IMUSensor;
            ExcavatorUnityOrientation.Text = excavator.ExcavatorData.Angle.UnityOrientation;
        }

        private void UpdateDozerResult(String lData, String mData)
        {
            //SetDataOutput(lData, mData);
            //수신받은 데이터
            LocationDataText = lData;
            MotionDataText = mData;
            //상대 좌표계(끝단, 위치, 수신기)
            RelativePointEnd.Text = dozer.DozerData.RelativePoint.EndPoint;
            RelativePointLocation.Text = dozer.DozerData.RelativePoint.Position;
            RelativePointReceiver.Text = dozer.DozerData.RelativePoint.Receiver;
            //절대 좌표계(끝단, 위치, 수신기)
            AbsolutePointEnd.Text = dozer.DozerData.AbsolutePoint.EndPoint;
            AbsolutePointLocation.Text = dozer.DozerData.AbsolutePoint.Position;
            AbsolutePointReceiver.Text = dozer.DozerData.AbsolutePoint.Receiver;

            //거리값(e)
            DozerP1Distance.Text = dozer.DozerData.Receiver.P1.DistanceB1 + " " + dozer.DozerData.Receiver.P1.DistanceB2 + " " + dozer.DozerData.Receiver.P1.DistanceB3 + " " + dozer.DozerData.Receiver.P1.DistanceB4;
            DozerP2Distance.Text = dozer.DozerData.Receiver.P2.DistanceB1 + " " + dozer.DozerData.Receiver.P2.DistanceB2 + " " + dozer.DozerData.Receiver.P2.DistanceB3 + " " + dozer.DozerData.Receiver.P2.DistanceB4;
            //도저 IMU
            DozerIMUBody.Text = dozer.DozerData.IMUSensor.Body.X + " " + dozer.DozerData.IMUSensor.Body.Y + " " + dozer.DozerData.IMUSensor.Body.Z;
            DozerIMUFront.Text = dozer.DozerData.IMUSensor.Front.X + " " + dozer.DozerData.IMUSensor.Front.Y + " " + dozer.DozerData.IMUSensor.Front.Z;
            DozerIMUBlade.Text = dozer.DozerData.IMUSensor.Blade.X + " " + dozer.DozerData.IMUSensor.Blade.Y + " " + dozer.DozerData.IMUSensor.Blade.Z + "(" + dozer.DozerData.IMUSensor.Blade.Z2 + ")";
            //상대 좌표계
            DozerRelativePoint1.Text = dozer.DozerData.RelativeCoordinate.MeasuringPoint1.X + " " + dozer.DozerData.RelativeCoordinate.MeasuringPoint1.Y + " " + dozer.DozerData.RelativeCoordinate.MeasuringPoint1.Z;
            DozerRelativePoint2.Text = dozer.DozerData.RelativeCoordinate.MeasuringPoint2.X + " " + dozer.DozerData.RelativeCoordinate.MeasuringPoint2.Y + " " + dozer.DozerData.RelativeCoordinate.MeasuringPoint2.Z;

            DozerRelativeBeacon1.Text = dozer.DozerData.RelativeCoordinate.Beacon1.X + " " + dozer.DozerData.RelativeCoordinate.Beacon1.Y + " " + dozer.DozerData.RelativeCoordinate.Beacon1.Z;
            DozerRelativeBeacon2.Text = dozer.DozerData.RelativeCoordinate.Beacon2.X + " " + dozer.DozerData.RelativeCoordinate.Beacon2.Y + " " + dozer.DozerData.RelativeCoordinate.Beacon2.Z;
            DozerRelativeBeacon3.Text = dozer.DozerData.RelativeCoordinate.Beacon3.X + " " + dozer.DozerData.RelativeCoordinate.Beacon3.Y + " " + dozer.DozerData.RelativeCoordinate.Beacon3.Z;
            DozerRelativeBeacon4.Text = dozer.DozerData.RelativeCoordinate.Beacon4.X + " " + dozer.DozerData.RelativeCoordinate.Beacon4.Y + " " + dozer.DozerData.RelativeCoordinate.Beacon4.Z;

            DozerRelativeCenter.Text = dozer.DozerData.RelativeCoordinate.Center.X + " " + dozer.DozerData.RelativeCoordinate.Center.Y + " " + dozer.DozerData.RelativeCoordinate.Center.Z;
            DozerRelativeHinge.Text = dozer.DozerData.RelativeCoordinate.Hinge.X + " " + dozer.DozerData.RelativeCoordinate.Hinge.Y + " " + dozer.DozerData.RelativeCoordinate.Hinge.Z;
            DozerRelativeBladeCenter.Text = dozer.DozerData.RelativeCoordinate.BladeCenter.X + " " + dozer.DozerData.RelativeCoordinate.BladeCenter.Y + " " + dozer.DozerData.RelativeCoordinate.BladeCenter.Z;
            //절대 좌표계
            DozerAbsoluteCenter.Text = dozer.DozerData.AbsoluteCoordinate.Center.X + " " + dozer.DozerData.AbsoluteCoordinate.Center.Y + " " + dozer.DozerData.AbsoluteCoordinate.Center.Z;
            DozerAbsoluteHinge.Text = dozer.DozerData.AbsoluteCoordinate.Hinge.X + " " + dozer.DozerData.AbsoluteCoordinate.Hinge.Y + " " + dozer.DozerData.AbsoluteCoordinate.Hinge.Z;
            DozerAbsoluteBladeCenter.Text = dozer.DozerData.AbsoluteCoordinate.BladeCenter.X + " " + dozer.DozerData.AbsoluteCoordinate.BladeCenter.Y + " " + dozer.DozerData.AbsoluteCoordinate.BladeCenter.Z;

            //ETC
            DozerOriginalDistance.Text = dozer.DozerData.Original.Distance;
            DozerOriginalIMU.Text = dozer.DozerData.Original.IMUSensor;
            DozerUnityOrientationBody.Text = dozer.DozerData.UnityOrientationBody;
            DozerUnityOrientationFront.Text = dozer.DozerData.UnityOrientationFront;
            DozerUnityOrientationBlade.Text = dozer.DozerData.UnityOrientationBlade;
        }

        private void UpdateMotorGraderResult(String lData, String mData)
        {
            //SetDataOutput(lData, mData);
            //수신받은 데이터
            LocationDataText = lData;
            MotionDataText = mData;
            //상대 좌표계(끝단, 위치, 수신기)
            RelativePointEnd.Text = motorGrader.MotorGraderData.RelativePoint.EndPoint;
            RelativePointLocation.Text = motorGrader.MotorGraderData.RelativePoint.Position;
            RelativePointReceiver.Text = motorGrader.MotorGraderData.RelativePoint.Receiver;
            //절대 좌표계(끝단, 위치, 수신기)
            AbsolutePointEnd.Text = motorGrader.MotorGraderData.AbsolutePoint.EndPoint;
            AbsolutePointLocation.Text = motorGrader.MotorGraderData.AbsolutePoint.Position;
            AbsolutePointReceiver.Text = motorGrader.MotorGraderData.AbsolutePoint.Receiver;

            //거리값(e)
            MGP1Distance.Text = motorGrader.MotorGraderData.Receiver.P1.DistanceB1 + " " + motorGrader.MotorGraderData.Receiver.P1.DistanceB2 + " " + motorGrader.MotorGraderData.Receiver.P1.DistanceB3 + " " + motorGrader.MotorGraderData.Receiver.P1.DistanceB4;
            MGP2Distance.Text = motorGrader.MotorGraderData.Receiver.P2.DistanceB1 + " " + motorGrader.MotorGraderData.Receiver.P2.DistanceB2 + " " + motorGrader.MotorGraderData.Receiver.P2.DistanceB3 + " " + motorGrader.MotorGraderData.Receiver.P2.DistanceB4;
            //모터 그레이더 IMU
            MGIMUBody.Text = motorGrader.MotorGraderData.IMUSensor.Body.X + " " + motorGrader.MotorGraderData.IMUSensor.Body.Y + " " + motorGrader.MotorGraderData.IMUSensor.Body.Z;
            MGIMURotor.Text = motorGrader.MotorGraderData.IMUSensor.Arm.X + " " + motorGrader.MotorGraderData.IMUSensor.Arm.Y + " " + motorGrader.MotorGraderData.IMUSensor.Arm.Z;
            MGIMUBlade.Text = motorGrader.MotorGraderData.IMUSensor.Blade.X + " " + motorGrader.MotorGraderData.IMUSensor.Blade.Y + " " + motorGrader.MotorGraderData.IMUSensor.Blade.Z + "(" + motorGrader.MotorGraderData.IMUSensor.Blade.Z2 + ")";
            //상대 좌표계
            MGRelativePoint1.Text = motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint3.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint3.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint3.Z;
            MGRelativePoint2.Text = motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint2.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint2.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.MeasuringPoint2.Z;

            MGRelativeBeacon1.Text = motorGrader.MotorGraderData.RelativeCoordinate.Beacon1.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon1.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon1.Z;
            MGRelativeBeacon2.Text = motorGrader.MotorGraderData.RelativeCoordinate.Beacon2.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon2.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon2.Z;
            MGRelativeBeacon3.Text = motorGrader.MotorGraderData.RelativeCoordinate.Beacon3.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon3.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon3.Z;
            MGRelativeBeacon4.Text = motorGrader.MotorGraderData.RelativeCoordinate.Beacon4.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon4.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Beacon4.Z;

            MGRelativeCenter.Text = motorGrader.MotorGraderData.RelativeCoordinate.Center.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Center.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Center.Z;
            MGRelativeHinge.Text = motorGrader.MotorGraderData.RelativeCoordinate.Hinge.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.Hinge.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.Hinge.Z;
            MGRelativeArmEnd.Text = motorGrader.MotorGraderData.RelativeCoordinate.ArmEnd.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.ArmEnd.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.ArmEnd.Z;
            MGRelativeBladeCenter.Text = motorGrader.MotorGraderData.RelativeCoordinate.BladeCenter.X + " " + motorGrader.MotorGraderData.RelativeCoordinate.BladeCenter.Y + " " + motorGrader.MotorGraderData.RelativeCoordinate.BladeCenter.Z;

            //ETC
            MGOriginalDistance.Text = motorGrader.MotorGraderData.Original.Distance;
            MGOriginalSensor.Text = motorGrader.MotorGraderData.Original.IMUSensor;
            MGUnityOrientationBody.Text = motorGrader.MotorGraderData.UnityOrientationBody;
            MGUnityOrientationArm.Text = motorGrader.MotorGraderData.UnityOrientationArm;
            MGUnityOrientationBlade.Text = motorGrader.MotorGraderData.UnityOrientationBlade;
        }

        //0809 임시 Z값 
        public String tempZValue = "0";
        public int arduinoCount = 0;

        private async Task updateLightWave()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile lightWaveFile = await storageFolder.GetFileAsync("light_wave.txt");
            tempZValue = await FileIO.ReadTextAsync(lightWaveFile);
        }

        private async Task updateArduino()
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile lightWaveFile = await storageFolder.GetFileAsync("arduino_data.txt");
            tempZValue = await FileIO.ReadTextAsync(lightWaveFile);
        }

        private async Task arduinoSend(JObject arduinoSend)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            var item = await storageFolder.TryGetItemAsync("arduino_send.txt");
            StorageFile arduinoFile;
            if (item == null)
            {
                arduinoFile = await storageFolder.CreateFileAsync("arduino_send.txt", CreationCollisionOption.ReplaceExisting);

            }
            arduinoFile = await storageFolder.GetFileAsync("arduino_send.txt");
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Task.Run(async () => await FileIO.WriteTextAsync(arduinoFile, arduinoSend.ToString()));
            });
        }

        private void SendDataToUnity(object sender, object e)
        {

            String lData = "";
            String mData = "";
            String receiver = "";

            //Task.Run(async () => await updateLightWave());
            Task.Run(async () => await updateArduino());
            if (tempZValue == "")
            {
                tempZValue = "0";
            }
            double tempZ = 0.0d;
            try
            {
                tempZ = Double.Parse(tempZValue);
            }catch(Exception ex)
            {
                tempZ = 0.0d;
            }
            
            //Debug.WriteLine("Light Wave : " + lightWave);//광파기 z값 디버깅 부분 - 추가된 부분(0811)

            if (currentEquipIndex == 0)
            {
                lData = locationData["prefix"] + locationData["p1"] + locationData["p2"];
                mData = motionData["prefix"] + motionData["body"] + motionData["boom"] + motionData["arm"] + motionData["bucket"];
                //arduino add
                if (excavator.arduinoSendCounter == 0)
                {
                    JObject jsonSend = new JObject();
                    jsonSend.Add("topic", "start");
                    jsonSend.Add("data", excavator.arduinoSend);
                    Task.Run(async () => await arduinoSend(jsonSend));
                }
                else
                {
                    //System.Threading.Tasks.Task.Delay(500);
                    if(arduinoCount == 10)
                    {
                        JObject jsonSend = new JObject();
                        jsonSend.Add("topic", "stop");
                        jsonSend.Add("data", excavator.arduinoSend);
                        Task.Run(async () => await arduinoSend(jsonSend));
                        arduinoCount = 0;
                    }
                    arduinoCount++;
                }

                excavator.ExcavatorDataReceived(lData, mData, tempZ);
                UpdateExcavatorResult(lData, mData);
                //Debug.WriteLine("P440 : " + lData + " IMU : " + mData);

                receiver = excavator.ExcavatorData.RelativePoint.Receiver;
            }
            else if (currentEquipIndex == 1)
            {
                lData = locationData["prefix"] + locationData["p1"] + locationData["p2"];
                //mData = motionData["prefix"] + motionData["body"] + motionData["blade"] + motionData["front"] + motionData["ultra_sonic_left"] + motionData["ultra_sonic_right"];
                //mData = motionData["prefix"] + motionData["body"] + motionData["blade"] + motionData["frontL"] + motionData["ultra_sonic_left"] + motionData["ultra_sonic_right"];//imu 추가
                mData = motionData["prefix"] + motionData["body"] + motionData["blade"] + motionData["frontL"] + motionData["frontR"] + motionData["ultra_sonic_left"] + motionData["ultra_sonic_right"];//imu 추가

                dozer.DozerDataReceived(lData, mData);
                UpdateDozerResult(lData, mData);
                //Debug.WriteLine("P440 : " + lData + " IMU : " + mData);

                receiver = dozer.DozerData.RelativePoint.Receiver;
            }
            else if (currentEquipIndex == 2)
            {
                lData = locationData["prefix"] + locationData["p1"] + locationData["p2"];
                mData = motionData["prefix"] + motionData["body"] + motionData["arm"] + motionData["blade"] + motionData["ultra_sonic"] + motionData["angle"];

                motorGrader.MotorGraderDataReceived(lData, mData);
                UpdateMotorGraderResult(lData, mData);
                //Debug.WriteLine("P440 : " + lData + " IMU : " + mData);

                receiver = motorGrader.MotorGraderData.RelativePoint.Receiver;
            }

            JObject json = new JObject();
            json.Add("topic", "data");
            json.Add("location", lData);
            json.Add("motion", mData);
            json.Add("machine_type", locationData["machine_type"]);
            json.Add("machine_id", locationData["machine_id"]);
            json.Add("light_wave", tempZValue);

            String now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //String nowms = DateTime.Now.Millisecond.ToString();
            String writer = now + " P440 : " + lData + " IMU : " + mData + " " + receiver;
            if (!isConfig)
            {
                Task.Run(async () => await WriteLogging(writer));
                Task.Run(async () => await connect(_ip, _port, json.ToString()));
            }

            //var t = Task.Run(async () => await this.send(data));
            //sqlite3.UpdateSensorPayload(lData, mData);
        }


        int CheckConstLength(string vals) {
            return vals.Split(new string[] { "\r" }, StringSplitOptions.RemoveEmptyEntries).Count();
        }


        private void BLESwitchToggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    SetStatusOutput("Started detecting ble signal.");
                    
                    //check input text length
                    if (excavatorConst.Count != CheckConstLength(ConstExcavator.Text) ||
                        dozerConst.Count != CheckConstLength(ConstDozer.Text) ||
                        motorGraderConst.Count != CheckConstLength(ConstMotorGrader.Text) ||
                        absolutePoint.Count != CheckConstLength(ConstAbsolutePoint.Text) ||
                        beaconPoint.Count != CheckConstLength(ConstBeaconPoint.Text) ||
                        calibration.Count != CheckConstLength(ConstCalibration.Text) ||
                        imuCalibration.Count != CheckConstLength(ConstIMUCalibration.Text)) {

                        MessageDialog msg = new MessageDialog("Fail to save selected equipment data. Please check empty fields!");
                        msg.ShowAsync();
                        toggleSwitch.IsOn = false;
                        return;

                    }

                    if (currentEquipIndex == 0)
                    {
                        excavator.InitExcavatorCalcData(ConstCalibration, ConstIMUCalibration, ConstAbsolutePoint, ConstBeaconPoint, ConstExcavator);
                    }
                    else if (currentEquipIndex == 1)
                    {
                        dozer.InitDozerCalcData(ConstCalibration, ConstIMUCalibration, ConstAbsolutePoint, ConstBeaconPoint, ConstDozer);
                    }
                    else if (currentEquipIndex == 2)
                    {
                        motorGrader.InitMotorGraderCalcData(ConstCalibration, ConstIMUCalibration, ConstAbsolutePoint, ConstBeaconPoint, ConstMotorGrader);
                    }
                    if (isConfig == true)
                    {

                        Task.Run(async () => await saveConfigFile());
                        isConfig = false;
                    }
                    _timer.Start();
                }
                else
                {
                    SetStatusOutput("Stopped detecting ble signal.");
                    _timer.Stop();
                    isConfig = true;
                }
            }
        }

        private void EquipComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            currentEquipIndex = comboBox.SelectedIndex;
            ChangedIMUCalibration(currentEquipIndex);
            if (comboBox.SelectedIndex == 0)
            {
                ConstExcavator.IsEnabled = true;
                ConstDozer.IsEnabled = false;
                ConstMotorGrader.IsEnabled = false;
                InitializeExcavatorData();
            }
            else if (comboBox.SelectedIndex == 1)
            {
                ConstExcavator.IsEnabled = false;
                ConstDozer.IsEnabled = true;
                ConstMotorGrader.IsEnabled = false;
                InitializeDozerData();
            }
            else if (comboBox.SelectedIndex == 2)
            {
                ConstExcavator.IsEnabled = false;
                ConstDozer.IsEnabled = false;
                ConstMotorGrader.IsEnabled = true;
                InitializeMotorGraderData();
            }
            EquipPivot.SelectedIndex = comboBox.SelectedIndex;
        }

        private void AdvertisementOnPacketReceived(object sender, BLEAdvertisementPacketArgs e)
        {
            //currentEquipIndex 0 : 굴삭기, 1 : 도저 : 2 : 모터 그레이더
            //String bluetoothAddress = e.Data.BluetoothAddress.ToString("X");
            if (currentEquipIndex == 0)
            {
                ReceivedExcavatorData(e);
            }
            else if (currentEquipIndex == 1)
            {
                RecivedDozerData(e);
            }
            else if (currentEquipIndex == 2)
            {
                RecivedMotorGraderData(e);
            }
        }

        private void ReceivedExcavatorData(BLEAdvertisementPacketArgs e)
        {
            //P440
            if (e.Data.Advertisement.ManufacturerData.Count == 0)
            {
                for (int i = 0; i < e.Data.Advertisement.DataSections.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.DataSections[i].Data);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;

                    //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " >> " + dataString);

                    if (dataLength == 26)//비컨 4개로 인한 추가
                    //if (dataLength == 21)
                    {
                        //dataString = dataString.Replace("\u0002\u0015", "");
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1); 
                        String miData = dataString.Substring(2, 1);
                        //String beaconData = dataString.Substring(3, 18);
                        string beaconData = dataString.Substring(2, 24);//비컨 4개로 인한 추가
                        String prefix = (int)Const.DataType.LOCATION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "0")
                        {
                            locationData["prefix"] = prefix;
                            locationData["machine_type"] = mtData;
                            locationData["machine_id"] = miData;
                            //P1 Data
                            if (siData == "0")
                            {
                                //if (beaconData != "000000000000000000")
                                if (beaconData != "000000000000000000000000")//비컨 4개로 인한 추가
                                {
                                    locationData["p1"] = beaconData;

                                }
                            }
                            //P2 Data
                            else if (siData == "1")
                            {

                                //if (beaconData != "000000000000000000")
                                if (beaconData != "000000000000000000000000")//비컨 4개로 인한 추가된
                                {
                                    locationData["p2"] = beaconData;
                                    //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + DateTime.Now.Millisecond + " >> " + beaconData);
                                }
                            }
                            else
                            {
                                Debug.WriteLine("Data Error!");
                            }
                        }

                    }
                }
            }
            //IMU
            else
            {
                for (int i = 0; i < e.Data.Advertisement.ManufacturerData.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.ManufacturerData[i].Data);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;

                    if (dataLength == 15)
                    {
                        //mt : 기계종류, si : 센서종류, mi : 기계번호(몇개 대수)
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String sensorData = dataString.Substring(3, 12);
                        String prefix = (int)Const.DataType.MOTION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "0")
                        {
                            motionData["prefix"] = prefix;
                            motionData["machine_type"] = mtData;
                            motionData["machine_id"] = miData;
                            //Body
                            if (siData == "2")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["body"] = sensorData;
                                }
                            }
                            //Boom
                            else if (siData == "3")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["boom"] = sensorData;
                                }
                            }
                            //Arm
                            else if (siData == "4")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["arm"] = sensorData;
                                }
                            }
                            //Bucket
                            else if (siData == "5")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["bucket"] = sensorData;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RecivedDozerData(BLEAdvertisementPacketArgs e)
        {
            //P440
            if (e.Data.Advertisement.ManufacturerData.Count == 0)
            //if(true)
            {
                for (int i = 0; i < e.Data.Advertisement.DataSections.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.DataSections[i].Data);
                    //Debug.WriteLine("send" + dataString);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;
                    //Debug.WriteLine("p440 length : " + dataLength);

                    //if (dataLength == 21)
                    if (dataLength == 26)//비컨 4개로 인한 추가
                    //if (dataLength == 25)//데카웨이버
                    {
                        //----------------------데카웨이브로 할때-------------
                        //String mtData = dataString.Substring(4, 1);
                        //String siData = dataString.Substring(5, 1);
                        //String miData = dataString.Substring(6, 1);
                        //String beaconData = dataString.Substring(7, 18);
                        //-----------------------------------------------------

                        //dataString = dataString.Replace("\u0002\u0015", "");

                        //----------------------p440으로 할때-------------
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        //String beaconData = dataString.Substring(3, 18);
                        string beaconData = dataString.Substring(2, 24);//비컨 4개로 인한 추가
                        //-----------------------------------------------------

                        String prefix = (int)Const.DataType.LOCATION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "1")
                        {
                            locationData["prefix"] = prefix;
                            locationData["machine_type"] = mtData;
                            locationData["machine_id"] = miData;
                            //P1 Data
                            if (siData == "0")
                            {

                                //if (beaconData != "000000000000000000")
                                if (beaconData != "000000000000000000000000")//비컨 4개로 인한 추가
                                {
                                    locationData["p1"] = beaconData;
                                }
                            }
                            //P2 Data
                            else if (siData == "1")
                            {
                                //if (beaconData != "000000000000000000")
                                if (beaconData != "000000000000000000000000")//비컨 4개로 인한 추가
                                {
                                    locationData["p2"] = beaconData;
                                    //Debug.WriteLine(beaconData);
                                }
                            }
                            else
                            {
                                Debug.WriteLine("Data Error!");
                            }
                        }

                    }
                }
            }
            //IMU
            else
            {
                for (int i = 0; i < e.Data.Advertisement.ManufacturerData.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.ManufacturerData[i].Data);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;

                    //Debug.WriteLine("imu length : " + dataLength);

                    if (dataLength == 15)
                    {
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String sensorData = dataString.Substring(3, 12);
                        String prefix = (int)Const.DataType.MOTION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "1")
                        {
                            motionData["prefix"] = prefix;
                            motionData["machine_type"] = mtData;
                            motionData["machine_id"] = miData;
                            //Body
                            if (siData == "2")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["body"] = sensorData;
                                }
                            }
                            //Blade
                            else if (siData == "3")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["blade"] = sensorData;
                                }
                            }
                            //Front(L)
                            else if (siData == "9")
                            {
                                if (sensorData != "000000000000")
                                {
                                    //motionData["front"] = sensorData;
                                    motionData["frontL"] = sensorData;//imu 추가로 인한 추가
                                }
                            }
                            //Front(R) - imu 추가로 인한 추가
                            else if (siData == "8")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["frontR"] = sensorData;
                                    //Debug.WriteLine("imu : " + sensorData);
                                }
                            }
                        }
                    }
                    else if (dataLength == 7)
                    {
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String sensorData = dataString.Substring(3, 4);
                        String prefix = (int)Const.DataType.MOTION + mtData;
                        if (mtData == "1")
                        {
                            // UltraSonic Left
                            if (siData == "4")
                            {
                                if (sensorData != "0000")
                                {
                                    motionData["ultra_sonic_left"] = sensorData;
                                }
                            }
                            // UltraSonic Right
                            else if (siData == "5")
                            {
                                motionData["ultra_sonic_right"] = sensorData;
                            }
                        }
                    }
                }
            }
        }

        private void RecivedMotorGraderData(BLEAdvertisementPacketArgs e)
        {
            //P440
            if (e.Data.Advertisement.ManufacturerData.Count == 0)
            {
                for (int i = 0; i < e.Data.Advertisement.DataSections.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.DataSections[i].Data);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;
                    //Debug.WriteLine("p440 length : " + dataLength);
                    if (dataLength == 21)
                    {
                        //dataString = dataString.Replace("\u0002\u0015", "");
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String beaconData = dataString.Substring(3, 18);
                        String prefix = (int)Const.DataType.LOCATION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "2")
                        {
                            locationData["prefix"] = prefix;
                            locationData["machine_type"] = mtData;
                            locationData["machine_id"] = miData;
                            //P1 Data
                            if (siData == "0")
                            {
                                if (beaconData != "000000000000000000")
                                {
                                    locationData["p1"] = beaconData;
                                    //Console.Write(System.DateTime.Now.Millisecond.ToString());

                                    //Debug.WriteLine(System.DateTime.Now.Millisecond.ToString());
                                    //Debug.WriteLine(System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "." + System.DateTime.Now.Millisecond.ToString("00#"));
                                }
                            }
                            //P2 Data
                            else if (siData == "1")
                            {
                                if (beaconData != "000000000000000000")
                                {
                                    locationData["p2"] = beaconData;
                                }
                            }
                            else
                            {
                                Debug.WriteLine("Data Error!");
                            }
                        }

                    }
                }
            }
            //IMU
            else
            {
                for (int i = 0; i < e.Data.Advertisement.ManufacturerData.Count; i++)
                {
                    String dataString = Encoding.ASCII.GetString(e.Data.Advertisement.ManufacturerData[i].Data);
                    dataString = dataString.Replace("-", "");
                    int dataLength = dataString.Length;
                    //Debug.WriteLine("imu length : " + dataLength);
                    if (dataLength == 15)
                    {
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String sensorData = dataString.Substring(3, 12);
                        String prefix = (int)Const.DataType.MOTION + mtData;
                        //mtData? 0: Backhoe, 1: Dozer, 2: Motorgrader
                        if (mtData == "2")
                        {
                            motionData["prefix"] = prefix;
                            motionData["machine_type"] = mtData;
                            motionData["machine_id"] = miData;
                            //Body
                            if (siData == "2")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["body"] = sensorData;
                                }
                            }
                            //Arm
                            else if (siData == "3")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["arm"] = sensorData;
                                }
                            }
                            //Blade
                            else if (siData == "4")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["blade"] = sensorData;
                                }
                            }
                            //
                            else if (siData == "9")
                            {
                                if (sensorData != "000000000000")
                                {
                                    motionData["front"] = sensorData;
                                }
                            }
                        }
                    }
                    else if (dataLength == 7)
                    {
                        String mtData = dataString.Substring(0, 1);
                        String siData = dataString.Substring(1, 1);
                        String miData = dataString.Substring(2, 1);
                        String sensorData = dataString.Substring(3, 4);
                        String prefix = (int)Const.DataType.MOTION + mtData;

                        if (mtData == "2")
                        {
                            // UltraSonic
                            if (siData == "5")
                            {
                                if (sensorData != "0000")
                                {
                                    motionData["ultra_sonic"] = sensorData;
                                }
                            }
                            // Angle
                            else if (siData == "A")
                            {
                                if (sensorData != "0000")
                                {
                                    motionData["angle"] = sensorData;
                                }
                            }
                        }
                    }
                }
            }
        }

        private async Task WriteLogging(String writeData)
        {
            StorageFile file = logFile;
            if (file != null)
            {
                string userContent = writeData;
                if (!String.IsNullOrEmpty(userContent))
                {
                    //IBuffer buffer = GetBufferFromString(userContent);
                    await FileIO.AppendTextAsync(file, userContent + Environment.NewLine);
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _resourceLoader = ResourceLoader.GetForCurrentView();
            _dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            _beaconManager.Start();

            if (_provider.WatcherStatus == BLEAdvertisementWatcherStatusCodes.Started)
            {
                SetStatusOutput(_resourceLoader.GetString("WatchingForBeacons"));
            }
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            SetStatusOutput(_resourceLoader.GetString("StoppedWatchingBeacons"));
            _beaconManager.Stop();
            _restartingBeaconWatch = false;
        }

        #region Bluetooth Beacons
        /// <summary>
        /// Method demonstrating how to handle individual new beacons found by the manager.
        /// This event will only be invoked once, the very first time a beacon is discovered.
        /// For more fine-grained status updates, subscribe to changes of the ObservableCollection in
        /// BeaconManager.BluetoothBeacons (_beaconManager).
        /// To handle all individual received Bluetooth packets in your main app and outside of the
        /// library, subscribe to AdvertisementPacketReceived event of the IBluetoothPacketProvider
        /// (_provider).
        /// </summary>
        /// <param name="sender">Reference to the sender instance of the event.</param>
        /// <param name="beacon">Beacon class instance containing all known and parsed information about
        /// the Bluetooth beacon.</param>
        private void BeaconManagerOnBeaconAdded(object sender, Beacon beacon)
        {

            Debug.WriteLine("\nBeacon: " + beacon.BluetoothAddressAsString);
            Debug.WriteLine("Type: " + beacon.BeaconType);
            Debug.WriteLine("Last Update: " + beacon.Timestamp);
            Debug.WriteLine("RSSI: " + beacon.Rssi);
            //Debug.WriteLine("beacon: " + beacon.ToString());
            //beacon.BeaconFrames.Add(new UnknownBeaconFrame(new byte[60]));
            foreach (var beaconFrame in beacon.BeaconFrames.ToList())
            {
                // Print a small sample of the available data parsed by the library
                if (beaconFrame is UidEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone UID Frame");
                    Debug.WriteLine("ID: " + ((UidEddystoneFrame)beaconFrame).NamespaceIdAsNumber.ToString("X") + " / " +
                                    ((UidEddystoneFrame)beaconFrame).InstanceIdAsNumber.ToString("X"));
                }
                else if (beaconFrame is UrlEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone URL Frame");
                    Debug.WriteLine("URL: " + ((UrlEddystoneFrame)beaconFrame).CompleteUrl);
                    Debug.WriteLine("Payload: " + BitConverter.ToString(((UrlEddystoneFrame)beaconFrame).Payload));
                }
                else if (beaconFrame is TlmEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone Telemetry Frame");
                    Debug.WriteLine("Temperature [°C]: " + ((TlmEddystoneFrame)beaconFrame).TemperatureInC);
                    Debug.WriteLine("Battery [mV]: " + ((TlmEddystoneFrame)beaconFrame).BatteryInMilliV);
                }
                else if (beaconFrame is EidEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone EID Frame");
                    Debug.WriteLine("Ranging Data: " + ((EidEddystoneFrame)beaconFrame).RangingData);
                    Debug.WriteLine("Ephemeral Identifier: " + BitConverter.ToString(((EidEddystoneFrame)beaconFrame).EphemeralIdentifier));
                }
                else if (beaconFrame is ProximityBeaconFrame)
                {
                    Debug.WriteLine("Proximity Beacon Frame (iBeacon compatible)");
                    Debug.WriteLine("Uuid: " + ((ProximityBeaconFrame)beaconFrame).UuidAsString);
                    Debug.WriteLine("Major: " + ((ProximityBeaconFrame)beaconFrame).MajorAsString);
                    Debug.WriteLine("Major: " + ((ProximityBeaconFrame)beaconFrame).MinorAsString);
                }
                else
                {
                    Debug.WriteLine("Unknown frame - not parsed by the library, write your own derived beacon frame type!");
                    Debug.WriteLine("Payload: " + BitConverter.ToString(((UnknownBeaconFrame)beaconFrame).Payload));
                }
            }
        }

        private void WatcherOnStopped(object sender, BTError btError)
        {
            string errorMsg = null;
            if (btError != null)
            {
                switch (btError.BluetoothErrorCode)
                {
                    case BTError.BluetoothError.Success:
                        errorMsg = "WatchingSuccessfullyStopped";
                        break;
                    case BTError.BluetoothError.RadioNotAvailable:
                        errorMsg = "ErrorNoRadioAvailable";
                        break;
                    case BTError.BluetoothError.ResourceInUse:
                        errorMsg = "ErrorResourceInUse";
                        break;
                    case BTError.BluetoothError.DeviceNotConnected:
                        errorMsg = "ErrorDeviceNotConnected";
                        break;
                    case BTError.BluetoothError.DisabledByPolicy:
                        errorMsg = "ErrorDisabledByPolicy";
                        break;
                    case BTError.BluetoothError.NotSupported:
                        errorMsg = "ErrorNotSupported";
                        break;
                    case BTError.BluetoothError.OtherError:
                        errorMsg = "ErrorOtherError";
                        break;
                    case BTError.BluetoothError.DisabledByUser:
                        errorMsg = "ErrorDisabledByUser";
                        break;
                    case BTError.BluetoothError.ConsentRequired:
                        errorMsg = "ErrorConsentRequired";
                        break;
                    case BTError.BluetoothError.TransportNotSupported:
                        errorMsg = "ErrorTransportNotSupported";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            if (errorMsg == null)
            {
                // All other errors - generic error message
                errorMsg = _restartingBeaconWatch
                    ? "FailedRestartingBluetoothWatch"
                    : "AbortedWatchingBeacons";
            }
            SetStatusOutput(_resourceLoader.GetString(errorMsg));
        }

        #endregion

        #region UI

        private async void SetStatusOutput(string newStatus)
        {
            // Update the status output UI element in the UI thread
            // (some of the callbacks are in a different thread that wouldn't be allowed
            // to modify the UI thread)
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                StatusText = newStatus;
            });
        }

        private async void SetDataOutput(string lData, string mData)
        {
            // Update the status output UI element in the UI thread
            // (some of the callbacks are in a different thread that wouldn't be allowed
            // to modify the UI thread)
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                LocationDataText = lData;
                MotionDataText = mData;
            });
        }

        private void AboutButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            MainSplitView.IsPaneOpen = !MainSplitView.IsPaneOpen;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Tools
        /// <summary>
        /// Convert minus-separated hex string to a byte array. Format example: "4E-66-63"
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string hex)
        {
            // Remove all space characters
            var hexPure = hex.Replace("-", "");
            if (hexPure.Length % 2 != 0)
            {
                // No even length of the string
                throw new Exception("No valid hex string");
            }
            var numberChars = hexPure.Length / 2;
            var bytes = new byte[numberChars];
            var sr = new StringReader(hexPure);
            try
            {
                for (var i = 0; i < numberChars; i++)
                {
                    bytes[i] = Convert.ToByte(new string(new[] { (char)sr.Read(), (char)sr.Read() }), 16);
                }
            }
            catch (Exception)
            {
                throw new Exception("No valid hex string");
            }
            finally
            {
                sr.Dispose();
            }
            return bytes;
        }
        #endregion

        public async Task connect(string host, string port, String message)
        {
            HostName hostName;

            using (_socket = new StreamSocket())
            {
                hostName = new HostName(host);

                // Set NoDelay to false so that the Nagle algorithm is not disabled
                _socket.Control.NoDelay = false;

                try
                {
                    // Connect to the server
                    await _socket.ConnectAsync(hostName, port);
                    // Send the message
                    //await this.send("socket client connectd.");
                    await this.send(message);
                    // Read response
                    //await this.read();
                }
                catch (Exception exception)
                {
                    //switch (Windows.Networking.Sockets.SocketError.GetStatus(exception.HResult))
                    //{
                    //    case SocketErrorStatus.HostNotFound:
                    //        // Handle HostNotFound Error
                    //        throw;
                    //    default:
                    //        // If this is an unknown status it means that the error is fatal and retry will likely fail.
                    //        throw;
                    //}
                    Console.WriteLine("Socket Connect Error.");
                }
            }
        }

        public async Task send(string message)
        {
            //Write data to the echo server.
            try
            {
                Stream streamOut = _socket.OutputStream.AsStreamForWrite();
                StreamWriter _writer = new StreamWriter(streamOut);
                string request = message;
                await _writer.WriteLineAsync(request);
                await _writer.FlushAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Socket Send Error." + e.Message);
                //throw;
            }

            //DataWriter writer;

            // Create the data writer object backed by the in-memory stream. 
            //using (writer = new DataWriter(_socket.OutputStream))
            //{
            //    // Set the Unicode character encoding for the output stream
            //    writer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
            //    // Specify the byte order of a stream.
            //    writer.ByteOrder = Windows.Storage.Streams.ByteOrder.LittleEndian;

            //    // Gets the size of UTF-8 string.
            //    writer.MeasureString(message);
            //    // Write a string value to the output stream.
            //    writer.WriteString(message);

            //    // Send the contents of the writer to the backing stream.
            //    try
            //    {
            //        await writer.StoreAsync();
            //    }
            //    catch (Exception exception)
            //    {
            //        switch (Windows.Networking.Sockets.SocketError.GetStatus(exception.HResult))
            //        {
            //            case SocketErrorStatus.HostNotFound:
            //                // Handle HostNotFound Error
            //                throw;
            //            default:
            //                // If this is an unknown status it means that the error is fatal and retry will likely fail.
            //                throw;
            //        }
            //    }

            //    await writer.FlushAsync();
            //    // In order to prolong the lifetime of the stream, detach it from the DataWriter
            //    writer.DetachStream();
            //}
        }


    }
}
